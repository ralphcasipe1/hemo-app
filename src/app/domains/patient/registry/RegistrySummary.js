import React, { Component }               from 'react'
import { Link, Route, Switch }            from 'react-router-dom'
import { Dropdown, Icon, Menu, Segment }  from 'semantic-ui-react'
import { CatheterContainer }              from 'app/domains/dialysis/assessment/catheter/CatheterContainer'
import { ObjectiveAssessmentContainer }   from 'app/domains/dialysis/assessment/objective/ObjectiveAssessmentContainer'
import { PreAssessmentContainer }         from 'app/domains/dialysis/assessment/pre-assessment/PreAssessmentContainer'
import { SubjectiveAssessmentContainer }  from 'app/domains/dialysis/assessment/subjective/SubjectiveAssessmentContainer'
import { VascularContainer }              from 'app/domains/dialysis/assessment/vascular/VascularContainer'
import { AntiCoagulantContainer }         from 'app/domains/dialysis/basic/anti-coagulant/AntiCoagulantContainer'
import { DialyzerContainer }              from 'app/domains/dialysis/basic/dialyzer/DialyzerContainer'
import { ParameterContainer }             from 'app/domains/dialysis/basic/parameter/ParameterContainer'
import { VaccineContainer }               from 'app/domains/dialysis/basic/vaccine/VaccineContainer'
import { WeightContainer }                from 'app/domains/dialysis/basic/weight/WeightContainer'
import { MedicationContainer }            from 'app/domains/dialysis/monitor/medication/MedicationContainer'
import { NurseNoteContainer }             from 'app/domains/dialysis/monitor/nurse-note/NurseNoteContainer'
import { PhysicianOrderContainer }        from 'app/domains/dialysis/monitor/physician-order/PhysicianOrderContainer'
import { VitalSignContainer }             from 'app/domains/dialysis/monitor/vital-sign/VitalSignContainer'
import { VitalSignCreateContainer }       from 'app/domains/dialysis/monitor/vital-sign/VitalSignCreateContainer'
class RegistrySummary extends Component {
  componentDidMount() {
    const {
      match: {
        params: { patientId, patientRegistryId }
      }
    } = this.props

    this.props.fetchPatientRegistry(patientId, patientRegistryId)
  }

  render() {
    const {
      match: { url }
    } = this.props

    const patientRegistryUrl =
      '/patients/:patientId/patient_registries/:patientRegistryId'

    return (
      <div>
        <Menu attached="top" icon="labeled">
          <Dropdown
            item
            icon={<Icon name="heartbeat" color="blue" />}
            text="Framework"
          >
            <Dropdown.Menu>
              <Dropdown.Item
                as={Link}
                key="parameter"
                text="Parameter"
                to={`${url}/parameters`}
              />
              <Dropdown.Item
                as={Link}
                key="weight"
                text="Weight"
                to={`${url}/weights`}
              />
              <Dropdown.Item
                as={Link}
                key="anti-coagulant"
                text="Anti-coagulant"
                to={`${url}/anti_coagulants`}
              />
              <Dropdown.Item
                as={Link}
                key="dialyzer"
                text="Dialyzer"
                to={`${url}/dialyzers`}
              />
              <Dropdown.Item
                as={Link}
                key="vaccine"
                text="Vacine"
                to={`${url}/vaccines`}
              />
            </Dropdown.Menu>
          </Dropdown>

          <Dropdown
            item
            icon={<Icon name="stethoscope" color="blue" />}
            text="Evaluation"
          >
            <Dropdown.Menu>
              <Dropdown.Item
                as={Link}
                key="pre-assessment"
                text="Pre-assessment"
                to={`${url}/pre_assessments`}
              />

              <Dropdown.Item
                as={Link}
                key="objective-assessment"
                text="Objective"
                to={`${url}/objective_assessments`}
              />

              <Dropdown.Item
                as={Link}
                key="subjective-assessment"
                text="Subjective"
                to={`${url}/subjective_assessments`}
              />

              <Dropdown.Item
                as={Link}
                key="catheter"
                text="Catheter"
                to={`${url}/catheters`}
              />

              <Dropdown.Item
                as={Link}
                key="vascular"
                text="Vascular"
                to={`${url}/vasculars`}
              />
            </Dropdown.Menu>
          </Dropdown>

          <Menu.Menu position="right">
            <Dropdown
              item
              icon={<Icon name="ellipsis vertical" color="blue" />}
              text="More"
            >
              <Dropdown.Menu>
                <Dropdown.Item
                  as={Link}
                  key="medications"
                  text="Medications"
                  to={`${url}/medications`}
                />

                <Dropdown.Item
                  as={Link}
                  key="nurse notes"
                  text="Nurse notes"
                  to={`${url}/nurse_notes`}
                />

                <Dropdown.Item
                  as={Link}
                  key="physician orders"
                  text="Physician Orders"
                  to={`${url}/physician_orders`}
                />
                
                <Dropdown.Item
                  as={Link}
                  key="vital signs"
                  text="Vital signs"
                  to={`${url}/vital_signs`}
                />
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>

        <Segment attached="bottom">
          <Switch>
            <Route
              component={ParameterContainer}
              path={`${patientRegistryUrl}/parameters`}
            />

            <Route
              component={WeightContainer}
              exact
              path={`${patientRegistryUrl}/weights`}
            />

            <Route
              component={AntiCoagulantContainer}
              path={`${patientRegistryUrl}/anti_coagulants`}
            />

            <Route
              component={DialyzerContainer}
              path={`${patientRegistryUrl}/dialyzers`}
            />

            <Route
              component={VaccineContainer}
              path={`${patientRegistryUrl}/vaccines`}
            />

            <Route
              component={PreAssessmentContainer}
              path={`${patientRegistryUrl}/pre_assessments`}
            />

            <Route
              component={ObjectiveAssessmentContainer}
              path={`${patientRegistryUrl}/objective_assessments`}
            />

            <Route
              component={SubjectiveAssessmentContainer}
              path={`${patientRegistryUrl}/subjective_assessments`}
            />

            <Route
              component={CatheterContainer}
              path={`${patientRegistryUrl}/catheters`}
            />

            <Route
              component={VascularContainer}
              path={`${patientRegistryUrl}/vasculars`}
            />

            <Route
              component={NurseNoteContainer}
              path={`${patientRegistryUrl}/nurse_notes`}
            />

            <Route
              component={MedicationContainer}
              path={`${patientRegistryUrl}/medications`}
            />

            <Route
              component={PhysicianOrderContainer}
              path={`${patientRegistryUrl}/physician_orders`}
            />

            <Route
              component={VitalSignContainer}
              path={`${patientRegistryUrl}/vital_signs`}
              exact
            />

            <Route
              component={VitalSignCreateContainer}
              path={`${patientRegistryUrl}/vital_signs/create`}
              exact
            />

            <Route component={ParameterContainer} />
          </Switch>
        </Segment>
      </div>
    )
  }
}

export { RegistrySummary }
