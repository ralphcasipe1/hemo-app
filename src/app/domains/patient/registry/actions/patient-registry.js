import {
  CLEAR_FETCHED_ALL_PATIENT_REGISTRIES,
  CLEAR_NEW_PATIENT_REGISTRY,
  CREATE_PATIENT_REGISTRY,
  CREATE_PATIENT_REGISTRY_FAILURE,
  CREATE_PATIENT_REGISTRY_SUCCESS,
  FETCH_ALL_PATIENT_REGISTRIES,
  FETCH_ALL_PATIENT_REGISTRIES_FAILURE,
  FETCH_ALL_PATIENT_REGISTRIES_SUCCESS,
  FETCH_PATIENT_REGISTRY,
  FETCH_PATIENT_REGISTRY_FAILURE,
  FETCH_PATIENT_REGISTRY_SUCCESS,
  RESET_SELECTED_PATIENT_REGISTRY
} from '../constants/patient-registry'
import { instance } from '../../../../config/connection'

export const clearPatientRegistry = () => ({
  type: CLEAR_NEW_PATIENT_REGISTRY
})

export const fetchAllPatientRegistries = id => dispatch => {
  dispatch({
    type: FETCH_ALL_PATIENT_REGISTRIES
  })

  return instance.get(`/patients/${id}/patient_registries`).then(
    response =>
      dispatch({
        type: FETCH_ALL_PATIENT_REGISTRIES_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_PATIENT_REGISTRIES_FAILURE
        , error: error.response
      })
  )
}

export function fetchPatientRegistry(patientId, registryId) {
  return dispatch => {
    dispatch({
      type: FETCH_PATIENT_REGISTRY
    })
    instance
      .get(`/patients/${patientId}/patient_registries/${registryId}`)
      .then(
        res =>
          dispatch({
            type: FETCH_PATIENT_REGISTRY_SUCCESS
            , payload: res.data.data
          }),
        error =>
          dispatch({
            type: FETCH_PATIENT_REGISTRY_FAILURE
            , error: error.response
          })
      )
  }
}

export function createPatientRegistry(id, values) {
  return dispatch => {
    dispatch({
      type: CREATE_PATIENT_REGISTRY
    })

    instance.post(`/patients/${id}/patient_registries`, values).then(
      res =>
        dispatch({
          type: CREATE_PATIENT_REGISTRY_SUCCESS
          , payload: res.data
        }),
      error =>
        dispatch({
          type: CREATE_PATIENT_REGISTRY_FAILURE
          , error: error.response
        })
    )
  }
}

export function selectPatientRegistry(id, patientRegistryId) {
  return dispatch => {
    dispatch({
      type: FETCH_PATIENT_REGISTRY
    })

    instance
      .get(`/patients/${id}/patient_registries/${patientRegistryId}`)
      .then(
        res =>
          dispatch({
            type: FETCH_PATIENT_REGISTRY_SUCCESS
            , payload: res.data.data
          }),
        error =>
          dispatch({
            type: FETCH_PATIENT_REGISTRY_FAILURE
            , error: error.response
          })
      )
  }
}

export const resetSelectedPatientRegistry = () => ({
  type: RESET_SELECTED_PATIENT_REGISTRY
})

export const clearFetchedAllPatientRegistries = () => ({
  type: CLEAR_FETCHED_ALL_PATIENT_REGISTRIES
})
