import {
  CLEAR_FETCHED_ALL_PATIENT_REGISTRIES,
  // CLEAR_NEW_PATIENT_REGISTRY,
  CREATE_PATIENT_REGISTRY,
  CREATE_PATIENT_REGISTRY_FAILURE,
  CREATE_PATIENT_REGISTRY_SUCCESS,
  FETCH_ALL_PATIENT_REGISTRIES,
  FETCH_ALL_PATIENT_REGISTRIES_FAILURE,
  FETCH_ALL_PATIENT_REGISTRIES_SUCCESS,
  FETCH_PATIENT_REGISTRY,
  FETCH_PATIENT_REGISTRY_FAILURE,
  FETCH_PATIENT_REGISTRY_SUCCESS,
  RESET_SELECTED_PATIENT_REGISTRY
} from '../constants/patient-registry'

const initialState = {
  error: null
  , isLoading: false
  , patientRegistries: []
  , patientRegistry: null
}

export const patientRegistryReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_FETCHED_ALL_PATIENT_REGISTRIES:
      return {
        ...state
        , isLoading: false
        , patientRegistries: []
        , error: null
      }
    /* case CLEAR_NEW_PATIENT_REGISTRY:
    return {
      ...state,
      patientRegisters: [],
      error: null,
      isLoading: false,
    }; */

    case CREATE_PATIENT_REGISTRY:
    case FETCH_ALL_PATIENT_REGISTRIES:
    case FETCH_PATIENT_REGISTRY:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_PATIENT_REGISTRY_FAILURE:
    case FETCH_ALL_PATIENT_REGISTRIES_FAILURE:
    case FETCH_PATIENT_REGISTRY_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      }

    case CREATE_PATIENT_REGISTRY_SUCCESS:
      return {
        ...state
        , isLoading: false
        , patientRegistries:
            !action.payload.exist
              ? [ action.payload.data, ...state.patientRegistries ]
              : state.patientRegistries.filter(registry => {
                  if (+registry.registry_no === +action.payload.registry_no) {
                    return registry
                  }
                  return action.payload
                })
      }

    case FETCH_ALL_PATIENT_REGISTRIES_SUCCESS:
      return {
        ...state
        , patientRegistries: action.payload
        , error: null
        , isLoading: false
      }

    case FETCH_PATIENT_REGISTRY_SUCCESS:
      return {
        ...state
        , isLoading: false
        , error: null
        , patientRegistry: action.payload
      }

    case RESET_SELECTED_PATIENT_REGISTRY:
      return {
        ...state
        , patientRegistry: null
        , error: null
        , isLoading: false
      }

    default:
      return state
  }
}
