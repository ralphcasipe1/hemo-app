import { connect } from 'react-redux';
import { fetchPatientRegistry } from '../registry/actions/patient-registry';
import { RegistrySummary } from './RegistrySummary';

const mapStateToProps = (state) => ({
  patientRegistry: state.patientRegistryReducer.patientRegistry,
});

export const RegistrySummaryContainer = connect(
  mapStateToProps,
  {
    fetchPatientRegistry,
  }
)(RegistrySummary);
