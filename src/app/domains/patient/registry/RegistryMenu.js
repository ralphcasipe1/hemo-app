import React                           from 'react'
import { Link, withRouter }            from 'react-router-dom'
import { Dropdown, Icon, Input, Menu } from 'semantic-ui-react'

export const RegistryMenu = ({ match: { params } }) => (
  <Menu attached="top" pointing>
    <Dropdown icon="wrench" item disabled>
      <Dropdown.Menu>
        <Dropdown.Header content="Actions" />
        <Dropdown.Item
          as={Link}
          icon={<Icon color="blue" name="plus" />}
          text="Add"
          to={`/patients/${params.patientId}/registries/create`}
        />
        <Dropdown.Item
          as={Link}
          icon={<Icon color="blue" name="list" />}
          text="View All"
          to={`/patients/${params.patientId}/registries/list`}
        />
      </Dropdown.Menu>
    </Dropdown>
    <Dropdown item text="Filter" disabled>
      <Dropdown.Menu>
        <Dropdown.Header content="Filter by ..." icon="filter" />
        <Dropdown.Item text="Created Date" />
      </Dropdown.Menu>
    </Dropdown>

    <Menu.Menu position="right">
      <Menu.Item>
        <Input
          icon="search"
          placeholder="Search..."
          transparent
          disabled
          iconPosition="left"
        />
      </Menu.Item>
    </Menu.Menu>
  </Menu>
)
