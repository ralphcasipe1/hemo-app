import React, { PureComponent }          from 'react'
import { Link }                          from 'react-router-dom'
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react'
import { menus, cleanAndCap, finale }    from '../helpers/mobileHeader'

class RegistryHeader extends PureComponent {
  render() {
    const { attached, patientRegistryId } = this.props

    const baseUrl = `/patient_registries/${patientRegistryId}/`

    return (
      <Segment compact>
        <Menu attached={attached} icon="labeled" secondary>
          {menus.map((m, index) => (
            <Dropdown
              key={index}
              item
              icon={<Icon name={m.icon} color="blue" />}
              text={m.name}
            >
              <Dropdown.Menu>
                {m.items.map((item, i) => (
                  <Dropdown.Item
                    as={Link}
                    key={i}
                    text={cleanAndCap(item)}
                    to={`${baseUrl}${item}`}
                  />
                ))}
              </Dropdown.Menu>
            </Dropdown>
          ))}
          <Menu.Menu position="right">
            <Dropdown
              item
              icon={<Icon name="ellipsis vertical" color="blue" />}
              text="More"
            >
              <Dropdown.Menu>
                {finale.map((fnl, index) => (
                  <Dropdown.Item
                    as={Link}
                    key={index}
                    text={cleanAndCap(fnl)}
                    to={`${baseUrl}${fnl}`}
                  />
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>
      </Segment>
    )
  }
}

export { RegistryHeader }
