import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Segment } from 'semantic-ui-react';
import { getPatient } from '../selectors/PatientSelectors';
import {
  fetchPatientRegister,
  selectPatientRegistry,
  resetSelectedPatientRegistry
} from '../actions/patients/patientRegistries';
import {
  fetchPatient,
  resetSelectedPatient
} from '../actions/patients/patients';
import { RegistryHeader } from './RegistryHeader';

class SummaryRegistryContainer extends Component {
  componentDidMount() {
    this.props.fetchPatient(this.props.match.params.patientId);
    this.props.selectPatientRegistry(
      this.props.match.params.patientId,
      this.props.match.params.patientRegistryId
    );
  }

  componentWillUnmount() {
    this.props.resetSelectedPatient();
  }

  render() {
    return (
      <Segment.Group>
        <RegistryHeader
          patientId={this.props.match.params.patientId}
          patientRegistryId={this.props.patientRegistryId}
        />
        <Segment>Parameter Weight Anti-coagulant Dialyzer Vaccine</Segment>
      </Segment.Group>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientRegistryId: ownProps.match.params.patientRegistryId
  , patient: getPatient(state)
});

export default connect(
  mapStateToProps,
  {
    fetchPatientRegister
    , fetchPatient
    , resetSelectedPatient
    , selectPatientRegistry
    , resetSelectedPatientRegistry
  }
)(SummaryRegistryContainer);
