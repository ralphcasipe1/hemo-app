import { connect } from 'react-redux'
import {
  clearFetchedBizboxPatient,
  fetchBizboxPatient
} from '../bizbox/actions/bizbox-patient'
import {
  clearFetchedAllPatientRegistries,
  clearPatientRegistry,
  createPatientRegistry,
  fetchAllPatientRegistries
} from './actions/patient-registry'
import { Registry } from './Registry'

const mapStateToProps = state => ({
  bizboxPatient: state.bizboxPatientReducer.bizboxPatient
  , patient: state.patientReducer.patient
  , patientRegistries: state.patientRegistryReducer.patientRegistries
})

const RegistryContainer = connect(
  mapStateToProps,
  {
    clearFetchedAllPatientRegistries
    , clearFetchedBizboxPatient
    , createPatientRegistry
    , fetchAllPatientRegistries
    , fetchBizboxPatient
    , clearPatientRegistry
  }
)(Registry)

export { RegistryContainer }
