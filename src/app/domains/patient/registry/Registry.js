import React, { Component } from 'react'
import { Link }             from 'react-router-dom'
import moment               from 'moment'
import { Header, List }     from 'semantic-ui-react'

class Registry extends Component {
  componentDidMount() {
    const {
      patient,
      match: {
        params: { patientId }
      }
    } = this.props

    this.props.fetchAllPatientRegistries(patientId)

    this.props.fetchBizboxPatient(+patient.bizbox_patient_no)
  }

  componentDidUpdate(prevProps) {
    const {
      patient,
      bizboxPatient,
      match: {
        params: { patientId }
      },
      patientRegistries
    } = this.props

    let registries

    if (prevProps.patientRegistries === this.props.patientRegistries) {
      registries = patientRegistries.map(
        patientRegistry => +patientRegistry.registry_no
      )
    }

    if (!prevProps.patient && !!patient) {
      this.props.fetchBizboxPatient(patient.bizbox_patient_no)
    }

    if (prevProps.bizboxPatient !== this.props.bizboxPatient) {
      if (
        +this.props.bizboxPatient.FK_emdPatients === +patient.bizbox_patient_no
      ) {
        if (
          registries.indexOf(this.props.bizboxPatient.PK_psPatRegisters) === -1
        ) {
          console.log(this.props.bizboxPatient.PK_psPatRegisters)
          this.props.createPatientRegistry(patientId, {
            patient_id: patientId
            , registry_no: bizboxPatient.PK_psPatRegisters
          })
        }
      }
    }
  }

  componentWillUnmount() {
    this.props.clearFetchedBizboxPatient()
    this.props.clearFetchedAllPatientRegistries()
  }

  render() {
    const { patientRegistries } = this.props
    const patientUrl = `/patients/${this.props.match.params.patientId}`

    return (
      <List divided link relaxed size="huge">
        {patientRegistries.map(patientRegistry => (
          <List.Item
            as={Link}
            key={patientRegistry.patient_registry_id}
            to={`${patientUrl}/patient_registries/${
              patientRegistry.patient_registry_id
            }`}
          >
            {moment().format('L') ===
            moment(patientRegistry.created_at).format('L') ? (
              <List.Icon
                color="green"
                name="warning circle"
                size="large"
                verticalAlign="middle"
              />
            ) : (
              <List.Icon
                name="check circle"
                size="large"
                verticalAlign="middle"
              />
            )}

            <List.Content>
              <List.Header>
                <Header
                  as="h4"
                  color="blue"
                  content={patientRegistry.registry_no}
                />
              </List.Header>
              <List.Description>
                {moment(patientRegistry.created_at).fromNow()}
              </List.Description>
            </List.Content>
          </List.Item>
        ))}
      </List>
    )
  }
}

export { Registry }
