import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Container, Menu, Segment } from 'semantic-ui-react';

export const PatientHOC = WrappedComponent =>
  class extends Component {
    render() {
      const {
        isLoading,
        location: { pathname }
      } = this.props;

      return (
        <Container>
          <Menu attached="top" tabular widths="two">
            <Menu.Item
              activeClassName={pathname === '/' ? 'active' : null}
              as={NavLink}
              icon="database"
              name="local"
              to="/"
            />

            <Menu.Item
              activeClassName={pathname === '/bizbox' ? 'active' : null}
              as={NavLink}
              icon="server"
              name="bizbox"
              to="/bizbox"
            />
          </Menu>

          <Segment attached="bottom" loading={isLoading}>
            <WrappedComponent {...this.props} />
          </Segment>
        </Container>
      );
    }
  };
