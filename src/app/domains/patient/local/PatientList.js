import React from 'react';
import { List } from 'semantic-ui-react';
import { PatientListItem } from './PatientListItem';

export const PatientList = ({ patients }) => (
  <List divided relaxed="very" verticalAlign="middle" size="large">
    {patients.map(patient => (
      <PatientListItem key={patient.patient_id} patient={patient} />
    ))}
  </List>
);
