import React                  from 'react'
import { NavLink }            from 'react-router-dom'
import { Menu }               from 'semantic-ui-react'
import { activeWithPathname } from './helpers/active-with-pathname'

export const InfoNavigation = ({ pathname, patientId }) => (
  <Menu color="blue" tabular widths="two" attached="top">
    <Menu.Item
      activeClassName={activeWithPathname(pathname, patientId)}
      as={NavLink}
      icon="ordered list"
      name="standing orders"
      to={`/patients/${patientId}/standing_orders`}
    />
    <Menu.Item
      activeClassName={
        pathname === `/patients/${patientId}/patient_registries`
          ? 'active'
          : null
      }
      as={NavLink}
      icon="registered"
      name="registries"
      to={`/patients/${patientId}/patient_registries`}
    />
  </Menu>
)
