import { connect } from 'react-redux'
import {
  fetchAllPatients,
  searchPatientByName
} from './actions/patient'
import { Patient } from './Patient'

const mapStateToProps = state => ({
  isLoading: state.patientReducer.isLoading
  , patients: state.patientReducer.patients
  , meta: state.patientReducer.meta
})

export const PatientContainer = connect(
  mapStateToProps,
  {
    fetchAllPatients
    , searchPatientByName
  }
)(Patient)
