import React, { Component } from 'react'
import { Menu }             from 'semantic-ui-react'


class PatientPagination extends Component {
  state = { activeItem: '1' }

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name })
    this.props.onClickPage(name)
  }

  renderMenuItems = (totalPages, activeItem) => {
    const menuItems = []
    for(let index = 1; index <= totalPages; index++) {
      menuItems.push(
        <Menu.Item
          key={index}
          name={`${index}`}
          active={activeItem === `${index}`}
          onClick={this.handleItemClick}
        />
      )
    }

    return menuItems
  }

  render() {
    const { activeItem } = this.state
    const { totalPages } = this.props

    return (
      <Menu pagination>
        {this.renderMenuItems(totalPages, activeItem)}
      </Menu>
    )
  }
}

export { PatientPagination }