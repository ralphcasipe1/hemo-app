import React            from 'react'
import type { Element } from 'react'
import { Input }        from 'semantic-ui-react'

type PropsType = {
  onSubmit: (e: any, {value: string}) => any
};

class PatientSearch extends React.Component {

  state = {
    fullName: ''
  }

  onSubmit = event => {
    event.preventDefault()


    this.props.onPatientSearch(this.state.fullName)
  }

  handleChange = event => this.setState({ fullName: event.target.value })

  render(): Element<'form'> {

    return (
      <form onSubmit={this.onSubmit}>
        <Input
          fluid
          icon="search"
          placeholder="Search Patient"
          size="big"
          name="fullName"
          value={this.state.fullName}
          onChange={this.handleChange}
        />
      </form>
    )
  }
}

export { PatientSearch }