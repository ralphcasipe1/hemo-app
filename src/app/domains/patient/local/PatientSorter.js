import React, { Component } from 'react'
import { Menu }             from 'semantic-ui-react'

class PatientSorter extends Component {
  state = {
    activeItem: 'closest'
    , createdAtIcon: 'sort content ascending'
    , createdAtSort: 'ASC'
    , lastNameIcon: 'sort content ascending'
    , lastNameSort: 'ASC'
  }

  handleItemClick = (e, { name }) => {
    if (this.state.createdAtIcon === 'sort content ascending') {
      this.setState({
        createdAtIcon: 'sort content descending'
        , createdAtSort: 'ASC'
      })

      this.props.sortCreatedAt(this.state.createdAtSort)
    }

    if (this.state.createdAtIcon === 'sort content descending') {
      this.setState({
        createdAtIcon: 'sort content ascending'
      })
    }
  }

  render() {
    const {
      activeItem,
      createdAtIcon,
      lastNameIcon
    } = this.state

    return (
      <Menu text>
        <Menu.Item header>Sort By</Menu.Item>
        <Menu.Item
          name='lastName'
          active={activeItem === 'lastName'}
          icon={ lastNameIcon }
          onClick={this.handleItemClick}
        />
        <Menu.Item
          name='createdAt'
          active={activeItem === 'createdAt'}
          icon={ createdAtIcon }
          onClick={this.handleItemClick}
        />
      </Menu>
    )
  }
}

export { PatientSorter }