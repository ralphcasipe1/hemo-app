export const activeWithPathname = (pathname, patientId = 1) => {
  const basePath = `/patients/${patientId}/standing_orders`;
  const paths = [
    `${basePath}`
    , `${basePath}/standing_order_parameters`
    , `${basePath}/standing_order_weights`
    , `${basePath}/standing_order_anti_coagulants`
    , `${basePath}/standing_order_dialyzers`
    , `${basePath}/standing_order_medications`
    , `${basePath}/standing_order_physician_orders`
  ];

  if (paths.indexOf(pathname) === -1) {
    return null;
  }

  return 'active';
};
