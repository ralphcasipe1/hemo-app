export const patientFullName = (patient) => {
  const { last_name, first_name, middle_name } = patient;

  return `${last_name}, ${first_name} ${middle_name}`;
};
