import { connect } from 'react-redux';
import { clearFetchedPatient, fetchPatient } from '../local/actions/patient';
import { PatientDetail } from './PatientDetail';

const mapStateToProps = state => ({
  patient: state.patientReducer.patient
});

export const PatientDetailContainer = connect(
  mapStateToProps,
  {
    clearFetchedPatient
    , fetchPatient
  }
)(PatientDetail);
