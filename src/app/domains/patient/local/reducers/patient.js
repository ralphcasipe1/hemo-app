import {
  CLEAR_FETCHED_PATIENT,
  CLEAR_NEW_PATIENT,
  CREATE_PATIENT,
  CREATE_PATIENT_FAILURE,
  CREATE_PATIENT_SUCCESS,
  FETCH_ALL_PATIENTS,
  FETCH_ALL_PATIENTS_FAILURE,
  FETCH_ALL_PATIENTS_SUCCESS,
  FETCH_PATIENT,
  FETCH_PATIENT_FAILURE,
  FETCH_PATIENT_SUCCESS
} from '../constants/patient'

const initialState = {
  error: null
  , isLoading: false
  , patients: []
  , patient: null
  , meta: null
}

export const patientReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_FETCHED_PATIENT:
    case CLEAR_NEW_PATIENT:
      return {
        ...state
        , error: null
        , isLoading: false
        , patient: null
      }

    case CREATE_PATIENT:
    case FETCH_ALL_PATIENTS:
    case FETCH_PATIENT:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_PATIENT_FAILURE:
    case FETCH_ALL_PATIENTS_FAILURE:
    case FETCH_PATIENT_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      }

    case CREATE_PATIENT_SUCCESS:
      return {
        ...state
        , isLoading: false
        , patients: [ ...state.patients, action.payload ]
      }

    case FETCH_ALL_PATIENTS_SUCCESS:
      return {
        ...state
        , patients: action.payload.data
        , meta: action.payload.meta
        , isLoading: false
      }

    case FETCH_PATIENT_SUCCESS:
      return {
        ...state
        , patient: action.payload
        , isLoading: false
      }

    default:
      return state
  }
}

// const patients = (state = initialState, action) => {
//   switch (action.type) {
//     case FETCH_PATIENTS:
//     case FETCH_PATIENT:
//     case CREATE_PATIENT:
//       return updateObject(state, {
//         isLoading: true
//       });
//     case FETCH_PATIENTS_SUCCESS:
//       return updateObject(state, {
//         patients: action.payload,
//         count: action.payload.meta.cursors.count,
//         after: action.payload.meta.cursors.after,
//         pages: action.payload.meta.cursors.pages,
//         total: action.payload.meta.cursors.total,
//         current: _.isEmpty(action.payload)
//           ? NaN
//           : action.payload[0].patient_id,
//         before: action.payload.meta.cursors.before,
//         isLoading: false
//       });
//     case FETCH_PATIENT_SUCCESS:
//       return updateObject(state, {
//         patient: action.payload,
//         isLoading: false
//       });
//     case CREATE_PATIENT_SUCCESS:
//       return updateObject(state, {
//         patients: [action.payload, ...state.patients],
//         isLoading: false
//       });
//     case NEXT_PAGE_PATIENT:
//       return updateObject(state, {
//         /* before: state.patients[0].patient_id,
//         current: state.after,
//         before: state.current */
//         page: state.page + 1
//       });
//     case PREV_PAGE_PATIENT:
//       return updateObject(state, {
//         /* after: state.patients[state.patients.length - 1].patient_id,
//         before: state.before
//         current: state.before,
//         after: state.current,
//         before: state.patients.reverse()[0].patient_id
//         before: state.before === 1 ? NaN : state.before - state.count */
//         page: state.page - 1
//       });
//     case RESET_PATIENT:
//       return updateObject(state, {
//         patient: null,
//         isLoading: false
//       });
//     case FETCH_PATIENTS_FAILURE:
//       return updateObject(state, {
//         isLoading: false,
//         error: action.payload.message
//       });
//     case RESET_PATIENTS:
//       return updateObject(state, {
//         patients: [],
//         limit: NaN,
//         offset: NaN,
//         pages: NaN,
//         page: 1,
//         isLoading: false
//       });
//     default:
//       return state;
//   }
// };
