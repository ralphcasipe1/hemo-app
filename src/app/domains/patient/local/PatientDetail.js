import React, { Component }  from 'react'
import { Route, Switch }     from 'react-router-dom'
import { Icon, Segment }     from 'semantic-ui-react'
import _                     from 'lodash'
import { StandingOrderMenu } from '../../standing-order/StandingOrderMenu'
import { RegistryContainer } from '../registry/RegistryContainer'
import { InfoNavigation }    from './InfoNavigation'
import './styles/PatientDetail.css'

class PatientDetail extends Component {
  componentDidMount() {
    this.props.fetchPatient(this.props.match.params.patientId)
  }

  componentWillUnmount() {
    this.props.clearFetchedPatient()
  }

  render() {
    const {
      location: { pathname },
      patient
    } = this.props

    if (!patient) {
      return <div />
    }

    return (
      <div className="patient-info-wrapper">
        <Segment>
          <div className="patient-info-item">
            <label>Patient No.:</label>
            <span>{patient.bizbox_patient_no}</span>
          </div>
          <div className="patient-info-item">
            <label>Hospital No.:</label>
            <span>{patient.hospital_no}</span>
          </div>
          <div className="patient-info-item">
            <label>Name:</label>
            <span>
              {_.upperCase(patient.first_name)}{' '}
              {_.upperCase(patient.middle_name)}{' '}
              {_.upperCase(patient.last_name)}
            </span>
          </div>
          <div className="patient-info-item">
            <label>Sex:</label>
            {patient.sex === 0 ? (
              <span>
                <Icon name="woman" />
                Female
              </span>
            ) : (
              <span>
                <Icon name="man" />
                Male
              </span>
            )}
          </div>
        </Segment>

        <Segment tertiary>
          <InfoNavigation patientId={patient.patient_id} pathname={pathname} />
          <Segment attached="bottom">
            <Switch>
              <Route
                component={RegistryContainer}
                exact
                path="/patients/:patientId/patient_registries"
              />

              <Route
                component={StandingOrderMenu}
                path="/patients/:patientId/standing_orders"
              />
            </Switch>
          </Segment>
        </Segment>
      </div>
    )
  }
}

export { PatientDetail }
