import React from 'react';
import { Link } from 'react-router-dom';
import { Icon, List } from 'semantic-ui-react';
import { patientFullName } from './helpers/patient-full-name';
import moment from 'moment';

export const PatientListItem = ({ patient }) => (
  <List.Item key={patient.patient_id}>
    <Icon color="grey" name={patient.sex === 1 ? 'male' : 'female'} />

    <List.Content>
      <List.Header>
        <Link to={`/patients/${patient.patient_id}`}>
          {patientFullName(patient).toUpperCase()}
        </Link>
      </List.Header>
      <List.Description>
        Created at {moment(patient.created_at).format('ll')}
      </List.Description>
    </List.Content>
  </List.Item>
);
