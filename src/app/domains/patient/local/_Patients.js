import { Container, Tab, Message } from 'semantic-ui-react'
import { cs }                      from '../utils/PatientUtils'
import _                           from 'lodash'
import moment                      from 'moment'
import BizboxPatientConfirm        from './BizboxPatientConfirm'
import BizboxPatientList           from './BizboxPatientList'
import PatientList                 from './PatientList'
import React, { Component }        from 'react'

class Patients extends Component {
  constructor() {
    super()

    this.state = {
      open: false
      ,flashMessage: false
    }

    this.handleMigrate = this.handleMigrate.bind(this)
    this.handleSelectAndOpen = this.handleSelectAndOpen.bind(this)
  }

  componentDidMount() {
    this.props.fetchPatients()
    this.props.fetchBizboxPatients(moment().format('YYYY-MM-DD'))
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.props.message && nextProps.message) {
      this.setState({
        flashMessage: true
      })

      setTimeout(
        () =>
          this.setState({
            flashMessage: false
          }),
        4000
      )
    }
  }

  handleSelectAndOpen(id) {
    this.setState({
      open: true
    })

    this.props.fetchBizboxPatient(id)
  }

  handleMigrate() {
    this.setState({
      open: false
    })

    const {
      firstname,
      middlename,
      lastname,
      FK_emdPatients,
      gender,
      civilstatus,
      specialization,
      AttendingDoctor,
      RoomNo,
      HospitalNo
    } = this.props.bizboxPatient

    let civil_status = cs[civilstatus] || 5

    return this.props.createPatient({
      bizbox_patient_no: FK_emdPatients
      ,fname: _.lowerCase(firstname)
      ,mname: _.lowerCase(middlename)
      ,lname: _.lowerCase(lastname)
      ,sex: gender === 'Male' ? 1 : 0
      ,civil_status
      ,room_no: RoomNo
      ,hospital_no: HospitalNo
      ,attending_physician: 0
    })
  }

  render() {
    const {
      bizboxLoading,
      bizboxPatient,
      bizboxPatients,
      localLoading,
      patients,
      message
    } = this.props

    const { flashMessage, open } = this.state

    const panes = [
      {
        menuItem: {
          key: 'local'
          ,icon: 'database'
          ,content: 'Local'
        }
        ,render: () => (
          <Tab.Pane loading={localLoading}>
            <PatientList patients={patients} />
          </Tab.Pane>
        )
      }
      ,{
        menuItem: {
          key: 'bizbox'
          ,icon: 'server'
          ,content: 'Bizbox'
        }
        ,render: () => (
          <Tab.Pane loading={bizboxLoading}>
            {open ? (
              <BizboxPatientConfirm
                onCancel={() => this.setState({ open: false })}
                onMigrate={this.handleMigrate}
                bizboxPatient={bizboxPatient}
              />
            ) : (
              <BizboxPatientList
                bizboxPatients={bizboxPatients}
                onMigrate={this.handleSelectAndOpen}
              />
            )}
          </Tab.Pane>
        )
      }
    ]

    return (
      <Container>
        {flashMessage ? (
          <Message
            icon="checkmark"
            header="Success"
            content={message}
            success
          />
        ) : null}

        <Tab
          menu={{ pointing: true, borderless: true, widths: 'two' }}
          panes={panes}
        />
      </Container>
    )
  }
}

export default Patients

/* class Patients extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleMigrate = this.handleMigrate.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
  }

  componentDidMount() {
    this.onInit(this.props, this.state);
  }

  componentDidUpdate(prevProps, nextProps) {
    if (prevProps.patients.length !== this.props.patients.length) {
      this.props.fetchBizboxPatients(moment().format('YYYY-MM-DD'));
    }
  }

  onInit(props, state) {
    props.fetchPatients();
    props.fetchBizboxPatients(moment().format('YYYY-MM-DD'));
  }

  componentWillUnmount() {
    this.props.resetBizboxPatient();
    this.props.resetPatients();
  }



  handleNext(after) {
    this.props.nextPage();
    this.props.fetchPatients({
      after
    });
  }

  handlePrev(before) {
    this.props.prevPage();
    this.props.fetchPatients({
      before
    });
  }

  handleCancel() {
    this.setState({
      open: false
    });
  }

  handleMigrate(id) {
    this.setState({
      open: true
    });

    this.props.selectBizboxPatient(id);
  }

  handleConfirm() {
    this.setState({
      open: false
    });

    const {
      firstname,
      middlename,
      lastname,
      FK_emdPatients,
      gender,
      civilstatus,
      specialization,
      AttendingDoctor,
      RoomNo,
      HospitalNo
    } = this.props.bizboxPatient;

    let civil_status = cs[civilstatus] || 5;

    this.props.createPatient({
      bizbox_patient_no: FK_emdPatients,
      fname: _.lowerCase(firstname),
      mname: _.lowerCase(middlename),
      lname: _.lowerCase(lastname),
      sex: gender === 'Male' ? 1 : 0,
      civil_status,
      room_no: RoomNo,
      hospital_no: HospitalNo,
      attending_physician: 0
    });
  }

  render() {
    const {
      bizboxPatients,
      isLoading,
      bizboxLoading,
      patients
    } = this.props;

    const { open } = this.state;

    const panes = [
      {
        menuItem: {
          key: 'local',
          icon: 'database',
          content: 'Local'
        },
        render: () => (
          <Tab.Pane loading={isLoading}>
            <PatientList patients={patients} />
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: 'bizbox',
          icon: 'server',
          content: 'Bizbox'
        },
        render: () => (
          <Tab.Pane loading={bizboxLoading}>
            <BizboxPatientList
              bizboxPatients={bizboxPatients}
              onMigrate={this.handleMigrate}
            />
          </Tab.Pane>
        )
      }
    ];

    return (
      <Container>
        <Tab
          menu={{ pointing: true, borderless: true, widths: 'two' }}
          panes={panes}
        />

        <BizboxPatientConfirm
          onCancel={this.handleCancel}
          onConfirm={this.handleConfirm}
          showConfirm={open}
        />
      </Container>
    );
  }
}

Patients.propTypes = propTypes;

export default Patients;
 */
