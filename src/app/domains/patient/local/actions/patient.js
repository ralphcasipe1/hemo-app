import {
  CLEAR_FETCHED_PATIENT,
  CLEAR_NEW_PATIENT,
  CREATE_PATIENT,
  CREATE_PATIENT_FAILURE,
  CREATE_PATIENT_SUCCESS,
  FETCH_ALL_PATIENTS,
  FETCH_ALL_PATIENTS_FAILURE,
  FETCH_ALL_PATIENTS_SUCCESS,
  FETCH_PATIENT,
  FETCH_PATIENT_FAILURE,
  FETCH_PATIENT_SUCCESS
} from '../constants/patient'
import { instance } from '../../../../config/connection'

interface PatientQuery {
  fullName: string | void
}

export const fetchAllPatients = (page = 1) => dispatch => {

  dispatch({
    type: FETCH_ALL_PATIENTS
  })

  return instance.get(`/patients?page=${page}`).then(
    response =>
      dispatch({
        type: FETCH_ALL_PATIENTS_SUCCESS
        , payload: response.data
      }),

    error =>
      dispatch({
        type: FETCH_ALL_PATIENTS_FAILURE
        , error: error.response
      })
  )
}

export const searchPatientByName = (fullName: string = '') => dispatch => {

  dispatch({
    type: FETCH_ALL_PATIENTS
  })

  return instance.get(`/patients?fullName=${fullName}`).then(
    response =>
      dispatch({
        type: FETCH_ALL_PATIENTS_SUCCESS
        , payload: response.data
      }),

    error =>
      dispatch({
        type: FETCH_ALL_PATIENTS_FAILURE
        , error: error.response
      })
  )
}

export const sortPatientsByCreatedAt = (createdAt = 'ASC') => dispatch => {
  dispatch({
    type: FETCH_ALL_PATIENTS
  })

  return instance.get(`/patients?createdAt=${createdAt}`).then(
    response =>
      dispatch({
        type: FETCH_ALL_PATIENTS_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_ALL_PATIENTS_FAILURE
        , error: error.response
      })
  )
}

export const fetchPatient = patientId => dispatch => {
  dispatch({
    type: FETCH_PATIENT
  })

  return instance.get(`/patients/${patientId}`).then(
    response =>
      dispatch({
        type: FETCH_PATIENT_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_PATIENT_FAILURE
        , error: error.response
      })
  )
}

export const createPatient = values => dispatch => {
  dispatch({
    type: CREATE_PATIENT
  })

  instance.post(`/patients`, values).then(
    response =>
      dispatch({
        type: CREATE_PATIENT_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: CREATE_PATIENT_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedPatient = () => ({ type: CLEAR_FETCHED_PATIENT })

export const clearNewPatient = () => dispatch =>
  dispatch({
    type: CLEAR_NEW_PATIENT
  })
