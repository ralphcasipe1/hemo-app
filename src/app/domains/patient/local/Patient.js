import React, { Component, Fragment } from 'react'
import type { Element }               from 'react'
import { Divider }                    from 'semantic-ui-react'
import { PatientHOC }                 from '../PatientHOC'
import { PatientList }                from './PatientList'
import { PatientPagination }          from './PatientPagination'
import { PatientSearch }              from './PatientSearch'
// import { PatientSorter }              from './PatientSorter'

type PropsType = {
  meta: object,
  patients: Array<any>,
  fetchAllPatients: any,
  searchPatientByName: any
};

class Patient extends Component<PropsType> {

  state = {
    patients: this.props.patients
  }

  componentDidMount() {
    this.props.fetchAllPatients()
  }

  componentDidUpdate(prevProps: PropsType) {
    if (prevProps.patients.length !== this.props.patients.length) {
      this.setState({
        patients: this.props.patients
      })
    }
  }

  handleClickPage = (page: string) =>
    this.props.fetchAllPatients(page)

  handlePatientSearch = (fullName: string) => {
    this.props.searchPatientByName(fullName)
  }


  render(): Element<'div'> {
    const { meta } = this.props
    const { patients } = this.state

    if (!meta) {
      return <div>Loading...</div>
    }

    return (
      <Fragment>

        <PatientSearch onPatientSearch={this.handlePatientSearch} />

        <Divider hidden />

        <PatientPagination totalPages={meta.totalPages} onClickPage={this.handleClickPage}/>

        <PatientList patients={patients} />

        <PatientPagination totalPages={meta.totalPages} onClickPage={this.handleClickPage}/>
      </Fragment>
    )
  }
}

Patient = PatientHOC(Patient)

export { Patient }
