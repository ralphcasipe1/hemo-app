import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { List } from 'semantic-ui-react';
import { BizboxPatientListItem } from './BizboxPatientListItem';

class BizboxPatientList extends PureComponent {
  static propTypes = {
    bizboxPatients: PropTypes.array
    , isLoading: PropTypes.bool
    , error: PropTypes.shape({
      message: PropTypes.string
    })
  };

  render() {
    const { bizboxPatients, openMigrationModal } = this.props;

    return (
      <Fragment>
        {_.isEmpty(bizboxPatients) ? (
          <h1>No Bizbox Patients</h1>
        ) : (
          <List divided relaxed="very" verticalAlign="middle" size="large">
            {bizboxPatients.map(bizboxPatient => (
              <BizboxPatientListItem
                key={bizboxPatient.FK_emdPatients}
                bizboxPatient={bizboxPatient}
                openMigrationModal={openMigrationModal}
              />
            ))}
          </List>
        )}
      </Fragment>
    );
  }
}

export { BizboxPatientList };
