import { connect } from 'react-redux';
import { createPatient } from '../local/actions/patient';
import {
  clearFetchedBizboxPatient,
  fetchAllBizboxPatients,
  fetchBizboxPatient
} from './actions/bizbox-patient';
import { BizboxPatient } from './BizboxPatient';

const mapStateToProps = state => {
  const {
    error,
    isLoading,
    bizboxPatient,
    bizboxPatients
  } = state.bizboxPatientReducer;

  return {
    error
    , isLoading
    , bizboxPatient
    , bizboxPatients
  };
};

export const BizboxPatientContainer = connect(
  mapStateToProps,
  {
    clearFetchedBizboxPatient
    , createPatient
    , fetchAllBizboxPatients
    , fetchBizboxPatient
  }
)(BizboxPatient);
