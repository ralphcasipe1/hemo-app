import moment from 'moment';
import {
  CLEAR_FETCHED_BIZBOX_PATIENT,
  FETCH_ALL_BIZBOX_PATIENTS,
  FETCH_ALL_BIZBOX_PATIENTS_FAILURE,
  FETCH_ALL_BIZBOX_PATIENTS_SUCCESS,
  FETCH_BIZBOX_PATIENT,
  FETCH_BIZBOX_PATIENT_FAILURE,
  FETCH_BIZBOX_PATIENT_SUCCESS
} from '../constants/bizbox-patient';
import { bizbox } from '../../../../config/connection';

export const fetchAllBizboxPatients = (
  date = moment().format('YYYY-MM-DD')
) => dispatch => {
  dispatch({
    type: FETCH_ALL_BIZBOX_PATIENTS
  });

  return bizbox.get(`/patients/date/${date}`).then(
    response => {
      console.log(response.data)
      dispatch({
        type: FETCH_ALL_BIZBOX_PATIENTS_SUCCESS
        , payload: response.data.data
      })
    },
      

    error =>
      dispatch({
        type: FETCH_ALL_BIZBOX_PATIENTS_FAILURE
        , error: error.response
      })
  );
};

export const fetchBizboxPatient = bizboxPatientId => dispatch => {
  dispatch({
    type: FETCH_BIZBOX_PATIENT
  });

  return bizbox.get(`patients/${bizboxPatientId}`).then(
    response =>
      dispatch({
        type: FETCH_BIZBOX_PATIENT_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_BIZBOX_PATIENT_FAILURE
        , error: error.response
      })
  );
};

export const clearFetchedBizboxPatient = () => ({
  type: CLEAR_FETCHED_BIZBOX_PATIENT
});
