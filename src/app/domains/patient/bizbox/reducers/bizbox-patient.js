import {
  CREATE_PATIENT,
  CREATE_PATIENT_SUCCESS
} from '../../local/constants/patient';
import {
  CLEAR_FETCHED_BIZBOX_PATIENT,
  FETCH_ALL_BIZBOX_PATIENTS,
  FETCH_ALL_BIZBOX_PATIENTS_FAILURE,
  FETCH_ALL_BIZBOX_PATIENTS_SUCCESS,
  FETCH_BIZBOX_PATIENT,
  FETCH_BIZBOX_PATIENT_FAILURE,
  FETCH_BIZBOX_PATIENT_SUCCESS
} from '../constants/bizbox-patient';

// import { CREATE_PATIENT_SUCCESS } from '../../local/constants/patient';

const initialState = {
  bizboxPatients: []
  , bizboxPatient: null
  , error: null
  , isLoading: false
};

export const bizboxPatientReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_FETCHED_BIZBOX_PATIENT:
      return {
        ...state
        , bizboxPatient: null
        , error: null
        , isLoading: false
      };

    case CREATE_PATIENT:
    case FETCH_ALL_BIZBOX_PATIENTS:
    case FETCH_BIZBOX_PATIENT:
      return {
        ...state
        , isLoading: true
      };

    case FETCH_ALL_BIZBOX_PATIENTS_FAILURE:
    case FETCH_BIZBOX_PATIENT_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      };

    case CREATE_PATIENT_SUCCESS:
      return {
        ...state
        , isLoading: false
        , bizboxPatients: state.bizboxPatients.filter(
          bizboxPatient =>
            +bizboxPatient.FK_emdPatients !== +action.payload.bizbox_patient_no
        )
      };

    case FETCH_ALL_BIZBOX_PATIENTS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , bizboxPatients: action.payload
      };

    case FETCH_BIZBOX_PATIENT_SUCCESS:
      return {
        ...state
        , isLoading: false
        , bizboxPatient: action.payload
      };

    /*case CREATE_PATIENT_SUCCESS:
      return {
        ...state,
        bizboxPatients: state.bizboxPatients.filter(
          (bizboxPatient) =>
            bizboxPatient.FK_emdPatients !==
            +action.payload.data.bizbox_patient_no
        ),
      };*/

    default:
      return state;
  }
};
