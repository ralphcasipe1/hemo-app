import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Icon, List } from 'semantic-ui-react';
import { patientFullName } from './helpers/patient-full-name';

class BizboxPatientListItem extends PureComponent {
  static propTypes = {
    bizboxPatient: PropTypes.shape({
      lastname: PropTypes.string.isRequired
      , firstname: PropTypes.string.isRequired
      , middlename: PropTypes.string.isRequired
      , FK_emdPatients: PropTypes.number.isRequired
    })
    , openMigrationModal: PropTypes.func.isRequired
  };

  render() {
    const { bizboxPatient, openMigrationModal } = this.props;

    return (
      <List.Item>
        <List.Content floated="right">
          <Icon
            color="blue"
            link
            name="share"
            onClick={() => openMigrationModal(bizboxPatient.FK_emdPatients)}
          />
        </List.Content>
        <List.Content>
          <List.Header>
            <Icon color="grey" name="wheelchair" />
            {patientFullName(bizboxPatient)}
          </List.Header>
        </List.Content>
      </List.Item>
    );
  }
}

export { BizboxPatientListItem };
