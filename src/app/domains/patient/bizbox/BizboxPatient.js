import React, { Component, Fragment } from 'react';
import { Button, Header, Message, Modal } from 'semantic-ui-react';
import { BizboxPatientList } from './BizboxPatientList';
import { civilStatuses } from './constants/civil-statuses';
import { PatientHOC } from '../PatientHOC';

class BizboxPatient extends Component {
  state = {
    modalConfirm: false
    , id: null
    , showSuccessMessage: false
  };

  componentDidMount() {
    this.props.fetchAllBizboxPatients();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.id !== prevState.id) {
      if (!!this.state.id) {
        this.props.fetchBizboxPatient(this.state.id);
      }
    }

    if (this.state.showSuccessMessage !== prevState.showSuccessMessage) {
      if (this.state.showSuccessMessage) {
        setTimeout(
          () =>
            this.setState({
              showSuccessMessage: false
            }),
          2000
        );
      }
    }
  }

  componentWillUnmount() {
    this.props.clearFetchedBizboxPatient();
  }

  closeModal = () =>
    this.setState({
      modalConfirm: false
      , id: null
    });

  openModal = fkEmdpatient =>
    this.setState({
      modalConfirm: true
      , id: fkEmdpatient
    });

  createPatient = () => {
    const { bizboxPatient } = this.props;
    console.log(bizboxPatient);
    this.props.createPatient({
      first_name: bizboxPatient.firstname
      , middle_name: bizboxPatient.middlename
      , last_name: bizboxPatient.lastname
      , bizbox_patient_no: bizboxPatient.FK_emdPatients
      , sex: bizboxPatient.gender === 'Male' ? 1 : 0
      , civil_status: civilStatuses[bizboxPatient.civilStatus] || 5
      , room_no: bizboxPatient.RoomNo
      , hospital_no: bizboxPatient.HospitalNo
      , attending_physician: bizboxPatient.dr_id
    });

    this.setState({
      modalConfirm: false
      , showSuccessMessage: true
    });
  };

  render() {
    const { bizboxPatients } = this.props;
    const { modalConfirm, showSuccessMessage } = this.state;

    return (
      <Fragment>
        {showSuccessMessage ? (
          <Message
            icon="checkmark"
            positive
            header="Migration Completed"
            content="Please see the patient in the Local Tab"
            size="tiny"
          />
        ) : null}

        <Modal
          open={modalConfirm}
          onClose={this.closeModal}
          basic
          size="fullscreen"
        >
          <Header icon="warning" color="yellow" content="Migration Notice" />

          <Modal.Content>
            <p>
              Do you want to
              <b> MIGRATE </b> <i>Patient's Information</i>
              <b> FROM </b> <i>bizbox</i> <b> TO </b> this <i>application</i>
            </p>
          </Modal.Content>

          <Modal.Actions>
            <Button color="red" icon="close" onClick={this.closeModal} />
            <Button
              color="green"
              icon="checkmark"
              onClick={this.createPatient}
            />
          </Modal.Actions>
        </Modal>

        <BizboxPatientList
          bizboxPatients={bizboxPatients}
          openMigrationModal={this.openModal}
        />
      </Fragment>
    );
  }
}

BizboxPatient = PatientHOC(BizboxPatient);

export { BizboxPatient };
