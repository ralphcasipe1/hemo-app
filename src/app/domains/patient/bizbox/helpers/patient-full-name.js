export const patientFullName = bizboxPatient => {
  const { firstname, middlename, lastname } = bizboxPatient;

  const fullname = `${lastname}, ${firstname} ${middlename}`;

  return fullname.toUpperCase();
};
