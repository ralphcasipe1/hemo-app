import { connect } from 'react-redux';
import { createPatient, fetchAllPatients } from '../actions/patient';
import {
  fetchBizboxPatient,
  fetchBizboxPatients
} from '../actions/bizboxPatients';
import {
  getLoading,
  getPatients,
  getMessage
} from '../selectors/PatientSelectors';
import {
  getBizboxLoading,
  getBizboxPatient,
  getBizboxPatients
} from '../selectors/BizboxPatientSelectors';
import Patients from '../components/Patients';
import React from 'react';

const PatientContainer = ({
  // createPatient,
  bizboxLoading,
  bizboxPatient,
  bizboxPatients,
  // fetchBizboxPatient,
  // fetchBizboxPatients,
  // fetchAllPatients,
  localLoading,
  message,
  patients
}) => (
  <Patients
    bizboxLoading={bizboxLoading}
    bizboxPatient={bizboxPatient}
    bizboxPatients={bizboxPatients}
    createPatient={createPatient}
    fetchBizboxPatient={fetchBizboxPatient}
    fetchBizboxPatients={fetchBizboxPatients}
    fetchPatients={fetchAllPatients}
    localLoading={localLoading}
    message={message}
    patients={patients}
  />
);

const mapStateToProps = state => ({
  bizboxLoading: getBizboxLoading(state)
  , bizboxPatient: getBizboxPatient(state)
  , bizboxPatients: getBizboxPatients(state)
  , localLoading: getLoading(state)
  , message: getMessage(state)
  , patients: getPatients(state)
});

export default connect(
  mapStateToProps,
  {
    createPatient
    , fetchBizboxPatient
    , fetchBizboxPatients
    // , fetchPatients
  }
)(PatientContainer);

/* import { connect } from 'react-redux';
import {
  fetchBizboxPatients,
  selectBizboxPatient,
  resetBizboxPatient
} from '../actions/bizboxPatients';
import {
  createPatient,
  fetchPatients,
} from '../actions/patients';
import {
  getBizboxPatients,
  getBizboxPatient,
  getBizboxLoading
} from '../selectors/BizboxPatientSelectors';
import {
  getPatients,
  getCount,
  getAfter,
  getBefore,
  getPage,
  getPages,
  getTotal,
  getLoading
} from '../selectors/PatientSelectors';
import moment from 'moment';
import Patients from '../components/Patients';
import React from 'react';

const PatientContainer = props => <Patients {...props} />;

const mapStateToProps = (state, props) => {
  return {
    bizboxPatients: getBizboxPatients(state),
    bizboxPatient: getBizboxPatient(state),
    bizboxLoading: getBizboxLoading(state),

    patients: getPatients(state),
    isLoading: getLoading(state)
  };
};

export default connect(mapStateToProps, {
  createPatient,
  fetchBizboxPatients,
  fetchPatients,
  nextPage,
  prevPage,
  resetPatients,
  selectBizboxPatient,
  resetBizboxPatient
})(PatientContainer);
 */
