import { setCurrentUser } from './set-current-user'
import { setAuthenticationToken } from '../helpers/set-authentication-token'

export const logout = () => dispatch => {
  localStorage.removeItem('accessToken')
  setAuthenticationToken(false)
  dispatch(setCurrentUser({}))
}
