import jwtDecode from 'jwt-decode'
import { instance } from 'app/config/connection'
import { setAuthenticationToken } from '../helpers/set-authentication-token'
import { setCurrentUser } from './set-current-user'

export const setToken = values => dispatch => {
  instance.post('/authentication', values).then(
    response => {
      let accessToken = response.data.accessToken
      localStorage.setItem('accessToken', accessToken)
      setAuthenticationToken(accessToken)
      dispatch(setCurrentUser(jwtDecode(accessToken)))
    },
    error =>
      dispatch({
        type: 'LOGIN_FAILED'
        , error: error.response.data.message
      })
  )
}
