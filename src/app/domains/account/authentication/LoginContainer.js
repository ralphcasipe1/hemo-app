import { connect } from 'react-redux'
import { setToken } from './actions/set-token'
import { Login } from './Login'

const mapStateToProps = state => ({
  authentication: state.authenticationReducer
})

export const LoginContainer = connect(
  mapStateToProps,
  { setToken }
)(Login)
