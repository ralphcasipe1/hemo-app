import axios from 'axios';

export const setAuthenticationToken = (token) => {
  if (token) {
    return (axios.defaults.headers.common['Authorization'] = token);
  } else {
    return delete axios.defaults.headers.common['Authorization'];
  }
};
