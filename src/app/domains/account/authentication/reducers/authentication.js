const initialState = {
  isAuthenticated: false
  , user: {}
  , error: null
};

export const authenticationReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_CURRENT_USER':
      return {
        ...state
        , isAuthenticated: !!action.user
        , user: action.user
      };

    case 'LOGIN_FAILED':
      return {
        ...state
        , error: action.error
      };
    default:
      return state;
  }
};
