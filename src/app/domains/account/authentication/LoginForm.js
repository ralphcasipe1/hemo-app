import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Button, Form } from 'semantic-ui-react'
import { InputField } from '../../../common/forms/InputField'

class LoginForm extends Component {
  render() {
    return (
      <Form
        loading={this.props.formLoading}
        onSubmit={this.props.handleSubmit(this.props.onFormSubmit)}
      >
        <Field
          component={InputField}
          icon="user"
          label="Username"
          name="id_number"
        />
        <Field
          component={InputField}
          icon="lock"
          label="Password"
          name="password"
          type="password"
        />
        <Button
          circular
          color="blue"
          icon="sign in"
          onClick={this.props.handleFormSubmit}
          size="large"
          floated="right"
        />
      </Form>
    )
  }
}

export default reduxForm({
  fields: [ 'username', 'password' ]
  , form: 'LoginForm'
})(LoginForm)
