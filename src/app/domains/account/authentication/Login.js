import React, { PureComponent } from 'react'
import { Header, Grid, Image, Segment, Message } from 'semantic-ui-react'
import './Login.css'
import bg from './assets/uk.png'
import LoginForm from './LoginForm'

class Login extends PureComponent {
  constructor() {
    super()

    this.handleFormSubmit = this.handleFormSubmit.bind(this)

    this.state = {
      visible: false
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.authentication.isAuthenticated) {
      this.props.history.push(`/`)
    }
  }

  handleFormSubmit(values) {
    this.props.setToken(values)
  }

  render() {
    const { authentication } = this.props

    return (
      <div className="login-wrapper">
        <div className="login-container">
          <Grid container verticalAlign="middle" doubling relaxed="very">
            <Grid.Column width={8} computer={8} mobile={16}>
              <Header
                content="Sign In"
                textAlign="center"
                color="blue"
                as="h2"
              />
              <Segment>
                {authentication.error ? (
                  <Message
                    error
                    content={authentication.error}
                    icon="warning circle"
                  />
                ) : null}

                <LoginForm onFormSubmit={this.handleFormSubmit} />
              </Segment>
            </Grid.Column>
            <Grid.Column width={8} computer={8} only="computer">
              <Image src={bg} size="massive" />
            </Grid.Column>
          </Grid>
        </div>
      </div>
    )
  }
}

export { Login }
