import React, { PureComponent }     from 'react'
import type { Node }                from 'react'
import { Link }                     from 'react-router-dom'
import _                            from 'lodash'
import { Dropdown, Header, Icon }   from 'semantic-ui-react'
import type { ItemType, StateType } from './AdminType'
import { ADMIN_MENU_ITEMS }         from './constants/admin-menu-items'
import { legibleName }              from './helpers/legibleName'

type PropType = {
  onItemClick: (e: SyntheticEvent<HTMLButtonElement>, ItemType) => StateType,
  activeItem: string
};

class AdminMenu extends PureComponent<PropType> {
  render(): Node {
    const { onItemClick, activeItem } = this.props

    return (
      <Header as="h4">
        <Icon name="universal access" />
        <Header.Content>
          Admin Menus{' '}
          <Dropdown inline header="Navigations">
            <Dropdown.Menu>
              <Dropdown.Header content="Navigation" />
              {ADMIN_MENU_ITEMS.map((item: ItemType, index: number): Element<'div'> => (
                <Dropdown.Item
                  key={index}
                  active={activeItem === item.name}
                  as={Link}
                  name={item.name}
                  onClick={onItemClick}
                  to={`/admins/${_.snakeCase(item.name)}`}
                >
                  <Icon name={item.icon} color="blue" />
                  {legibleName(item.name)}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </Header.Content>
      </Header>
    )
  }
}

export { AdminMenu }
