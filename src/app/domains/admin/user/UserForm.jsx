import React, { PureComponent, Fragment } from 'react'
import { Field, reduxForm }               from 'redux-form'
import { Form }                           from 'semantic-ui-react'
import { ButtonDecision }                 from 'app/common/forms/ButtonDecision'
import { InputField }                     from 'app/common/forms/InputField'

class UserForm extends PureComponent {
  render() {
    const {
      formLoading,
      handleSubmit,
      onFormSubmit,
      valid,
      submitting
    } = this.props

    return (
      <Fragment>
        <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
          <Field component={InputField} label="First Name" name="first_name" />

          <Field component={InputField} label="Middle Name" name="middle_name" />

          <Field component={InputField} label="Last Name" name="last_name" />

          <Field component={InputField} label="ID Number" name="id_number" />

          {/* <Field
            component={InputField}
            label="Password"
            name="password"
            type="password"
          /> */}

          <ButtonDecision
            expanded
            isDisable={!valid || submitting}
            {...this.props}
          />
        </Form>
      </Fragment>
    )
  }
}

/*const validate = values => {
  const errors = {}

  if (!values.fname) {
    errors.fname = 'Required'
  } else if (values.fname.length < 2) {
    errors.fname = 'Min length is 2'
  }

  if (!values.lname) {
    errors.lname = 'Required'
  } else if (values.lname.length < 2) {
    errors.lname = 'Min length is 2'
  }

  if (!values.id_number) {
    errors.id_number = 'Required'
  } else if (values.id_number.length < 6) {
    errors.id_number = 'Min length is 6'
  }

   if(!values.password) {
        errors.password = 'Required'
    } else if(values.password.length < 6) {
        errors.password = 'Min length is 6'
    } else if(checkSequence(values.password) === true) {
        errors.password = 'Must not have repetitive characters'
    } else if(mustHaveNumber(values.password) === true) {
        errors.password = 'Must container at least one number'
    } else if(mustHaveSpecChar(values.password) === true) {
        errors.password = 'Must container at least one special character'
    } 

  return errors
}*/

UserForm = reduxForm({
  fields: [
    'first_name'
    , 'middle_name'
    , 'last_name'
    , 'id_number'
    // 'password'
  ]
  // , validate
})(UserForm)

export { UserForm }