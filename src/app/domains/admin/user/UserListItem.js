import React from 'react'
import {
  Dropdown,
  List,
  Label
} from 'semantic-ui-react'
import { userIdentity } from './helpers/user-identity'

const UserListItem = ({ user, onClickEdit, onClickDelete }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item
            content="Edit"
            key="edit"
            onClick={() => onClickEdit(user.user_ud)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(user.user_ud)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={userIdentity(user)} />
      <List.Description>
        {user.deleted_at !== null ? 
          <Label color="red" content="Inactive" /> :
          <Label color="blue" content="Active" />
        }
      </List.Description>
    </List.Content>
  </List.Item>
)
export { UserListItem }