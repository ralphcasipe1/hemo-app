import {
  CREATE_USER,
  CREATE_USER_FAILURE,
  CREATE_USER_SUCCESS,
  FETCH_ALL_USERS,
  FETCH_ALL_USERS_FAILURE,
  FETCH_ALL_USERS_SUCCESS
} from '../constants'

const initialState = {
  error: null
  , isLoading: false
  , users: []
}

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_USER:
    case FETCH_ALL_USERS:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_USER_FAILURE:
    case FETCH_ALL_USERS_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      }

    case FETCH_ALL_USERS_SUCCESS:
      return {
        ...state
        , users: action.payload
        , isLoading: false
      }

    case CREATE_USER_SUCCESS:
      return {
        ...state
        , users: [ action.payload, ...state.users ]
        , isLoading: false
      }

    default:
      return state
  }
}
