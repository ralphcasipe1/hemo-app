import React            from 'react'
import { List }         from 'semantic-ui-react'
import { UserListItem } from './UserListItem'

const UserList = props => (
  <List divided relaxed="very" size="big">
    {props.users.map(user => (
      <UserListItem
        key={user.user_id}
        user={user}
        {...props}
      />
    ))}
  </List>
)

export { UserList }