import { connect }                   from 'react-redux'
import { createUser, fetchAllUsers } from './actions'
import { User }                      from './User'

const mapStateToProps = state => ({
  loading: state.userReducer.loading
  , data: state.userReducer.users
})
export const UserContainer = connect(mapStateToProps, {
  create: createUser
  , deleteData: () => null
  , fetchAllData: fetchAllUsers
  , fetchData: () => null
  , clearData: () => null
  , updateData: () => null
})(User)