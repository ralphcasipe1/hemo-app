export const userIdentity = user => (
  `${user.id_number} ${user.last_name} ${user.first_name} ${user.middle_name}`
)