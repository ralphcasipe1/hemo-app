import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { UserForm }                   from './UserForm'
import { UserList }                   from './UserList'

class User extends Component {
  render()  {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      user
    } = this.props

    if (data.length === 0 || openCreateForm) {
      return (
        <UserForm
          form="EditUserForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    return (
      <Fragment>
        <UserList
          users={data}
        />    
      </Fragment>
    )  
  }
}

User = AdminBuilderHOC(User, {
  title: 'User'
})

export { User }