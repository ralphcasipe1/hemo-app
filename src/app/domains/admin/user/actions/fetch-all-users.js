import {
  FETCH_ALL_USERS,
  FETCH_ALL_USERS_FAILURE,
  FETCH_ALL_USERS_SUCCESS
} from '../constants'
import { instance } from 'app/config/connection'

export const fetchAllUsers = () => dispatch => {
  dispatch({
    type: FETCH_ALL_USERS
  })

  return instance.get('/users').then(
    response =>
      dispatch({
        type: FETCH_ALL_USERS_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_ALL_USERS_FAILURE
        , error: error.response
      })
  )
}
