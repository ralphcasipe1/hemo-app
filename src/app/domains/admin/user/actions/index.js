import { createUser }    from './create-user'
import { fetchAllUsers } from './fetch-all-users'

export {
  createUser,
  fetchAllUsers
}