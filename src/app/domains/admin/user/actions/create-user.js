import {
  CREATE_USER,
  CREATE_USER_FAILURE,
  CREATE_USER_SUCCESS
} from '../constants'
import { instance } from 'app/config/connection'

export const createUser = values => dispatch => {
  dispatch({
    type: CREATE_USER
  })

  return instance.post('/users', values).then(
    response =>
      dispatch({
        type: CREATE_USER_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: CREATE_USER_FAILURE
        , error: error.response
      })
  )
}
