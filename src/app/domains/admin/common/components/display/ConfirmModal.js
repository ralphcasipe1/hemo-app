import React, { PureComponent } from 'react';
import { Button, Modal } from 'semantic-ui-react';

class ConfirmModal extends PureComponent {
  render() {
    const { open, onClose, onDelete } = this.props;

    return (
      <Modal open={open} onClose={onClose} basic>
        <Modal.Content>Are you sure?</Modal.Content>
        <Modal.Actions>
          <Button color="red" icon="cancel" onClick={onClose} />

          <Button color="green" icon="checkmark" onClick={onDelete} />
        </Modal.Actions>
      </Modal>
    );
  }
}

export { ConfirmModal };
