import React, { Component, Fragment } from 'react'
import {
  Container,
  Header,
  Icon, 
  Segment
} from 'semantic-ui-react'
import './styles/AdminBuilderHOC.css'

export const AdminBuilderHOC = (WrappedComponent, { title }) => {
  return class extends Component {
    state = {
      openCreateForm: false
      , openEditForm: false
      , openModal: false
      , id: null
    };

    componentDidMount() {
      this.props.fetchAllData()
    }

    handleCreate = values => {
      this.props.create(values)
      this.handleCloseCreateForm()
    };

    handleUpdate = values => {
      this.props.updateData(this.state.id, values)
      this.handleCloseEditForm()
    }

    handleDelete = () => {
      this.props.deleteData(this.state.id)
      this.handleCloseModal()
    }

    handleOpenCreateForm = () => this.setState({ openCreateForm: true });

    handleCloseCreateForm = () => this.setState({ openCreateForm: false });

    handleOpenEditForm = id => {
      this.props.fetchData(id)
      this.setState({ id, openEditForm: true })
    }

    handleCloseEditForm = () => {
      this.setState({ id: null, openEditForm: false })
      this.props.clearData()
    }

    handleOpenModal = id => this.setState({ id, openModal: true });

    handleCloseModal = () => {
      this.setState({ id: null, openModal: false })
      this.props.clearData()
    }

    render() {
      const { data, loading } = this.props
      const { openCreateForm, openEditForm, openModal } = this.state
      
      return (
        <Fragment>
          <Header as="h3" content={title} dividing />

          {openCreateForm || data.length === 0 ? null : (
            <Header as="h3" textAlign="right">
              <a className="add-link" onClick={this.handleOpenCreateForm}>
                <Icon name="plus" /> Add
              </a>
            </Header>
          )}

          <Segment loading={loading}>
            <Container>
              <WrappedComponent
                data={data}
                handleCreate={this.handleCreate}
                handleCloseCreateForm={this.handleCloseCreateForm}
                openCreateForm={openCreateForm}

                openModal={openModal}
                handleOpenModal={this.handleOpenModal}
                handleCloseModal={this.handleCloseModal}
                handleDelete={this.handleDelete}

                handleOpenEditForm={this.handleOpenEditForm}
                handleCloseEditForm={this.handleCloseEditForm}
                handleUpdate={this.handleUpdate}
                openEditForm={openEditForm}
               
                {...this.props}
              />
            </Container>
          </Segment>
        </Fragment>
      )
    }
  }
}
