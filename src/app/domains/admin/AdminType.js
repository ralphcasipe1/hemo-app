export type StateType = {
  activeItem: string
};

export type ItemType = {
  name: string
};