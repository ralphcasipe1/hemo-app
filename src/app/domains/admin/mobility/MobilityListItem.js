import React              from 'react'
import { Dropdown, List } from 'semantic-ui-react'

export const MobilityListItem = ({ mobility, onClickDelete, onClickEdit }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item 
            content="Edit" 
            key="edit" 
            onClick={() => onClickEdit(mobility.mobility_id)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(mobility.mobility_id)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={mobility.name} />
      <List.Description content={mobility.description} />
    </List.Content>
  </List.Item>
)
