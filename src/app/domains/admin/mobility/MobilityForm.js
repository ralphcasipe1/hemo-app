import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import { ButtonDecision } from '../../../common/forms/ButtonDecision';
import { InputField } from '../../../common/forms/InputField';

class MobilityForm extends PureComponent {
  render() {
    return (
      <Form
        className="attached"
        loading={this.props.formLoading}
        onSubmit={this.props.handleSubmit(this.props.onFormSubmit)}
      >
        <Field component={InputField} name="name" label="Name" />
        <Field component={InputField} name="description" label="Description" />
        <ButtonDecision
          expanded
          isDisable={!this.props.valid || this.props.submitting}
          {...this.props}
        />
      </Form>
    );
  }
}

const validate = values => {
  let errors = {};

  if (!values.name) {
    errors.name = 'Required';
  } else if (values.name.length < 4) {
    errors.name = 'Min length is 4';
  }

  return errors;
};

MobilityForm = reduxForm({
  fields: ['name', 'description'],
  validate,
})(MobilityForm);

export { MobilityForm };
