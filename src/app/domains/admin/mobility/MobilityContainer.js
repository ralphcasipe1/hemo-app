import { connect } from 'react-redux'
import {
  createMobility,
  deleteMobility,
  fetchAllMobilities,
  fetchMobility,
  clearFetchedMobility,
  updateMobility
} from './actions/mobility'
import { Mobility } from './Mobility'

const mapStateToProps = state => ({
  data: state.mobilityReducer.mobilities
  , loading: state.mobilityReducer.loading
  , mobility: state.mobilityReducer.mobility
})

export const MobilityContainer = connect(
  mapStateToProps,
  {
    create: createMobility
    , deleteData: deleteMobility
    , fetchAllData: fetchAllMobilities
    , fetchData: fetchMobility
    , clearData: clearFetchedMobility
    , updateData: updateMobility
  }
)(Mobility)
