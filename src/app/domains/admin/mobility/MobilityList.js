import React from 'react';
import { List } from 'semantic-ui-react';
import { MobilityListItem } from './MobilityListItem';

export const MobilityList = props => (
  <List divided relaxed="very" size="big">
    {props.mobilities.map(mobility => (
      <MobilityListItem
        key={mobility.mobility_id}
        mobility={mobility}
        {...props}
      />
    ))}
  </List>
);
