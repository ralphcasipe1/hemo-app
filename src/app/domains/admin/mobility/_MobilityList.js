import { Button, Icon, List, Loader } from 'semantic-ui-react';
import _ from 'lodash';
import ExceptionMessage from 'ExceptionMessage';
import MobilityForm from 'MobilityForm';
import RecommendMessage from 'RecommendMessage';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class MobilitiesList extends PureComponent {
  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    mobilities: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
      })
    ),
    onDeleteClick: PropTypes.func.isRequired,
    onEditClick: PropTypes.func.isRequired,
  };

  constructor() {
    super();

    this.state = {
      mobilityId: [],
      deleteMobility: [],
    };
  }

  renderListItem(mobility) {
    if (this.state.mobilityId.indexOf(mobility.mobility_id) !== -1) {
      return (
        <List.Item key={mobility.mobility_id}>
          <List.Content>
            <MobilityForm
              form={`EditMobilityForm${mobility.mobility_id}`}
              onCancel={() =>
                this.setState({
                  mobilityId: this.state.mobilityId.filter(
                    id => id !== mobility.mobility_id
                  ),
                })
              }
              onFormSubmit={values => {
                this.props.onEditClick(mobility.mobility_id, values);
                this.setState({
                  mobilityId: this.state.mobilityId.filter(
                    id => id !== mobility.mobility_id
                  ),
                });
              }}
              initialValues={{
                name: mobility.name,
                description: mobility.description,
              }}
              simple
            />
          </List.Content>
        </List.Item>
      );
    } else if (this.state.deleteMobility.indexOf(mobility.mobility_id) !== -1) {
      return (
        <List.Item key={mobility.mobility_id}>
          <List.Content floated="right" verticalAlign="middle">
            <Button
              content="Yes"
              floated="right"
              color="red"
              icon="checkmark"
              onClick={() => this.props.onDeleteClick(mobility.mobility_id)}
            />
            <Button
              content="No"
              color="blue"
              floated="right"
              basic
              icon="cancel"
              onClick={() =>
                this.setState({
                  deleteMobility: this.state.deleteMobility.filter(
                    id => id !== mobility.mobility_id
                  ),
                })
              }
            />
          </List.Content>
          <Icon name="warning sign" color="yellow" />
          <List.Content>
            <List.Header>You want to delete {mobility.name}?</List.Header>
            <List.Description>{mobility.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    } else {
      return (
        <List.Item key={mobility.mobility_id}>
          <List.Content floated="right">
            <Icon
              color="red"
              link
              name="trash"
              onClick={() =>
                this.setState({
                  deleteMobility: [
                    ...this.state.deleteMobility,
                    mobility.mobility_id,
                  ],
                })
              }
            />
            <Icon
              color="green"
              link
              name="edit"
              onClick={() =>
                this.setState({
                  mobilityId: [...this.state.mobilityId, mobility.mobility_id],
                })
              }
            />
          </List.Content>
          <List.Content>
            <List.Header>{mobility.name}</List.Header>
            <List.Description>{mobility.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    }
  }

  render() {
    if (this.props.isLoading) {
      return <Loader active inline="centered" />;
    } else if (this.props.error) {
      return <ExceptionMessage exception={this.props.error} />;
    } else if (_.isEmpty(this.props.mobilities)) {
      return <RecommendMessage header="No Mobilities" />;
    }

    return (
      <List divided relaxed="very">
        {this.props.mobilities.map(mobility => this.renderListItem(mobility))}
      </List>
    );
  }
}

export default MobilitiesList;
