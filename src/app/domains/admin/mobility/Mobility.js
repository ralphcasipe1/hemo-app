import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { ConfirmModal }               from '../common/components/display/ConfirmModal'
import { MobilityForm }               from './MobilityForm'
import { MobilityList }               from './MobilityList'

class Mobility extends Component {
  render() {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      mobility
    } = this.props
    
    if (data.length === 0 || openCreateForm) {
      return (
        <MobilityForm
          form="CreateMobilityForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    if (openEditForm && mobility) {
      return (
        <MobilityForm
          form="EditMobilityForm"
          onFormSubmit={handleUpdate}
          onCancel={handleCloseEditForm}
          initialValues={{
            name: mobility.name
            , description: mobility.description
          }}
        />
      )
    }

    return (
      <Fragment>
        <ConfirmModal
          open={openModal}
          onClose={handleCloseModal}
          onDelete={handleDelete}
        />

        <MobilityList 
          mobilities={data} 
          onClickDelete={handleOpenModal}
          onClickEdit={handleOpenEditForm} 
        />
      </Fragment>
    )
  }
}

Mobility = AdminBuilderHOC(Mobility, {
  title: 'Mobility'
})

export { Mobility }
