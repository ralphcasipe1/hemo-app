import {
  CREATE_MOBILITY,
  CREATE_MOBILITY_FAILURE,
  CREATE_MOBILITY_SUCCESS,
  DELETE_MOBILITY,
  DELETE_MOBILITY_FAILURE,
  DELETE_MOBILITY_SUCCESS,
  FETCH_ALL_MOBILITIES,
  FETCH_ALL_MOBILITIES_FAILURE,
  FETCH_ALL_MOBILITIES_SUCCESS,
  FETCH_MOBILITY,
  FETCH_MOBILITY_FAILURE,
  FETCH_MOBILITY_SUCCESS,
  CLEAR_FETCHED_MOBILITY,
  UPDATE_MOBILITY,
  UPDATE_MOBILITY_FAILURE,
  UPDATE_MOBILITY_SUCCESS
} from '../constants/mobility'

const initialState = {
  error: null
  , isLoading: false
  , mobilities: []
  , mobility: null
}

export const mobilityReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_MOBILITY:
    case DELETE_MOBILITY:
    case FETCH_ALL_MOBILITIES:
    case FETCH_MOBILITY:
    case UPDATE_MOBILITY:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_MOBILITY_FAILURE:
    case DELETE_MOBILITY_FAILURE:
    case FETCH_ALL_MOBILITIES_FAILURE:
    case FETCH_MOBILITY_FAILURE:
    case UPDATE_MOBILITY_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      }

    case FETCH_ALL_MOBILITIES_SUCCESS:
      return {
        ...state
        , mobilities: action.payload
        , isLoading: false
      }

    case FETCH_MOBILITY_SUCCESS:
      return {
        ...state
        , isLoading: false
        , mobility: action.payload
      }
      
    case CLEAR_FETCHED_MOBILITY:
      return {
        ...state
        , isLoading: false
        , mobility: null
      }
      
    case CREATE_MOBILITY_SUCCESS:
      return {
        ...state
        , mobilities: [ action.payload, ...state.mobilities ]
        , isLoading: false
      }

    case UPDATE_MOBILITY_SUCCESS:
      return {
        ...state
        , mobilities: state.mobilities.map(mobility => {
          if (mobility.mobility_id !== action.payload.mobility_id) {
            return mobility
          }
          return Object.assign({}, mobility, {
            name: action.payload.name
            , description: action.payload.description
          })
        })
        , isLoading: false
      }

    case DELETE_MOBILITY_SUCCESS:
      return {
        ...state
        , mobilities: state.mobilities.filter(
          mobility => mobility.mobility_id !== action.payload.mobility_id
        )
        , isLoading: false
      }

    default:
      return state
  }
}
