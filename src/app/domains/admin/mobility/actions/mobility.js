import {
  CREATE_MOBILITY,
  CREATE_MOBILITY_FAILURE,
  CREATE_MOBILITY_SUCCESS,
  DELETE_MOBILITY,
  DELETE_MOBILITY_FAILURE,
  DELETE_MOBILITY_SUCCESS,
  FETCH_ALL_MOBILITIES,
  FETCH_ALL_MOBILITIES_FAILURE,
  FETCH_ALL_MOBILITIES_SUCCESS,
  FETCH_MOBILITY,
  FETCH_MOBILITY_FAILURE,
  FETCH_MOBILITY_SUCCESS,
  CLEAR_FETCHED_MOBILITY,
  UPDATE_MOBILITY,
  UPDATE_MOBILITY_FAILURE,
  UPDATE_MOBILITY_SUCCESS
} from '../constants/mobility'
import { instance } from 'app/config/connection'

export const fetchAllMobilities = () => dispatch => {
  dispatch({
    type: FETCH_ALL_MOBILITIES
  })

  instance.get(`/mobilities`).then(
    response =>
      dispatch({
        type: FETCH_ALL_MOBILITIES_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_ALL_MOBILITIES_FAILURE
        , error: error.response
      })
  )
}

export const fetchMobility = mobilityId => dispatch => {
  dispatch({
    type: FETCH_MOBILITY
  })

  instance.get(`/mobilities/${mobilityId}`).then(
    response =>
      dispatch({
        type: FETCH_MOBILITY_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_MOBILITY_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedMobility = () => ({ type: CLEAR_FETCHED_MOBILITY })

export const createMobility = values => dispatch => {
  dispatch({
    type: CREATE_MOBILITY
  })

  instance.post(`/mobilities`, values).then(
    response =>
      dispatch({
        type: CREATE_MOBILITY_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: CREATE_MOBILITY_FAILURE
        , error: error.response
      })
  )
}

export const updateMobility = (mobilityId, values) => dispatch => {
  dispatch({
    type: UPDATE_MOBILITY
  })
  instance.patch(`/mobilities/${mobilityId}`, values).then(
    response =>
      dispatch({
        type: UPDATE_MOBILITY_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: UPDATE_MOBILITY_FAILURE
        , error: error.response
      })
  )
}

export const deleteMobility = mobilityId => dispatch => {
  dispatch({
    type: DELETE_MOBILITY
  })
  instance.delete(`mobilities/${mobilityId}`).then(
    response =>
      dispatch({
        type: DELETE_MOBILITY_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: DELETE_MOBILITY_FAILURE
        , payload: error.response
      })
  )
}
