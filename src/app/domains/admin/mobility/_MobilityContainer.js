import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import {
  deleteMobility,
  editMobility,
  createMobility,
  fetchMobilities,
  resetDeletedMobility,
  resetEditMobility,
  resetMobilities,
  resetNewMobility,
} from '../actions/mobilities';
import MobilityForm from 'MobilityForm';
import MobilitiesList from 'MobilitiesList';

class MobilityContainer extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchMobilities();
  }

  componentWillUnmount() {
    this.props.deleteMobility();
    this.props.resetEditMobility();
    this.props.resetMobilities();
    this.props.resetNewMobility();
  }

  componentWillReceiveProps(nextProps) {
    if (
      !this.props.creation.successMessage &&
      nextProps.creation.successMessage
    ) {
      setTimeout(() => this.props.resetNewMobility(), 3000);
    }

    if (
      !this.props.deletion.successMessage &&
      nextProps.deletion.successMessage
    ) {
      setTimeout(() => this.props.resetDeletedMobility(), 3000);
    }

    if (
      !this.props.edition.successMessage &&
      nextProps.edition.successMessage
    ) {
      setTimeout(() => this.props.resetEditMobility(), 3000);
    }
  }

  handleCancel() {
    this.props.router.push('/admin');
  }

  handleDeleteClick(id) {
    this.props.deleteMobility(id);
  }

  handleEditClick(id, values) {
    this.props.editMobility(id, values);
  }

  handleSubmit(values) {
    this.props.createMobility(values);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane>
              <MobilityForm
                onFormSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
                form="MobilityForm"
              />
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'List',
        render: () => {
          return (
            <Tab.Pane>
              <MobilitiesList
                error={this.props.mobilitiesList.error}
                isLoading={this.props.mobilitiesList.isLoading}
                mobilities={this.props.mobilitiesList.mobilities}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
              />
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div>
        <Header content="Mobilities" />

        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  deletion: state.mobilities.deletion,
  edition: state.mobilities.edition,
  mobilitiesList: state.mobilities.mobilitiesList,
  creation: state.mobilities.creation,
});

export default connect(
  mapStateToProps,
  {
    deleteMobility,
    editMobility,
    createMobility,
    fetchMobilities,
    resetDeletedMobility,
    resetEditMobility,
    resetMobilities,
    resetNewMobility,
  }
)(MobilityContainer);
