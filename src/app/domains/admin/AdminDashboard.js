import React, { Component }         from 'react'
import type { Node }                from 'react'
import { Route, Switch }            from 'react-router-dom'
import { Segment }                  from 'semantic-ui-react'
import { AdminMenu }                from './AdminMenu'
import type { ItemType, StateType } from './AdminType'
import { AbnormalContainer }        from './abnormal/AbnormalContainer'
import { DialyzerBrandContainer }   from './dialyzer-brand/DialyzerBrandContainer'
import { DialyzerSizeContainer }    from './dialyzer-size/DialyzerSizeContainer'
import { MobilityContainer }        from './mobility/MobilityContainer'
import { ObjectiveContainer }       from './objective/ObjectiveContainer'
import { PhysicianContainer }       from './physician/PhysicianContainer'
import { SubjectiveContainer }      from './subjective/SubjectiveContainer'
import { UserContainer }            from './user/UserContainer'


class AdminDashboard extends Component<{}, StateType> {
  constructor() {
    super()

    this.state = {
      activeItem: ''
    }

    this.handleItemClick = this.handleItemClick.bind(this)
  }

  handleItemClick = (e: SyntheticEvent<HTMLButtonElement>, item: ItemType): StateType => 
    this.setState({ activeItem: item.name });

  render(): Node {
    const { activeItem } = this.state

    return (
      <Segment>
        <AdminMenu activeItem={activeItem} onItemClick={this.handleItemClick} />

        <Switch>
          <Route component={AbnormalContainer} exact path="/admins/abnormals" />
          <Route
            component={DialyzerBrandContainer}
            path="/admins/dialyzer_brands"
          />

          <Route
            component={DialyzerSizeContainer}
            path="/admins/dialyzer_sizes"
          />

          <Route component={MobilityContainer} path="/admins/mobilities" />

          <Route component={ObjectiveContainer} path="/admins/objectives" />

          <Route component={PhysicianContainer} path="/admins/physicians" />
          
          <Route component={SubjectiveContainer} path="/admins/subjectives" />

          <Route component={UserContainer} path="/admins/users" />
          
          <Route component={DialyzerBrandContainer} />
        </Switch>
      </Segment>
    )
  }
}

export { AdminDashboard }
