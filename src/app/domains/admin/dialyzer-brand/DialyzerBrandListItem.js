import React              from 'react'
import { Dropdown, List } from 'semantic-ui-react'

export const DialyzerBrandListItem = ({ dialyzerBrand, onClickEdit, onClickDelete }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item
            content="Edit"
            key="edit"
            onClick={() => onClickEdit(dialyzerBrand.dialyzer_brand_id)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(dialyzerBrand.dialyzer_brand_id)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={dialyzerBrand.brand_name} />
      <List.Description content={dialyzerBrand.description} />
    </List.Content>
  </List.Item>
)
