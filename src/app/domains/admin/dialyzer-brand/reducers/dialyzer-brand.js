import {
  CREATE_DIALYZER_BRAND,
  CREATE_DIALYZER_BRAND_FAILURE,
  CREATE_DIALYZER_BRAND_SUCCESS,
  DELETE_DIALYZER_BRAND,
  DELETE_DIALYZER_BRAND_FAILURE,
  DELETE_DIALYZER_BRAND_SUCCESS,
  FETCH_ALL_DIALYZER_BRANDS,
  FETCH_ALL_DIALYZER_BRANDS_FAILURE,
  FETCH_ALL_DIALYZER_BRANDS_SUCCESS,
  FETCH_DIALYZER_BRAND,
  FETCH_DIALYZER_BRAND_FAILURE,
  FETCH_DIALYZER_BRAND_SUCCESS,
  CLEAR_FETCHED_DIALYZER_BRAND,
  UPDATE_DIALYZER_BRAND,
  UPDATE_DIALYZER_BRAND_FAILURE,
  UPDATE_DIALYZER_BRAND_SUCCESS
} from '../constants'

const initialState = {
  dialyzerBrand: null
  , dialyzerBrands: []
  , error: null
  , isLoading: false
}

export const dialyzerBrandReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_DIALYZER_BRAND:
    case DELETE_DIALYZER_BRAND:
    case FETCH_ALL_DIALYZER_BRANDS:
    case FETCH_DIALYZER_BRAND:
    case UPDATE_DIALYZER_BRAND:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_DIALYZER_BRAND_FAILURE:
    case DELETE_DIALYZER_BRAND_FAILURE:
    case FETCH_ALL_DIALYZER_BRANDS_FAILURE:
    case FETCH_DIALYZER_BRAND_FAILURE:
    case UPDATE_DIALYZER_BRAND_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      }

    case CREATE_DIALYZER_BRAND_SUCCESS:
      return {
        ...state
        , dialyzerBrands: [ action.payload, ...state.dialyzerBrands ]
        , isLoading: false
      }

    case FETCH_ALL_DIALYZER_BRANDS_SUCCESS:
      return {
        ...state
        , dialyzerBrands: action.payload
        , isLoading: false
      }

    case FETCH_DIALYZER_BRAND_SUCCESS:
      return {
        ...state
        , dialyzerBrand: action.payload
        , isLoading: false
      }

    case CLEAR_FETCHED_DIALYZER_BRAND:
      return {
        ...state
        , dialyzerBrand: null
        , isLoading: false
      }
      
    case UPDATE_DIALYZER_BRAND_SUCCESS:
      return {
        ...state
        , dialyzerBrands: state.dialyzerBrands.map(brand => {
          if (brand.dialyzer_brand_id !== action.payload.dialyzer_brand_id) {
            return brand
          }
          return Object.assign({}, brand, {
            brand_name: action.payload.brand_name
            , description: action.payload.description
          })
        })
        , error: null
        , isLoading: false
      }
    case DELETE_DIALYZER_BRAND_SUCCESS:
      return {
        ...state
        , dialyzerBrands: state.dialyzerBrands.filter(
          dialyzerBrand =>
            dialyzerBrand.dialyzer_brand_id !== action.payload.dialyzer_brand_id
        )
        , isLoading: false
      }

    default:
      return state
  }
}
