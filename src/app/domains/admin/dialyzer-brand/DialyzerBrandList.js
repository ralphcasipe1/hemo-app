import React                     from 'react'
import { List }                  from 'semantic-ui-react'
import { DialyzerBrandListItem } from './DialyzerBrandListItem'

export const DialyzerBrandList = props => (
  <List divided relaxed="very" size="big">
    {props.dialyzerBrands.map(dialyzerBrand => (
      <DialyzerBrandListItem
        key={dialyzerBrand.dialyzer_brand_id}
        dialyzerBrand={dialyzerBrand}
        {...props}
      />
    ))}
  </List>
)
