import React, { PureComponent } from 'react'
import { Field, reduxForm }     from 'redux-form'
import { Form }                 from 'semantic-ui-react'
import { ButtonDecision }       from 'app/common/forms/ButtonDecision'
import { InputField }           from 'app/common/forms/InputField'

class DialyzerBrandForm extends PureComponent {
  render() {
    const {
      handleSubmit,
      onFormSubmit,
      valid,
      submitting,
      onCancel,
      disableCancel
    } = this.props

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field component={InputField} name="brand_name" label="Brand Name" />
        <Field component={InputField} name="description" label="Description" />
        <ButtonDecision
          isDisable={!valid || submitting}
          {...this.props}
          expanded
          onCancel={onCancel}
          disableCancel={disableCancel}
        />
      </Form>
    )
  }
}

const validate = values => {
  let errors = {}

  if (!values.brand_name) {
    errors.brand_name = 'Required'
  } else if (values.brand_name.length < 4) {
    errors.brand_name = 'Min length is 4'
  }

  return errors
}

DialyzerBrandForm = reduxForm({
  fields: [ 'brand_name', 'description' ]
  , validate
})(DialyzerBrandForm)

export { DialyzerBrandForm }
