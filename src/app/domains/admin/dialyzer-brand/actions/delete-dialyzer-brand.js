import {
  DELETE_DIALYZER_BRAND,
  DELETE_DIALYZER_BRAND_FAILURE,
  DELETE_DIALYZER_BRAND_SUCCESS,
} from '../constants'
import { instance } from 'app/config/connection'

export const deleteDialyzerBrand = dialyzerBrandId => dispatch => {
  dispatch({
    type: DELETE_DIALYZER_BRAND
  })
  instance.delete(`/dialyzer_brands/${dialyzerBrandId}`).then(
    response =>
      dispatch({
        type: DELETE_DIALYZER_BRAND_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: DELETE_DIALYZER_BRAND_FAILURE
        , error: error.response
      })
  )
}