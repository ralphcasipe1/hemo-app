import {
  FETCH_ALL_DIALYZER_BRANDS,
  FETCH_ALL_DIALYZER_BRANDS_FAILURE,
  FETCH_ALL_DIALYZER_BRANDS_SUCCESS,
} from '../constants'
import { instance } from 'app/config/connection'

export const fetchAllDialyzerBrands = () => dispatch => {
  dispatch({
    type: FETCH_ALL_DIALYZER_BRANDS
  })

  instance.get(`/dialyzer_brands`).then(
    response =>
      dispatch({
        type: FETCH_ALL_DIALYZER_BRANDS_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_DIALYZER_BRANDS_FAILURE
        , error: error.response
      })
  )
}