import {  
  UPDATE_DIALYZER_BRAND,
  UPDATE_DIALYZER_BRAND_FAILURE,
  UPDATE_DIALYZER_BRAND_SUCCESS
} from '../constants'
import { instance } from 'app/config/connection'

export const updateDialyzerBrand = (dialyzerBrandId, values) => dispatch => {
  dispatch({
    type: UPDATE_DIALYZER_BRAND
  })
  instance.patch(`/dialyzer_brands/${dialyzerBrandId}`, values).then(
    response =>
      dispatch({
        type: UPDATE_DIALYZER_BRAND_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: UPDATE_DIALYZER_BRAND_FAILURE
        , error: error.response
      })
  )
}