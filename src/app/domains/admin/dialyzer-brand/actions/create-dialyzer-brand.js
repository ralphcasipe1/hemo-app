import {
  CREATE_DIALYZER_BRAND,
  CREATE_DIALYZER_BRAND_FAILURE,
  CREATE_DIALYZER_BRAND_SUCCESS,
} from '../constants'
import { instance } from 'app/config/connection'

export const createDialyzerBrand = values => dispatch => {
  dispatch({
    type: CREATE_DIALYZER_BRAND
  })
  instance.post(`/dialyzer_brands`, values).then(
    response =>
      dispatch({
        type: CREATE_DIALYZER_BRAND_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: CREATE_DIALYZER_BRAND_FAILURE
        , error: error.response
      })
  )
}