import {
  FETCH_DIALYZER_BRAND,
  FETCH_DIALYZER_BRAND_FAILURE,
  FETCH_DIALYZER_BRAND_SUCCESS,
  CLEAR_FETCHED_DIALYZER_BRAND
} from '../constants'
import { instance } from 'app/config/connection'

export const fetchDialyzerBrand = dialyzerBrandId => dispatch => {
  dispatch({
    type: FETCH_DIALYZER_BRAND
  })

  instance.get(`/dialyzer_brands/${dialyzerBrandId}`).then(
    response =>
      dispatch({
        type: FETCH_DIALYZER_BRAND_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_DIALYZER_BRAND_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedDialyzerBrand = () => ({ type: CLEAR_FETCHED_DIALYZER_BRAND })