import { connect } from 'react-redux'
import {
  createDialyzerBrand,
  deleteDialyzerBrand,
  fetchAllDialyzerBrands,
  fetchDialyzerBrand,
  clearFetchedDialyzerBrand,
  updateDialyzerBrand
} from './actions'
import { DialyzerBrand } from './DialyzerBrand'

const mapStateToProps = state => ({
  data: state.dialyzerBrandReducer.dialyzerBrands
  , loading: state.dialyzerBrandReducer.loading
  , dialyzerBrand: state.dialyzerBrandReducer.dialyzerBrand
})

export const DialyzerBrandContainer = connect(
  mapStateToProps,
  {
    create: createDialyzerBrand
    , deleteData: deleteDialyzerBrand
    , fetchAllData: fetchAllDialyzerBrands
    , fetchData: fetchDialyzerBrand
    , clearData: clearFetchedDialyzerBrand
    , updateData: updateDialyzerBrand
  }
)(DialyzerBrand)
