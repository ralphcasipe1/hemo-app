import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import {
  deleteDialyzerBrand,
  editDialyzerBrand,
  createDialyzerBrand,
  fetchDialyzerBrands,
  resetDeletedDialyzerBrand,
  resetDialyzerBrands,
  resetEditDialyzerBrand,
  resetNewDialyzerBrand,
} from '../actions/dialyzerBrands';
import DialyzerBrandForm from 'DialyzerBrandForm';
import DialyzerBrandsList from 'DialyzerBrandsList';

class DialyzerBrandContainer extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchDialyzerBrands();
  }

  componentWillUnmount() {
    this.props.resetDeletedDialyzerBrand();
    this.props.resetDialyzerBrands();
    this.props.resetEditDialyzerBrand();
    this.props.resetNewDialyzerBrand();
  }

  handleCancel() {
    this.props.router.push('/admin');
  }

  handleDeleteClick(id) {
    this.props.deleteDialyzerBrand(id);
  }

  handleEditClick(id, values) {
    this.props.editDialyzerBrand(id, values);
  }

  handleSubmit(values) {
    this.props.createDialyzerBrand(values);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane>
              <DialyzerBrandForm
                expanded
                form="DialyzerBrandForm"
                onCancel={this.handleCancel}
                onFormSubmit={this.handleSubmit}
              />
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'List',
        render: () => {
          return (
            <Tab.Pane>
              <DialyzerBrandsList
                dialyzerBrands={this.props.dialyzerBrandsList.dialyzerBrands}
                error={this.props.dialyzerBrandsList.error}
                isLoading={this.props.dialyzerBrandsList.isLoading}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
                simple
              />
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div>
        <Header content="Dialyzer's Brands" />

        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dialyzerBrandsList: state.dialyzerBrands.dialyzerBrandsList,
});

export default connect(
  mapStateToProps,
  {
    deleteDialyzerBrand,
    editDialyzerBrand,
    createDialyzerBrand,
    fetchDialyzerBrands,
    resetDeletedDialyzerBrand,
    resetDialyzerBrands,
    resetEditDialyzerBrand,
    resetNewDialyzerBrand,
  }
)(DialyzerBrandContainer);
