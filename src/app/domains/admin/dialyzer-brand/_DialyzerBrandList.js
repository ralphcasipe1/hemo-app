import React, { PureComponent } from 'react';
import { Button, Icon, List, Loader } from 'semantic-ui-react';
import _ from 'lodash';
import DialyzerBrandForm from 'DialyzerBrandForm';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';
import PropTypes from 'prop-types';

class DialyzerBrandList extends PureComponent {
  static propTypes = {
    dialyzerBrands: PropTypes.arrayOf(
      PropTypes.shape({
        dialyzer_brand_id: PropTypes.number.isRequired,
        brand_name: PropTypes.string.isRequired,
        description: PropTypes.string,
      })
    ),
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    onDeleteClick: PropTypes.func.isRequired,
    onEditClick: PropTypes.func.isRequired,
  };

  constructor() {
    super();

    this.state = {
      dialyzerBrandId: [],
      deleteDialyzerBrand: [],
    };
  }

  renderListItem(dialyzerBrand) {
    if (
      this.state.dialyzerBrandId.indexOf(dialyzerBrand.dialyzer_brand_id) !== -1
    ) {
      return (
        <List.Item key={dialyzerBrand.dialyzer_brand_id}>
          <List.Content>
            <DialyzerBrandForm
              form={`EditDialyzerBrandForm_${dialyzerBrand.dialyzer_brand_id}`}
              onCancel={() =>
                this.setState({
                  dialyzerBrandId: this.state.dialyzerBrandId.filter(
                    id => id !== dialyzerBrand.dialyzer_brand_id
                  ),
                })
              }
              onFormSubmit={values => {
                this.props.onEditClick(dialyzerBrand.dialyzer_brand_id, values);
                this.setState({
                  dialyzerBrandId: this.state.dialyzerBrandId.filter(
                    id => id !== dialyzerBrand.dialyzer_brand_id
                  ),
                });
              }}
              initialValues={{
                brand_name: dialyzerBrand.brand_name,
                description: dialyzerBrand.description,
              }}
              simple
            />
          </List.Content>
        </List.Item>
      );
    } else if (
      this.state.deleteDialyzerBrand.indexOf(
        dialyzerBrand.dialyzer_brand_id
      ) !== -1
    ) {
      return (
        <List.Item key={dialyzerBrand.dialyzer_brand_id}>
          <List.Content floated="right" verticalAlign="middle">
            <Button
              content="Yes"
              color="blue"
              floated="right"
              color="red"
              icon="checkmark"
              onClick={() =>
                this.props.onDeleteClick(dialyzerBrand.dialyzer_brand_id)
              }
            />
            <Button
              content="No"
              color="blue"
              floated="right"
              basic
              icon="cancel"
              onClick={() =>
                this.setState({
                  deleteDialyzerBrand: this.state.deleteDialyzerBrand.filter(
                    id => id !== dialyzerBrand.dialyzer_brand_id
                  ),
                })
              }
            />
          </List.Content>
          <Icon name="warning sign" color="yellow" />
          <List.Content>
            <List.Header>
              You want to delete {dialyzerBrand.brand_name}?
            </List.Header>
            <List.Description>{dialyzerBrand.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    } else {
      return (
        <List.Item key={dialyzerBrand.dialyzer_brand_id}>
          <List.Content floated="right">
            <Icon
              color="red"
              link
              name="trash"
              onClick={() => {
                this.setState({
                  deleteDialyzerBrand: [
                    ...this.state.deleteDialyzerBrand,
                    dialyzerBrand.dialyzer_brand_id,
                  ],
                });
              }}
            />
            <Icon
              color="green"
              link
              name="edit"
              onClick={() =>
                this.setState({
                  dialyzerBrandId: [
                    ...this.state.dialyzerBrandId,
                    dialyzerBrand.dialyzer_brand_id,
                  ],
                })
              }
            />
          </List.Content>
          <List.Content>
            <List.Header>{dialyzerBrand.brand_name}</List.Header>
            <List.Description>{dialyzerBrand.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    }
  }

  render() {
    if (this.props.isLoading) {
      return <Loader active inline="centered" />;
    } else if (this.props.error) {
      return <ExceptionMessage exception={this.props.error} />;
    } else if (_.isEmpty(this.props.dialyzerBrands)) {
      return <RecommendMessage header="No Dialyzer's Brands" />;
    }

    return (
      <List divided relaxed="very">
        {this.props.dialyzerBrands.map(dialyzerBrand =>
          this.renderListItem(dialyzerBrand)
        )}
      </List>
    );
  }
}

export { DialyzerBrandList };
