import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { ConfirmModal }               from '../common/components/display/ConfirmModal'
import { DialyzerBrandForm }          from './DialyzerBrandForm'
import { DialyzerBrandList }          from './DialyzerBrandList'

class DialyzerBrand extends Component {
  render() {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      dialyzerBrand
    } = this.props
    
    if (data.length === 0 || openCreateForm) {
      return (
        <DialyzerBrandForm
          form="CreateDialyzerBrandForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    if (openEditForm && dialyzerBrand) {
      return (
        <DialyzerBrandForm
          form="EditDialyzerBrandForm"
          onFormSubmit={handleUpdate}
          onCancel={handleCloseEditForm}
          initialValues={{
            brand_name: dialyzerBrand.brand_name
            , description: dialyzerBrand.description
          }}
        />
      )
    }
    
    return (
      <Fragment>
        <ConfirmModal
          open={openModal}
          onClose={handleCloseModal}
          onDelete={handleDelete}
        />

        <DialyzerBrandList
          dialyzerBrands={data}
          onClickDelete={handleOpenModal}
          onClickEdit={handleOpenEditForm}
        />
      </Fragment>
    )
  }
}

DialyzerBrand = AdminBuilderHOC(DialyzerBrand, {
  title: 'Dialyzer Brand'
})

export { DialyzerBrand }
