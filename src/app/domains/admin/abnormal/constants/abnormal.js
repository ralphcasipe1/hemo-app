export const CREATE_ABNORMAL = 'CREATE_ABNORMAL'
export type CreateAbnormalType = typeof CREATE_ABNORMAL;

export const CREATE_ABNORMAL_SUCCESS = 'CREATE_ABNORMAL_SUCCESS'
export type CreateAbnormalSuccessType = typeof CREATE_ABNORMAL_SUCCESS;

export const CREATE_ABNORMAL_FAILURE = 'CREATE_ABNORMAL_FAILURE'
export type CreateAbnormalFailureType = typeof CREATE_ABNORMAL_FAILURE;

export const FETCH_ALL_ABNORMALS = 'FETCH_ALL_ABNORMALS'
export type FetchAllAbnormalsType = typeof FETCH_ALL_ABNORMALS;

export const FETCH_ALL_ABNORMALS_SUCCESS = 'FETCH_ALL_ABNORMALS_SUCCESS'
export type FetchAllAbnormalsSuccessType = typeof FETCH_ALL_ABNORMALS_SUCCESS;

export const FETCH_ALL_ABNORMALS_FAILURE = 'FETCH_ALL_ABNORMALS_FAILURE'
export type FetchAbnormalsFailureType = typeof FETCH_ALL_ABNORMALS_FAILURE;

export const FETCH_ABNORMAL = 'FETCH_ABNORMAL'
export type FetchAbnormalType = typeof FETCH_ABNORMAL;

export const FETCH_ABNORMAL_SUCCESS = 'FETCH_ABNORMAL_SUCCESS'
export type FetchAbnormalSuccessType = typeof FETCH_ABNORMAL_SUCCESS;

export const FETCH_ABNORMAL_FAILURE = 'FETCH_ABNORMAL_FAILURE'
export type FetchAbnormalFailureType = typeof FETCH_ABNORMAL_FAILURE;

export const CLEAR_FETCHED_ABNORMAL = 'CLEAR_FETCHED_ABNORMAL'
export type ClearFetchAbnormalType = typeof CLEAR_FETCHED_ABNORMAL;

export const UPDATE_ABNORMAL = 'UPDATE_ABNORMAL'
export type UpdateAbnormalType = typeof UPDATE_ABNORMAL;

export const UPDATE_ABNORMAL_SUCCESS = 'UPDATE_ABNORMAL_SUCCESS'
export type UpdateAbnormalSuccessType = typeof UPDATE_ABNORMAL_SUCCESS;

export const UPDATE_ABNORMAL_FAILURE = 'UPDATE_ABNORMAL_FAILURE'
export type UpdateAbnormalFailureType = typeof UPDATE_ABNORMAL_FAILURE;


export const DELETE_ABNORMAL = 'DELETE_ABNORMAL'
export type DeleteAbnormalType = typeof DELETE_ABNORMAL;

export const DELETE_ABNORMAL_SUCCESS = 'DELETE_ABNORMAL_SUCCESS'
export type DeleteAbnormalSuccessType = typeof DELETE_ABNORMAL_SUCCESS;

export const DELETE_ABNORMAL_FAILURE = 'DELETE_ABNORMAL_FAILURE'
export type DeleteAbnormalFailureType = typeof DELETE_ABNORMAL_FAILURE;
