import React              from 'react'
import { Dropdown, List } from 'semantic-ui-react'

export const AbnormalListItem = ({ abnormal, onClickEdit, onClickDelete }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item
            content="Edit"
            key="edit"
            onClick={() => onClickEdit(abnormal.abnormal_id)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(abnormal.abnormal_id)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={abnormal.name} />
      <List.Description content={abnormal.description} />
    </List.Content>
  </List.Item>
)
