import type { AxiosError, AxiosPromise, AxiosXHR } from 'axios'
import {
  CREATE_ABNORMAL,
  CREATE_ABNORMAL_FAILURE,
  CREATE_ABNORMAL_SUCCESS,
  DELETE_ABNORMAL,
  DELETE_ABNORMAL_FAILURE,
  DELETE_ABNORMAL_SUCCESS,
  FETCH_ALL_ABNORMALS,
  FETCH_ALL_ABNORMALS_FAILURE,
  FETCH_ALL_ABNORMALS_SUCCESS,
  FETCH_ABNORMAL,
  FETCH_ABNORMAL_FAILURE,
  FETCH_ABNORMAL_SUCCESS,
  CLEAR_FETCHED_ABNORMAL,
  UPDATE_ABNORMAL,
  UPDATE_ABNORMAL_FAILURE,
  UPDATE_ABNORMAL_SUCCESS
} from '../constants/abnormal'
import type {
  ClearFetchAbnormalType,
  CreateAbnormalType,
  CreateAbnormalSuccessType,
  CreateAbnormalFailureType,
  DeleteAbnormalType,
  DeleteAbnormalSuccessType,
  DeleteAbnormalFailureType,
  FetchAllAbnormalsType,
  FetchAllAbnormalsSuccessType,
  FetchAllAbnormalsFailureType,
  FetchAbnormalType,
  FetchAbnormalSuccessType,
  FetchAbnormalFailureType
} from '../constants/abnormal'
import { instance } from 'app/config/connection'

export type CreateAbnormalActionsType = {
  +type: CreateAbnormalType
    | CreateAbnormalSuccessType
    | CreateAbnormalFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type DeleteAbnormalActionsType = {
  +type: DeleteAbnormalType
    | DeleteAbnormalSuccessType
    | DeleteAbnormalFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type FetchAllAbnormalsActionsType = {
  +type: FetchAllAbnormalsType
    | FetchAllAbnormalsSuccessType
    | FetchAllAbnormalsFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type FetchAbnormalActionsType = {
  +type: FetchAbnormalType
    | FetchAbnormalSuccessType
    | FetchAbnormalFailureType
    | ClearFetchAbnormalType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type AbnormalActionsType
  = CreateAbnormalActionsType
  | DeleteAbnormalActionsType
  | FetchAllAbnormalsActionsType
  | FetchAbnormalActionType;

type GetStateType = () => State;
type PromiseActionType = AxiosPromise<AbnormalActionsType>;
type ThunkActionType = (dispatch: DispatchType, getState: GetStateType) => any;
type DispatchType = (action: AbnormalActionsType | ThunkActionType | PromiseActionType) => any;

interface IAbnormalValues {
  name: string,
  description?: string
}

export const createAbnormal = (values: IAbnormalValues): ThunkActionType => (dispatch: DispatchType) => {
  dispatch({
    type: CREATE_ABNORMAL
  })

  instance.post('/abnormals', values).then(
    (response: AxiosXHR): DispatchType =>
      dispatch({
        type: CREATE_ABNORMAL_SUCCESS
        , payload: response.data.data
      }),
    (error: AxiosError): DispatchType =>
      dispatch({
        type: CREATE_ABNORMAL_FAILURE
        , error: error.response
      })
  )
}

export const fetchAllAbnormals = (): ThunkActionType => (dispatch: DispatchType) => {
  dispatch({
    type: FETCH_ALL_ABNORMALS
  })

  instance.get('/abnormals').then(
    (response: AxiosXHR): DispatchType =>
      dispatch({
        type: FETCH_ALL_ABNORMALS_SUCCESS
        , payload: response.data.data
      }),
    (error: AxiosError): DispatchType =>
      dispatch({
        type: FETCH_ALL_ABNORMALS_FAILURE
        , payload: error.response
      })
  )
}

export const fetchAbnormal = (abnormalId: number): ThunkActionType => (dispatch: DispatchType) =>  {
  dispatch({
    type: FETCH_ABNORMAL
  })

  instance.get(`/abnormals/${abnormalId}`).then(
    (response: AxiosXHR): DispatchType =>
      dispatch({
        type: FETCH_ABNORMAL_SUCCESS
        , payload: response.data.data
      }),
    (error: AxiosError): DispatchType =>
      dispatch({
        type: FETCH_ABNORMAL_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedAbnormal = (): FetchAbnormalActionsType => ({ type: CLEAR_FETCHED_ABNORMAL })

export const updateAbnormal = (abnormalId: number, values: IAbnormalValues): ThunkActionType => (dispatch: DispatchType) => {
  dispatch({
    type: UPDATE_ABNORMAL
  })

  instance.patch(`/abnormals/${abnormalId}`, values).then(
    (response: AxiosXHR): DispatchType =>
      dispatch({
        type: UPDATE_ABNORMAL_SUCCESS
        , payload: response.data.data
      }),
    (error: AxiosError): DispatchType =>
      dispatch({
        type: UPDATE_ABNORMAL_FAILURE
        , error: error.response
      })
  )
}

export const deleteAbnormal = (abnormalId: number): ThunkActionType => (dispatch: DispatchType) => {
  dispatch({
    type: DELETE_ABNORMAL
  })

  instance.delete(`/abnormals/${abnormalId}`).then(
    (response: AxiosXHR): DispatchType =>
      dispatch({
        type: DELETE_ABNORMAL_SUCCESS
        , payload: response.data.data
      }),
    (error: AxiosError): DispatchType =>
      dispatch({
        type: DELETE_ABNORMAL_FAILURE
        , payload: error.response
      })
  )
}
