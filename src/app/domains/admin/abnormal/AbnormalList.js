import React from 'react';
import { List } from 'semantic-ui-react';
import { AbnormalListItem } from './AbnormalListItem';

export const AbnormalList = props => (
  <List divided relaxed="very" size="big">
    {props.abnormals.map(abnormal => (
      <AbnormalListItem
        key={abnormal.abnormal_id}
        abnormal={abnormal}
        {...props}
      />
    ))}
  </List>
);
