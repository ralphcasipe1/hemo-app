import type {
	CreateAbnormalType,
	CreateAbnormalFailureType,
	CreateAbnormalSuccessType,
	DeleteAbnormalType,
	DeleteAbnormalFailureType,
	DeleteAbnormalSuccessType,
	FetchAllAbnormalsType,
	FetchAllAbnormalsFailureType,
	FetchAllAbnormalsSuccessType,
	FetchAbnormalType,
	FetchAbnormalFailureType,
	FetchAbnormalSuccessType,
	UpdateAbnormalType,
	UpdateAbnormalFailureType,
	UpdateAbnormalSuccessType
} from './constant'

export type AbnormalType = {
	+abnormal_id: number,
	+name: string,
	+description: string,
	+created_by: number,
	+updated_by: number,
	+deleted_by: number,
	+created_at: string,
	+updated_at: string,
	+deleted_at: string
};

export type CreateAbnormalActionsType = {
  +type: CreateAbnormalType
    | CreateAbnormalSuccessType
    | CreateAbnormalFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type DeleteAbnormalActionsType = {
  +type: DeleteAbnormalType
    | DeleteAbnormalSuccessType
    | DeleteAbnormalFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type FetchAllAbnormalsActionsType = {
  +type: FetchAllAbnormalsType
    | FetchAllAbnormalsSuccessType
    | FetchAllAbnormalsFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type FetchAbnormalActionsType = {
  +type: FetchAbnormalType
    | FetchAbnormalSuccessType
    | FetchAbnormalFailureType
    | ClearFetchAbnormalType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type UpdateAbnormalActionsType = {
  +type: UpdateAbnormalType
    | UpdateAbnormalSuccessType
    | UpdateAbnormalFailureType,
  +payload?: AxiosXHR,
  +error ?: AxiosError
};

export type AbnormalActionsType
  = CreateAbnormalActionsType
  | DeleteAbnormalActionsType
  | FetchAllAbnormalsActionsType
  | FetchAbnormalActionType;