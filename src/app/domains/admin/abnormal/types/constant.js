import {
  CLEAR_FETCHED_ABNORMAL,
	CREATE_ABNORMAL,
  CREATE_ABNORMAL_FAILURE,
  CREATE_ABNORMAL_SUCCESS,
  DELETE_ABNORMAL,
  DELETE_ABNORMAL_FAILURE,
  DELETE_ABNORMAL_SUCCESS,
  FETCH_ALL_ABNORMALS,
  FETCH_ALL_ABNORMALS_FAILURE,
  FETCH_ALL_ABNORMALS_SUCCESS,
  FETCH_ABNORMAL,
  FETCH_ABNORMAL_FAILURE,
  FETCH_ABNORMAL_SUCCESS,
  UPDATE_ABNORMAL,
  UPDATE_ABNORMAL_FAILURE,
  UPDATE_ABNORMAL_SUCCESS
} from './constants/abnormal'

export type CreateAbnormalType = typeof CREATE_ABNORMAL;
export type CreateAbnormalFailureType = typeof CREATE_ABNORMAL_FAILURE;
export type CreateAbnormalSuccessType = typeof CREATE_ABNORMAL_SUCCESS;

export type DeleteAbnormalType = typeof DELETE_ABNORMAL;
export type DeleteAbnormalFailureType = typeof DELETE_ABNORMAL_FAILURE;
export type DeleteAbnormalSuccessType = typeof DELETE_ABNORMAL_SUCCESS;

export type FetchAbnormalType = typeof FETCH_ABNORMAL;
export type FetchAbnormalFailureType = typeof FETCH_ABNORMAL_FAILURE;
export type FetchAbnormalSuccessType = typeof FETCH_ABNORMAL_SUCCESS;
export type ClearFetchedAbnormalType = typeof CLEAR_FETCHED_ABNORMAL;

export type FetchAllAbnormalsType = typeof FETCH_ALL_ABNORMALS;
export type FetchAllAbnormalsFailureType = typeof FETCH_ALL_ABNORMALS_FAILURE;
export type FetchAllAbnormalsSuccessType = typeof FETCH_ALL_ABNORMALS_SUCCESS;

export type UpdateAbnormalType = typeof UPDATE_ABNORMAL;
export type UpdateAbnormalFailureType = typeof UPDATE_ABNORMAL_FAILURE;
export type UpdateAbnormalSuccessType = typeof UPDATE_ABNORMAL_SUCCESS;