import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { ConfirmModal }               from '../common/components/display/ConfirmModal'
import { AbnormalForm }               from './AbnormalForm'
import { AbnormalList }               from './AbnormalList'

class Abnormal extends Component {
  render() {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      abnormal
    } = this.props
    
    if (data.length === 0 || openCreateForm) {
      return (
        <AbnormalForm
          form="CreateAbnormalForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    if (openEditForm && abnormal) {
      return (
        <AbnormalForm
          form="EditAbnormalForm"
          onFormSubmit={handleUpdate}
          onCancel={handleCloseEditForm}
          initialValues={{
            name: abnormal.name
            , description: abnormal.description
          }}
        />
      )
    }

    return (
      <Fragment>
        <ConfirmModal
          open={openModal}
          onClose={handleCloseModal}
          onDelete={handleDelete}
        />

        <AbnormalList 
          abnormals={data}
          onClickDelete={handleOpenModal}
          onClickEdit={handleOpenEditForm}
        />
      </Fragment>
    )
  }
}

Abnormal = AdminBuilderHOC(Abnormal, {
  title: 'Abnormal'
})

export { Abnormal }
