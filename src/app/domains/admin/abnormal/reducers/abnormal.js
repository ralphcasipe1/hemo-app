import {
  CREATE_ABNORMAL,
  CREATE_ABNORMAL_FAILURE,
  CREATE_ABNORMAL_SUCCESS,
  DELETE_ABNORMAL,
  DELETE_ABNORMAL_FAILURE,
  DELETE_ABNORMAL_SUCCESS,
  FETCH_ALL_ABNORMALS,
  FETCH_ALL_ABNORMALS_FAILURE,
  FETCH_ALL_ABNORMALS_SUCCESS,
  FETCH_ABNORMAL,
  FETCH_ABNORMAL_FAILURE,
  FETCH_ABNORMAL_SUCCESS,
  CLEAR_FETCHED_ABNORMAL,
  UPDATE_ABNORMAL,
  UPDATE_ABNORMAL_FAILURE,
  UPDATE_ABNORMAL_SUCCESS
} from '../constants/abnormal'
import type { 
  AbnormalActionType, 
  AbnormalStateType, 
  AbnormalType 
} from '../types/AbnormalType'

const initialState: AbnormalStateType = {
  abnormals: []
  , abnormal: null
  , isLoading: false
  , error: null
}

export const abnormalReducer = (
  state: AbnormalStateType = initialState, 
  action: AbnormalActionType
): AbnormalStateType => {
  
  switch (action.type) {
    case CREATE_ABNORMAL:
    case FETCH_ALL_ABNORMALS:
    case FETCH_ABNORMAL:
    case UPDATE_ABNORMAL:
    case DELETE_ABNORMAL:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_ABNORMAL_FAILURE:
    case FETCH_ALL_ABNORMALS_FAILURE:
    case FETCH_ABNORMAL_FAILURE:
    case UPDATE_ABNORMAL_FAILURE:
    case DELETE_ABNORMAL_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.payload
      }

    case FETCH_ALL_ABNORMALS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , abnormals: action.payload
      }

    case FETCH_ABNORMAL_SUCCESS:
      return {
        ...state
        , isLoading: false
        , abnormal: action.payload
      }

    case CLEAR_FETCHED_ABNORMAL:
      return {
        ...state
        , isLoading: false
        , abnormal: null
      }
      
    case CREATE_ABNORMAL_SUCCESS:
      return {
        ...state
        , isLoading: false
        , abnormals: [ ...state.abnormals, action.payload ]
      }

    case DELETE_ABNORMAL_SUCCESS:
      return {
        ...state
        , isLoading: false
        , abnormals: state.abnormals.filter(
          (abnormal: AbnormalType): Array<AbnormalType> => 
            abnormal.abnormal_id !== action.payload.abnormal_id
        )
      }

    case UPDATE_ABNORMAL_SUCCESS:
      return {
        ...state
        , isLoading: false
        , abnormals: state.abnormals.map(
          (abnormal: AbnormalType): Array<AbnormalType> => {
            const {
              abnormal_id, 
              name, 
              description 
            } = action.payload
            if (abnormal.abnormal_id !== abnormal_id) {
              return abnormal
            }

            return Object.assign({}, abnormal, {
              name
              , description
            })
          }
        )
      }

    default:
      return state
  }
}
