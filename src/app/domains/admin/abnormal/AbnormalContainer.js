import { connect } from 'react-redux'
import {
  createAbnormal,
  deleteAbnormal,
  fetchAllAbnormals,
  fetchAbnormal,
  clearFetchedAbnormal,
  updateAbnormal
} from './actions/abnormal'
import { Abnormal } from './Abnormal'

const mapStateToProps = state => ({
  data: state.abnormalReducer.abnormals
  , loading: state.abnormalReducer.isLoading
  , abnormal: state.abnormalReducer.abnormal
})

export const AbnormalContainer = connect(
  mapStateToProps,
  {
    create: createAbnormal
    , deleteData: deleteAbnormal
    , fetchAllData: fetchAllAbnormals
    , fetchData: fetchAbnormal
    , clearData: clearFetchedAbnormal
    , updateData: updateAbnormal
  }
)(Abnormal)
