import { connect } from 'react-redux'
import {
  createSubjective,
  deleteSubjective,
  fetchAllSubjectives,
  fetchSubjective,
  clearFetchedSubjective,
  updateSubjective
} from './actions/subjective'
import { Subjective } from './Subjective'

const mapStateToProps = state => ({
  data: state.subjectiveReducer.subjectives
  , loading: state.subjectiveReducer.loading
  , subjective: state.subjectiveReducer.subjective
})

export const SubjectiveContainer = connect(
  mapStateToProps,
  {
    create: createSubjective
    , deleteData: deleteSubjective
    , fetchAllData: fetchAllSubjectives
    , fetchData: fetchSubjective
    , clearData: clearFetchedSubjective
    , updateData: updateSubjective
  }
)(Subjective)
