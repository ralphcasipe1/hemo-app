import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import { ButtonDecision } from '../../../common/forms/ButtonDecision';
import { InputField } from '../../../common/forms/InputField';

class SubjectiveForm extends PureComponent {
  render() {
    const { handleSubmit, onFormSubmit, valid, submitting } = this.props;

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field component={InputField} name="name" label="Subjective Name" />
        <Field component={InputField} name="description" label="Description" />
        <ButtonDecision
          expanded
          isDisable={!valid || submitting}
          {...this.props}
        />
      </Form>
    );
  }
}

const validate = values => {
  let errors = {};

  if (!values.name) {
    errors.name = 'Required';
  } else if (values.name.length < 4) {
    errors.name = 'Min length is 4';
  }

  return errors;
};

SubjectiveForm = reduxForm({
  fields: ['name', 'description'],
  validate,
})(SubjectiveForm);

export { SubjectiveForm };
