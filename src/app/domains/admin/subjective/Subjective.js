import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { ConfirmModal }               from '../common/components/display/ConfirmModal'
import { SubjectiveForm }             from './SubjectiveForm'
import { SubjectiveList }             from './SubjectiveList'

class Subjective extends Component {
  render() {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      subjective
    } = this.props

    if (data.length === 0 || openCreateForm) {
      return (
        <SubjectiveForm
          form="CreateSubjectiveForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    if (openEditForm && subjective) {
      return (
        <SubjectiveForm
          form="EditSubjectiveForm"
          onFormSubmit={handleUpdate}
          onCancel={handleCloseEditForm}
          initialValues={{
            name: subjective.name
            , description: subjective.description
          }}
        />
      )
    }

    return (
      <Fragment>
        <ConfirmModal
          open={openModal}
          onClose={handleCloseModal}
          onDelete={handleDelete}
        />

        <SubjectiveList 
          subjectives={data} 
          onClickDelete={handleOpenModal}
          onClickEdit={handleOpenEditForm}
        />
      </Fragment>
    )
  }
}

Subjective = AdminBuilderHOC(Subjective, {
  title: 'Subjective'
})

export { Subjective }
