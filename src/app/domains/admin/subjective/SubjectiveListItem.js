import React              from 'react'
import { Dropdown, List } from 'semantic-ui-react'

export const SubjectiveListItem = ({ subjective, onClickEdit, onClickDelete }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item 
            content="Edit" 
            key="edit" 
            onClick={() => onClickEdit(subjective.subjective_id)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(subjective.subjective_id)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={subjective.name} />
      <List.Description content={subjective.description} />
    </List.Content>
  </List.Item>
)
