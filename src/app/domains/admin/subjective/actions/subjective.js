import {
  CREATE_SUBJECTIVE,
  CREATE_SUBJECTIVE_FAILURE,
  CREATE_SUBJECTIVE_SUCCESS,
  DELETE_SUBJECTIVE,
  DELETE_SUBJECTIVE_FAILURE,
  DELETE_SUBJECTIVE_SUCCESS,
  FETCH_ALL_SUBJECTIVES,
  FETCH_ALL_SUBJECTIVES_FAILURE,
  FETCH_ALL_SUBJECTIVES_SUCCESS,
  FETCH_SUBJECTIVE,
  FETCH_SUBJECTIVE_FAILURE,
  FETCH_SUBJECTIVE_SUCCESS,
  CLEAR_FETCHED_SUBJECTIVE,
  UPDATE_SUBJECTIVE,
  UPDATE_SUBJECTIVE_FAILURE,
  UPDATE_SUBJECTIVE_SUCCESS
} from '../constants/subjective'
import { instance } from 'app/config/connection'

export const fetchAllSubjectives = () => dispatch => {
  dispatch({
    type: FETCH_ALL_SUBJECTIVES
  })

  instance.get('/subjectives').then(
    response =>
      dispatch({
        type: FETCH_ALL_SUBJECTIVES_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_SUBJECTIVES_FAILURE
        , error: error.response
      })
  )
}

export const fetchSubjective = subjectiveId => dispatch => {
  dispatch({
    type: FETCH_SUBJECTIVE
  })

  instance.get(`/subjectives/${subjectiveId}`).then(
    response =>
      dispatch({
        type: FETCH_SUBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_SUBJECTIVE_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedSubjective = () => ({ type: CLEAR_FETCHED_SUBJECTIVE })

export const createSubjective = values => dispatch => {
  dispatch({
    type: CREATE_SUBJECTIVE
  })

  return instance.post(`/subjectives`, values).then(
    response =>
      dispatch({
        type: CREATE_SUBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: CREATE_SUBJECTIVE_FAILURE
        , error: error.response
      })
  )
}

export const updateSubjective = (subjectiveId, values) => dispatch => {
  dispatch({
    type: UPDATE_SUBJECTIVE
  })

  return instance.patch(`/subjectives/${subjectiveId}`, values).then(
    response =>
      dispatch({
        type: UPDATE_SUBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: UPDATE_SUBJECTIVE_FAILURE
        , error: error.response
      })
  )
}

export const deleteSubjective = subjectiveId => dispatch => {
  dispatch({
    type: DELETE_SUBJECTIVE
  })
  instance.delete(`/subjectives/${subjectiveId}`).then(
    response =>
      dispatch({
        type: DELETE_SUBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: DELETE_SUBJECTIVE_FAILURE
        , error: error.response
      })
  )
}
