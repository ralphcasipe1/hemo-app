import { Button, Icon, List, Loader } from 'semantic-ui-react';
import _ from 'lodash';
import SubjectiveForm from 'SubjectiveForm';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';
import React, { PureComponent } from 'react';

class SubjectivesList extends PureComponent {
  constructor() {
    super();

    this.state = {
      subjectiveId: [],
      deleteSubjective: [],
    };
  }

  renderListItem(subjective) {
    if (this.state.subjectiveId.indexOf(subjective.subjective_id) !== -1) {
      return (
        <List.Item key={subjective.subjective_id}>
          <SubjectiveForm
            form={`EditSubjectiveForm_${subjective.subjective_id}`}
            onCancel={() =>
              this.setState({
                subjectiveId: this.state.subjectiveId.filter(
                  id => id !== subjective.subjective_id
                ),
              })
            }
            onFormSubmit={values => {
              this.props.onEditClick(subjective.subjective_id, values);
              this.setState({
                subjectiveId: this.state.subjectiveId.filter(
                  id => id !== subjective.subjective_id
                ),
              });
            }}
            initialValues={{
              name: subjective.name,
              description: subjective.description,
            }}
            simple
          />
        </List.Item>
      );
    } else if (
      this.state.deleteSubjective.indexOf(subjective.subjective_id) !== -1
    ) {
      return (
        <List.Item key={subjective.subjective_id}>
          <List.Content floated="right" verticalAlign="middle">
            <Button
              content="Yes"
              color="blue"
              floated="right"
              color="red"
              icon="checkmark"
              onClick={() => this.props.onDeleteClick(subjective.subjective_id)}
            />
            <Button
              content="No"
              color="blue"
              floated="right"
              basic
              icon="cancel"
              onClick={() =>
                this.setState({
                  deleteSubjective: this.state.deleteSubjective.filter(
                    id => id !== subjective.subjective_id
                  ),
                })
              }
            />
          </List.Content>
          <Icon name="warning sign" color="yellow" />
          <List.Content>
            <List.Header>You want to delete {subjective.name}?</List.Header>
            <List.Description>{subjective.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    } else {
      return (
        <List.Item key={subjective.subjective_id}>
          <List.Content floated="right">
            <Icon
              color="red"
              link
              name="trash"
              onClick={() =>
                this.setState({
                  deleteSubjective: [
                    ...this.state.deleteSubjective,
                    subjective.subjective_id,
                  ],
                })
              }
            />
            <Icon
              color="green"
              link
              name="edit"
              onClick={() =>
                this.setState({
                  subjectiveId: [
                    ...this.state.subjectiveId,
                    subjective.subjective_id,
                  ],
                })
              }
            />
          </List.Content>
          <List.Content>
            <List.Header>{subjective.name}</List.Header>
          </List.Content>
        </List.Item>
      );
    }
  }

  render() {
    if (this.props.isLoading) {
      return <Loader active inline="centered" />;
    } else if (this.props.error) {
      return <ExceptionMessage exception={this.props.error} />;
    } else if (_.isEmpty(this.props.subjectives)) {
      return <RecommendMessage header="No Subjectives" />;
    }

    return (
      <List divided relaxed="very">
        {this.props.subjectives.map(subjective =>
          this.renderListItem(subjective)
        )}
      </List>
    );
  }
}

export default SubjectivesList;
