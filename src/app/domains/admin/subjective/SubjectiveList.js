import React                  from 'react'
import { List }               from 'semantic-ui-react'
import { SubjectiveListItem } from './SubjectiveListItem'

export const SubjectiveList = props => (
  <List divided relaxed="very" size="big">
    {props.subjectives.map(subjective => (
      <SubjectiveListItem
        key={subjective.subjective_id}
        subjective={subjective}
        {...props}
      />
    ))}
  </List>
)
