import {
  CREATE_SUBJECTIVE,
  CREATE_SUBJECTIVE_FAILURE,
  CREATE_SUBJECTIVE_SUCCESS,
  DELETE_SUBJECTIVE,
  DELETE_SUBJECTIVE_FAILURE,
  DELETE_SUBJECTIVE_SUCCESS,
  FETCH_ALL_SUBJECTIVES,
  FETCH_ALL_SUBJECTIVES_FAILURE,
  FETCH_ALL_SUBJECTIVES_SUCCESS,
  FETCH_SUBJECTIVE,
  FETCH_SUBJECTIVE_FAILURE,
  FETCH_SUBJECTIVE_SUCCESS,
  CLEAR_FETCHED_SUBJECTIVE,
  UPDATE_SUBJECTIVE,
  UPDATE_SUBJECTIVE_FAILURE,
  UPDATE_SUBJECTIVE_SUCCESS
} from '../constants/subjective'

const initialState = {
  subjectives: []
  , subjective: null
  , error: null
  , isLoading: false
}

export const subjectiveReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_SUBJECTIVES:
    case FETCH_SUBJECTIVE:
    case CREATE_SUBJECTIVE:
    case UPDATE_SUBJECTIVE:
    case DELETE_SUBJECTIVE:
      return {
        ...state
        , isLoading: true
      }

    case FETCH_ALL_SUBJECTIVES_SUCCESS:
      return {
        ...state
        , isLoading: false
        , subjectives: action.payload
      }

    case FETCH_SUBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , subjective: action.payload
      }

    case CLEAR_FETCHED_SUBJECTIVE:
      return {
        ...state
        , isLoading: false
        , subjective: null
      }
      
    case CREATE_SUBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , subjectives: [ ...state.subjectives, action.payload ]
      }

    case UPDATE_SUBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , subjectives: state.subjectives.map(subjective => {
          if (subjective.subjective_id !== action.payload.subjective_id) {
            return subjective
          }
          return Object.assign({}, subjective, {
            name: action.payload.name
            , description: action.payload.description
          })
        })
      }

    case DELETE_SUBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , subjectives: state.subjectives.filter(
          subjective =>
            subjective.subjective_id !== action.payload.subjective_id
        )
      }

    case FETCH_ALL_SUBJECTIVES_FAILURE:
    case FETCH_SUBJECTIVE_FAILURE:
    case CREATE_SUBJECTIVE_FAILURE:
    case DELETE_SUBJECTIVE_FAILURE:
    case UPDATE_SUBJECTIVE_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.payload.message
      }

    default:
      return state
  }
}
