import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { ConfirmModal }               from '../common/components/display/ConfirmModal'
import { DialyzerSizeForm }           from './DialyzerSizeForm'
import { DialyzerSizeList }           from './DialyzerSizeList'

class DialyzerSize extends Component {
  render() {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      dialyzerSize
    } = this.props

    if (data.length === 0 || openCreateForm) {
      return (
        <DialyzerSizeForm
          form="CreateDialyzerSizeForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    if (openEditForm && dialyzerSize) {
      return (
        <DialyzerSizeForm
          form="EditDialyzerSizeForm"
          onFormSubmit={handleUpdate}
          onCancel={handleCloseEditForm}
          initialValues={{
            size_name: dialyzerSize.size_name
            , description: dialyzerSize.description
          }}
        />
      )
    }

    return (
      <Fragment>
        <ConfirmModal
          open={openModal}
          onClose={handleCloseModal}
          onDelete={handleDelete}
        />
        
        <DialyzerSizeList
          dialyzerSizes={data}
          onClickDelete={handleOpenModal}
          onClickEdit={handleOpenEditForm}
        />
      </Fragment>
    )
  }
}

DialyzerSize = AdminBuilderHOC(DialyzerSize, {
  title: 'Dialyzer Size'
})

export { DialyzerSize }
