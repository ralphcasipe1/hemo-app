import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import { ButtonDecision } from '../../../common/forms/ButtonDecision';
import { InputField } from '../../../common/forms/InputField';

class DialyzerSizeForm extends PureComponent {
  render() {
    const { handleSubmit, onFormSubmit, valid, submitting } = this.props;

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field component={InputField} name="size_name" label="Size Name" />
        <Field component={InputField} name="description" label="Description" />
        <ButtonDecision
          expanded
          isDisable={!valid || submitting}
          {...this.props}
        />
      </Form>
    );
  }
}

const validate = values => {
  let errors = {};

  if (!values.size_name) {
    errors.size_name = 'Required';
  } else if (values.size_name.length < 2) {
    errors.size_name = 'Min length is 2';
  }

  return errors;
};

DialyzerSizeForm = reduxForm({
  fields: ['size_name', 'description'],
  validate,
})(DialyzerSizeForm);

export { DialyzerSizeForm };
