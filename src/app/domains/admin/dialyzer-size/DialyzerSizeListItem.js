import React              from 'react'
import { Dropdown, List } from 'semantic-ui-react'

export const DialyzerSizeListItem = ({ dialyzerSize, onClickEdit, onClickDelete }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item 
            content="Edit"
            key="edit" 
            onClick={() => onClickEdit(dialyzerSize.dialyzer_size_id)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(dialyzerSize.dialyzer_size_id)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={dialyzerSize.size_name} />
      <List.Description content={dialyzerSize.description} />
    </List.Content>
  </List.Item>
)
