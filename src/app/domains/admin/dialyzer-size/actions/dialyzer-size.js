import {
  CREATE_DIALYZER_SIZE,
  CREATE_DIALYZER_SIZE_FAILURE,
  CREATE_DIALYZER_SIZE_SUCCESS,
  DELETE_DIALYZER_SIZE,
  DELETE_DIALYZER_SIZE_FAILURE,
  DELETE_DIALYZER_SIZE_SUCCESS,
  FETCH_ALL_DIALYZER_SIZES,
  FETCH_ALL_DIALYZER_SIZES_FAILURE,
  FETCH_ALL_DIALYZER_SIZES_SUCCESS,
  FETCH_DIALYZER_SIZE,
  FETCH_DIALYZER_SIZE_FAILURE,
  FETCH_DIALYZER_SIZE_SUCCESS,
  CLEAR_FETCHED_DIALYZER_SIZE,
  UPDATE_DIALYZER_SIZE,
  UPDATE_DIALYZER_SIZE_FAILURE,
  UPDATE_DIALYZER_SIZE_SUCCESS
} from '../constants/dialyzer-size'
import { instance } from 'app/config/connection'

export const fetchAllDialyzerSizes = () => dispatch => {
  dispatch({
    type: FETCH_ALL_DIALYZER_SIZES
  })

  return instance.get(`/dialyzer_sizes`).then(
    response =>
      dispatch({
        type: FETCH_ALL_DIALYZER_SIZES_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_ALL_DIALYZER_SIZES_FAILURE
        , error: error.response
      })
  )
}

export const fetchDialyzerSize = dialyzerId => dispatch => {
  dispatch({
    type: FETCH_DIALYZER_SIZE
  })

  instance.get(`/dialyzer_sizes/${dialyzerId}`).then(
    response =>
      dispatch({
        type: FETCH_DIALYZER_SIZE_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: FETCH_DIALYZER_SIZE_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedDialyzerSize = () => ({ type: CLEAR_FETCHED_DIALYZER_SIZE })

export const createDialyzerSize = values => dispatch => {
  dispatch({
    type: CREATE_DIALYZER_SIZE
  })

  return instance.post(`/dialyzer_sizes`, values).then(
    response =>
      dispatch({
        type: CREATE_DIALYZER_SIZE_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: CREATE_DIALYZER_SIZE_FAILURE
        , error: error.response
      })
  )
}

export const updateDialyzerSize = (dialyzerId, values) => dispatch => {
  dispatch({
    type: UPDATE_DIALYZER_SIZE
  })

  instance.patch(`/dialyzer_sizes/${dialyzerId}`, values).then(
    response =>
      dispatch({
        type: UPDATE_DIALYZER_SIZE_SUCCESS
        , payload: response.data.data
      }),

    error =>
      dispatch({
        type: UPDATE_DIALYZER_SIZE_FAILURE
        , error: error.response
      })
  )
}

export const deleteDialyzerSize = dialyzerId => dispatch => {
  dispatch({
    type: DELETE_DIALYZER_SIZE
  })

  instance.delete(`/dialyzer_sizes/${dialyzerId}`).then(
    response =>
      dispatch({
        type: DELETE_DIALYZER_SIZE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: DELETE_DIALYZER_SIZE_FAILURE
        , error: error.response
      })
  )
}
