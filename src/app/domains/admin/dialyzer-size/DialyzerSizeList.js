import React from 'react';
import { List } from 'semantic-ui-react';
import { DialyzerSizeListItem } from './DialyzerSizeListItem';

export const DialyzerSizeList = props => (
  <List divided relaxed="very" size="big">
    {props.dialyzerSizes.map(dialyzerSize => (
      <DialyzerSizeListItem
        key={dialyzerSize.dialyzer_size_id}
        dialyzerSize={dialyzerSize}
        {...props}
      />
    ))}
  </List>
);
