import { Button, Icon, List, Loader } from 'semantic-ui-react';
import _ from 'lodash';
import DialyzerSizeForm from 'DialyzerSizeForm';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class DialyzerSizesList extends PureComponent {
  static propTypes = {
    dialyzerSizes: PropTypes.arrayOf(
      PropTypes.shape({
        dialyzer_size_id: PropTypes.number.isRequired,
        description: PropTypes.string,
        size_name: PropTypes.string.isRequired,
      })
    ),
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    onDeleteClick: PropTypes.func.isRequired,
    onEditClick: PropTypes.func.isRequired,
  };

  constructor() {
    super();

    this.state = {
      dialyzerSizeId: [],
      deleteDialyzerSize: [],
    };
  }

  renderListItem(dialyzerSize) {
    if (
      this.state.dialyzerSizeId.indexOf(dialyzerSize.dialyzer_size_id) !== -1
    ) {
      return (
        <List.Item key={dialyzerSize.dialyzer_size_id}>
          <List.Content>
            <DialyzerSizeForm
              form={`EditDialyzerSizeForm_${dialyzerSize.dialyzer_size_id}`}
              onCancel={() =>
                this.setState({
                  dialyzerSizeId: this.state.dialyzerSizeId.filter(
                    id => id !== dialyzerSize.dialyzer_size_id
                  ),
                })
              }
              onFormSubmit={values => {
                this.props.onEditClick(dialyzerSize.dialyzer_size_id, values);
                this.setState({
                  dialyzerSizeId: this.state.dialyzerSizeId.filter(
                    id => id !== dialyzerSize.dialyzer_size_id
                  ),
                });
              }}
              initialValues={{
                size_name: dialyzerSize.size_name,
                description: dialyzerSize.description,
              }}
              simple
            />
          </List.Content>
        </List.Item>
      );
    } else if (
      this.state.deleteDialyzerSize.indexOf(dialyzerSize.dialyzer_size_id) !==
      -1
    ) {
      return (
        <List.Item key={dialyzerSize.dialyzer_size_id}>
          <List.Content floated="right" verticalAlign="middle">
            <Button
              content="Yes"
              color="blue"
              floated="right"
              color="red"
              icon="checkmark"
              onClick={() =>
                this.props.onDeleteClick(dialyzerSize.dialyzer_size_id)
              }
            />
            <Button
              content="No"
              color="blue"
              floated="right"
              basic
              icon="cancel"
              onClick={() =>
                this.setState({
                  deleteDialyzerSize: this.state.deleteDialyzerSize.filter(
                    id => id !== dialyzerSize.dialyzer_size_id
                  ),
                })
              }
            />
          </List.Content>
          <Icon name="warning sign" color="yellow" />
          <List.Content>
            <List.Header>
              You want to delete {dialyzerSize.size_name}?
            </List.Header>
            <List.Description>{dialyzerSize.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    } else {
      return (
        <List.Item key={dialyzerSize.dialyzer_size_id}>
          <List.Content floated="right">
            <Icon
              color="red"
              link
              name="trash"
              onClick={() =>
                this.setState({
                  deleteDialyzerSize: [
                    ...this.state.deleteDialyzerSize,
                    dialyzerSize.dialyzer_size_id,
                  ],
                })
              }
            />

            <Icon
              color="green"
              link
              name="edit"
              onClick={() =>
                this.setState({
                  dialyzerSizeId: [
                    ...this.state.dialyzerSizeId,
                    dialyzerSize.dialyzer_size_id,
                  ],
                })
              }
            />
          </List.Content>
          <List.Content>
            <List.Header>{dialyzerSize.size_name}</List.Header>
            <List.Description>{dialyzerSize.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    }
  }

  render() {
    if (this.props.isLoading) {
      return <Loader active inline="centered" />;
    } else if (this.props.error) {
      return <ExceptionMessage exception={this.props.error} />;
    } else if (_.isEmpty(this.props.dialyzerSizes)) {
      return <RecommendMessage header="No Dialyzer's Sizes" />;
    }

    return (
      <List divided relaxed="very">
        {this.props.dialyzerSizes.map(dialyzerSize =>
          this.renderListItem(dialyzerSize)
        )}
      </List>
    );
  }
}

export default DialyzerSizesList;
