import { connect } from 'react-redux'
import {
  createDialyzerSize,
  deleteDialyzerSize,
  fetchAllDialyzerSizes,
  fetchDialyzerSize,
  clearFetchedDialyzerSize,
  updateDialyzerSize
} from './actions/dialyzer-size'
import { DialyzerSize } from './DialyzerSize'

const mapStateToProps = state => ({
  data: state.dialyzerSizeReducer.dialyzerSizes
  , dialyzerSize: state.dialyzerSizeReducer.dialyzerSize
  , loading: state.dialyzerSizeReducer.loading
})

export const DialyzerSizeContainer = connect(
  mapStateToProps,
  {
    create: createDialyzerSize
    , deleteData: deleteDialyzerSize
    , fetchAllData: fetchAllDialyzerSizes
    , fetchData: fetchDialyzerSize
    , clearData: clearFetchedDialyzerSize
    , updateData: updateDialyzerSize
  }
)(DialyzerSize)
