import {
  CREATE_DIALYZER_SIZE,
  CREATE_DIALYZER_SIZE_FAILURE,
  CREATE_DIALYZER_SIZE_SUCCESS,
  DELETE_DIALYZER_SIZE,
  DELETE_DIALYZER_SIZE_FAILURE,
  DELETE_DIALYZER_SIZE_SUCCESS,
  FETCH_ALL_DIALYZER_SIZES,
  FETCH_ALL_DIALYZER_SIZES_FAILURE,
  FETCH_ALL_DIALYZER_SIZES_SUCCESS,
  FETCH_DIALYZER_SIZE,
  FETCH_DIALYZER_SIZE_FAILURE,
  FETCH_DIALYZER_SIZE_SUCCESS,
  CLEAR_FETCHED_DIALYZER_SIZE,
  UPDATE_DIALYZER_SIZE,
  UPDATE_DIALYZER_SIZE_FAILURE,
  UPDATE_DIALYZER_SIZE_SUCCESS
} from '../constants/dialyzer-size'

const initialState = {
  dialyzerSize: null
  , dialyzerSizes: []
  , error: null
  , isLoading: false
}

export const dialyzerSizeReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_DIALYZER_SIZE:
    case DELETE_DIALYZER_SIZE:
    case UPDATE_DIALYZER_SIZE:
    case FETCH_ALL_DIALYZER_SIZES:
    case FETCH_DIALYZER_SIZE:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_DIALYZER_SIZE_FAILURE:
    case DELETE_DIALYZER_SIZE_FAILURE:
    case UPDATE_DIALYZER_SIZE_FAILURE:
    case FETCH_ALL_DIALYZER_SIZES_FAILURE:
    case FETCH_DIALYZER_SIZE_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      }

    case CREATE_DIALYZER_SIZE_SUCCESS:
      return {
        ...state
        , dialyzerSizes: [ action.payload, ...state.dialyzerSizes ]
        , isLoading: false
      }

    case DELETE_DIALYZER_SIZE_SUCCESS:
      return {
        ...state
        , dialyzerSizes: state.dialyzerSizes.filter(
          dialyzerSize =>
            dialyzerSize.dialyzer_size_id !== action.payload.dialyzer_size_id
        )
        , isLoading: false
      }

    case UPDATE_DIALYZER_SIZE_SUCCESS:
      const { dialyzer_size_id, size_name, description } = action.payload
      const newDialyzers = state.dialyzerSizes.map(dialyzerSize => {
        if (dialyzerSize.dialyzer_size_id !== dialyzer_size_id) {
          return dialyzerSize
        }
        return {
          ...dialyzerSize
          , size_name
          , description
        }
      })

      return {
        ...state
        , dialyzerSizes: newDialyzers
        , isLoading: false
      }

    case FETCH_ALL_DIALYZER_SIZES_SUCCESS:
      return {
        ...state
        , dialyzerSizes: action.payload
        , isLoading: false
      }

    case FETCH_DIALYZER_SIZE_SUCCESS:
      return {
        ...state
        , dialyzerSize: action.payload
        , isLoading: false
      }
      
    case CLEAR_FETCHED_DIALYZER_SIZE:
      return {
        ...state
        , dialyzerSize: null
        , isLoading: false
      }
      
    default:
      return state
  }
}
