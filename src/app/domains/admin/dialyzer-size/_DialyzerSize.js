import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Header, Tab } from 'semantic-ui-react';
import {
  createDialyzerSize,
  deleteDialyzerSize,
  editDialyzerSize,
  fetchDialyzerSizes,
  resetDialyzerSizeMessage,
} from '../actions/dialyzerSizes';
import DialyzerSizeForm from 'DialyzerSizeForm';
import DialyzerSizesList from 'DialyzerSizesList';

class DialyzerSizeContainer extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onInit = props => props.fetchDialyzerSizes();

  componentDidMount() {
    this.onInit(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.dialyzerSizes.message && nextProps.dialyzerSizes.message) {
      setTimeout(() => this.props.resetDialyzerSizeMessage(), 3000);
    }
  }

  handleCancel() {
    this.props.router.push('/admin');
  }

  handleDeleteClick(id) {
    this.props.deleteDialyzerSize(id);
  }

  handleEditClick(id, values) {
    this.props.editDialyzerSize(id, values);
  }

  handleSubmit(values) {
    this.props.createDialyzerSize(values);
  }

  render() {
    const {
      message,
      dialyzerSizes,
      isLoading,
      error,
    } = this.props.dialyzerSizes;
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane>
              <DialyzerSizeForm
                onFormSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
                form="DialyzerSizeForm"
              />
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'List',
        render: () => {
          return (
            <Tab.Pane>
              <DialyzerSizesList
                dialyzerSizes={dialyzerSizes}
                error={error}
                isLoading={isLoading}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
              />
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div>
        <Header content="Dialyzer's Sizes" />

        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dialyzerSizes: state.dialyzerSizes,
});

export default connect(
  mapStateToProps,
  {
    createDialyzerSize,
    deleteDialyzerSize,
    editDialyzerSize,
    fetchDialyzerSizes,
    resetDialyzerSizeMessage,
  }
)(DialyzerSizeContainer);
