import { Button, Icon, List, Loader } from 'semantic-ui-react';
import _ from 'lodash';
import ObjectiveForm from 'ObjectiveForm';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class ObjectivesList extends PureComponent {
  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    objectives: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
      })
    ),
    onDeleteClick: PropTypes.func.isRequired,
    onEditClick: PropTypes.func.isRequired,
  };

  constructor() {
    super();

    this.state = {
      objectiveId: [],
      deleteObjective: [],
    };
  }

  renderListItem(objective) {
    if (this.state.objectiveId.indexOf(objective.objective_id) !== -1) {
      return (
        <List.Item key={objective.objective_id}>
          <ObjectiveForm
            form={`EditObjectiveForm${objective.objective_id}`}
            onCancel={() =>
              this.setState({
                objectiveId: this.state.objectiveId.filter(
                  id => id !== objective.objective_id
                ),
              })
            }
            onFormSubmit={values => {
              this.props.onEditClick(objective.objective_id, values);
              this.setState({
                objectiveId: this.state.objectiveId.filter(
                  id => id !== objective.objective_id
                ),
              });
            }}
            initialValues={{
              name: objective.name,
              description: objective.description,
            }}
            simple
          />
        </List.Item>
      );
    } else if (
      this.state.deleteObjective.indexOf(objective.objective_id) !== -1
    ) {
      return (
        <List.Item key={objective.objective_id}>
          <List.Content floated="right" verticalAlign="middle">
            <Button
              content="Yes"
              floated="right"
              color="red"
              icon="checkmark"
              onClick={() => this.props.onDeleteClick(objective.objective_id)}
            />
            <Button
              content="No"
              color="blue"
              floated="right"
              basic
              icon="cancel"
              onClick={() =>
                this.setState({
                  deleteObjective: this.state.deleteObjective.filter(
                    id => id !== objective.objective_id
                  ),
                })
              }
            />
          </List.Content>
          <Icon name="warning sign" color="yellow" />
          <List.Content>
            <List.Header>You want to delete {objective.name}?</List.Header>
            <List.Description>{objective.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    } else {
      return (
        <List.Item key={objective.objective_id}>
          <List.Content floated="right">
            <Icon
              color="red"
              link
              name="trash"
              onClick={() =>
                this.setState({
                  deleteObjective: [
                    ...this.state.deleteObjective,
                    objective.objective_id,
                  ],
                })
              }
            />
            <Icon
              color="green"
              link
              name="edit"
              onClick={() =>
                this.setState({
                  objectiveId: [
                    ...this.state.objectiveId,
                    objective.objective_id,
                  ],
                })
              }
            />
          </List.Content>
          <List.Content>
            <List.Header>{objective.name}</List.Header>
            <List.Description>{objective.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    }
  }

  render() {
    if (this.props.isLoading) {
      return <Loader active inline="centered" />;
    } else if (this.props.error) {
      return <ExceptionMessage exception={this.props.error} />;
    } else if (_.isEmpty(this.props.objectives)) {
      return <RecommendMessage header="No Objectives" />;
    }

    return (
      <List divided relaxed="very">
        {this.props.objectives.map(objective => this.renderListItem(objective))}
      </List>
    );
  }
}

export default ObjectivesList;
