import {
  CREATE_OBJECTIVE,
  CREATE_OBJECTIVE_FAILURE,
  CREATE_OBJECTIVE_SUCCESS,
  DELETE_OBJECTIVE,
  DELETE_OBJECTIVE_FAILURE,
  DELETE_OBJECTIVE_SUCCESS,
  FETCH_ALL_OBJECTIVES,
  FETCH_ALL_OBJECTIVES_FAILURE,
  FETCH_ALL_OBJECTIVES_SUCCESS,
  FETCH_OBJECTIVE,
  FETCH_OBJECTIVE_FAILURE,
  FETCH_OBJECTIVE_SUCCESS,
  CLEAR_FETCHED_OBJECTIVE,
  UPDATE_OBJECTIVE,
  UPDATE_OBJECTIVE_FAILURE,
  UPDATE_OBJECTIVE_SUCCESS
} from '../constants/objective'

const initialState = {
  objectives: []
  , objective: null
  , error: null
  , isLoading: false
}

export const objectiveReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_OBJECTIVES:
    case FETCH_OBJECTIVE:
    case CREATE_OBJECTIVE:
    case DELETE_OBJECTIVE:
    case UPDATE_OBJECTIVE:
      return {
        ...state
        , isLoading: true
      }

    case FETCH_ALL_OBJECTIVES_FAILURE:
    case FETCH_OBJECTIVE_FAILURE:
    case CREATE_OBJECTIVE_FAILURE:
    case DELETE_OBJECTIVE_FAILURE:
    case UPDATE_OBJECTIVE_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      }

    case FETCH_ALL_OBJECTIVES_SUCCESS:
      return {
        ...state
        , isLoading: false
        , objectives: action.payload
      }

    case FETCH_OBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , objective: action.payload
      }
      
    case CLEAR_FETCHED_OBJECTIVE:
      return {
        ...state
        , isLoading: false
        , objective: null
      }
      
    case CREATE_OBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , objectives: [ ...state.objectives, action.payload ]
      }

    case UPDATE_OBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , objectives: state.objectives.map(objective => {
          if (objective.objective_id !== action.payload.objective_id) {
            return objective
          }
          return Object.assign({}, objective, {
            name: action.payload.name
            , description: action.payload.description
          })
        })
      }

    case DELETE_OBJECTIVE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , objectives: state.objectives.filter(
          objective => objective.objective_id !== action.payload.objective_id
        )
      }

    default:
      return state
  }
}
