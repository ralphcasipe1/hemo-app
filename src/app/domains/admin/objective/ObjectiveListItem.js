import React              from 'react'
import { Dropdown, List } from 'semantic-ui-react'

export const ObjectiveListItem = ({ objective, onClickEdit, onClickDelete }) => (
  <List.Item>
    <List.Content floated="right" verticalAlign="middle">
      <Dropdown icon="vertical ellipsis">
        <Dropdown.Menu>
          <Dropdown.Item 
            content="Edit" 
            key="edit" 
            onClick={() => onClickEdit(objective.objective_id)}
          />
          <Dropdown.Item
            content="Delete"
            key="delete"
            onClick={() => onClickDelete(objective.objective_id)}
          />
        </Dropdown.Menu>
      </Dropdown>
    </List.Content>
    <List.Content>
      <List.Header content={objective.name} />
      <List.Description content={objective.description} />
    </List.Content>
  </List.Item>
)
