import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import _ from 'lodash';
import {
  createObjective,
  deleteObjective,
  editObjective,
  fetchObjectives,
  resetDeletedObjective,
  resetEditObjective,
  resetObjectives,
  resetNewObjective,
} from '../actions/objectives';
import ObjectiveForm from 'ObjectiveForm';
import ObjectivesList from 'ObjectivesList';

class ObjectiveContainer extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchObjectives();
  }

  componentWillUnmount() {
    this.props.resetDeletedObjective();
    this.props.resetEditObjective();
    this.props.resetObjectives();
    this.props.resetNewObjective();
  }

  handleCancel() {
    this.props.router.push('/admin');
  }

  handleDeleteClick(id) {
    this.props.deleteObjective(id);
  }

  handleEditClick(id, values) {
    this.props.editObjective(id, values);
  }

  handleSubmit(values) {
    this.props.createObjective(values);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane>
              <ObjectiveForm
                onFormSubmit={this.handleSubmit}
                hasCancel
                onCancel={this.handleCancel}
                form="ObjectiveForm"
              />
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'List',
        render: () => {
          return (
            <Tab.Pane>
              <ObjectivesList
                error={this.props.error}
                isLoading={this.props.isLoading}
                objectives={this.props.objectives}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
              />
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div>
        <Header content="Objectives" />
        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  objectives: state.objectives.objectives,
  isLoading: state.objectives.isLoading,
  error: state.objectives.error,
});

export default connect(
  mapStateToProps,
  {
    createObjective,
    deleteObjective,
    editObjective,
    fetchObjectives,
    resetDeletedObjective,
    resetEditObjective,
    resetObjectives,
    resetNewObjective,
  }
)(ObjectiveContainer);
