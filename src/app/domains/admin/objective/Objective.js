import React, { Component, Fragment } from 'react'
import { AdminBuilderHOC }            from '../AdminBuilderHOC'
import { ConfirmModal }               from '../common/components/display/ConfirmModal'
import { ObjectiveForm }              from './ObjectiveForm'
import { ObjectiveList }              from './ObjectiveList'

class Objective extends Component {
  render() {
    const { 
      data, 
      handleCreate,
      handleUpdate,
      handleCloseCreateForm, 
      handleOpenEditForm,
      handleCloseEditForm,
      openCreateForm, 
      openEditForm,
      openModal,
      handleOpenModal, 
      handleCloseModal, 
      handleDelete,
      objective
    } = this.props

    if (data.length === 0 || openCreateForm) {
      return (
        <ObjectiveForm
          form="CreateObjectiveForm"
          onFormSubmit={handleCreate}
          disableCancel={data.length === 0}
          onCancel={handleCloseCreateForm}
        />
      )
    }

    if (openEditForm && objective) {
      return (
        <ObjectiveForm
          form="EditObjectiveForm"
          onFormSubmit={handleUpdate}
          onCancel={handleCloseEditForm}
          initialValues={{
            name: objective.name
            , description: objective.description
          }}
        />
      )
    }

    return (
      <Fragment>
        <ConfirmModal
          open={openModal}
          onClose={handleCloseModal}
          onDelete={handleDelete}
        />

        <ObjectiveList 
          objectives={data} 
          onClickDelete={handleOpenModal}
          onClickEdit={handleOpenEditForm}
        />
      </Fragment>
    )
  }
}

Objective = AdminBuilderHOC(Objective, {
  title: 'Objective'
})

export { Objective }
