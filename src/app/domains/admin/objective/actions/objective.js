import {
  CREATE_OBJECTIVE,
  CREATE_OBJECTIVE_FAILURE,
  CREATE_OBJECTIVE_SUCCESS,
  DELETE_OBJECTIVE,
  DELETE_OBJECTIVE_FAILURE,
  DELETE_OBJECTIVE_SUCCESS,
  FETCH_ALL_OBJECTIVES,
  FETCH_ALL_OBJECTIVES_FAILURE,
  FETCH_ALL_OBJECTIVES_SUCCESS,
  FETCH_OBJECTIVE,
  FETCH_OBJECTIVE_FAILURE,
  FETCH_OBJECTIVE_SUCCESS,
  CLEAR_FETCHED_OBJECTIVE,
  UPDATE_OBJECTIVE,
  UPDATE_OBJECTIVE_FAILURE,
  UPDATE_OBJECTIVE_SUCCESS
} from '../constants/objective'
import { instance } from 'app/config/connection'

export const fetchAllObjectives = () => dispatch => {
  dispatch({
    type: FETCH_ALL_OBJECTIVES
  })

  instance.get(`/objectives`).then(
    response =>
      dispatch({
        type: FETCH_ALL_OBJECTIVES_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_OBJECTIVES_FAILURE
        , error: error.response
      })
  )
}

export const fetchObjective = objectiveId => dispatch => {
  dispatch({
    type: FETCH_OBJECTIVE
  })
  instance.get(`/objectives/${objectiveId}`).then(
    response =>
      dispatch({
        type: FETCH_OBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_OBJECTIVE_FAILURE
        , error: error.response
      })
  )
}

export const clearFetchedObjective = () => ({ type: CLEAR_FETCHED_OBJECTIVE })

export const createObjective = values => dispatch => {
  dispatch({
    type: CREATE_OBJECTIVE
  })
  instance.post(`/objectives`, values).then(
    response =>
      dispatch({
        type: CREATE_OBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: CREATE_OBJECTIVE_FAILURE
        , error: error.response
      })
  )
}

export const updateObjective = (objectiveId, values) => dispatch => {
  dispatch({
    type: UPDATE_OBJECTIVE
  })
  instance.patch(`/objectives/${objectiveId}`, values).then(
    response =>
      dispatch({
        type: UPDATE_OBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: UPDATE_OBJECTIVE_FAILURE
        , error: error.response
      })
  )
}

export const deleteObjective = objectiveId => dispatch => {
  dispatch({
    type: DELETE_OBJECTIVE
  })
  instance.delete(`/objectives/${objectiveId}`).then(
    response =>
      dispatch({
        type: DELETE_OBJECTIVE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: DELETE_OBJECTIVE_FAILURE
        , error: error.response
      })
  )
}
