import { connect } from 'react-redux'
import {
  createObjective,
  deleteObjective,
  fetchAllObjectives,
  fetchObjective,
  clearFetchedObjective,
  updateObjective
} from './actions/objective'
import { Objective } from './Objective'

const mapStateToProps = state => ({
  data: state.objectiveReducer.objectives
  , loading: state.objectiveReducer.loading
  , objective: state.objectiveReducer.objective
})

export const ObjectiveContainer = connect(
  mapStateToProps,
  {
    create: createObjective
    , deleteData: deleteObjective
    , fetchAllData: fetchAllObjectives
    , fetchData: fetchObjective
    , clearData: clearFetchedObjective
    , updateData: updateObjective
  }
)(Objective)
