import React from 'react';
import { List } from 'semantic-ui-react';
import { ObjectiveListItem } from './ObjectiveListItem';

export const ObjectiveList = props => (
  <List divided relaxed="very" size="big">
    {props.objectives.map(objective => (
      <ObjectiveListItem
        key={objective.objective_id}
        objective={objective}
        {...props}
      />
    ))}
  </List>
);
