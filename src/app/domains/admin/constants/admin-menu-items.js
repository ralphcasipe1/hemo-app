export const ADMIN_MENU_ITEMS = [
  {
    name: 'dialyzerBrands',
    icon: 'settings',
  },
  {
    name: 'dialyzerSizes',
    icon: 'settings',
  },
  {
    name: 'mobilities',
    icon: 'wheelchair',
  },
  {
    name: 'abnormals',
    icon: 'deafness',
  },
  {
    name: 'subjectives',
    icon: 'treatment',
  },
  {
    name: 'objectives',
    icon: 'book',
  },
  {
    name: 'users',
    icon: 'users',
  },
  {
    name: 'physicians',
    icon: 'doctor',
  },
];
