import _ from 'lodash';

export const legibleName = str =>
  _.join(_.split(_.snakeCase(str), '_').map(t => _.upperFirst(t)), ' ');
