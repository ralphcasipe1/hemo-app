import React              from 'react'
import { WeightListItem } from './WeightListItem'

export const WeightList = props => (
  <div>
    {props.standingOrderWeights.map(standingOrderWeight => (
      <WeightListItem
        key={standingOrderWeight.template_weight_id}
        standingOrderWeight={standingOrderWeight}
        {...props}
      />
    ))}
  </div>
)
