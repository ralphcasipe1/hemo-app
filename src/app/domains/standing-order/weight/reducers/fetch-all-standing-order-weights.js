export const fetchAllStandingOrderWeights = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderWeights: action.payload
})
