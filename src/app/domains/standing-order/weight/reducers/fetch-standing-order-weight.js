export const fetchStandingOrderWeight = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderWeight: action.payload
})
