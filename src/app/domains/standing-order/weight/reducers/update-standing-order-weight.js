export const updateStandingOrderWeight = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderWeights: state.standingOrderWeights.map(standingOrderWeight => {
    if (
      standingOrderWeight.template_weight_id !==
      action.payload.template_weight_id
    ) {
      return standingOrderWeight
    }

    return Object.assign({}, standingOrderWeight, {
      dry_weight: action.payload.dry_weight
    })
  })
})
