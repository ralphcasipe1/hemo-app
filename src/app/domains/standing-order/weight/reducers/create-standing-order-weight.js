export const createStandingOrderWeight = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderWeights: [ action.payload, ...state.standingOrderWeights ]
})
