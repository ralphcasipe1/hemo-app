export const deleteStandingOrderWeight = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderWeights: state.standingOrderWeights.filter(
    standingOrderWeights =>
      standingOrderWeights.template_weight_id !==
      action.payload.template_weight_id
  )
})
