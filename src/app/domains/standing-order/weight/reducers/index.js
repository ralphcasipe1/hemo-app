import { loadReducer } from '../../../../common/helpers/load-reducer'
import { failReducer } from '../../../../common/helpers/fail-reducer'
import {
  CREATE_STANDING_ORDER_WEIGHT,
  CREATE_STANDING_ORDER_WEIGHT_FAILURE,
  CREATE_STANDING_ORDER_WEIGHT_SUCCESS,
  DELETE_STANDING_ORDER_WEIGHT,
  DELETE_STANDING_ORDER_WEIGHT_FAILURE,
  DELETE_STANDING_ORDER_WEIGHT_SUCCESS,
  FETCH_ALL_STANDING_ORDER_WEIGHTS,
  FETCH_ALL_STANDING_ORDER_WEIGHTS_FAILURE,
  FETCH_ALL_STANDING_ORDER_WEIGHTS_SUCCESS,
  FETCH_STANDING_ORDER_WEIGHT,
  FETCH_STANDING_ORDER_WEIGHT_FAILURE,
  FETCH_STANDING_ORDER_WEIGHT_SUCCESS,
  UPDATE_STANDING_ORDER_WEIGHT,
  UPDATE_STANDING_ORDER_WEIGHT_FAILURE,
  UPDATE_STANDING_ORDER_WEIGHT_SUCCESS
} from '../constants'
import { createStandingOrderWeight }    from './create-standing-order-weight'
import { deleteStandingOrderWeight }    from './delete-standing-order-weight'
import { fetchAllStandingOrderWeights } from './fetch-all-standing-order-weights'
import { fetchStandingOrderWeight }     from './fetch-standing-order-weight'
import { updateStandingOrderWeight }    from './update-standing-order-weight'

const initialState = {
  standingOrderWeights: []
  , standingOrderWeight: null
  , isLoading: false
  , error: null
}

export const standingOrderWeightReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_STANDING_ORDER_WEIGHT:
    case FETCH_ALL_STANDING_ORDER_WEIGHTS:
    case FETCH_STANDING_ORDER_WEIGHT:
    case UPDATE_STANDING_ORDER_WEIGHT:
    case DELETE_STANDING_ORDER_WEIGHT:
      return loadReducer(state)

    case CREATE_STANDING_ORDER_WEIGHT_FAILURE:
    case FETCH_ALL_STANDING_ORDER_WEIGHTS_FAILURE:
    case FETCH_STANDING_ORDER_WEIGHT_FAILURE:
    case UPDATE_STANDING_ORDER_WEIGHT_FAILURE:
    case DELETE_STANDING_ORDER_WEIGHT_FAILURE:
      return failReducer(state, action)

    case CREATE_STANDING_ORDER_WEIGHT_SUCCESS:
      return createStandingOrderWeight(state, action)

    case FETCH_ALL_STANDING_ORDER_WEIGHTS_SUCCESS:
      return fetchAllStandingOrderWeights(state, action)

    case FETCH_STANDING_ORDER_WEIGHT_SUCCESS:
      return fetchStandingOrderWeight(state, action)

    case UPDATE_STANDING_ORDER_WEIGHT_SUCCESS:
      return updateStandingOrderWeight(state, action)

    case DELETE_STANDING_ORDER_WEIGHT_SUCCESS:
      return deleteStandingOrderWeight(state, action)

    default:
      return state
  }
}
