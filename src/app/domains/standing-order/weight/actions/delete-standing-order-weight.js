import {
  DELETE_STANDING_ORDER_WEIGHT,
  DELETE_STANDING_ORDER_WEIGHT_FAILURE,
  DELETE_STANDING_ORDER_WEIGHT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const deleteStandingOrderWeight = (
  patientId,
  standingOrderWeightId
) => dispatch => {
  dispatch({
    type: DELETE_STANDING_ORDER_WEIGHT
  })

  return instance
    .delete(
      `/patients/${patientId}/standing_order_weights/${standingOrderWeightId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_STANDING_ORDER_WEIGHT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_STANDING_ORDER_WEIGHT_FAILURE
          , error: error.response
        })
    )
}
