import {
  FETCH_STANDING_ORDER_WEIGHT,
  FETCH_STANDING_ORDER_WEIGHT_FAILURE,
  FETCH_STANDING_ORDER_WEIGHT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchStandingOrderWeight = (
  patientId,
  standingOrderWeightId
) => dispatch => {
  dispatch({
    type: FETCH_STANDING_ORDER_WEIGHT
  })

  return instance
    .get(
      `patients/${patientId}/standing_order_weights/${standingOrderWeightId}`
    )
    .then(
      response =>
        dispatch({
          type: FETCH_STANDING_ORDER_WEIGHT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_STANDING_ORDER_WEIGHT_FAILURE
          , error: error.response
        })
    )
}
