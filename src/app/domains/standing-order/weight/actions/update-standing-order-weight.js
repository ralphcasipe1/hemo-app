import {
  UPDATE_STANDING_ORDER_WEIGHT,
  UPDATE_STANDING_ORDER_WEIGHT_FAILURE,
  UPDATE_STANDING_ORDER_WEIGHT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const updateStandingOrderWeight = (
  patientId,
  standingOrderWeightId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_STANDING_ORDER_WEIGHT
  })

  return instance
    .patch(
      `/patients/${patientId}/standing_order_weights/${standingOrderWeightId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_STANDING_ORDER_WEIGHT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_STANDING_ORDER_WEIGHT_FAILURE
          , error: error.response
        })
    )
}
