import {
  FETCH_ALL_STANDING_ORDER_WEIGHTS,
  FETCH_ALL_STANDING_ORDER_WEIGHTS_FAILURE,
  FETCH_ALL_STANDING_ORDER_WEIGHTS_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchAllStandingOrderWeights = patientId => dispatch => {
  dispatch({
    type: FETCH_ALL_STANDING_ORDER_WEIGHTS
  })

  return instance.get(`/patients/${patientId}/standing_order_weights`).then(
    response =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_WEIGHTS_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_WEIGHTS_FAILURE
        , error: error.response
      })
  )
}
