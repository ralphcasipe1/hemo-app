import {
  CREATE_STANDING_ORDER_WEIGHT,
  CREATE_STANDING_ORDER_WEIGHT_FAILURE,
  CREATE_STANDING_ORDER_WEIGHT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const createStandingOrderWeight = (patientId, values) => dispatch => {
  dispatch({
    type: CREATE_STANDING_ORDER_WEIGHT
  })

  return instance
    .post(`/patients/${patientId}/standing_order_weights`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_STANDING_ORDER_WEIGHT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_STANDING_ORDER_WEIGHT_FAILURE
          , error: error.response
        })
    )
}
