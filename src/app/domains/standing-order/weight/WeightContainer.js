import { connect } from 'react-redux'
import {
  createStandingOrderWeight,
  deleteStandingOrderWeight,
  fetchAllStandingOrderWeights
} from './actions'
import { Weight } from './Weight'

const mapStateToProps = state => {
  const { standingOrderWeights } = state.standingOrderWeightReducer

  return {
    standingOrderWeights
  }
}

export const WeightContainer = connect(
  mapStateToProps,
  {
    create: createStandingOrderWeight
    , deleteStandingOrderWeight
    , fetchAllData: fetchAllStandingOrderWeights
  }
)(Weight)
