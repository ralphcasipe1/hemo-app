import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { InputField } from 'app/common/forms/InputField'

class WeightForm extends PureComponent {
  render() {
    const {
      formLoading,
      handleSubmit,
      onFormSubmit,
      submitting,
      valid,
      onCancel,
      disableCancel
    } = this.props

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Field
          component={InputField}
          inputLabel="kg"
          label="DRY Weight"
          labelPosition="right"
          name="dry_weight"
          placeholder="DRY Weight"
          type="number"
        />

        <ButtonDecision
          expanded
          isDisable={!valid || submitting}
          onCancel={onCancel}
          disableCancel={disableCancel}
        />
      </Form>
    )
  }
}

const validate = values => {
  const errors = {}

  if (!values.dry_weight) {
    errors.dry_weight = 'Required'
  }

  return errors
}

WeightForm = reduxForm({
  field: ['dry_weight']
  , validate
})(WeightForm)

export { WeightForm }
