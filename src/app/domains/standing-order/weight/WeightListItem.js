import React            from 'react'
import moment           from 'moment'
import { Button }       from 'semantic-ui-react'
import { CardList }     from '../../../common/display/CardList'
import { CardListItem } from '../../../common/display/CardListItem'

export const WeightListItem = ({
  standingOrderWeight,
  onClickDelete,
  onClickEdit
}) => (
  <CardList>
    <CardListItem
      label="Created at"
      value={moment(standingOrderWeight.created_at).format('ll')}
    />
    <CardListItem label="Dry Weight" value={standingOrderWeight.dry_weight} />

    <Button color="green" content="Edit" icon="edit" onClick={onClickEdit} />
    <Button
      color="red"
      content="Delete"
      icon="trash"
      onClick={() => onClickDelete(standingOrderWeight.template_weight_id)}
    />
  </CardList>
)
