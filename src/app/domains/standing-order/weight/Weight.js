import React, { Component, Fragment } from 'react'
import { Button, Modal }              from 'semantic-ui-react'
import { StandingOrderHOC }           from '../StandingOrderHOC'
import { WeightForm }                 from './WeightForm'
import { WeightList }                 from './WeightList'

class Weight extends Component {
  state = {
    openModal: false
    , id: null
    , openForm: false
  };

  handleDelete = () => {
    this.props.deleteStandingOrderWeight(
      this.props.match.params.patientId,
      this.state.id
    )

    this.closeModal()
  };

  closeModal = () => this.setState({ openModal: false, id: null });

  openModal = id => this.setState({ openModal: true, id });

  openForm = () => this.setState({ openForm: true });

  closeForm = () => this.setState({ openForm: false });

  render() {
    const { handleSubmit, standingOrderWeights } = this.props
    const { openModal, openForm } = this.state

    return (
      <Fragment>
        {standingOrderWeights.length === 0 || openForm ? (
          <WeightForm
            disableCancel={standingOrderWeights.length === 0}
            form="CreateStandingOrderWeightForm"
            onCancel={this.closeForm}
            onFormSubmit={handleSubmit}
          />
        ) : (
          <div>
            <Modal open={openModal} onClose={this.closeModal} basic>
              <Modal.Content>Do you want to delete it?</Modal.Content>
              <Modal.Actions>
                <Button color="red" icon="delete" onClick={this.closeModal} />
                <Button
                  color="green"
                  icon="checkmark"
                  onClick={this.handleDelete}
                />
              </Modal.Actions>
            </Modal>
            <Button
              content="Add New"
              icon="plus"
              primary
              onClick={this.openForm}
            />
            <WeightList
              standingOrderWeights={standingOrderWeights}
              onClickDelete={this.openModal}
            />
          </div>
        )}
      </Fragment>
    )
  }
}

Weight = StandingOrderHOC(Weight)

export { Weight }
