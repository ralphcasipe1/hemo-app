import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { toInt } from 'app/common/helpers/to-int'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { InputField } from 'app/common/forms/InputField'

class ParameterForm extends PureComponent {
  render() {
    const {
      disableCancel,
      handleSubmit,
      onFormSubmit,
      submitting,
      valid,
      onCancel
    } = this.props

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field
          component={InputField}
          inputLabel="hours"
          label="Duration"
          labelPosition="right"
          name="duration"
          parse={toInt}
          placeholder="Duration"
        />

        <Field
          component={InputField}
          inputLabel="ml/min"
          label="Qb (BFR)"
          labelPosition="right"
          placeholder="Blood Flow Rate"
          name="blood_flow_rate"
          parse={toInt}
        />

        <Field
          component={InputField}
          label="Dialysate Bath"
          placeholder="Dialysate Bath"
          name="dialysate_bath"
        />

        <Field
          component={InputField}
          label="Dialysate Additives"
          placeholder="Dialysate Additives"
          name="dialysate_additive"
        />

        <Field
          component={InputField}
          label="Qb (DFR)"
          placeholder="Dialysate Flow Rate"
          name="dialysate_flow_rate"
          parse={toInt}
        />

        <ButtonDecision
          expanded
          isDisable={!valid || submitting}
          onCancel={onCancel}
          disableCancel={disableCancel}
        />
      </Form>
    )
  }
}

const validate = values => {
  const errors = {}

  if (!values.duration) {
    errors.duration = 'Required'
  } else if (!values.duration) {
    errors.duration = 'The duration is too much...'
  }

  if (!values.blood_flow_rate) {
    errors.blood_flow_rate = 'Required'
  }

  if (!values.dialysate_bath) {
    errors.dialysate_bath = 'Required'
  }

  if (!values.dialysate_flow_rate) {
    errors.dialysate_flow_rate = 'Required'
  }

  return errors
}

const warn = () => {
  const warnings = {}

  return warnings
}

ParameterForm = reduxForm({
  fields: [
    'duration'
    , 'blood_flow_rate'
    , 'dialysate_bath'
    , 'dialysate_additive'
    , 'dialysate_flow_rate'
  ]
  , validate
  , warn
})(ParameterForm)

export { ParameterForm }
