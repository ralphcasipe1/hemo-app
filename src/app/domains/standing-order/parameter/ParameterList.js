import React                 from 'react'
import { ParameterListItem } from './ParameterListItem'

export const ParameterList = props => (
  <div>
    {props.standingOrderParameters.map(standingOrderParameter => (
      <ParameterListItem
        key={standingOrderParameter.template_parameter_id}
        standingOrderParameter={standingOrderParameter}
        {...props}
      />
    ))}
  </div>
)
