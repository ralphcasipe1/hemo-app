import React            from 'react'
import moment           from 'moment'
import { Button }       from 'semantic-ui-react'
import { CardList }     from '../../../common/display/CardList'
import { CardListItem } from '../../../common/display/CardListItem'

export const ParameterListItem = ({
  standingOrderParameter,
  onClickDelete,
  onClickEdit
}) => (
  <CardList>
    <CardListItem
      label="Created at"
      value={moment(standingOrderParameter.created_at).format('ll')}
    />

    <CardListItem label="Duration" value={standingOrderParameter.duration} />

    <CardListItem
      label="Blood Flow Rate"
      value={standingOrderParameter.blood_flow_rate}
    />

    <CardListItem
      label="Dialysate Bath"
      value={standingOrderParameter.dialysate_bath}
    />

    <CardListItem
      label="Dialysate Additive"
      value={standingOrderParameter.dialysate_additive}
    />

    <CardListItem
      label="Dialysate Flow Rate"
      value={standingOrderParameter.dialysate_flow_rate}
    />

    <Button color="green" content="Edit" icon="edit" onClick={() => onClickEdit(standingOrderParameter.template_parameter_id)} />
    <Button
      color="red"
      content="Delete"
      icon="trash"
      onClick={() =>
        onClickDelete(standingOrderParameter.template_parameter_id)
      }
    />
  </CardList>
)
