import { loadReducer } from '../../../../common/helpers/load-reducer'
import { failReducer } from '../../../../common/helpers/fail-reducer'
import {
  CREATE_STANDING_ORDER_PARAMETER,
  CREATE_STANDING_ORDER_PARAMETER_FAILURE,
  CREATE_STANDING_ORDER_PARAMETER_SUCCESS,
  DELETE_STANDING_ORDER_PARAMETER,
  DELETE_STANDING_ORDER_PARAMETER_FAILURE,
  DELETE_STANDING_ORDER_PARAMETER_SUCCESS,
  FETCH_ALL_STANDING_ORDER_PARAMETERS,
  FETCH_ALL_STANDING_ORDER_PARAMETERS_FAILURE,
  FETCH_ALL_STANDING_ORDER_PARAMETERS_SUCCESS,
  FETCH_STANDING_ORDER_PARAMETER,
  FETCH_STANDING_ORDER_PARAMETER_FAILURE,
  FETCH_STANDING_ORDER_PARAMETER_SUCCESS
} from '../constants'
import { createStandingOrderParameter }    from './create-standing-order-parameter'
import { deleteStandingOrderParameter }    from './delete-standing-order-parameter'
import { fetchAllStandingOrderParameters } from './fetch-all-standing-order-parameters'
import { fetchStandingOrderParameter }     from './fetch-standing-order-parameter'

const initialState = {
  error: null
  , isLoading: false
  , standingOrderParameter: null
  , standingOrderParameters: []
}

export const standingOrderParameterReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_STANDING_ORDER_PARAMETER:
    case DELETE_STANDING_ORDER_PARAMETER:
    case FETCH_ALL_STANDING_ORDER_PARAMETERS:
    case FETCH_STANDING_ORDER_PARAMETER:
      return loadReducer(state)

    case CREATE_STANDING_ORDER_PARAMETER_FAILURE:
    case DELETE_STANDING_ORDER_PARAMETER_FAILURE:
    case FETCH_ALL_STANDING_ORDER_PARAMETERS_FAILURE:
    case FETCH_STANDING_ORDER_PARAMETER_FAILURE:
      return failReducer(state, action)

    case CREATE_STANDING_ORDER_PARAMETER_SUCCESS:
      return createStandingOrderParameter(state, action)

    case DELETE_STANDING_ORDER_PARAMETER_SUCCESS:
      return deleteStandingOrderParameter(state, action)

    case FETCH_ALL_STANDING_ORDER_PARAMETERS_SUCCESS:
      return fetchAllStandingOrderParameters(state, action)

    case FETCH_STANDING_ORDER_PARAMETER_SUCCESS:
      return fetchStandingOrderParameter(state, action)
      
    default:
      return state
  }
}
