export const deleteStandingOrderParameter = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderParameters: state.standingOrderParameters.filter(
    standingOrderParameter =>
      standingOrderParameter.template_parameter_id !==
      action.payload.template_parameter_id
  )
})
