export const createStandingOrderParameter = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderParameters: [ action.payload, ...state.standingOrderParameters ]
})
