export const fetchStandingOrderParameter = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderParameter: action.payload
})
