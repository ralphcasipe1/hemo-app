export const fetchAllStandingOrderParameters = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderParameters: action.payload
})
