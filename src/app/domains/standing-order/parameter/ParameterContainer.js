import { connect } from 'react-redux'
import {
  createStandingOrderParameter,
  deleteStandingOrderParameter,
  fetchStandingOrderParameter,
  fetchAllStandingOrderParameters
} from './actions'
import { Parameter } from './Parameter'

const mapStateToProps = state => {
  const { standingOrderParameter, standingOrderParameters } = state.standingOrderParameterReducer

  return {
    standingOrderParameter
    , standingOrderParameters
  }
}

export const ParameterContainer = connect(
  mapStateToProps,
  {
    create: createStandingOrderParameter
    , deleteStandingOrderParameter
    , fetchStandingOrderParameter
    , fetchAllData: fetchAllStandingOrderParameters
  }
)(Parameter)
