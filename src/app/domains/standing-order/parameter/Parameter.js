import React, { Component, Fragment } from 'react'
import type { Element }               from 'react'
import { Button, Modal }              from 'semantic-ui-react'
import { StandingOrderHOC }           from '../StandingOrderHOC'
import { ParameterForm }              from './ParameterForm'
import { ParameterList }              from './ParameterList'

type PropType = {
  standingOrderParameters: Array<any>,
  handleSubmit: Function
};

type StateType = {
  id: ?number,
  openCreateForm: boolean,
  openModal: boolean
};

class Parameter extends Component<PropType, StateType> {
  state = {
    openModal: false
    , id: null
    , openCreateForm: false
    , openEditForm: false
  };

  handleDelete = () => {
    this.props.deleteStandingOrderParameter(
      this.props.match.params.patientId,
      this.state.id
    )

    this.closeModal()
  };

  closeModal = (): StateType => this.setState({ openModal: false, id: null });

  openModal = (id: number): StateType => this.setState({ openModal: true, id });

  openCreateForm = (): StateType => this.setState({ openCreateForm: true });

  closeCreateForm = (): StateType => this.setState({ openCreateForm: false });

  closeEditForm = () => this.setState({ openEditForm: false, id: null })

  openEditForm = (id: number) => {
    this.setState({ openEditForm: true })
    this.props.fetchStandingOrderParameter(
      this.props.match.params.patientId,
      id
    )
  }
  
  render(): Element<'div'> {
    const { handleSubmit, standingOrderParameter, standingOrderParameters } = this.props
    const { openModal, openCreateForm, openEditForm } = this.state

    if (openEditForm && !!standingOrderParameter) {
      return (
        <ParameterForm
          form="EditStandingOrderForm"
          onCancel={this.closeEditForm}
          onFormSubmit={handleSubmit}
          initialValues={{
            duration: standingOrderParameter.duration
            , blood_flow_rate: standingOrderParameter.blood_flow_rate
            , dialysate_bath: standingOrderParameter.dialysate_bath
            , dialysate_additive: standingOrderParameter.dialysate_additive
            , dialysate_flow_rate: standingOrderParameter.dialysate_flow_rate
          }}
        />
      )
    }
    return (
      <Fragment>
        {standingOrderParameters.length === 0 || openCreateForm ? (
          <ParameterForm
            disableCancel={standingOrderParameters.length === 0}
            form="CreateStandingOrderParameterForm"
            onCancel={this.closeCreateForm}
            onFormSubmit={handleSubmit}
          />
        ) : (
          <div>
            <Modal open={openModal} onClose={this.closeModal} basic>
              <Modal.Content>Do you want to delete it?</Modal.Content>
              <Modal.Actions>
                <Button color="red" icon="delete" onClick={this.closeModal} />
                <Button
                  color="green"
                  icon="checkmark"
                  onClick={this.handleDelete}
                />
              </Modal.Actions>
            </Modal>

            <Button
              content="Add New"
              icon="plus"
              primary
              onClick={this.openCreateForm}
            />

            <ParameterList
              standingOrderParameters={standingOrderParameters}
              onClickDelete={this.openModal}
              onClickEdit={this.openEditForm}
            />
          </div>
        )}
      </Fragment>
    )
  }
}

Parameter = StandingOrderHOC(Parameter)

export { Parameter }
