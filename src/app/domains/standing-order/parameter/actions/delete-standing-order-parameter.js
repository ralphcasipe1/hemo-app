import {
  DELETE_STANDING_ORDER_PARAMETER,
  DELETE_STANDING_ORDER_PARAMETER_FAILURE,
  DELETE_STANDING_ORDER_PARAMETER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const deleteStandingOrderParameter = (
  patientId,
  standingOrderParameterId
) => dispatch => {
  dispatch({
    type: DELETE_STANDING_ORDER_PARAMETER
  })

  return instance
    .delete(
      `/patients/${patientId}/standing_order_parameters/${standingOrderParameterId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_STANDING_ORDER_PARAMETER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_STANDING_ORDER_PARAMETER_FAILURE
          , error: error.response
        })
    )
}
