import {
  FETCH_ALL_STANDING_ORDER_PARAMETERS,
  FETCH_ALL_STANDING_ORDER_PARAMETERS_FAILURE,
  FETCH_ALL_STANDING_ORDER_PARAMETERS_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchAllStandingOrderParameters = patientId => dispatch => {
  dispatch({
    type: FETCH_ALL_STANDING_ORDER_PARAMETERS
  })

  instance.get(`/patients/${patientId}/standing_order_parameters`).then(
    response =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_PARAMETERS_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_PARAMETERS_FAILURE
        , error: error.response
      })
  )
}
