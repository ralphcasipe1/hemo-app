import {
  FETCH_STANDING_ORDER_PARAMETER,
  FETCH_STANDING_ORDER_PARAMETER_FAILURE,
  FETCH_STANDING_ORDER_PARAMETER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchStandingOrderParameter = (patientId, standingOrderParameterId)=> dispatch => {
  dispatch({
    type: FETCH_STANDING_ORDER_PARAMETER
  })

  instance.get(`/patients/${patientId}/standing_order_parameters/${standingOrderParameterId}`).then(
    response =>
      dispatch({
        type: FETCH_STANDING_ORDER_PARAMETER_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_STANDING_ORDER_PARAMETER_FAILURE
        , error: error.response
      })
  )
}
