import {
  UPDATE_STANDING_ORDER_PARAMETER,
  UPDATE_STANDING_ORDER_PARAMETER_FAILURE,
  UPDATE_STANDING_ORDER_PARAMETER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const updateStandingOrderParameter = (
  patientId,
  standingOrderParameterId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_STANDING_ORDER_PARAMETER
  })

  return instance
    .patch(
      `/patients/${patientId}/standing_order_parameters/${standingOrderParameterId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_STANDING_ORDER_PARAMETER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_STANDING_ORDER_PARAMETER_FAILURE
          , error: error.response
        })
    )
}
