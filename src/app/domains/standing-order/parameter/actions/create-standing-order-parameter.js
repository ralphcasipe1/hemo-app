import {
  CREATE_STANDING_ORDER_PARAMETER,
  CREATE_STANDING_ORDER_PARAMETER_FAILURE,
  CREATE_STANDING_ORDER_PARAMETER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const createStandingOrderParameter = (patientId, values) => dispatch => {
  dispatch({
    type: CREATE_STANDING_ORDER_PARAMETER
  })

  instance.post(`patients/${patientId}/standing_order_parameters`, values).then(
    response =>
      dispatch({
        type: CREATE_STANDING_ORDER_PARAMETER_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: CREATE_STANDING_ORDER_PARAMETER_FAILURE
        , error: error.response
      })
  )
}
