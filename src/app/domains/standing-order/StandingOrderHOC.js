import React, { Component } from 'react'
import { Container }        from 'semantic-ui-react'

export const StandingOrderHOC = WrappedComponent =>
  class extends Component {
    componentDidMount() {
      this.props.fetchAllData(this.props.match.params.patientId)
    }

    handleSubmit = values =>
      this.props.create(this.props.match.params.patientId, values);

    render() {
      return (
        <Container>
          <WrappedComponent {...this.props} handleSubmit={this.handleSubmit} />
        </Container>
      )
    }
  }
