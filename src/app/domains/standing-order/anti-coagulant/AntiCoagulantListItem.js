import React            from 'react'
import moment           from 'moment'
import { Button }       from 'semantic-ui-react'
import { CardList }     from '../../../common/display/CardList'
import { CardListItem } from '../../../common/display/CardListItem'

export const AntiCoagulantListItem = ({
  standingOrderAntiCoagulant,
  onClickDelete,
  onClickEdit
}) => (
  <CardList>
    <CardListItem
      label="Created at"
      value={moment(standingOrderAntiCoagulant.created_at).format('ll')}
    />
    <CardListItem
      label="NSS Flushing"
      value={`${standingOrderAntiCoagulant.nss_flushing} ${
        standingOrderAntiCoagulant.nss_flushing_every
      }`}
    />
    <CardListItem
      label="LMWH (IV)"
      value={`${standingOrderAntiCoagulant.lmwh_iv} ${
        standingOrderAntiCoagulant.lmwh_iv_iu
      }`}
    />
    <CardListItem
      label="UFH (IV)"
      value={`${standingOrderAntiCoagulant.ufh_iv} ${
        standingOrderAntiCoagulant.ufh_iv_iu
      } ${standingOrderAntiCoagulant.ufh_iv_iu_every}`}
    />
    <CardListItem
      label="Bleeding Desc"
      value={standingOrderAntiCoagulant.bleeding_desc}
    />

    <Button color="green" content="Edit" icon="edit" onClick={onClickEdit} />
    <Button
      color="red"
      content="Delete"
      icon="trash"
      onClick={() =>
        onClickDelete(standingOrderAntiCoagulant.template_anti_coagulant_id)
      }
    />
  </CardList>
)
