import {
  CREATE_STANDING_ORDER_ANTI_COAGULANT,
  CREATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  CREATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const createStandingOrderAntiCoagulant = (
  patientId,
  values
) => dispatch => {
  dispatch({
    type: CREATE_STANDING_ORDER_ANTI_COAGULANT
  })

  return instance
    .post(`/patients/${patientId}/standing_order_anti_coagulants`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE
          , error: error.response
        })
    )
}
