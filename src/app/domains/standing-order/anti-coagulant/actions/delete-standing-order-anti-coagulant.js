import {
  DELETE_STANDING_ORDER_ANTI_COAGULANT,
  DELETE_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  DELETE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const deleteStandingOrderAntiCoagulant = (
  patientId,
  standingOrderAntiCoagulantId
) => dispatch => {
  dispatch({
    type: DELETE_STANDING_ORDER_ANTI_COAGULANT
  })

  return instance
    .delete(
      `/patients/${patientId}/standing_order_anti_coagulants/${standingOrderAntiCoagulantId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_STANDING_ORDER_ANTI_COAGULANT_FAILURE
          , error: error.response
        })
    )
}
