import {
  UPDATE_STANDING_ORDER_ANTI_COAGULANT,
  UPDATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  UPDATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const updateStandingOrderAntiCoagulant = (
  patientRegistryId,
  standingOrderAntiCoagulantId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_STANDING_ORDER_ANTI_COAGULANT
  })

  return instance
    .patch(
      `/patients/${patientRegistryId}/standing_order_anti_coagulants/${standingOrderAntiCoagulantId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE
          , error: error.response
        })
    )
}
