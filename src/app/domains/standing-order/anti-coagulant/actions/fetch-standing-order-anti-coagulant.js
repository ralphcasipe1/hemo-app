import {
  FETCH_STANDING_ORDER_ANTI_COAGULANT,
  FETCH_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  FETCH_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchStandingAntiCoagulant = (
  patientId,
  standingOrderAntiCoagulantId
) => dispatch => {
  dispatch({
    type: FETCH_STANDING_ORDER_ANTI_COAGULANT
  })

  return instance
    .get(
      `/patients/${patientId}/standing_order_anti_coagulants/${standingOrderAntiCoagulantId}`
    )
    .then(
      response =>
        dispatch({
          type: FETCH_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_STANDING_ORDER_ANTI_COAGULANT_FAILURE
          , error: error.response
        })
    )
}
