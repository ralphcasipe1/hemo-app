import {
  FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS,
  FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_FAILURE,
  FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchAllStandingOrderAntiCoagulants = patientId => dispatch => {
  dispatch({
    type: FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS
  })

  return instance
    .get(`/patients/${patientId}/standing_order_anti_coagulants`)
    .then(
      response =>
        dispatch({
          type: FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_FAILURE
          , error: error.response
        })
    )
}
