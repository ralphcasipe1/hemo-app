import React, { Component, Fragment } from 'react'
import type { Element }               from 'react'
import { Button, Modal }              from 'semantic-ui-react'
import { StandingOrderHOC }           from '../StandingOrderHOC'
import { AntiCoagulantForm }          from './AntiCoagulantForm'
import { AntiCoagulantList }          from './AntiCoagulantList'

type PropType = {
  standingOrderParameters: Array<any>,
  handleSubmit: Function
};

type StateType = {
  id: ?number,
  openForm: boolean,
  openModal: boolean
};

class AntiCoagulant extends Component<PropType, StateType> {
  state = {
    openModal: false
    , id: null
    , openForm: false
  };

  handleDelete = () => {
    this.props.deleteStandingOrderAntiCoagulant(
      this.props.match.params.patientId,
      this.state.id
    )

    this.closeModal()
  };

  closeModal = (): StateType => this.setState({ openModal: false, id: null });

  openModal = (id: number): StateType => this.setState({ openModal: true, id });

  openForm = (): StateType => this.setState({ openForm: true });

  closeForm = (): StateType => this.setState({ openForm: false });

  render(): Element<'div'> {
    const { handleSubmit, standingOrderAntiCoagulants } = this.props
    const { openModal, openForm } = this.state
    
    return (
      <Fragment>
        {standingOrderAntiCoagulants.length === 0 || openForm ? (
          <AntiCoagulantForm
            disableCancel={standingOrderAntiCoagulants.length === 0}
            form="CreateStandingOrderAntiCoagulantForm"
            onCancel={this.closeForm}
            onFormSubmit={handleSubmit}
          />
        ) : (
          <div>
            <Modal open={openModal} onClose={this.closeModal} basic>
              <Modal.Content>Do you want to delete it?</Modal.Content>
              <Modal.Actions>
                <Button color="red" icon="delete" onClick={this.closeModal} />
                <Button
                  color="green"
                  icon="checkmark"
                  onClick={this.handleDelete}
                />
              </Modal.Actions>
            </Modal>

            <Button
              content="Add New"
              icon="plus"
              primary
              onClick={this.openForm}
            />

            <AntiCoagulantList
              onClickDelete={this.openModal}
              standingOrderAntiCoagulants={standingOrderAntiCoagulants}
            />
          </div>
        )}
      </Fragment>
    )
  }
}

AntiCoagulant = StandingOrderHOC(AntiCoagulant)

export { AntiCoagulant }
