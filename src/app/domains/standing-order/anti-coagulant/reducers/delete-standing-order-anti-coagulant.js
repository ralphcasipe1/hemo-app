export const deleteStandingOrderAntiCoagulant = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderAntiCoagulants: state.standingOrderAntiCoagulants.filter(
    standingOrderAntiCoagulant =>
      standingOrderAntiCoagulant.template_anti_coagulant_id !==
      action.payload.template_anti_coagulant_id
  )
})
