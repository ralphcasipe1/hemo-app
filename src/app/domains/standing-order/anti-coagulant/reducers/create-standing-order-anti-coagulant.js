export const createStandingOrderAntiCoagulant = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderAntiCoagulants: [
    action.payload
    , ...state.standingOrderAntiCoagulants
  ]
})
