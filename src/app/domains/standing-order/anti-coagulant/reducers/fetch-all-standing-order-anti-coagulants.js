export const fetchAllStandingOrderAntiCoagulants = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderAntiCoagulants: action.payload
})
