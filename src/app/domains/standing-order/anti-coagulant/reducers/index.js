import { loadReducer } from '../../../../common/helpers/load-reducer'
import { failReducer } from '../../../../common/helpers/fail-reducer'
import {
  CREATE_STANDING_ORDER_ANTI_COAGULANT,
  CREATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  CREATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS,
  DELETE_STANDING_ORDER_ANTI_COAGULANT,
  DELETE_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  DELETE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS,
  FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS,
  FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_FAILURE,
  FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_SUCCESS,
  FETCH_STANDING_ORDER_ANTI_COAGULANT,
  FETCH_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  FETCH_STANDING_ORDER_ANTI_COAGULANT_SUCCESS,
  UPDATE_STANDING_ORDER_ANTI_COAGULANT,
  UPDATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE,
  UPDATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS
} from '../constants'
import { createStandingOrderAntiCoagulant }    from './create-standing-order-anti-coagulant'
import { deleteStandingOrderAntiCoagulant }    from './delete-standing-order-anti-coagulant'
import { fetchAllStandingOrderAntiCoagulants } from './fetch-all-standing-order-anti-coagulants'
import { fetchStandingOrderAntiCoagulant }     from './fetch-standing-order-anti-coagulant'
import { updateStandingOrderAntiCoagulant }    from './update-standing-order-anti-coagulant'

const initialState = {
  error: null
  , isLoading: false
  , standingOrderAntiCoagulant: null
  , standingOrderAntiCoagulants: []
}

export const standingOrderAntiCoagulantReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case CREATE_STANDING_ORDER_ANTI_COAGULANT:
    case FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS:
    case FETCH_STANDING_ORDER_ANTI_COAGULANT:
    case UPDATE_STANDING_ORDER_ANTI_COAGULANT:
    case DELETE_STANDING_ORDER_ANTI_COAGULANT:
      return loadReducer(state)

    case CREATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE:
    case FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_FAILURE:
    case FETCH_STANDING_ORDER_ANTI_COAGULANT_FAILURE:
    case UPDATE_STANDING_ORDER_ANTI_COAGULANT_FAILURE:
    case DELETE_STANDING_ORDER_ANTI_COAGULANT_FAILURE:
      return failReducer(state, action)

    case CREATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS:
      return createStandingOrderAntiCoagulant(state, action)

    case FETCH_ALL_STANDING_ORDER_ANTI_COAGULANTS_SUCCESS:
      return fetchAllStandingOrderAntiCoagulants(state, action)

    case FETCH_STANDING_ORDER_ANTI_COAGULANT_SUCCESS:
      return fetchStandingOrderAntiCoagulant(state, action)

    case UPDATE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS:
      return updateStandingOrderAntiCoagulant(state, action)

    case DELETE_STANDING_ORDER_ANTI_COAGULANT_SUCCESS:
      return deleteStandingOrderAntiCoagulant(state, action)

    default:
      return state
  }
}
