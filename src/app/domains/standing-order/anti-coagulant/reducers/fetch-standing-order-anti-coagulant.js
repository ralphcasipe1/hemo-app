export const fetchStandingOrderAntiCoagulant = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderAntiCoagulant: action.payload
})
