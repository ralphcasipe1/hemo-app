export const updateStandingOrderAntiCoagulant = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderAntiCoagulants: state.standingOrderAntiCoagulants.map(
    standingOrderAntiCoagulant => {
      if (
        standingOrderAntiCoagulant.template_anti_coagulant_id !==
        action.payload.template_anti_coagulant_id
      ) {
        return standingOrderAntiCoagulant
      }

      return Object.assign({}, standingOrderAntiCoagulant, {
        nss_flushing: action.payload.nss_flushing
        , nss_flushing_every: action.payload.nss_flushing_every
        , lmwh_iv: action.payload.lmwh_iv
        , lmwh_iv_iu: action.payload.lmwh_iv_iu
        , ufh_iv: action.payload.ufh_iv
        , ufh_iv_iu: action.payload.ufh_iv_iu
        , ufh_iv_iu_every: action.payload.ufh_iv_iu_every
        , bleeding_desc: action.payload.bleeding_desc
      })
    }
  )
})
