import { connect } from 'react-redux'
import {
  createStandingOrderAntiCoagulant,
  deleteStandingOrderAntiCoagulant,
  fetchAllStandingOrderAntiCoagulants
} from './actions'
import { AntiCoagulant } from './AntiCoagulant'

const mapStateToProps = state => {
  const {
    standingOrderAntiCoagulants
  } = state.standingOrderAntiCoagulantReducer

  return {
    standingOrderAntiCoagulants
  }
}

export const AntiCoagulantContainer = connect(
  mapStateToProps,
  {
    create: createStandingOrderAntiCoagulant
    , deleteStandingOrderAntiCoagulant
    , fetchAllData: fetchAllStandingOrderAntiCoagulants
  }
)(AntiCoagulant)
