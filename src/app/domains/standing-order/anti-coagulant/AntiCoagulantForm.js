import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { isAggregatedValues } from 'app/common/helpers/is-aggregated-values'
import { toInt } from 'app/common/helpers/to-int'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { InputField } from 'app/common/forms/InputField'
import { TextAreaField } from 'app/common/forms/TextAreaField'

class AntiCoagulantForm extends PureComponent {
  render() {
    const {
      handleSubmit,
      loading,
      onFormSubmit,
      submitting,
      valid,
      disableCancel,
      onCancel
    } = this.props

    return (
      <Form
        className="attached"
        onSubmit={handleSubmit(onFormSubmit)}
        loading={loading}
        size="large"
      >
        <Field
          component={InputField}
          inputLabel="ml"
          label="NSS Flushing"
          labelPosition="right"
          name="nss_flushing"
          ref="nss_flushing"
          withRef
        />

        <Field
          component={InputField}
          inputLabel="mins"
          label="Every"
          labelPosition="right"
          name="nss_flushing_every"
          ref="nss_flushing_every"
          withRef
        />

        <Field
          component={InputField}
          label="LMWH (IV)"
          name="lmwh_iv"
          ref="lmwh_iv"
          withRef
        />

        <Field
          component={InputField}
          label="IU"
          name="lmwh_iv_iu"
          parse={toInt}
          ref="lmwh_iv_iu"
          withRef
        />

        <Field
          component={InputField}
          label="UFH (IV)"
          name="ufh_iv"
          ref="ufh_iv"
          withRef
        />

        <Field
          component={InputField}
          label="IU"
          name="ufh_iv_iu"
          ref="ufh_iv_iu"
          withRef
        />

        {/* No every */}
        <Field
          component={InputField}
          label="Every"
          name="ufh_iv_iu_every"
          ref="ufh_iv_iu_every"
          withRef
        />

        <Field
          component={TextAreaField}
          name="bleeding_desc"
          label="Bleeding description"
        />

        <ButtonDecision
          expanded
          isDisable={!valid || submitting}
          disableCancel={disableCancel}
          onCancel={onCancel}
        />
      </Form>
    )
  }
}

const validate = values => {
  let errors = {}

  if (
    isAggregatedValues(values.nss_flushing, values.nss_flushing_every) === false
  ) {
    errors.nss_flushing = 'Needs data if the other one is present'
    errors.nss_flushing_every = 'Needs data if the other one is present'
  }

  if (isAggregatedValues(values.lmwh_iv, values.lmwh_iv_iu) === false) {
    errors.lmwh_iv = 'Needs data if the other one is present'
    errors.lmwh_iv_iu = 'Needs data if the other one is present'
  }

  if (isAggregatedValues(values.ufh_iv, values.ufh_iv_iu) === false) {
    errors.ufh_iv = 'Needs data if the other one is present'
    errors.ufh_iv_iu = 'Needs data if the other one is present'
  }

  return errors
}

AntiCoagulantForm = reduxForm({
  field: [
    'nss_flushing'
    , 'nss_flushing_every'
    , 'lmwh_iv'
    , 'lmwh_iv_iu'
    , 'ufh_iv'
    , 'ufh_iv_iu'
    , 'ufh_iv_iu_every'
    , 'bleeding_desc'
  ]
  , validate
})(AntiCoagulantForm)

export { AntiCoagulantForm }
