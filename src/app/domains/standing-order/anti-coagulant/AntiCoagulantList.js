import React                     from 'react'
import { AntiCoagulantListItem } from './AntiCoagulantListItem'

export const AntiCoagulantList = props => (
  <div>
    {props.standingOrderAntiCoagulants.map(standingOrderAntiCoagulant => (
      <AntiCoagulantListItem
        key={standingOrderAntiCoagulant.template_anti_coagulant_id}
        standingOrderAntiCoagulant={standingOrderAntiCoagulant}
        {...props}
      />
    ))}
  </div>
)
