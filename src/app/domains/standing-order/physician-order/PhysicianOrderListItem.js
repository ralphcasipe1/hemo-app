import React from 'react';
import moment from 'moment';
import { CardList } from '../../../common/display/CardList';
import { CardListItem } from '../../../common/display/CardListItem';

export const PhysicianOrderListItem = ({ standingOrderPhysicianOrder }) => (
  <CardList>
    <CardListItem
      label="Created at"
      value={moment(standingOrderPhysicianOrder.created_at).format('ll')}
    />
  </CardList>
);
