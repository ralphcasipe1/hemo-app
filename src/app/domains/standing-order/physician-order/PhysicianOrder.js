import React, { Component, Fragment } from 'react';
import { StandingOrderHOC } from '../StandingOrderHOC';
import { PhysicianOrderList } from './PhysicianOrderList';

class PhysicianOrder extends Component {
  render() {
    const { standingOrderPhysicianOrders } = this.props;

    return (
      <Fragment>
        <PhysicianOrderList
          standingOrderPhysicianOrders={standingOrderPhysicianOrders}
        />
      </Fragment>
    );
  }
}

PhysicianOrder = StandingOrderHOC(PhysicianOrder);

export { PhysicianOrder };
