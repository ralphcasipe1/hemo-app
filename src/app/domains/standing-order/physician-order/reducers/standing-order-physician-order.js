import {
  CREATE_STANDING_ORDER_PHYSICIAN_ORDER,
  CREATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE,
  CREATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS,
  DELETE_STANDING_ORDER_PHYSICIAN_ORDER,
  DELETE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE,
  DELETE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS,
  FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS,
  FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_FAILURE,
  FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_SUCCESS,
  UPDATE_STANDING_ORDER_PHYSICIAN_ORDER,
  UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE,
  UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS
} from '../constants/standing-order-physician-order';

const initialState = {
  error: null
  , isLoading: false
  , standingOrderPhysicianOrders: []
};

export const standingOrderPhysicianOrderReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case CREATE_STANDING_ORDER_PHYSICIAN_ORDER:
    case DELETE_STANDING_ORDER_PHYSICIAN_ORDER:
    case FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS:
    case UPDATE_STANDING_ORDER_PHYSICIAN_ORDER:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE:
    case DELETE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE:
    case FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_FAILURE:
    case UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      };

    case CREATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state
        , isLoading: false
        , standingOrderPhysicianOrders: [
          action.payload
          , ...state.standingOrderPhysicianOrders
        ]
      };

    case FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , standingOrderPhysicianOrders: action.payload
      };

    case UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state
      };

    case DELETE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state
      };

    default:
      return state;
  }
};
