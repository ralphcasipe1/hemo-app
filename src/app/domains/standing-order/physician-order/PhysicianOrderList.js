import React from 'react';
import { PhysicianOrderListItem } from './PhysicianOrderListItem';

export const PhysicianOrderList = ({ standingOrderPhysicianOrders }) => (
  <div>
    {standingOrderPhysicianOrders.map(standingOrderPhysicianOrder => (
      <PhysicianOrderListItem
        key={standingOrderPhysicianOrder.template_physician_order_id}
        standingOrderPhysicianOrder={standingOrderPhysicianOrder}
      />
    ))}
  </div>
);
