import { connect } from 'react-redux';
import { fetchAllStandingOrderPhysicianOrders } from './actions/standing-order-physician-order';
import { PhysicianOrder } from './PhysicianOrder';

const mapStateToProps = state => {
  const {
    standingOrderPhysicianOrders
  } = state.standingOrderPhysicianOrderReducer;

  return {
    standingOrderPhysicianOrders
  };
};

export const PhysicianOrderContainer = connect(
  mapStateToProps,
  {
    fetchAllData: fetchAllStandingOrderPhysicianOrders
  }
)(PhysicianOrder);
