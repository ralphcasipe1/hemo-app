import {
  CREATE_STANDING_ORDER_PHYSICIAN_ORDER,
  CREATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE,
  CREATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS,
  DELETE_STANDING_ORDER_PHYSICIAN_ORDER,
  DELETE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE,
  DELETE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS,
  FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS,
  FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_FAILURE,
  FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_SUCCESS,
  UPDATE_STANDING_ORDER_PHYSICIAN_ORDER,
  UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE,
  UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS
} from '../constants/standing-order-physician-order';
import { instance } from '../../../../config/connection';

export const createStandingOrderPhysicianOrder = (
  patientId,
  props
) => dispatch => {
  dispatch({
    type: CREATE_STANDING_ORDER_PHYSICIAN_ORDER
  });

  instance
    .post(`/patients/${patientId}/standing_order_physician_orders`, props)
    .then(
      response =>
        dispatch({
          type: CREATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE
          , error: error.response
        })
    );
};

export const fetchAllStandingOrderPhysicianOrders = patientId => dispatch => {
  dispatch({
    type: FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS
  });

  instance.get(`patients/${patientId}/standing_order_physician_orders`).then(
    response =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_PHYSICIAN_ORDERS_FAILURE
        , error: error.response
      })
  );
};

export const updateStandingOrderPhysicianOrder = (
  patientId,
  standingOrderPhysicianOrderId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_STANDING_ORDER_PHYSICIAN_ORDER
  });

  instance
    .patch(
      `patients/${patientId}/standing_order_physician_orders/${standingOrderPhysicianOrderId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE
          , error: error.response
        })
    );
};

export const deleteStandingOrderPhysicianOrder = (
  patientId,
  standingOrderPhysicianOrderId
) => dispatch => {
  dispatch({
    type: DELETE_STANDING_ORDER_PHYSICIAN_ORDER
  });

  instance
    .delete(
      `patients/${patientId}/standing_order_physician_orders/${standingOrderPhysicianOrderId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_STANDING_ORDER_PHYSICIAN_ORDER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_STANDING_ORDER_PHYSICIAN_ORDER_FAILURE
          , error: error.response
        })
    );
};
