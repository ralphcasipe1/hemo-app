import React            from 'react'
import _                from 'lodash'
import moment           from 'moment'
import { Button }       from 'semantic-ui-react'
import { CardList }     from '../../../common/display/CardList'
import { CardListItem } from '../../../common/display/CardListItem'

export const MedicationListItem = ({
  standingOrderMedication,
  onClickDelete,
  onClickEdit
}) => (
  <CardList>
    <CardListItem
      label="Created at"
      value={moment(standingOrderMedication.created_at).format('ll')}
    />

    <CardListItem
      label="Generic (Brand):"
      value={`${_.lowerCase(standingOrderMedication.generic)} ${_.upperFirst(
        _.lowerCase(standingOrderMedication.brand)
      )}`}
    />

    <CardListItem
      label="Preparation"
      value={standingOrderMedication.preparation}
    />

    <CardListItem label="Dosage" value={standingOrderMedication.dosage} />

    <CardListItem label="Route" value={standingOrderMedication.route} />

    <CardListItem label="Timing" value={standingOrderMedication.timing} />

    <Button color="green" content="Edit" icon="edit" onClick={onClickEdit} />
    <Button
      color="red"
      content="Delete"
      icon="trash"
      onClick={() =>
        onClickDelete(standingOrderMedication.template_medication_id)
      }
    />
  </CardList>
)
