import {
  CREATE_STANDING_ORDER_MEDICATION,
  CREATE_STANDING_ORDER_MEDICATION_FAILURE,
  CREATE_STANDING_ORDER_MEDICATION_SUCCESS,
  DELETE_STANDING_ORDER_MEDICATION,
  DELETE_STANDING_ORDER_MEDICATION_FAILURE,
  DELETE_STANDING_ORDER_MEDICATION_SUCCESS,
  FETCH_ALL_STANDING_ORDER_MEDICATIONS,
  FETCH_ALL_STANDING_ORDER_MEDICATIONS_FAILURE,
  FETCH_ALL_STANDING_ORDER_MEDICATIONS_SUCCESS,
  FETCH_STANDING_ORDER_MEDICATION,
  FETCH_STANDING_ORDER_MEDICATION_FAILURE,
  FETCH_STANDING_ORDER_MEDICATION_SUCCESS,
  UPDATE_STANDING_ORDER_MEDICATION,
  UPDATE_STANDING_ORDER_MEDICATION_FAILURE,
  UPDATE_STANDING_ORDER_MEDICATION_SUCCESS,
} from '../constants/standing-order-medication';

const initialState = {
  standingOrderMedications: [],
  standingOrderMedication: null,
  isLoading: false,
  error: null,
};

export const standingOrderMedicationReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case CREATE_STANDING_ORDER_MEDICATION:
    case FETCH_ALL_STANDING_ORDER_MEDICATIONS:
    case FETCH_STANDING_ORDER_MEDICATION:
    case UPDATE_STANDING_ORDER_MEDICATION:
    case DELETE_STANDING_ORDER_MEDICATION:
      return {
        ...state,
        isLoading: true,
      };

    case CREATE_STANDING_ORDER_MEDICATION_FAILURE:
    case FETCH_ALL_STANDING_ORDER_MEDICATIONS_FAILURE:
    case FETCH_STANDING_ORDER_MEDICATION_FAILURE:
    case UPDATE_STANDING_ORDER_MEDICATION_FAILURE:
    case DELETE_STANDING_ORDER_MEDICATION_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    case CREATE_STANDING_ORDER_MEDICATION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        standingOrderMedications: [
          action.payload,
          ...state.standingOrderMedications,
        ],
      };

    case FETCH_ALL_STANDING_ORDER_MEDICATIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        standingOrderMedications: action.payload,
      };

    case FETCH_STANDING_ORDER_MEDICATION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        standingOrderMedication: action.payload,
      };

    case UPDATE_STANDING_ORDER_MEDICATION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        standingOrderMedications: state.standingOrderMedications.map(
          (standingOrderMedication) => {
            if (
              standingOrderMedication.template_medication_id !==
              action.payload.template_medication_id
            ) {
              return standingOrderMedication;
            }

            return Object.assign({}, standingOrderMedication, {
              generic: action.payload.generic,
              brand: action.payload.brand,
              preparation: action.payload.preparation,
              dosage: action.payload.dosage,
              route: action.payload.route,
              timing: action.payload.timing,
            });
          }
        ),
      };

    case DELETE_STANDING_ORDER_MEDICATION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        standingOrderMedications: state.standingOrderMedications.filter(
          (standingOrderMedication) =>
            standingOrderMedication.template_medication_id !==
            action.payload.template_medication_id
        ),
      };

    default:
      return state;
  }
};
