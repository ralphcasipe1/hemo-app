import React, { Component } from 'react';
import { AddOrBack } from './AddOrBack';
import { Header, Segment } from 'semantic-ui-react';
import { SOMedicationList } from './SOMedicationList';
import SOMedicationForm from './SOMedicationForm';

class Medication extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openForm: false
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.openForm = this.openForm.bind(this);
  }

  handleFormSubmit = values => {
    this.props.createSOMedication(this.props.patientId, values);

    this.setState({
      openForm: false
    });
  };

  openForm() {
    this.setState({
      openForm: !this.state.openForm
    });
  }

  componentDidMount() {
    this.props.fetchAllSOMedications(this.props.patientId);
  }

  render() {
    const { isLoading, sOMedications } = this.props;
    const { openForm } = this.state;

    return (
      <Segment loading={isLoading}>
        <Header content="Standing Order Medication" />

        <AddOrBack onClick={this.openForm} openForm={openForm} />
        {openForm ? (
          <SOMedicationForm
            form="SOMedicationForm"
            onFormSubmit={this.handleFormSubmit}
          />
        ) : (
          <SOMedicationList sOMedications={sOMedications} />
        )}
      </Segment>
    );
  }
}

export { Medication };
