import {
  CREATE_STANDING_ORDER_MEDICATION,
  CREATE_STANDING_ORDER_MEDICATION_FAILURE,
  CREATE_STANDING_ORDER_MEDICATION_SUCCESS,
  DELETE_STANDING_ORDER_MEDICATION,
  DELETE_STANDING_ORDER_MEDICATION_FAILURE,
  DELETE_STANDING_ORDER_MEDICATION_SUCCESS,
  FETCH_ALL_STANDING_ORDER_MEDICATIONS,
  FETCH_ALL_STANDING_ORDER_MEDICATIONS_FAILURE,
  FETCH_ALL_STANDING_ORDER_MEDICATIONS_SUCCESS,
  FETCH_STANDING_ORDER_MEDICATION,
  FETCH_STANDING_ORDER_MEDICATION_FAILURE,
  FETCH_STANDING_ORDER_MEDICATION_SUCCESS,
  UPDATE_STANDING_ORDER_MEDICATION,
  UPDATE_STANDING_ORDER_MEDICATION_FAILURE,
  UPDATE_STANDING_ORDER_MEDICATION_SUCCESS
} from '../constants/standing-order-medication';
import { instance } from '../../../../config/connection';

export const fetchAllStandingOrderMedications = patientId => dispatch => {
  dispatch({
    type: FETCH_ALL_STANDING_ORDER_MEDICATIONS
  });

  return instance.get(`/patients/${patientId}/standing_order_medications`).then(
    response =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_MEDICATIONS_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_MEDICATIONS_FAILURE
        , error: error.response
      })
  );
};

export const fetchStandingOrderMedication = (
  patientId,
  standingOrderMedicationId
) => dispatch => {
  dispatch({
    type: FETCH_STANDING_ORDER_MEDICATION
  });

  return instance
    .get(
      `/patients/${patientId}/standing_order_medications/${standingOrderMedicationId}`
    )
    .then(
      response =>
        dispatch({
          type: FETCH_STANDING_ORDER_MEDICATION_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_STANDING_ORDER_MEDICATION_FAILURE
          , error: error.response
        })
    );
};

export const deleteStandingOrderMedication = (
  patientId,
  standingOrderMedicationId
) => dispatch => {
  dispatch({
    type: DELETE_STANDING_ORDER_MEDICATION
  });

  return instance
    .delete(
      `/patients/${patientId}/standing_order_medications/${standingOrderMedicationId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_STANDING_ORDER_MEDICATION_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_STANDING_ORDER_MEDICATION_FAILURE
          , error: error.response
        })
    );
};

export const createStandingOrderMedication = (
  patientId,
  values
) => dispatch => {
  dispatch({
    type: CREATE_STANDING_ORDER_MEDICATION
  });

  return instance
    .post(`/patients/${patientId}/standing_order_medications`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_STANDING_ORDER_MEDICATION_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_STANDING_ORDER_MEDICATION_FAILURE
          , error: error.response
        })
    );
};

export const updateStandingOrderMedication = (
  patientId,
  standingOrderMedicationId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_STANDING_ORDER_MEDICATION
  });

  return instance
    .patch(
      `/patients/${patientId}/standing_order_medications/${standingOrderMedicationId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_STANDING_ORDER_MEDICATION_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_STANDING_ORDER_MEDICATION_FAILURE
          , error: error.response
        })
    );
};
