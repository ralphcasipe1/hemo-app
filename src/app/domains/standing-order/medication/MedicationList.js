import React, { PureComponent } from 'react'
import { List }                 from 'semantic-ui-react'
import { MedicationListItem }   from './MedicationListItem'

class MedicationList extends PureComponent {
  render() {
    const { standingOrderMedications } = this.props
    return (
      <List divided relaxed>
        {standingOrderMedications.map(standingOrderMedication => (
          <MedicationListItem
            key={standingOrderMedication.template_medication_id}
            standingOrderMedication={standingOrderMedication}
            {...this.props}
          />
        ))}
      </List>
    )
  }
}

export { MedicationList }
