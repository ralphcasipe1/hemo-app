import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import { ButtonDecision } from '../../../common/forms/ButtonDecision';
import { InputField } from '../../../common/forms/InputField';

class MedicationForm extends PureComponent {
  render() {
    return (
      <Form
        loading={this.props.formLoading}
        onSubmit={this.props.handleSubmit(this.props.onFormSubmit)}
      >
        <Field component={InputField} name="generic" label="Generic" />
        <Field component={InputField} name="brand" label="Brand" />
        <Field component={InputField} name="preparation" label="Preparation" />
        <Field component={InputField} name="dosage" label="Dosage" />
        <Field component={InputField} name="route" label="Route" />
        <Field component={InputField} name="timing" label="Timing" />
        <ButtonDecision
          expanded
          isDisable={!this.props.valid || this.props.submitting}
          {...this.props}
        />
      </Form>
    );
  }
}

const validate = values => {
  let errors = {};

  if (!values.generic) {
    errors.generic = 'Required';
  }

  if (!values.brand) {
    errors.brand = 'Required';
  }

  if (!values.preparation) {
    errors.preparation = 'Required';
  }

  return errors;
};

MedicationForm = reduxForm({
  fields: [ 'brand', 'generic', 'preparation', 'dosage', 'route', 'timing' ]
  , validate
})(MedicationForm);

export { MedicationForm };
