import { connect } from 'react-redux'
import {
  createStandingOrderMedication,
  deleteStandingOrderMedication,
  fetchAllStandingOrderMedications
} from './actions/standing-order-medication'
import { Medication } from './Medication'

const mapStateToProps = state => {
  const { standingOrderMedications } = state.standingOrderMedicationReducer

  return {
    standingOrderMedications
  }
}

export const MedicationContainer = connect(
  mapStateToProps,
  {
    create: createStandingOrderMedication
    , deleteStandingOrderMedication
    , fetchAllData: fetchAllStandingOrderMedications
  }
)(Medication)
