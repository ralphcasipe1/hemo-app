import React            from 'react'
import moment           from 'moment'
import { Button }       from 'semantic-ui-react'
import { CardList }     from '../../../common/display/CardList'
import { CardListItem } from '../../../common/display/CardListItem'

export const DialyzerListItem = ({
  standingOrderDialyzer,
  onClickDelete,
  onClickEdit
}) => (
  <CardList>
    <CardListItem
      label="Created at"
      value={moment(standingOrderDialyzer.created_at).format('ll')}
    />

    <CardListItem
      label="Brand"
      value={standingOrderDialyzer.dialyzerBrand.brand_name}
    />

    <CardListItem label="Type" value={standingOrderDialyzer.type} />

    <CardListItem
      label="Size"
      value={standingOrderDialyzer.dialyzerSize.size_name}
    />
    <CardListItem label="Date" value={standingOrderDialyzer.dialyzer_date} />

    <CardListItem label="count" value={standingOrderDialyzer.count} />

    <CardListItem label="D/t" value={standingOrderDialyzer.d_t} />

    <Button color="green" content="Edit" icon="edit" onClick={onClickEdit} />
    <Button
      color="red"
      content="Delete"
      icon="trash"
      onClick={() => onClickDelete(standingOrderDialyzer.template_dialyzer_id)}
    />
  </CardList>
)
