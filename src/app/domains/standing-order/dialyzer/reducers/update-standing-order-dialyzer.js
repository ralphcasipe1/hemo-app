export const updateStandingOrderDialyzer = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderDialyzers: state.standingOrderDialyzers.map(
    standingOrderDialyzer => {
      if (
        standingOrderDialyzer.template_dialyzer_id !==
        action.payload.template_dialyzer_id
      ) {
        return standingOrderDialyzer
      }

      return Object.assign({}, standingOrderDialyzer, {
        dialyzer_brand_id: action.payload.dialyzer_brand_id
        , type: action.payload.type
        , dialyzer_size_id: action.payload.dialyzer_size_id
        , dialyzer_date: action.payload.dialyzer_date
        , count: action.payload.count
        , d_t: action.payload.d_t
      })
    }
  )
})
