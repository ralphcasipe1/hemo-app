export const createStandingOrderDialyzer = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderDialyzers: [ action.payload, ...state.standingOrderDialyzers ]
})
