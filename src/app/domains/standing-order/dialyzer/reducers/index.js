import { loadReducer } from '../../../../common/helpers/load-reducer'
import { failReducer } from '../../../../common/helpers/fail-reducer'
import {
  CREATE_STANDING_ORDER_DIALYZER,
  CREATE_STANDING_ORDER_DIALYZER_FAILURE,
  CREATE_STANDING_ORDER_DIALYZER_SUCCESS,
  DELETE_STANDING_ORDER_DIALYZER,
  DELETE_STANDING_ORDER_DIALYZER_FAILURE,
  DELETE_STANDING_ORDER_DIALYZER_SUCCESS,
  FETCH_ALL_STANDING_ORDER_DIALYZERS,
  FETCH_ALL_STANDING_ORDER_DIALYZERS_FAILURE,
  FETCH_ALL_STANDING_ORDER_DIALYZERS_SUCCESS,
  FETCH_STANDING_ORDER_DIALYZER,
  FETCH_STANDING_ORDER_DIALYZER_FAILURE,
  FETCH_STANDING_ORDER_DIALYZER_SUCCESS,
  UPDATE_STANDING_ORDER_DIALYZER,
  UPDATE_STANDING_ORDER_DIALYZER_FAILURE,
  UPDATE_STANDING_ORDER_DIALYZER_SUCCESS
} from '../constants'
import { createStandingOrderDialyzer } from './create-standing-order-dialyzer'
import { deleteStandingOrderDialyzer } from './delete-standing-order-dialyzer'
import { fetchAllStandingOrderDialyzers } from './fetch-all-standing-order-dialyzers'
import { fetchStandingOrderDialyzer } from './fetch-standing-order-dialyzer'
import { updateStandingOrderDialyzer } from './update-standing-order-dialyzer'

const initialState = {
  error: null
  , isLoading: false
  , standingOrderDialyzers: []
  , standingOrderDialyzer: null
}

export const standingOrderDialyzerReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_STANDING_ORDER_DIALYZER:
    case FETCH_STANDING_ORDER_DIALYZER:
    case FETCH_ALL_STANDING_ORDER_DIALYZERS:
    case UPDATE_STANDING_ORDER_DIALYZER:
    case DELETE_STANDING_ORDER_DIALYZER:
      return loadReducer(state)

    case CREATE_STANDING_ORDER_DIALYZER_FAILURE:
    case FETCH_STANDING_ORDER_DIALYZER_FAILURE:
    case FETCH_ALL_STANDING_ORDER_DIALYZERS_FAILURE:
    case UPDATE_STANDING_ORDER_DIALYZER_FAILURE:
    case DELETE_STANDING_ORDER_DIALYZER_FAILURE:
      return failReducer(state, action)

    case CREATE_STANDING_ORDER_DIALYZER_SUCCESS:
      return createStandingOrderDialyzer(state, action)

    case FETCH_ALL_STANDING_ORDER_DIALYZERS_SUCCESS:
      return fetchAllStandingOrderDialyzers(state, action)

    case FETCH_STANDING_ORDER_DIALYZER_SUCCESS:
      return fetchStandingOrderDialyzer(state, action)

    case UPDATE_STANDING_ORDER_DIALYZER_SUCCESS:
      return updateStandingOrderDialyzer(state, action)

    case DELETE_STANDING_ORDER_DIALYZER_SUCCESS:
      return deleteStandingOrderDialyzer(state, action)

    default:
      return state
  }
}
