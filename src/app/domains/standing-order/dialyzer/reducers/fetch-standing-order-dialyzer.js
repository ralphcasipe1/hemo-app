export const fetchStandingOrderDialyzer = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderDialyzer: action.payload
})
