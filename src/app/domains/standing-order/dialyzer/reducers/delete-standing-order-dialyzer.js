export const deleteStandingOrderDialyzer = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderDialyzers: state.standingOrderDialyzers.filter(
    standingOrderDialyzers =>
      standingOrderDialyzers.template_dialyzer_id !==
      action.payload.template_dialyzer_id
  )
})
