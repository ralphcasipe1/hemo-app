export const fetchAllStandingOrderDialyzers = (state, action) => ({
  ...state
  , isLoading: false
  , standingOrderDialyzers: action.payload
})
