import { connect }                from 'react-redux'
import { fetchAllDialyzerBrands } from 'app/domains/admin/dialyzer-brand/actions'
import { fetchAllDialyzerSizes }  from 'app/domains/admin/dialyzer-size/actions/dialyzer-size'
import {
  createStandingOrderDialyzer,
  deleteStandingOrderDialyzer,
  fetchAllStandingOrderDialyzers
} from './actions'
import { Dialyzer } from './Dialyzer'

const mapStateToProps = state => {
  const { standingOrderDialyzers } = state.standingOrderDialyzerReducer

  return {
    standingOrderDialyzers
    , dialyzerBrands: state.dialyzerBrandReducer.dialyzerBrands
    , dialyzerSizes: state.dialyzerSizeReducer.dialyzerSizes
  }
}

export const DialyzerContainer = connect(
  mapStateToProps,
  {
    create: createStandingOrderDialyzer
    , deleteStandingOrderDialyzer
    , fetchAllDialyzerBrands
    , fetchAllDialyzerSizes
    , fetchAllData: fetchAllStandingOrderDialyzers
  }
)(Dialyzer)
