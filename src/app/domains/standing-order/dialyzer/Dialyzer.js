import React, { Component, Fragment } from 'react'
import type { Element }               from 'react'
import { Button, Modal }              from 'semantic-ui-react'
import { StandingOrderHOC }           from '../StandingOrderHOC'
import { DialyzerForm }               from './DialyzerForm'
import { DialyzerList }               from './DialyzerList'

type PropType = {
  standingOrderParameters: Array<any>,
  handleSubmit: Function
};

type StateType = {
  id: ?number,
  openForm: boolean,
  openModal: boolean
};

class Dialyzer extends Component<PropType, StateType> {
  state = {
    openModal: false
    , id: null
    , openForm: false
  };

  handleDelete = () => {
    this.props.deleteStandingOrderDialyzer(
      this.props.match.params.patientId,
      this.state.id
    )

    this.closeModal()
  };

  closeModal = (): StateType => this.setState({ openModal: false, id: null });

  openModal = (id: number): StateType => this.setState({ openModal: true, id });

  openForm = (): StateType => this.setState({ openForm: true });

  closeForm = (): StateType => this.setState({ openForm: false });

  componentDidMount() {
    this.props.fetchAllDialyzerBrands()
    this.props.fetchAllDialyzerSizes()
  }

  render(): Element<'div'> {
    const {
      dialyzerBrands,
      dialyzerSizes,
      handleSubmit,
      standingOrderDialyzers
    } = this.props
    const { openModal, openForm } = this.state

    return (
      <Fragment>
        {standingOrderDialyzers.length === 0 || openForm ? (
          <DialyzerForm
            dialyzerBrands={dialyzerBrands}
            dialyzerSizes={dialyzerSizes}
            disableCancel={standingOrderDialyzers.length === 0}
            form="CreateStandingOrderDialyzerForm"
            onCancel={this.closeForm}
            onFormSubmit={handleSubmit}
          />
        ) : (
          <div>
            <Modal open={openModal} onClose={this.closeModal} basic>
              <Modal.Content>Do you want to delete it?</Modal.Content>
              <Modal.Actions>
                <Button color="red" icon="delete" onClick={this.closeModal} />
                <Button
                  color="green"
                  icon="checkmark"
                  onClick={this.handleDelete}
                />
              </Modal.Actions>
            </Modal>

            <Button
              content="Add New"
              icon="plus"
              primary
              onClick={this.openForm}
            />

            <DialyzerList
              standingOrderDialyzers={standingOrderDialyzers}
              onClickDelete={this.openModal}
            />
          </div>
        )}
      </Fragment>
    )
  }
}

Dialyzer = StandingOrderHOC(Dialyzer)

export { Dialyzer }
