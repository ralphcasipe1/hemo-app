import {
  CREATE_STANDING_ORDER_DIALYZER,
  CREATE_STANDING_ORDER_DIALYZER_FAILURE,
  CREATE_STANDING_ORDER_DIALYZER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const createStandingOrderDialyzer = (patientId, values) => dispatch => {
  dispatch({
    type: CREATE_STANDING_ORDER_DIALYZER
  })

  return instance
    .post(`/patients/${patientId}/standing_order_dialyzers`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_STANDING_ORDER_DIALYZER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_STANDING_ORDER_DIALYZER_FAILURE
          , error: error.response
        })
    )
}
