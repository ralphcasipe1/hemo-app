import {
  DELETE_STANDING_ORDER_DIALYZER,
  DELETE_STANDING_ORDER_DIALYZER_FAILURE,
  DELETE_STANDING_ORDER_DIALYZER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const deleteStandingOrderDialyzer = (
  patientId,
  standingOrderDialyzerId
) => dispatch => {
  dispatch({
    type: DELETE_STANDING_ORDER_DIALYZER
  })

  return instance
    .delete(
      `/patients/${patientId}/standing_order_dialyzers/${standingOrderDialyzerId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_STANDING_ORDER_DIALYZER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_STANDING_ORDER_DIALYZER_FAILURE
          , error: error.response
        })
    )
}
