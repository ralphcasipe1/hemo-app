import {
  FETCH_ALL_STANDING_ORDER_DIALYZERS,
  FETCH_ALL_STANDING_ORDER_DIALYZERS_FAILURE,
  FETCH_ALL_STANDING_ORDER_DIALYZERS_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchAllStandingOrderDialyzers = patientId => dispatch => {
  dispatch({
    type: FETCH_ALL_STANDING_ORDER_DIALYZERS
  })

  return instance.get(`/patients/${patientId}/standing_order_dialyzers`).then(
    response =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_DIALYZERS_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_ALL_STANDING_ORDER_DIALYZERS_FAILURE
        , error: error.response
      })
  )
}
