import {
  FETCH_STANDING_ORDER_DIALYZER,
  FETCH_STANDING_ORDER_DIALYZER_FAILURE,
  FETCH_STANDING_ORDER_DIALYZER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const fetchStandingOrderDialyzer = (
  patientId,
  standingOrderDialyzerId
) => dispatch => {
  dispatch({
    type: FETCH_STANDING_ORDER_DIALYZER
  })

  return instance
    .get(
      `/patients/${patientId}/standing_order_dialyzers/${standingOrderDialyzerId}`
    )
    .then(
      response =>
        dispatch({
          type: FETCH_STANDING_ORDER_DIALYZER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_STANDING_ORDER_DIALYZER_FAILURE
          , error: error.response
        })
    )
}
