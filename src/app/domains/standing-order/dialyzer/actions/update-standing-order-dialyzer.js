import {
  UPDATE_STANDING_ORDER_DIALYZER,
  UPDATE_STANDING_ORDER_DIALYZER_FAILURE,
  UPDATE_STANDING_ORDER_DIALYZER_SUCCESS
} from '../constants'
import { instance } from '../../../../config/connection'

export const updateStandingOrderDialyzer = (
  patientId,
  standingOrderDialyzerId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_STANDING_ORDER_DIALYZER
  })

  return instance
    .patch(
      `/patients/${patientId}/standing_order_dialyzers/${standingOrderDialyzerId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_STANDING_ORDER_DIALYZER_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_STANDING_ORDER_DIALYZER_FAILURE
          , error: error.response
        })
    )
}
