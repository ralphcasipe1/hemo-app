import React                from 'react'
import { DialyzerListItem } from './DialyzerListItem'

export const DialyzerList = props => (
  <div>
    {props.standingOrderDialyzers.map(standingOrderDialyzer => (
      <DialyzerListItem
        key={standingOrderDialyzer.template_dialyzer_id}
        standingOrderDialyzer={standingOrderDialyzer}
        {...props}
      />
    ))}
  </div>
)
