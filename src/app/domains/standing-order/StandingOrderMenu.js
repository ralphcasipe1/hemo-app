import React, { Fragment }          from 'react'
import { NavLink, Route, Switch }   from 'react-router-dom'
import { Menu }                     from 'semantic-ui-react'
import { AntiCoagulantContainer }   from './anti-coagulant/AntiCoagulantContainer'
import { DialyzerContainer }        from './dialyzer/DialyzerContainer'
import { MedicationContainer }      from './medication/MedicationContainer'
import { ParameterContainer }       from './parameter/ParameterContainer'
import { ParameterDetailContainer } from './parameter/ParameterDetailContainer'
import { PhysicianOrderContainer }  from './physician-order/PhysicianOrderContainer'
import { WeightContainer }          from './weight/WeightContainer'

export const StandingOrderMenu = ({
  match: { url },
  location: { pathname }
}) => (
  <Fragment>
    <Menu pointing vertical fluid>
      <Menu.Item
        activeClassName={
          pathname === `${url}/standing_order_parameters` ? 'active' : null
        }
        as={NavLink}
        name="parameter"
        to={`${url}/standing_order_parameters`}
      />
      <Menu.Item
        activeClassName={
          pathname === `${url}/standing_order_weights` ? 'active' : null
        }
        as={NavLink}
        name="weight"
        to={`${url}/standing_order_weights`}
      />

      <Menu.Item
        activeClassName={
          pathname === `${url}/standing_order_anti_coagulants` ? 'active' : null
        }
        as={NavLink}
        name="anti-coagulant"
        to={`${url}/standing_order_anti_coagulants`}
      />
      <Menu.Item
        activeClassName={
          pathname === `${url}/standing_order_dialyzers` ? 'active' : null
        }
        as={NavLink}
        name="dialyzer"
        to={`${url}/standing_order_dialyzers`}
      />
      <Menu.Item
        activeClassName={
          pathname === `${url}/standing_order_medications` ? 'active' : null
        }
        as={NavLink}
        name="medication"
        to={`${url}/standing_order_medications`}
      />
      <Menu.Item
        activeClassName={
          pathname === `${url}/standing_order_physician_orders`
            ? 'active'
            : null
        }
        as={NavLink}
        name="physician order"
        to={`${url}/standing_order_physician_orders`}
      />
    </Menu>

    <Switch>
      <Route
        component={ParameterContainer}
        path="/patients/:patientId/standing_orders/standing_order_parameters"
      />
      <Route
        component={ParameterDetailContainer}
        path="/patients/:patientId/standing_orders/standing_order_parameters/:standingOrderParameterId"
      />
      
      <Route
        component={WeightContainer}
        path="/patients/:patientId/standing_orders/standing_order_weights"
      />

      <Route
        component={AntiCoagulantContainer}
        path="/patients/:patientId/standing_orders/standing_order_anti_coagulants"
      />

      <Route
        component={DialyzerContainer}
        path="/patients/:patientId/standing_orders/standing_order_dialyzers"
      />

      <Route
        component={MedicationContainer}
        path="/patients/:patientId/standing_orders/standing_order_medications"
      />

      <Route
        component={PhysicianOrderContainer}
        path="/patients/:patientId/standing_orders/standing_order_physician_orders"
      />
    </Switch>
  </Fragment>
)
