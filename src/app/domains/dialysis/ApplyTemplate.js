import React, { PureComponent } from 'react'
import { Button, Icon }         from 'semantic-ui-react'

class ApplyTemplate extends PureComponent {
  state = {
    rotated: 'counterclockwise'
    , color: 'red'
  };

  onApply = () => {
    const { rotated } = this.state
    if (rotated === 'counterclockwise') {
      this.setState({ rotated: null, color: 'green' })
    } else {
      this.setState({ rotated: 'counterclockwise', color: 'red' })
    }
  };

  render() {
    const { color, rotated } = this.state

    return (
      <Button onClick={this.onApply} color={color} disabled>
        <Icon name="thumb tack" rotated={rotated} />
        Apply Standing Order
      </Button>
    )
  }
}

export { ApplyTemplate }
