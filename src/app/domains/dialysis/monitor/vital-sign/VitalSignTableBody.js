import React from 'react';
import moment from 'moment';

export const VitalSignTableBody = ({ vitalSigns }) => (
  <tbody>
    {vitalSigns.map(vitalSign => (
      <tr key={vitalSign.vital_sign_id}>
        <td>
          <b>{moment(vitalSign.created_at).format('lll')}</b>
        </td>
        <td>{vitalSign.blood_pressure} mmHg</td>
        <td>{vitalSign.cardiac_arrest} bpm</td>
        <td>{vitalSign.respiratory_rate} cpm</td>
        <td>{vitalSign.temperature} &#x002DA;c</td>
        <td>{vitalSign.oxygen_saturation}%</td>
        <td>{vitalSign.arterial_pressure}</td>
        <td>{vitalSign.venous_pressure}</td>
        <td>{vitalSign.transmembrane_pressure}</td>
        <td>{vitalSign.blood_flow_rate}</td>
        <td>{vitalSign.blood_glucose} mg/dL</td>
        <td>
          {vitalSign.remarks
            ? vitalSign.remarks.split('\n').map((field, index) => (
                <div key={index}>
                  {field}
                  <br />
                </div>
              ))
            : ''}
        </td>
        <td />
      </tr>
    ))}
  </tbody>
);
