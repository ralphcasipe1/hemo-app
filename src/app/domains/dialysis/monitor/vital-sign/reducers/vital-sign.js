import {
  CREATE_VITAL_SIGN,
  CREATE_VITAL_SIGN_FAILURE,
  CREATE_VITAL_SIGN_SUCCESS,

  // DELETE_VITAL_SIGN,
  // DELETE_VITAL_SIGN_FAILURE,
  // DELETE_VITAL_SIGN_SUCCESS,
  FETCH_ALL_VITAL_SIGNS,
  FETCH_ALL_VITAL_SIGNS_FAILURE,
  FETCH_ALL_VITAL_SIGNS_SUCCESS

  // UPDATE_VITAL_SIGN,
  // UPDATE_VITAL_SIGN_FAILURE,
  // UPDATE_VITAL_SIGN_SUCCESS
} from '../constants/vital-sign';

const initialState = {
  vitalSigns: []
  , isLoading: false
  , error: null
};

export const vitalSignReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_VITAL_SIGN:
    case FETCH_ALL_VITAL_SIGNS:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_VITAL_SIGN_FAILURE:
    case FETCH_ALL_VITAL_SIGNS_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      };

    case CREATE_VITAL_SIGN_SUCCESS:
      return {
        ...state
        , isLoading: false
        , vitalSigns: [ action.payload, ...state.vitalSigns ]
      };

    case FETCH_ALL_VITAL_SIGNS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , vitalSigns: action.payload
      };

    default:
      return state;
  }
};
