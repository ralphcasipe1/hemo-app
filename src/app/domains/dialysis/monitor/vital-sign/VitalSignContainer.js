import { connect } from 'react-redux';
import { createVitalSign, fetchAllVitalSigns } from './actions/vital-sign';
import { VitalSign } from './VitalSign';

const mapStateToProps = state => ({
  vitalSigns: state.vitalSignReducer.vitalSigns
});

export const VitalSignContainer = connect(
  mapStateToProps,
  {
    create: createVitalSign
    , fetchAllData: fetchAllVitalSigns
  }
)(VitalSign);
