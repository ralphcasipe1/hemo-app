import React from 'react';
import { VitalSignTableBody } from './VitalSignTableBody';
import { VitalSignTableHead } from './VitalSignTableHead';
// import './VitalSignTable.css';

export const VitalSignTable = ({ vitalSigns }) => (
  <table className="vital-sign-table">
    <VitalSignTableHead />

    <VitalSignTableBody vitalSigns={vitalSigns} />
  </table>
);
