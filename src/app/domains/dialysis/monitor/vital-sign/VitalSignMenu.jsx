import React from 'react';
import { Icon, Menu } from 'semantic-ui-react';

export const VitalSignMenu = ({ active, onMenuItemClick }) => (
  <Menu fluid tabular icon widths={2}>
    <Menu.Item
      name="add vital sign"
      active={active === 'add vital sign'}
      onClick={onMenuItemClick}
    >
      <Icon name="plus" color="blue" />
    </Menu.Item>
    <Menu.Item
      name="view vital signs"
      active={active === 'view vital signs'}
      onClick={onMenuItemClick}
    >
      <Icon name="sticky note" color="blue" />
    </Menu.Item>
  </Menu>
);
