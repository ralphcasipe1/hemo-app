import React from 'react';

export const VitalSignTableHead = () => (
  <thead>
    <tr>
      <th>Date / Time</th>
      <th>BP</th>
      <th>CR</th>
      <th>RR</th>
      <th>Temp</th>
      <th>
        O<sub>2</sub> Sat.
      </th>
      <th>Art. P.</th>
      <th>Ven. P.</th>
      <th>TMP</th>
      <th>Qb</th>
      <th>Blood Glucose</th>
      <th>Remarks</th>
      <th />
    </tr>
  </thead>
);
