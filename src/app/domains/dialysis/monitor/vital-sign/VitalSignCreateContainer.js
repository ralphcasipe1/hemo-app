import { connect } from 'react-redux';
import { createVitalSign } from './actions/vital-sign';
import { VitalSignCreate } from './VitalSignCreate';

const mapStateToProps = state => ({
  vitalSigns: state.vitalSignReducer.vitalSigns
})

export const VitalSignCreateContainer = connect(
  mapStateToProps,
  {
    create: createVitalSign
  }
)(VitalSignCreate)
