import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'semantic-ui-react'
import { VitalSignForm }  from './VitalSignForm'

class VitalSignCreate extends Component {

	handleSubmit = values => {
    this.props.create(this.props.match.params.patientRegistryId, values)
    this.props.history.push(
    	`/patients/${this.props.match.params.patientId}/patient_registries/${this.props.match.params.patientRegistryId}/vital_signs`
		)
  };

	render() {
		return (
			<Fragment>
				<Button
					as={Link}
					to={`/patients/${this.props.match.params.patientId}/patient_registries/${this.props.match.params.patientRegistryId}/vital_signs`}
				>
					Back
				</Button>
				<VitalSignForm
	        form="CreateVitalSignForm"
	        onFormSubmit={this.handleSubmit}
	      />
      </Fragment>
		)
	}
}

export { VitalSignCreate }