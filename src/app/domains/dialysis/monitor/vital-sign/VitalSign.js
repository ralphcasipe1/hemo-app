import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'semantic-ui-react'
import { MonitorHOC }                 from '../MonitorHOC'
import { VitalSignForm }              from './VitalSignForm'
import { VitalSignList }              from './VitalSignList'
// import { VitalSignTable } from './VitalSignTable';

class VitalSign extends Component {
  render() {
    const { handleSubmit, vitalSigns } = this.props
    
    return (
      <Fragment>
        {vitalSigns.length !== 0 
          ? <Button
              as={Link}
              to={`${this.props.location.pathname}/create`}
              primary
            >
              Add
            </Button>
          : null
        }
        

        {vitalSigns.length === 0 ? (
          <VitalSignForm
            form="CreateVitalSignForm"
            onFormSubmit={handleSubmit}
          />
        ) : (
          <VitalSignList vitalSigns={vitalSigns} />
        )}
      </Fragment>
    )
  }
}

VitalSign = MonitorHOC(VitalSign, {
  title: 'Vital Sign'
})

export { VitalSign }
