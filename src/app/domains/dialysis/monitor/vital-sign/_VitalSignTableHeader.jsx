import React from 'react';
import { Table } from 'semantic-ui-react';

export const VitalSignTableHeader = () => (
  <Table.Header>
    <Table.Row>
      <Table.HeaderCell>Date / Time</Table.HeaderCell>
      <Table.HeaderCell>BP</Table.HeaderCell>
      <Table.HeaderCell>CR</Table.HeaderCell>
      <Table.HeaderCell>RR</Table.HeaderCell>
      <Table.HeaderCell>Temp</Table.HeaderCell>
      <Table.HeaderCell>
        O<sub>2</sub> Sat.
      </Table.HeaderCell>
      <Table.HeaderCell>Art. P.</Table.HeaderCell>
      <Table.HeaderCell>Ven. P.</Table.HeaderCell>
      <Table.HeaderCell>TMP</Table.HeaderCell>
      <Table.HeaderCell>Qb</Table.HeaderCell>
      <Table.HeaderCell>Blood Glucose</Table.HeaderCell>
      <Table.HeaderCell>Remarks</Table.HeaderCell>
      <Table.HeaderCell />
    </Table.Row>
  </Table.Header>
);
