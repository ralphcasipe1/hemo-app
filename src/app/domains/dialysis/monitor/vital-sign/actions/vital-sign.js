import {
  CREATE_VITAL_SIGN,
  CREATE_VITAL_SIGN_FAILURE,
  CREATE_VITAL_SIGN_SUCCESS,

  // DELETE_VITAL_SIGN,
  // DELETE_VITAL_SIGN_FAILURE,
  // DELETE_VITAL_SIGN_SUCCESS,
  FETCH_ALL_VITAL_SIGNS,
  FETCH_ALL_VITAL_SIGNS_FAILURE,
  FETCH_ALL_VITAL_SIGNS_SUCCESS

  // UPDATE_VITAL_SIGN,
  // UPDATE_VITAL_SIGN_FAILURE,
  // UPDATE_VITAL_SIGN_SUCCESS
} from '../constants/vital-sign';
import { instance } from '../../../../../config/connection';

export const fetchAllVitalSigns = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_ALL_VITAL_SIGNS
  });

  return instance
    .get(`/patient_registries/${patientRegistryId}/vital_signs`)
    .then(
      response =>
        dispatch({
          type: FETCH_ALL_VITAL_SIGNS_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_ALL_VITAL_SIGNS_FAILURE
          , error: error.response
        })
    );
};

export const createVitalSign = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_VITAL_SIGN
  });

  return instance
    .post(`/patient_registries/${patientRegistryId}/vital_signs`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_VITAL_SIGN_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_VITAL_SIGN_FAILURE
          , error: error.response
        })
    );
};

/* export const selectPatientRegistryVitalSigns = (
  id,
  vitalSignId
) => dispatch => {
  dispatch({
    type: SELECT_PATIENT_REGISTRY_VITAL_SIGN
  });
  instance.get(`patient_registries/${id}/vital_signs/${vitalSignId}`).then(
    res =>
      dispatch({
        type: SELECT_PATIENT_REGISTRY_VITAL_SIGN_SUCCESS,
        payload: res.data
      }),
    err =>
      dispatch({
        type: SELECT_PATIENT_REGISTRY_VITAL_SIGN_FAILURE,
        payload: err.response.data
      })
  );
};

export const resetSelectedPatientRegistryVitalSign = () => ({
  type: RESET_SELECTED_PATIENT_REGISTRY_VITAL_SIGN
});

export const editPatientRegistryVitalSigns = (
  id,
  vitalSignId,
  props
) => dispatch => {
  dispatch({
    type: UPDATE_PATIENT_REGISTRY_VITAL_SIGN
  });
  instance
    .patch(`patient_registries/${id}/vital_signs/${vitalSignId}`, props)
    .then(
      res =>
        dispatch({
          type: UPDATE_PATIENT_REGISTRY_VITAL_SIGN_SUCCESS,
          payload: res.data
        }),
      err =>
        dispatch({
          type: UPDATE_PATIENT_REGISTRY_VITAL_SIGN_FAILURE,
          payload: err.response.data
        })
    );
};

export const resetEditedPatientRegistryVitalSign = () => ({
  type: RESET_UPDATED_PATIENT_REGISTRY_VITAL_SIGN
});

export const deletePatientRegistryVitalSigns = (
  id,
  vitalSignId
) => dispatch => {
  dispatch({
    type: DELETE_PATIENT_REGISTRY_VITAL_SIGN
  });
  instance.delete(`patient_registries/${id}/vital_signs/${vitalSignId}`).then(
    res =>
      dispatch({
        type: DELETE_PATIENT_REGISTRY_VITAL_SIGN_SUCCESS,
        payload: res.data
      }),
    err =>
      dispatch({
        type: DELETE_PATIENT_REGISTRY_VITAL_SIGN_FAILURE,
        payload: err.response.data
      })
  );
};

export const resetDeletedPatientRegistryVitalSign = () => ({
  type: RESET_DELETED_PATIENT_REGISTRY_VITAL_SIGN
});
 */
