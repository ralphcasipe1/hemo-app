import React    from 'react'
import moment   from 'moment'
import { List } from 'semantic-ui-react'

const VitalSignList = ({ vitalSigns }) => (
	<List>
		{vitalSigns.map(vitalSign => (
			<List.Item key={vitalSign.vital_sign_id}>
				<List.Header content={moment(vitalSign.created_at).format('ll')} />
				<List.Content>
					<dl>
						<dt>BP</dt>
						<dd>{vitalSign.blood_pressure}</dd>

						<dt>CR</dt>
						<dd>{vitalSign.cardiac_arrest}</dd>

						<dt>RR</dt>
						<dd>{vitalSign.respiratory_rate}</dd>

						<dt>Temp</dt>
						<dd>{vitalSign.temperature}</dd>

						<dt>O<sub>2</sub> Sat.</dt>
						<dd>{vitalSign.oxygen_saturation}</dd>

						<dt>Art. P.</dt>
						<dd>{vitalSign.arterial_pressure}</dd>

						<dt>Ven. P.</dt>
						<dd>{vitalSign.venous_pressure}</dd>

						<dt>TMP</dt>
						<dd>{vitalSign.transmembrane_pressure}</dd>

						<dt>Qb</dt>
						<dd>{vitalSign.blood_flow_rate}</dd>

						<dt>Blood Glucose</dt>
						<dd>{vitalSign.blood_glucose}</dd>

						<dt>Remarks</dt>
						<dd>{vitalSign.remarks}</dd>
					</dl>
				</List.Content>
			</List.Item>
		))}
	</List>)

export { VitalSignList }