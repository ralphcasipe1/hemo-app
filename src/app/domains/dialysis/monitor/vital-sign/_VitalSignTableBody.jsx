import React from 'react';
import moment from 'moment';
import { Table } from 'semantic-ui-react';

export const VitalSignTableBody = ({ vitalSigns }) => (
  <Table.Body>
    {vitalSigns.map(vitalSign => (
      <Table.Row key={vitalSign.vital_sign_id}>
        <Table.Cell>
          <b>{moment(vitalSign.created_at).format('lll')}</b>
        </Table.Cell>
        <Table.Cell>{vitalSign.blood_pressure} mmHg</Table.Cell>
        <Table.Cell>{vitalSign.cardiac_arrest} bpm</Table.Cell>
        <Table.Cell>{vitalSign.respiratory_rate} cpm</Table.Cell>
        <Table.Cell>{vitalSign.temperature} &#x002DA;c</Table.Cell>
        <Table.Cell>{vitalSign.oxygen_saturation}%</Table.Cell>
        <Table.Cell>{vitalSign.arterial_pressure}</Table.Cell>
        <Table.Cell>{vitalSign.venous_pressure}</Table.Cell>
        <Table.Cell>{vitalSign.transmembrane_pressure}</Table.Cell>
        <Table.Cell>{vitalSign.blood_flow_rate}</Table.Cell>
        <Table.Cell>{vitalSign.blood_glucose} mg/dL</Table.Cell>
        <Table.Cell>
          {vitalSign.remarks
            ? vitalSign.remarks.split('\n').map((field, index) => (
                <div key={index}>
                  {field}
                  <br />
                </div>
              ))
            : ''}
        </Table.Cell>
        <Table.Cell />
      </Table.Row>
    ))}
  </Table.Body>
);

/* <Table.Cell>
          {this.state.deleteVital.indexOf(vitalSign.vital_sign_id) !== -1 ? (
            <div>
              <Button
                basic
                color="blue"
                compact
                icon="delete"
                content="Cancel"
                onClick={() =>
                  this.setState({
                    deleteVital: this.state.vitalSign.filter(
                      id => id !== vitalSign.vital_sign_id
                    )
                  })}
              />
              <Button
                color="red"
                compact
                icon="checkmark"
                content="Yes"
                onClick={() =>
                  this.props.onDeleteClick(
                    this.props.params.patientRegistryId,
                    vitalSign.vital_sign_id
                  )}
              />
            </div>
          ) : (
            <Button.Group widths={2}>
              <Button
                basic
                color="red"
                compact
                icon="trash"
                onClick={() =>
                  this.setState({
                    deleteVital: [
                      ...this.state.deleteVital,
                      vitalSign.vital_sign_id
                    ]
                  })}
                disabled={this.props.isDisabled}
              />
              <Button
                as={Link}
                basic
                color="green"
                compact
                icon="edit"
                to={`/patients/${this.props.params
                  .patientId}/patient_registries/${this.props.params
                  .patientRegistryId}/vital_signs/${vitalSign.vital_sign_id}`}
                disabled={this.props.isDisabled}
              />
            </Button.Group>
          )}
        </Table.Cell> */
