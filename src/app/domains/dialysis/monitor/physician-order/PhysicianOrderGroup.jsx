import { Item } from 'semantic-ui-react';
import PhysicianOrderItem from './PhysicianOrderItem';
import PropTypes from 'prop-types';
import React from 'react';

const { Group } = Item;

const PhysicianOrderGroup = props => (
  <Group divided relaxed="very">
    {props.physicianOrders.map(po => (
      <PhysicianOrderItem
        order={po}
        key={po.physician_order_id}
        onBlur={props.onBlur}
        onYes={props.onYes}
        {...props}
      />
    ))}
  </Group>
);

PhysicianOrderGroup.propTypes = {
  physicianOrders: PropTypes.array
  , onBlur: PropTypes.func.isRequired
  , onYes: PropTypes.func.isRequired
};

export default PhysicianOrderGroup;
