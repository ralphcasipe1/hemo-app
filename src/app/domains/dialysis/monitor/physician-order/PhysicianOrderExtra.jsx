import { Button, Item } from 'semantic-ui-react';
import AskDelete from './Common/AskDelete';
import PhysicianOrderAccess from './PhysicianOrderAccess';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

const { Extra } = Item;

class PhysicianOrderExtra extends PureComponent {
  constructor() {
    super();

    this.state = {
      editing: false
      , deleting: false
    };

    this.editClick = this.editClick.bind(this);
    this.deleteClick = this.deleteClick.bind(this);
  }

  editClick() {
    this.setState({
      editing: !this.state.editing
    });

    this.props.onEdit();
  }

  deleteClick() {
    this.setState({
      deleting: true
    });
  }

  render() {
    const { onYes, order } = this.props;

    const { editing, deleting } = this.state;

    if (deleting) {
      return (
        <AskDelete
          onCancel={() =>
            this.setState({
              deleting: !deleting
            })
          }
          onYes={onYes}
        />
      );
    }

    return (
      <Extra>
        {order.physician_order_status === 'pending' ? (
          <div>
            <Button
              basic={editing ? false : true}
              color={editing ? 'red' : 'green'}
              content={editing ? 'Close' : 'Edit'}
              floated="right"
              icon={editing ? 'cancel' : 'edit'}
              onClick={this.editClick}
              size="small"
            />
            <Button
              basic
              color="red"
              content="Delete"
              floated="right"
              icon="trash"
              onClick={this.deleteClick}
              size="small"
            />

            <PhysicianOrderAccess {...this.props} />
          </div>
        ) : (
          ''
        )}
      </Extra>
    );
  }
}

PhysicianOrderExtra.defaultProps = {
  disablePassword: true
};

PhysicianOrderExtra.propTypes = {
  order: PropTypes.shape({
    physician_order_status: PropTypes.string.isRequired
  })
  , onEdit: PropTypes.func.isRequired
  , onYes: PropTypes.func.isRequired
};

export default PhysicianOrderExtra;
