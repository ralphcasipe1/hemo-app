import { Item } from 'semantic-ui-react';
import _ from 'lodash';
import PhysicianOrderDesc from './PhysicianOrderDesc';
import PhysicianOrderExtra from './PhysicianOrderExtra';
import PhysicianOrderHeader from './PhysicianOrderHeader';
import PhysicianOrderMeta from './PhysicianOrderMeta';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

const { Content, Description } = Item;
class PhysicianOrderItem extends PureComponent {
  constructor() {
    super();

    this.state = {
      editId: []
      , accessId: NaN
      , showPw: false
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleUnameChange = this.handleUnameChange.bind(this);
    this.handlePwChange = this.handlePwChange.bind(this);
  }

  handleBlur(e, patientRegistryId, physicianOrderId) {
    this.props.onBlur(patientRegistryId, physicianOrderId, e);
  }

  handleEdit(id) {
    const { editId } = this.state;

    if (editId.indexOf(id) !== -1) {
      this.setState({
        editId: editId.filter(stateId => stateId !== id)
      });
    } else {
      this.setState({
        editId: [ ...editId, id ]
      });
    }
  }

  handleYes(e, patientRegistryId, physicianOrderId) {
    this.props.onYes(patientRegistryId, physicianOrderId);
  }

  handleOpen(physicianId) {
    this.setState({
      accessId: physicianId
    });
  }

  handleClose() {
    this.setState({
      accessId: NaN
      , showPw: false
    });
  }

  handleUnameChange(e) {
    const unameChangeDelay = _.debounce(
      () =>
        this.setState({
          showPw: true
        }),
      1000
    );

    let fname = _.toLower(this.props.order.physician.fname.charAt(0));
    let lname = _.toLower(this.props.order.physician.lname.charAt(0));
    let year = this.props.order.physician.date_of_birth.charAt(0, 3);

    let username = fname + lname + year;

    if (e.target.value === username) {
      unameChangeDelay();
    }
  }

  handlePwChange(e, patientRegistryId, physicianOrderId) {
    const pWChangeDelay = _.debounce(() => {
      this.props.approve(patientRegistryId, physicianOrderId);
      this.handleClose();
    }, 2000);

    let fname = _.toLower(this.props.order.physician.fname.charAt(0));
    let lname = _.toLower(this.props.order.physician.lname.charAt(0));
    let year = this.props.order.physician.date_of_birth.charAt(0, 3);

    let username = fname + lname + year;

    if (e.target.value === username) {
      pWChangeDelay();
    }
  }

  render() {
    const { order } = this.props;
    const { editId, accessId, showPw } = this.state;

    let editing = editId.indexOf(order.physician_order_id) !== -1;

    return (
      <Item>
        <Content verticalAlign="middle">
          <PhysicianOrderHeader {...this.props} />
          <PhysicianOrderMeta {...this.props} />
          <Description>
            <PhysicianOrderDesc
              desc={order.assessment}
              header="Assessment"
              editing={editing}
              name="assessment"
              onBlur={e =>
                this.handleBlur(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
            />

            <PhysicianOrderDesc
              desc={order.medication}
              header="Medication"
              editing={editing}
              name="medication"
              onBlur={e =>
                this.handleBlur(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
            />

            <PhysicianOrderDesc
              desc={order.procedure}
              header="Procedure"
              editing={editing}
              name="procedure"
              onBlur={e =>
                this.handleBlur(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
            />

            <PhysicianOrderDesc
              desc={order.diagnostic_test}
              header="Diagnostic Test"
              editing={editing}
              name="diagnostic_test"
              onBlur={e =>
                this.handleBlur(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
            />

            <PhysicianOrderDesc
              desc={order.other_remark}
              header="Other remarks"
              editing={editing}
              name="other_remark"
              onBlur={e =>
                this.handleBlur(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
            />

            <PhysicianOrderExtra
              {...this.props}
              disableApprove={order.physician_id === null}
              open={!!accessId}
              onOpen={() => this.handleOpen(order.physician_id)}
              onClose={this.handleClose}
              onEdit={() => this.handleEdit(order.physician_order_id)}
              onYes={e =>
                this.handleYes(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
              onUnameChange={this.handleUnameChange}
              onPwChange={e =>
                this.handlePwChange(
                  e,
                  order.patient_registry_id,
                  order.physician_order_id
                )
              }
              successUname={showPw}
            />
          </Description>
        </Content>
      </Item>
    );
  }
}

PhysicianOrderItem.propTypes = {
  order: PropTypes.shape({
    physician_order_id: PropTypes.number.isRequired
    , assessment: PropTypes.string
    , medication: PropTypes.string
    , procedure: PropTypes.string
    , diagnostic_test: PropTypes.string
    , other_remark: PropTypes.string
  }).isRequired
  , physicianUsername: PropTypes.string.isRequired
};

export default PhysicianOrderItem;
