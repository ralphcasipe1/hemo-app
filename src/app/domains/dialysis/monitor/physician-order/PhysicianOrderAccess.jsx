import { Button, Form, Modal } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PhysicianOrderAccess extends PureComponent {
  render() {
    const {
      onOpen,
      onClose,
      open,
      onUnameChange,
      onPwChange,
      successUname,
      disableApprove
    } = this.props;
    return (
      <Modal
        trigger={
          <Button
            color="blue"
            floated="right"
            onClick={onOpen}
            size="small"
            icon="thumbs up"
            circular
            disabled={disableApprove}
          />
        }
        open={open}
        onClose={onClose}
        closeIcon
      >
        <Modal.Content>
          <Form>
            {successUname ? (
              <Form.Field>
                <Form.Input
                  icon="lock"
                  label="Password"
                  placeholder="Password"
                  type="password"
                  onChange={onPwChange}
                />
              </Form.Field>
            ) : (
              <Form.Field>
                <Form.Input
                  icon="doctor"
                  label="Username"
                  placeholder="Username"
                  onChange={onUnameChange}
                />
              </Form.Field>
            )}
          </Form>
        </Modal.Content>
      </Modal>
    );
  }
}

PhysicianOrderAccess.defaultProps = {
  open: false
  , disableApprove: false
  , successUname: false
};

PhysicianOrderAccess.propTypes = {
  onOpen: PropTypes.func.isRequired
  , onClose: PropTypes.func.isRequired
  , open: PropTypes.bool.isRequired
  , onUnameChange: PropTypes.func.isRequired
  , onPwChange: PropTypes.func.isRequired
  , disableApprove: PropTypes.bool.isRequired
  , successUname: PropTypes.bool.isRequired
};

export default PhysicianOrderAccess;
