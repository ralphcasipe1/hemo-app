import { withRouter } from 'react-router-dom';
import { MODULE } from '../constants/ModuleConstants';
import { ternary } from '../utils/CommonUtils';
import { Segment } from 'semantic-ui-react';
import AddButton from './Common/AddButton';
import PhysicianOrderForm from './PhysicianOrderForm';
import PhysicianOrderGroup from './PhysicianOrderGroup';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PhysicianOrders extends PureComponent {
  constructor() {
    super();

    this.state = {
      DOMContent: MODULE.VIEW
      , showPw: false
    };

    this.handleAdd = this.handleAdd.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleYes = this.handleYes.bind(this);
    this.handleApprove = this.handleApprove.bind(this);
  }

  componentDidMount() {
    this.onInit();
  }

  onInit() {
    const { patientRegistryId } = this.props.params;
    this.props.fetchPatientRegistryPhyOrders(patientRegistryId);
    this.props.fetchPhysicians();
  }

  handleAdd() {
    this.setState({
      DOMContent: MODULE.CREATE
    });
  }

  handleBlur(patientRegistryId, physicianOrderId, { target: { name, value } }) {
    this.props.editPatientRegistryPhyOrder(
      patientRegistryId,
      physicianOrderId,
      {
        [name]: value
      }
    );
  }

  handleCancel() {
    this.setState({
      DOMContent: MODULE.VIEW
    });
  }

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createPatientRegistryPhyOrder(patientRegistryId, values);

    this.handleCancel();
  }

  handleYes(patientRegistryId, physicianOrderId) {
    this.props.deletePatientRegistryPhyOrder(
      patientRegistryId,
      physicianOrderId
    );
  }

  handleApprove(patientRegistryId, physicianOrderId) {
    this.props.editPatientRegistryPhyOrder(
      patientRegistryId,
      physicianOrderId,
      {
        physician_order_status: 'approved'
      }
    );
  }

  render() {
    const { physicians, isLoading } = this.props;
    const { DOMContent } = this.state;

    return (
      <Segment loading={isLoading} basic>
        <AddButton onAdd={this.handleAdd} />

        {ternary(
          DOMContent === 'view',
          <PhysicianOrderGroup
            {...this.props}
            onBlur={this.handleBlur}
            onYes={this.handleYes}
            approve={this.handleApprove}
            physicianUsername="213"
          />,
          <PhysicianOrderForm
            expanded
            onCancel={this.handleCancel}
            onFormSubmit={this.handleFormSubmit}
            form="CreatePhysicianOrderForm"
            physicians={physicians}
          />
        )}
      </Segment>
    );
  }
}

PhysicianOrders.propTypes = {
  physicianOrders: PropTypes.array
  , physicians: PropTypes.array
  , createPatientRegistryPhyOrder: PropTypes.func.isRequired
  , deletePatientRegistryPhyOrder: PropTypes.func.isRequired
  , editPatientRegistryPhyOrder: PropTypes.func.isRequired
  , fetchPatientRegistryPhyOrders: PropTypes.func.isRequired
  , fetchPhysicians: PropTypes.func.isRequired
};

export default withRouter(PhysicianOrders);
