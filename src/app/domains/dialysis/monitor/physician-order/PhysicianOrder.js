import React, { Component } from 'react'

class PhysicianOrder extends Component {

  render() {
    return (
    	<div>
      	<h1>Under Maintenance</h1>
      	<ul>
    			<li>Possible False authorization with doctor's credentials</li>
    		</ul>
      </div>
    )
  }
}

export { PhysicianOrder }