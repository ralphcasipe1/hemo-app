import React from 'react';
import { Icon, Item } from 'semantic-ui-react';
import _ from 'lodash';

const { Header } = Item;
const PhysicianOrderHeader = ({ order }) => (
  <Header>
    {!!order.physician ? (
      <div>
        <Icon name="cart" /> Dr. {order.physician.fname} {order.physician.lname}
      </div>
    ) : (
      ''
    )}
    <div>
      <Icon name="write" /> {_.upperFirst(order.createdBy.lname)},{' '}
      {_.upperFirst(order.createdBy.fname)}{' '}
      {_.upperFirst(order.createdBy.mname)}
    </div>
  </Header>
);

export default PhysicianOrderHeader;
