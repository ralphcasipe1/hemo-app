import { Form, Header, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';
const { TextArea } = Form;

const PhysicianOrderDesc = ({ desc, header, editing, onBlur, name }) => (
  <div>
    {desc ? (
      <div>
        <Header attached="top" content={header} icon="hashtag" as="h4" />
        <Segment attached="bottom" tertiary={editing} basic>
          {editing ? (
            <Form>
              <TextArea
                defaultValue={desc}
                autoHeight
                onBlur={onBlur}
                name={name}
              />
            </Form>
          ) : desc ? (
            desc.split('\n').map((field, index) => (
              <div key={index}>
                {field}
                <br />
              </div>
            ))
          ) : (
            ''
          )}
        </Segment>
      </div>
    ) : (
      ''
    )}
  </div>
);

PhysicianOrderDesc.defaultProps = {
  editing: false
};

PhysicianOrderDesc.propTypes = {
  desc: PropTypes.string
  , header: PropTypes.string.isRequired
  , editing: PropTypes.bool.isRequired
  , onBlur: PropTypes.func.isRequired
  , name: PropTypes.string.isRequired
};

export default PhysicianOrderDesc;
