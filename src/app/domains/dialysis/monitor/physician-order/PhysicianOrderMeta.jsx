import { Item, Label } from 'semantic-ui-react';
import { labelColor } from '../utils/PhysicianOrderUtils';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

const { Meta } = Item;

class PhysicianOrderMeta extends PureComponent {
  render() {
    const { order } = this.props;

    return (
      <Meta>
        <p>{moment(order.updated_at).format('lll')}</p>
        <Label tag color={labelColor(order.physician_order_status)}>
          {_.upperCase(order.physician_order_status)}
        </Label>
      </Meta>
    );
  }
}

PhysicianOrderMeta.propTypes = {
  order: PropTypes.shape({
    updated_at: PropTypes.string.isRequired
    , physician_order_status: PropTypes.string.isRequired
  })
};

export default PhysicianOrderMeta;
