import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { FieldSelect } from 'app/common/forms/FieldSelect'
import { TextAreaField } from 'app/common/forms/TextAreaField'

class PhysicianOrderForm extends PureComponent {
  renderPhysicians(physicians) {
    return physicians.map(physician => ({
      key: physician.physician_id
      , text: `${physician.fname} ${physician.lname}`
      , value: physician.physician_id
    }))
  }

  render() {
    const { handleSubmit, onFormSubmit, formLoading, physicians } = this.props

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Field
          component={TextAreaField}
          type=""
          name="assessment"
          label="Assessment"
        />
        <Field
          component={TextAreaField}
          name="medication"
          label="Medications"
        />
        <Field component={TextAreaField} name="procedure" label="Procedures" />
        <Field
          component={TextAreaField}
          name="diagnostic_test"
          label="Diagnostic Tests"
        />
        <Field
          component={TextAreaField}
          name="other_remark"
          label="Other Remarks"
        />
        <Field
          component={FieldSelect}
          name="physician_id"
          label="Attending Physician"
          items={this.renderPhysicians(physicians)}
        />
        <Field
          component={FieldSelect}
          name="duty_physician_id"
          label="Physician on duty"
          items={this.renderPhysicians(physicians)}
        />
        <ButtonDecision {...this.props} />
      </Form>
    )
  }
}

export default reduxForm({
  fields: [
    'assessment'
    , 'medication'
    , 'procedure'
    , 'diagnostic_test'
    , 'other_remark'
  ]
})(PhysicianOrderForm)
