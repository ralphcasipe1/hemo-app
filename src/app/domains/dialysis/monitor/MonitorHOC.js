import React, { Component, Fragment } from 'react';
import { Header, Loader } from 'semantic-ui-react';

export const MonitorHOC = (WrappedComponent, { title }) => {
  return class extends Component {
    componentDidMount() {
      this.props.fetchAllData(this.props.match.params.patientRegistryId);
    }

    handleSubmit = values => {
      this.props.create(this.props.match.params.patientRegistryId, values);
    };

    render() {
      const { isLoading } = this.props;

      if (isLoading) {
        return <Loader active inline="centered" />;
      }

      return (
        <Fragment>
          <Header as="h3" content={title} />
          <WrappedComponent handleSubmit={this.handleSubmit} {...this.props} />
        </Fragment>
      );
    }
  };
};
