import React from 'react';
import { Table } from 'semantic-ui-react';
import { MedicationTableBody } from './MedicationTableBody';
import { MedicationTableHeader } from './MedicationTableHeader';

export const MedicationTable = ({ medications }) => (
  <Table fixed sortable compact="very">
    <MedicationTableHeader />
    <MedicationTableBody medications={medications} />
  </Table>
);
