import React from 'react';
import { MedicationTableBody } from './MedicationTableBody';
import { MedicationTableHead } from './MedicationTableHead';
// import './MedicationTable.css';

export const MedicationTable = ({ medications }) => (
  <table className="medication-table">
    <MedicationTableHead />

    <MedicationTableBody medications={medications} />
  </table>
);
