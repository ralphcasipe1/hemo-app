import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';
import MedicationForm from './MedicationForm';
import { MedicationMenu } from './MedicationMenu';
import { MedicationTable } from './MedicationTable';

class Medication extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 'view medications'
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
  }

  handleMenuItemClick(e, { name }) {
    this.setState({
      active: name
    });
  }

  handleFormSubmit(values) {
    this.props.createMedication(this.props.patientRegistryId, values);

    this.setState({
      active: 'view medications'
    });
  }

  componentDidMount() {
    this.props.fetchAllMedications(this.props.patientRegistryId);
  }

  renderContent() {
    const { active } = this.state;
    const { medications } = this.props;

    if (active === 'add medication') {
      return (
        <MedicationForm
          form="CreateMedicationForm"
          onFormSubmit={this.handleFormSubmit}
        />
      );
    } else {
      return <MedicationTable medications={medications} />;
    }
  }
  render() {
    const { active } = this.state;
    const { isLoading } = this.props;

    return (
      <Segment basic loading={isLoading}>
        <MedicationMenu
          active={active}
          onMenuItemClick={this.handleMenuItemClick}
        />
        {this.renderContent()}
      </Segment>
    );
  }
}

export { Medication };
