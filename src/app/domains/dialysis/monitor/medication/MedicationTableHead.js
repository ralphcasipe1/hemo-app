import React from 'react';

export const MedicationTableHead = () => (
  <thead>
    <tr>
      <th>Date / Time</th>
      <th>Generic</th>
      <th>Brand</th>
      <th>Preparation</th>
      <th>Dosage</th>
      <th>Route</th>
      <th>Nurse</th>
    </tr>
  </thead>
);
