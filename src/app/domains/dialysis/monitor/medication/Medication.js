import React, { Component, Fragment } from 'react'
import { ApplyTemplate }              from '../../ApplyTemplate'
import { MonitorHOC }                 from '../MonitorHOC'
import { MedicationForm }             from './MedicationForm'
import { MedicationList }             from './MedicationList'
// import { MedicationTable } from './MedicationTable';

class Medication extends Component {
  render() {
    const { handleSubmit, medications } = this.props

    return (
      <Fragment>
        {medications.length === 0 ? (
          <div>
            <ApplyTemplate />
            <MedicationForm
              form="CreateMedicationForm"
              onFormSubmit={handleSubmit}
            />
          </div>
        ) : (
          <MedicationList medications={medications} />
        )}
      </Fragment>
    )
  }
}

Medication = MonitorHOC(Medication, {
  title: 'Medication'
})

export { Medication }
