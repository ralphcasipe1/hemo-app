import React    from 'react'
import _        from 'lodash'
import moment   from 'moment'
import { List } from 'semantic-ui-react'

const MedicationList = ({ medications }) => (
	<List>
		{medications.map(medication => (
			<List.Item key={medication.medication_id}>
				<List.Header content={moment(medication.created_at).format('ll')} />
				<List.Content>
					<dl>
						<dt>Generic</dt>
						<dd>{medication.generic}</dd>

						<dt>Brand</dt>
						<dd>{_.startCase(medication.brand)}</dd>

						<dt>Preparation</dt>
						<dd>{medication.preparation}</dd>

						<dt>Dosage</dt>
						<dd>{medication.dosage}</dd>

						<dt>Route</dt>
						<dd>{medication.route}</dd>

						{/* <dt>Nurse</dt>
						<dd>{medication.arterial_pressure}</dd>*/}
					</dl>
				</List.Content>
			</List.Item>
		))}
	</List>
)

export { MedicationList }