import React     from 'react'
import { Table } from 'semantic-ui-react'

export const MedicationTableHeader = () => (
  <Table.Header>
    <Table.Row>
      <Table.HeaderCell>Date / Time</Table.HeaderCell>

      <Table.HeaderCell>Generic</Table.HeaderCell>

      <Table.HeaderCell>Brand</Table.HeaderCell>

      <Table.HeaderCell>Preparation</Table.HeaderCell>

      <Table.HeaderCell>Dosage</Table.HeaderCell>

      <Table.HeaderCell>Route</Table.HeaderCell>

      <Table.HeaderCell>Nurse</Table.HeaderCell>

      <Table.HeaderCell />
    </Table.Row>
  </Table.Header>
)
