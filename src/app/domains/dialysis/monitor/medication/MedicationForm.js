import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { InputField } from 'app/common/forms/InputField'

class MedicationForm extends PureComponent {
  render() {
    return (
      <Form
        loading={this.props.formLoading}
        onSubmit={this.props.handleSubmit(this.props.onFormSubmit)}
      >
        <Field component={InputField} name="generic" label="Generic" />
        <Field component={InputField} name="brand" label="Brand" />
        <Field component={InputField} name="preparation" label="Preparation" />
        <Field component={InputField} name="dosage" label="Dosage" />
        <Field component={InputField} name="route" label="Route" />
        <ButtonDecision {...this.props} expanded />
      </Form>
    )
  }
}

MedicationForm = reduxForm({
  fields: [ 'brand', 'generic', 'preparation', 'dosage', 'route' ]
})(MedicationForm)

export { MedicationForm }
