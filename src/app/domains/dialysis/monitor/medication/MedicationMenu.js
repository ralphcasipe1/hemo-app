import React from 'react';
import { Icon, Menu } from 'semantic-ui-react';

export const MedicationMenu = ({ active, onMenuItemClick }) => (
  <Menu fluid tabular icon widths={2}>
    <Menu.Item
      name="add medication"
      active={active === 'add medication'}
      onClick={onMenuItemClick}
    >
      <Icon name="plus" color="blue" />
    </Menu.Item>
    <Menu.Item
      name="view medications"
      active={active === 'view medications'}
      onClick={onMenuItemClick}
    >
      <Icon name="sticky note" color="blue" />
    </Menu.Item>
  </Menu>
);
