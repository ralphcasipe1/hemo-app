import {
  CREATE_MEDICATION,
  CREATE_MEDICATION_FAILURE,
  CREATE_MEDICATION_SUCCESS,

  // DELETE_MEDICATION,
  // DELETE_MEDICATION_FAILURE,
  // DELETE_MEDICATION_SUCCESS,
  FETCH_ALL_MEDICATIONS,
  FETCH_ALL_MEDICATIONS_FAILURE,
  FETCH_ALL_MEDICATIONS_SUCCESS

  // UPDATE_MEDICATION,
  // UPDATE_MEDICATION_FAILURE,
  // UPDATE_MEDICATION_SUCCESS
} from '../constants/medication';
import { instance } from '../../../../../config/connection';

export const fetchAllMedications = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_ALL_MEDICATIONS
  });

  return instance
    .get(`/patient_registries/${patientRegistryId}/medications`)
    .then(
      response =>
        dispatch({
          type: FETCH_ALL_MEDICATIONS_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_ALL_MEDICATIONS_FAILURE
          , error: error.response
        })
    );
};

export const createMedication = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_MEDICATION
  });

  return instance
    .post(`/patient_registries/${patientRegistryId}/medications`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_MEDICATION_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_MEDICATION_FAILURE
          , error: error.response
        })
    );
};
