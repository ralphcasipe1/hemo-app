import { connect } from 'react-redux';
import { createMedication, fetchAllMedications } from './actions/medication';
import { Medication } from './Medication';

const mapStateToProps = state => ({
  isLoading: state.medicationReducer.isLoading
  , medications: state.medicationReducer.medications
});

export const MedicationContainer = connect(
  mapStateToProps,
  {
    create: createMedication
    , fetchAllData: fetchAllMedications
  }
)(Medication);
