import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { Button, Table } from 'semantic-ui-react';

export const MedicationTableBody = ({ medications }) => (
  <Table.Body>
    {medications.map(medication => (
      <Table.Row key={medication.medication_id}>
        <Table.Cell>
          <b>{moment(medication.created_at).format('lll')}</b>
        </Table.Cell>
        <Table.Cell>{medication.generic}</Table.Cell>
        <Table.Cell>{_.startCase(medication.brand)}</Table.Cell>
        <Table.Cell>{medication.preparation}</Table.Cell>
        <Table.Cell>{medication.dosage}</Table.Cell>
        <Table.Cell>{medication.route}</Table.Cell>
        <Table.Cell>
          {medication.createdBy.fname} {medication.createdBy.lname}
        </Table.Cell>
        <Table.Cell>
          <Button icon="edit" color="green" basic disabled />
          <Button icon="trash" color="red" basic disabled />
        </Table.Cell>
      </Table.Row>
    ))}
  </Table.Body>
);
