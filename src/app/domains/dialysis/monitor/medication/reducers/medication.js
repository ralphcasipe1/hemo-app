import {
  CREATE_MEDICATION,
  CREATE_MEDICATION_FAILURE,
  CREATE_MEDICATION_SUCCESS,

  // DELETE_MEDICATION,
  // DELETE_MEDICATION_FAILURE,
  // DELETE_MEDICATION_SUCCESS,
  FETCH_ALL_MEDICATIONS,
  FETCH_ALL_MEDICATIONS_FAILURE,
  FETCH_ALL_MEDICATIONS_SUCCESS

  // UPDATE_MEDICATION,
  // UPDATE_MEDICATION_FAILURE,
  // UPDATE_MEDICATION_SUCCESS
} from '../constants/medication';

const initialState = {
  medications: []
  , isLoading: false
  , error: null
};

export const medicationReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_MEDICATIONS:
    case CREATE_MEDICATION:
      return {
        ...state
        , isLoading: true
      };

    case FETCH_ALL_MEDICATIONS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , medications: action.payload
      };

    case CREATE_MEDICATION_SUCCESS:
      return {
        ...state
        , isLoading: false
        , medications: [ action.payload, ...state.medications ]
      };

    case FETCH_ALL_MEDICATIONS_FAILURE:
    case CREATE_MEDICATION_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      };

    default:
      return state;
  }
};
