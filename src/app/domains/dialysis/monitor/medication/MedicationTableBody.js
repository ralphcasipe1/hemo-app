import React from 'react';
import _ from 'lodash';
import moment from 'moment';

export const MedicationTableBody = ({ medications }) => (
  <tbody>
    {medications.map(medication => (
      <tr key={medication.medication_id}>
        <td>
          <b>{moment(medication.created_at).format('lll')}</b>
        </td>
        <td>{medication.generic}</td>
        <td>{_.startCase(medication.brand)}</td>
        <td>{medication.preparation}</td>
        <td>{medication.dosage}</td>
        <td>{medication.route}</td>
      </tr>
    ))}
  </tbody>
);
