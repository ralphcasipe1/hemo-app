import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { TextAreaField } from 'app/common/forms/TextAreaField'

class NurseNoteForm extends PureComponent {
  render() {
    const { handleSubmit, onFormSubmit } = this.props

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field
          component={TextAreaField}
          name="note_desc"
          label="Nurse Notes"
          placeholder="Write your note here..."
        />
        <Form.Button icon="checkmark" content="Save" color="blue" />
      </Form>
    )
  }
}

NurseNoteForm = reduxForm({
  fields: ['nurse_desc']
})(NurseNoteForm)

export { NurseNoteForm }
