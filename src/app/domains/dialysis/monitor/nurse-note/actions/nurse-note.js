import {
  CREATE_NURSE_NOTE,
  CREATE_NURSE_NOTE_FAILURE,
  CREATE_NURSE_NOTE_SUCCESS,
  DELETE_NURSE_NOTE,
  DELETE_NURSE_NOTE_FAILURE,
  DELETE_NURSE_NOTE_SUCCESS,
  FETCH_ALL_NURSE_NOTES,
  FETCH_ALL_NURSE_NOTES_FAILURE,
  FETCH_ALL_NURSE_NOTES_SUCCESS,
  UPDATE_NURSE_NOTE,
  UPDATE_NURSE_NOTE_FAILURE,
  UPDATE_NURSE_NOTE_SUCCESS
} from '../constants/nurse-note';
import { instance } from '../../../../../config/connection';

export const createNurseNote = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_NURSE_NOTE
  });

  return instance
    .post(`/patient_registries/${patientRegistryId}/nurse_notes`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_NURSE_NOTE_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_NURSE_NOTE_FAILURE
          , error: error.response
        })
    );
};

export const deleteNurseNote = (patientRegistryId, nurseNoteId) => dispatch => {
  dispatch({
    type: DELETE_NURSE_NOTE
  });

  return instance
    .delete(
      `/patient_registries/${patientRegistryId}/nurse_notes/${nurseNoteId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_NURSE_NOTE_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_NURSE_NOTE_FAILURE
          , error: error.response
        })
    );
};

export const updateNurseNote = (
  patientRegistryId,
  nurseNoteId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_NURSE_NOTE
  });

  return instance
    .patch(
      `/patient_registries/${patientRegistryId}/nurse_notes/${nurseNoteId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_NURSE_NOTE_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_NURSE_NOTE_FAILURE
          , error: error.response
        })
    );
};

export const fetchAllNurseNotes = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_ALL_NURSE_NOTES
  });

  return instance
    .get(`/patient_registries/${patientRegistryId}/nurse_notes`)
    .then(
      response =>
        dispatch({
          type: FETCH_ALL_NURSE_NOTES_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: FETCH_ALL_NURSE_NOTES_FAILURE
          , error: error.response
        })
    );
};
