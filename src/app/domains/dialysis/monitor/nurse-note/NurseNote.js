import React, { Component, Fragment } from 'react'
import { MonitorHOC }                 from '../MonitorHOC'
import { NurseNoteForm }              from './NurseNoteForm'
import { NurseNoteList }              from './NurseNoteList'

class NurseNote extends Component {
  render() {
    const { handleSubmit, nurseNotes } = this.props

    return (
      <Fragment>
        {nurseNotes.length === 0 ? (
          <NurseNoteForm
            form="CreateNurseNoteForm"
            onFormSubmit={handleSubmit}
          />
        ) : (
          <NurseNoteList nurseNotes={nurseNotes} />
        )}
      </Fragment>
    )
  }
}

NurseNote = MonitorHOC(NurseNote, {
  title: 'Nurse notes'
})

export { NurseNote }
