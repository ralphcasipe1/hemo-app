import {
  CREATE_NURSE_NOTE,
  CREATE_NURSE_NOTE_FAILURE,
  CREATE_NURSE_NOTE_SUCCESS,
  DELETE_NURSE_NOTE,
  DELETE_NURSE_NOTE_FAILURE,
  // DELETE_NURSE_NOTE_SUCCESS,
  FETCH_ALL_NURSE_NOTES,
  FETCH_ALL_NURSE_NOTES_FAILURE,
  FETCH_ALL_NURSE_NOTES_SUCCESS,
  UPDATE_NURSE_NOTE,
  UPDATE_NURSE_NOTE_FAILURE
  // UPDATE_NURSE_NOTE_SUCCESS
} from '../constants/nurse-note';

const initialState = {
  error: null
  , nurseNotes: []
  , message: null
  , isLoading: false
};

export const nurseNoteReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_NURSE_NOTE:
    case FETCH_ALL_NURSE_NOTES:
    case DELETE_NURSE_NOTE:
    case UPDATE_NURSE_NOTE:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_NURSE_NOTE_FAILURE:
    case DELETE_NURSE_NOTE_FAILURE:
    case UPDATE_NURSE_NOTE_FAILURE:
    case FETCH_ALL_NURSE_NOTES_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      };

    case FETCH_ALL_NURSE_NOTES_SUCCESS:
      return {
        ...state
        , isLoading: false
        , nurseNotes: action.payload
      };

    case CREATE_NURSE_NOTE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , nurseNotes: [ action.payload, ...state.nurseNotes ]
      };

    /*case DELETE_NURSE_NOTE_SUCCESS:
      return updateObject(state, {
        nurseNotes: state.nurseNotes.map((note) =>
          Object.assign({}, note, {
            nurseNotes: note.nurseNotes.filter(
              (note) => note.nurse_note_id !== action.payload.nurse_note_id
            ),
          })
        ),
        isLoading: false,
        message: 'Note successfully deleted',
      });

    case UPDATE_NURSE_NOTE_SUCCESS:
      const { note_desc, nurse_note_id, updated_at } = action.payload;
      return updateObject(state, {
        nurseNotes: state.nurseNotes.map((note) =>
          Object.assign({}, note, {
            nurseNotes: note.nurseNotes.map((data) => {
              if (data.nurse_note_id !== nurse_note_id) {
                return data;
              }
              return Object.assign({}, data, {
                note_desc,
                updated_at,
              });
            }),
          })
        ),
        isLoading: false,
        message: 'Note successfully updated',
      });*/

    default:
      return state;
  }
};
