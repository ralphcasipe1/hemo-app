import React, { PureComponent } from 'react';
import { List } from 'semantic-ui-react';
import { NurseNoteListItem } from './NurseNoteListItem';

export class NurseNoteList extends PureComponent {
  render() {
    const { nurseNotes } = this.props;

    return (
      <List>
        {nurseNotes.map(nurseNote => (
          <NurseNoteListItem
            nurseNote={nurseNote}
            key={nurseNote.nurse_note_id}
          />
        ))}
      </List>
    );
  }
}
