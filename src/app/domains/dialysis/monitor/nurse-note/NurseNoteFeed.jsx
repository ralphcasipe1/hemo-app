import React, { PureComponent } from 'react';
import { Feed, Icon } from 'semantic-ui-react';
import moment from 'moment';

export class NurseNoteFeed extends PureComponent {
  render() {
    const { nurseNote } = this.props;

    return (
      <Feed size="small">
        <Feed.Event>
          <Feed.Label>
            <Icon color="blue" name="pencil" />
          </Feed.Label>
          <Feed.Content>
            <Feed.Summary>
              Note added at
              <Feed.Date>
                {moment(nurseNote.created_at).format('lll')}
              </Feed.Date>
            </Feed.Summary>
            <Feed.Extra text>
              {nurseNote.note_desc
                .split('\n')
                .map((field, index) => <div key={index}>{field}</div>)}
            </Feed.Extra>
          </Feed.Content>
        </Feed.Event>
      </Feed>
    );
  }
}
