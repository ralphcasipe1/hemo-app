import React from 'react';
import { Icon, Menu } from 'semantic-ui-react';

export const NurseNoteMenu = ({ active, onMenuItemClick }) => (
  <Menu fluid vertical tabular icon>
    <Menu.Item
      name="add note"
      active={active === 'add note'}
      onClick={onMenuItemClick}
    >
      <Icon name="plus" color="blue" />
    </Menu.Item>
    <Menu.Item
      name="view notes"
      active={active === 'view notes'}
      onClick={onMenuItemClick}
    >
      <Icon name="sticky note" color="blue" />
    </Menu.Item>
  </Menu>
);
