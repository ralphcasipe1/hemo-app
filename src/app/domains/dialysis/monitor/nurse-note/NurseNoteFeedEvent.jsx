import React, { PureComponent } from 'react';
import moment from 'moment';
import { Button, Feed, Icon, TextArea } from 'semantic-ui-react';

export class NurseNoteFeedEvent extends PureComponent {
  render() {
    const { nurseNote } = this.props;

    return (
      <Feed.Event>
        <Feed.Label>
          <Icon color="blue" name="pencil" />
        </Feed.Label>
        <Feed.Content>
          <Feed.Summary>
            Added Note
            <Feed.Date>{moment(nurseNote.created_at).format('lll')}</Feed.Date>
          </Feed.Summary>
          <Feed.Extra text>
            <TextArea autoHeight name="note_desc" />
          </Feed.Extra>
          <Feed.Meta>
            <Button basic color="blue" content="Cancel" icon="delete" />
            <Button color="red" content="Yes" icon="checkmark" />
          </Feed.Meta>
        </Feed.Content>
      </Feed.Event>
    );
  }
}
