import { connect } from 'react-redux';
import { createNurseNote, fetchAllNurseNotes } from './actions/nurse-note';
import { NurseNote } from './NurseNote';

const mapStateToProps = state => ({
  isLoading: state.nurseNoteReducer.isLoading
  , nurseNotes: state.nurseNoteReducer.nurseNotes
});

export const NurseNoteContainer = connect(
  mapStateToProps,
  {
    create: createNurseNote
    , fetchAllData: fetchAllNurseNotes
  }
)(NurseNote);
