import React, { Component } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import { NurseNoteMenu } from './NurseNoteMenu';
import { NurseNoteList } from './NurseNoteList';
import NurseNoteForm from './NurseNoteForm';

export class NurseNote extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 'view notes'
    };

    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchPatientRegistryNurseNotes(this.props.patientRegistryId);
  }

  handleMenuItemClick(e, { name }) {
    this.setState({
      active: name
    });
  }

  handleFormSubmit(values) {
    this.props.createPatientRegistryNurseNote(
      this.props.patientRegistryId,
      values
    );

    this.setState({
      active: 'view notes'
    });
  }

  renderContent() {
    const { active } = this.state;
    const { nurseNotes } = this.props;

    if (active === 'add note') {
      return (
        <NurseNoteForm
          expanded
          form="NurseNoteForm"
          onFormSubmit={this.handleFormSubmit}
        />
      );
    } else {
      return <NurseNoteList nurseNotes={nurseNotes} />;
    }
  }

  render() {
    const { isLoading } = this.props;
    const { active } = this.state;

    return (
      <Segment basic loading={isLoading}>
        <Grid>
          <Grid.Column width={4}>
            <NurseNoteMenu
              active={active}
              onMenuItemClick={this.handleMenuItemClick}
            />
          </Grid.Column>
          <Grid.Column stretched width={12}>
            {this.renderContent()}
          </Grid.Column>
        </Grid>
      </Segment>
    );
  }
}
