import React, { PureComponent } from 'react';
import { Icon, List } from 'semantic-ui-react';
// import { NurseNoteFeed } from './NurseNoteFeed';

export class NurseNoteListItem extends PureComponent {
  render() {
    const { nurseNote } = this.props;
    // const name = {nurseNote.updatedBy.fname} {nurseNote.updatedBy.lname}
    return (
      <List.Item>
        <Icon color="blue" name="user" size="large" />
        <List.Content>
          <List.Header>{nurseNote.note_desc}</List.Header>
          <List.Description>
            {/*<NurseNoteFeed nurseNote={nurseNote} />*/}
          </List.Description>
        </List.Content>
      </List.Item>
    );
  }
}
