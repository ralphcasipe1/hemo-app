import React, { Component } from 'react';
import { ApplyTemplate } from '../../ApplyTemplate';
import { BasicHemodialysisHOC } from '../BasicHemodialysisHOC';
import { WeightForm } from './WeightForm';
import { WeightTable } from './WeightTable';

class Weight extends Component {
  render() {
    const { data, handleSubmit, interHDWtGainValue } = this.props;

    const interHDWeightGainValue = Math.round(interHDWtGainValue * 100) / 100;

    return (
      <div>
        {!data ? (
          <div>
            <ApplyTemplate />

            <WeightForm
              interHDWtGainValue={interHDWeightGainValue}
              form="CreateWeightForm"
              onFormSubmit={handleSubmit}
            />
          </div>
        ) : (
          <WeightTable weight={data} dataId={data.weight_id} />
        )}
      </div>
    );
  }
}

Weight = BasicHemodialysisHOC(Weight, { title: 'Weight' });

export { Weight };
