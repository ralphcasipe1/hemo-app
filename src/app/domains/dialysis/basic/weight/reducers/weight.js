import {
  CLEAR_NEW_WEIGHT,
  CREATE_WEIGHT,
  CREATE_WEIGHT_FAILURE,
  CREATE_WEIGHT_SUCCESS,
  DELETE_WEIGHT,
  DELETE_WEIGHT_FAILURE,
  DELETE_WEIGHT_SUCCESS,
  FETCH_WEIGHT,
  FETCH_WEIGHT_FAILURE,
  FETCH_WEIGHT_SUCCESS,
  UPDATE_WEIGHT,
  UPDATE_WEIGHT_FAILURE,
  UPDATE_WEIGHT_SUCCESS,
} from '../constants/weight';

const initialState = {
  error: null,
  isLoading: false,
  weight: null,
};

export const weightReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_WEIGHT:
    case UPDATE_WEIGHT:
    case FETCH_WEIGHT:
    case CREATE_WEIGHT:
      return {
        ...state,
        isLoading: true,
      };

    case CREATE_WEIGHT_FAILURE:
    case DELETE_WEIGHT_FAILURE:
    case FETCH_WEIGHT_FAILURE:
    case UPDATE_WEIGHT_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    case UPDATE_WEIGHT_SUCCESS:
    case FETCH_WEIGHT_SUCCESS:
    case CREATE_WEIGHT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        weight: action.payload,
      };

    case DELETE_WEIGHT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        weight: null,
      };

    case CLEAR_NEW_WEIGHT:
      return {
        ...state,
        isLoading: false,
        error: null,
        weight: null,
      };
    default:
      return state;
  }
};
