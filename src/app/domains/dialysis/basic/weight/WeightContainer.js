import { formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { createWeight, deleteWeight, fetchWeight } from './actions/weight';
import { Weight } from './Weight';

const selector = formValueSelector('CreateWeightForm');

const mapStateToProps = state => {
  const { error, isLoading, weight } = state.weightReducer;

  const preHDWeight = selector(state, 'pre_hd_weight');
  const dryWeight = selector(state, 'dry_weight');

  return {
    interHDWtGainValue: preHDWeight - dryWeight || 0,
    isLoading,
    error,
    data: weight,
  };
};

export const WeightContainer = connect(
  mapStateToProps,
  {
    create: createWeight,
    deleteData: deleteWeight,
    fetchData: fetchWeight,
  }
)(Weight);
