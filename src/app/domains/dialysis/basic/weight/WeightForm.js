import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { SaveButton } from 'app/common/forms/SaveButton'
import { InputField } from 'app/common/forms/InputField'

class WeightForm extends PureComponent {
  render() {
    const {
      loading,
      handleSubmit,
      interHDWtGainValue,
      onFormSubmit
    } = this.props

    return (
      <Form
        className="attached"
        loading={loading}
        onSubmit={handleSubmit(onFormSubmit)}
      >
        <Field
          component={InputField}
          inputLabel="kg"
          labelPosition="right"
          label="Last Post HD Weight"
          name="last_post_hd_wt"
          type="number"
        />

        <Field
          component={InputField}
          inputLabel="kg"
          labelPosition="right"
          label="Pre HD Weight"
          name="pre_hd_weight"
          type="number"
        />

        <Field
          component={InputField}
          inputLabel="kg"
          labelPosition="right"
          label="Dry Weight"
          name="dry_weight"
          type="number"
        />

        <div
          style={{
            backgroundColor: '#bdc3c7'
            , height: '100%'
            , padding: '2em 0'
          }}
        >
          <b style={{ marginRight: '20px' }}>Inter HD Weight Gain:</b>
          {interHDWtGainValue} <b>kg</b>
        </div>

        <Field
          component={InputField}
          inputLabel="kg"
          labelPosition="right"
          label="Post HD Weight"
          name="post_hd_weight"
          type="number"
        />

        <Field
          component={InputField}
          inputLabel="L"
          labelPosition="right"
          label="Net Fluid Removed"
          name="net_fluid_removed"
          type="number"
        />

        <SaveButton />
      </Form>
    )
  }
}

const validate = values => {
  const errors = {}

  if (!values.pre_hd_weight) {
    errors.pre_hd_weight = 'Required'
  }

  if (!values.dry_weight) {
    errors.dry_weight = 'Required'
  }

  return errors
}

WeightForm = reduxForm({
  fields: [
    'last_post_hd_wt'
    , 'pre_hd_weight'
    , 'dry_weight'
    , 'inter_hd_wt_gain'
    , 'post_hd_weight'
    , 'net_fluid_removed'
  ]
  , enableReinitialize: true
  , validate
})(WeightForm)

export { WeightForm }
