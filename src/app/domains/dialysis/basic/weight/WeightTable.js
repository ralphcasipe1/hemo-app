import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableHeader } from '../TableHeader';
import { WeightTableRow } from './WeightTableRow';

export const WeightTable = props => (
  <Table basic="very" compact="very" definition striped unstackable fixed>
    <TableHeader />

    <WeightTableRow {...props} />
  </Table>
);
