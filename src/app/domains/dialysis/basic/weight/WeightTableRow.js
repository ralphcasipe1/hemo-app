import React from 'react';
import { Table } from 'semantic-ui-react';

export const WeightTableRow = ({ weight }) => (
  <Table.Body>
    <Table.Row>
      <Table.Cell content="Last Post HD Weight" />
      <Table.Cell textAlign="center">{weight.last_post_hd_wt} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Pre HD Weight" />
      <Table.Cell textAlign="center">{weight.pre_hd_weight} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="DRY Weight" />
      <Table.Cell textAlign="center">{weight.dry_weight} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Inter-HD Weight Gain" />
      <Table.Cell textAlign="center">{weight.inter_hd_wt_gain} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Post HD Weight" />
      <Table.Cell textAlign="center">{weight.post_hd_weight} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Net Fluid Removed" />
      <Table.Cell textAlign="center">{weight.net_fluid_removed} kg</Table.Cell>
    </Table.Row>
  </Table.Body>
);
