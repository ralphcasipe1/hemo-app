import {
  CLEAR_NEW_WEIGHT,
  CREATE_WEIGHT,
  CREATE_WEIGHT_FAILURE,
  CREATE_WEIGHT_SUCCESS,
  DELETE_WEIGHT,
  DELETE_WEIGHT_FAILURE,
  DELETE_WEIGHT_SUCCESS,
  FETCH_WEIGHT,
  FETCH_WEIGHT_FAILURE,
  FETCH_WEIGHT_SUCCESS,
  UPDATE_WEIGHT,
  UPDATE_WEIGHT_FAILURE,
  UPDATE_WEIGHT_SUCCESS,
} from '../constants/weight';
import { instance } from '../../../../../config/connection';

export const createWeight = (patientRegistryId, values) => (dispatch) => {
  dispatch({
    type: CREATE_WEIGHT,
  });

  return instance
    .post(`patient_registries/${patientRegistryId}/weights`, values)
    .then(
      (response) =>
        dispatch({
          type: CREATE_WEIGHT_SUCCESS,
          payload: response.data.data,
        }),
      (error) =>
        dispatch({
          type: CREATE_WEIGHT_FAILURE,
          error: error.response,
        })
    );
};

export const deleteWeight = (patientRegistryId, weightId) => (dispatch) => {
  dispatch({
    type: DELETE_WEIGHT,
  });

  return instance
    .delete(`patient_registries/${patientRegistryId}/weights/${weightId}`)
    .then(
      (response) =>
        dispatch({
          type: DELETE_WEIGHT_SUCCESS,
          payload: response.data.data,
        }),
      (error) =>
        dispatch({
          type: DELETE_WEIGHT_FAILURE,
          error: error.response,
        })
    );
};

export const fetchWeight = (patientRegistryId) => (dispatch) => {
  dispatch({
    type: FETCH_WEIGHT,
  });

  instance.get(`patient_registries/${patientRegistryId}/weights`).then(
    (response) =>
      dispatch({
        type: FETCH_WEIGHT_SUCCESS,
        payload: response.data.data,
      }),
    (error) =>
      dispatch({
        type: FETCH_WEIGHT_FAILURE,
        error: error.response,
      })
  );
};

export const updateWeight = (patientRegistryId, weightId, values) => (
  dispatch
) => {
  dispatch({
    type: UPDATE_WEIGHT,
  });

  instance
    .patch(
      `patient_registries/${patientRegistryId}/weights/${weightId}`,
      values
    )
    .then(
      (response) =>
        dispatch({
          type: UPDATE_WEIGHT_SUCCESS,
          payload: response.data.data,
        }),
      (error) =>
        dispatch({
          type: UPDATE_WEIGHT_FAILURE,
          error: error.resaponse,
        })
    );
};

export const clearNewWeight = () => ({ type: CLEAR_NEW_WEIGHT });
