import React, { Component, Fragment }            from 'react'
import { Link }                                  from 'react-router-dom'
import { Dropdown, Header, Icon, Loader, Modal } from 'semantic-ui-react'

export const BasicHemodialysisHOC = (WrappedComponent, { title }) => {
  return class extends Component {
    componentDidMount() {
      this.props.fetchData(this.props.match.params.patientRegistryId)
    }

    handleSubmit = values => {
      this.props.create(this.props.match.params.patientRegistryId, values)
    };

    render() {
      const {
        data,
        dataId,
        isLoading,
        match: {
          params: { patientRegistryId }
        }
      } = this.props

      if (isLoading) {
        return <Loader active inline="centered" />
      }

      return (
        <Fragment>
          {data ? (
            <Dropdown icon={<Icon name="configure" color="grey" />} disabled>
              <Dropdown.Menu>
                <Dropdown.Item
                  as={Link}
                  key="edit"
                  text="Edit"
                  to={`/patient_registries/${patientRegistryId}/parameters/${dataId}/edit`}
                />
                <Dropdown.Item>
                  <Modal
                    basic
                    trigger={<span>Delete</span>}
                    header="Warning!"
                    content="Are you sure?"
                    actions={[
                      {
                        key: 'no'
                        , icon: <Icon name="remove" />
                        , content: 'No'
                        , color: 'red'
                        , inverted: true
                      }
                      , {
                        key: 'yes'
                        , icon: <Icon name="check" />
                        , content: 'Yes'
                        , color: 'green'
                        , onClick: this.handleDelete
                        , inverted: true
                      }
                    ]}
                  />
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          ) : null}

          <Header as="h3" content={title} />

          <WrappedComponent
            data={data}
            dataId={dataId}
            handleSubmit={this.handleSubmit}
            {...this.props}
          />
        </Fragment>
      )
    }
  }
}
