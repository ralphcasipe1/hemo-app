import {
  CREATE_DIALYZER,
  CREATE_DIALYZER_FAILURE,
  CREATE_DIALYZER_SUCCESS,
  DELETE_DIALYZER,
  DELETE_DIALYZER_FAILURE,
  DELETE_DIALYZER_SUCCESS,
  FETCH_DIALYZER,
  FETCH_DIALYZER_FAILURE,
  FETCH_DIALYZER_SUCCESS,
  UPDATE_DIALYZER,
  UPDATE_DIALYZER_FAILURE,
  UPDATE_DIALYZER_SUCCESS,
} from '../constants/dialyzer';
import { instance } from '../../../../../config/connection';

export const createDialyzer = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_DIALYZER,
  });

  return instance
    .post(`/patient_registries/${patientRegistryId}/dialyzers`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_DIALYZER_SUCCESS,
          payload: response.data.data,
        }),
      error =>
        dispatch({
          type: CREATE_DIALYZER_FAILURE,
          error: error.response,
        })
    );
};

export const deleteDialyzer = (patientRegistryId, dialyzerId) => dispatch => {
  dispatch({
    type: DELETE_DIALYZER,
  });

  return instance
    .delete(`/patient_registries/${patientRegistryId}/dialyzers/${dialyzerId}`)
    .then(
      response =>
        dispatch({
          type: DELETE_DIALYZER_SUCCESS,
          payload: response.data.data,
        }),
      error =>
        dispatch({
          type: DELETE_DIALYZER_FAILURE,
          error: error.response,
        })
    );
};

export const fetchDialyzer = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_DIALYZER,
  });

  return instance
    .get(`/patient_registries/${patientRegistryId}/dialyzers`)
    .then(
      response =>
        dispatch({
          type: FETCH_DIALYZER_SUCCESS,
          payload: response.data.data,
        }),
      error =>
        dispatch({
          type: FETCH_DIALYZER_FAILURE,
          error: error.response,
        })
    );
};

export const updateDialyzer = (
  patientRegistryId,
  dialyzerId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_DIALYZER,
  });

  return instance
    .update(
      `/patient_registries/${patientRegistryId}/dialyzers/${dialyzerId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_DIALYZER_SUCCESS,
          payload: response.data.data,
        }),
      error =>
        dispatch({
          type: UPDATE_DIALYZER_FAILURE,
          error: error.response,
        })
    );
};
