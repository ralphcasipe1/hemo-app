import React from 'react';
import { Table } from 'semantic-ui-react';

export const DialyzerTableRow = ({ dialyzer }) => (
  <Table.Body>
    <Table.Row>
      <Table.Cell content="Brand" />
      <Table.Cell textAlign="center">
        {dialyzer.dialyzerBrand.brand_name}
      </Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Type" />
      <Table.Cell textAlign="center">{dialyzer.type}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Size" />
      <Table.Cell textAlign="center">
        {dialyzer.dialyzerSize.size_name}
      </Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Date" />
      <Table.Cell textAlign="center">{dialyzer.dialyzer_date} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Count" />
      <Table.Cell textAlign="center">{dialyzer.count} kg</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Discarded d/t" />
      <Table.Cell textAlign="center">{dialyzer.d_t} kg</Table.Cell>
    </Table.Row>
  </Table.Body>
);
