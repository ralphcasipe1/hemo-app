import { connect } from 'react-redux';
import { fetchAllDialyzerBrands } from 'app/domains/admin/dialyzer-brand/actions';
import { fetchAllDialyzerSizes } from 'app/domains/admin/dialyzer-size/actions/dialyzer-size';
import {
  createDialyzer,
  deleteDialyzer,
  fetchDialyzer,
} from './actions/dialyzer';
import { Dialyzer } from './Dialyzer';

const mapStateToProps = state => {
  const { error, isLoading, dialyzer } = state.dialyzerReducer;

  return {
    error,
    isLoading,
    data: dialyzer,
    dialyzerBrands: state.dialyzerBrandReducer.dialyzerBrands,
    dialyzerSizes: state.dialyzerSizeReducer.dialyzerSizes,
  };
};

export const DialyzerContainer = connect(
  mapStateToProps,
  {
    fetchAllDialyzerBrands,
    fetchAllDialyzerSizes,
    create: createDialyzer,
    deleteDialyzer,
    fetchData: fetchDialyzer,
  }
)(Dialyzer);
