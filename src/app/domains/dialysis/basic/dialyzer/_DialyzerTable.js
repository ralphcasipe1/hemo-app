import { Button, Confirm, Icon, Input, Loader, Table } from 'semantic-ui-react';
import moment from 'moment';
import ExceptionMessage from 'ExceptionMessage';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import RecommendMessage from 'RecommendMessage';

class DialyzerTable extends PureComponent {
  static defaultProps = {
    patientRegistryDialyzer: null,
  };

  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    onEditClick: PropTypes.func,
    patientRegistryDialyzer: PropTypes.shape({
      type: PropTypes.string.isRequired,

      dialyzer_date: PropTypes.string,
      count: PropTypes.string,
      d_t: PropTypes.string,
    }),
  };

  constructor() {
    super();

    this.state = {
      edit: false,
      openConfirm: false,
    };
  }

  render() {
    let element;

    const {
      error,
      isLoading,
      onDeleteClick,
      onEditClick,
      onValueChange,
      patientRegistryDialyzer,
      readOnly,
    } = this.props;

    if (isLoading) {
      element = <Loader active inline="centered" />;
    } else if (!patientRegistryDialyzer) {
      element = <RecommendMessage header="No Dialyzer" />;
    } else if (error) {
      element = <ExceptionMessage exception={error} />;
    } else {
      element = (
        <Table basic="very" compact="very" definition striped unstackable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell textAlign="center" content="Result" />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row textAlign="center">
              <Table.Cell content="Brand" />
              <Table.Cell
                content={patientRegistryDialyzer.dialyzerBrand.brand_name}
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Type" />
              <Table.Cell
                content={
                  readOnly ? (
                    patientRegistryDialyzer.type
                  ) : (
                    <Input
                      defaultValue={patientRegistryDialyzer.type}
                      name="type"
                      onBlur={onValueChange}
                      transparent
                      icon={<Icon name="edit" />}
                      iconPosition="left"
                      fluid
                    />
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Size" />
              <Table.Cell
                content={patientRegistryDialyzer.dialyzerSize.size_name}
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Date" />
              <Table.Cell
                content={
                  readOnly ? (
                    moment(patientRegistryDialyzer.dialyzer_date).format('ll')
                  ) : (
                    <Input
                      defaultValue={patientRegistryDialyzer.dialyzer_date}
                      name="dialyzer_date"
                      onBlur={onValueChange}
                      icon={<Icon name="edit" />}
                      iconPosition="left"
                      fluid
                      transparent
                      type="date"
                    />
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Count" />
              <Table.Cell
                content={
                  readOnly ? (
                    patientRegistryDialyzer.count
                  ) : (
                    <Input
                      defaultValue={patientRegistryDialyzer.count}
                      name="count"
                      onBlur={onValueChange}
                      icon={<Icon name="edit" />}
                      iconPosition="left"
                      fluid
                      transparent
                    />
                  )
                }
              />
            </Table.Row>

            {patientRegistryDialyzer.d_t ? (
              <Table.Row textAlign="center">
                <Table.Cell content="Discarded d/t" />
                <Table.Cell
                  content={
                    readOnly ? (
                      moment(patientRegistryDialyzer.d_t).format('ll')
                    ) : (
                      <Input
                        defaultValue={patientRegistryDialyzer.d_t}
                        name="d_t"
                        onBlur={onValueChange}
                        icon={<Icon name="edit" />}
                        iconPosition="left"
                        fluid
                        transparent
                      />
                    )
                  }
                />
              </Table.Row>
            ) : null}
          </Table.Body>
          {onEditClick ? (
            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell
                  textAlign="right"
                  content={
                    <Button
                      basic
                      color="red"
                      compact
                      icon="delete"
                      content="Delete"
                      onClick={() =>
                        this.setState({
                          openConfirm: true,
                        })
                      }
                      disabled={
                        moment(patientRegistryDialyzer.created_at).format(
                          'll'
                        ) !== moment().format('ll')
                      }
                    />
                  }
                />
                <Table.HeaderCell
                  content={
                    <Button
                      basic={this.state.edit ? false : true}
                      color={this.state.edit ? 'red' : 'green'}
                      compact
                      icon={this.state.edit ? 'delete' : 'edit'}
                      content={this.state.edit ? 'Close' : 'Edit'}
                      onClick={() => {
                        this.setState({
                          edit: !this.state.edit,
                        });
                        this.props.onEditClick();
                      }}
                      disabled={
                        moment(patientRegistryDialyzer.created_at).format(
                          'll'
                        ) !== moment().format('ll')
                      }
                    />
                  }
                />
              </Table.Row>
            </Table.Footer>
          ) : null}
        </Table>
      );
    }

    return (
      <div>
        {element}
        <Confirm
          open={this.state.openConfirm}
          onCancel={() => this.setState({ openConfirm: false })}
          onConfirm={() => {
            onDeleteClick();
            this.setState({
              openConfirm: false,
            });
          }}
        />
      </div>
    );
  }
}

export default DialyzerTable;
