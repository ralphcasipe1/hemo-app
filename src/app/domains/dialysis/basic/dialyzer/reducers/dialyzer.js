import {
  CREATE_DIALYZER,
  CREATE_DIALYZER_FAILURE,
  CREATE_DIALYZER_SUCCESS,
  DELETE_DIALYZER,
  DELETE_DIALYZER_FAILURE,
  DELETE_DIALYZER_SUCCESS,
  FETCH_DIALYZER,
  FETCH_DIALYZER_FAILURE,
  FETCH_DIALYZER_SUCCESS,
  UPDATE_DIALYZER,
  UPDATE_DIALYZER_FAILURE,
  UPDATE_DIALYZER_SUCCESS,
} from '../constants/dialyzer';

const initialState = {
  dialyzer: null,
  error: null,
  isLoading: false,
};

export const dialyzerReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_DIALYZER:
    case FETCH_DIALYZER:
    case UPDATE_DIALYZER:
    case DELETE_DIALYZER:
      return {
        ...state,
        isLoading: true,
      };

    case CREATE_DIALYZER_FAILURE:
    case UPDATE_DIALYZER_FAILURE:
    case DELETE_DIALYZER_FAILURE:
    case FETCH_DIALYZER_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    case FETCH_DIALYZER_SUCCESS:
    case CREATE_DIALYZER_SUCCESS:
    case UPDATE_DIALYZER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        dialyzer: action.payload,
      };
    case DELETE_DIALYZER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        dialyzer: null,
      };

    default:
      return state;
  }
};
