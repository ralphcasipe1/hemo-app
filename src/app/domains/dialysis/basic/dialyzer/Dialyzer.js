import React, { Component, Fragment } from 'react';
import { ApplyTemplate } from '../../ApplyTemplate';
import { BasicHemodialysisHOC } from '../BasicHemodialysisHOC';
import { DialyzerForm } from './DialyzerForm';
import { DialyzerTable } from './DialyzerTable';

class Dialyzer extends Component {
  componentDidMount() {
    this.props.fetchAllDialyzerBrands();
    this.props.fetchAllDialyzerSizes();
  }

  render() {
    const { data, dialyzerBrands, dialyzerSizes, handleSubmit } = this.props;

    return (
      <Fragment>
        {!data ? (
          <div>
            <ApplyTemplate />

            <DialyzerForm
              dialyzerBrands={dialyzerBrands}
              dialyzerSizes={dialyzerSizes}
              form="CreateDialyzerForm"
              onFormSubmit={handleSubmit}
            />
          </div>
        ) : (
          <DialyzerTable dialyzer={data} dataId={data.dialyzer_id} />
        )}
      </Fragment>
    );
  }
}

Dialyzer = BasicHemodialysisHOC(Dialyzer, { title: 'Dialyzer' });

export { Dialyzer };
