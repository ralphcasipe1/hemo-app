import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { FieldSelect } from 'app/common/forms/FieldSelect'
import { InputField } from 'app/common/forms/InputField'

class DialyzerForm extends PureComponent {
  renderBrands = brands => {
    return brands.map(brand => ({
      key: brand.dialyzer_brand_id
      , text: brand.brand_name
      , value: brand.dialyzer_brand_id
    }))
  };

  renderSizes = sizes =>
    sizes.map(size => ({
      key: size.dialyzer_size_id
      , text: size.size_name
      , value: size.dialyzer_size_id
    }));

  render() {
    const { dialyzerBrands, dialyzerSizes, submitting, valid } = this.props

    return (
      <Form
        loading={this.props.formLoading}
        onSubmit={this.props.handleSubmit(this.props.onFormSubmit)}
      >
        <Field
          component={FieldSelect}
          name="dialyzer_brand_id"
          items={this.renderBrands(dialyzerBrands)}
          label="Brand"
        />
        <Field component={InputField} name="type" label="Type" />
        <Field
          component={FieldSelect}
          name="dialyzer_size_id"
          items={this.renderSizes(dialyzerSizes)}
          label="Size"
        />
        <Field
          component={InputField}
          type="date"
          name="dialyzer_date"
          label="First Use Date"
        />
        <Field component={InputField} name="count" label="Count" />

        <Field
          component={InputField}
          type="date"
          name="d_t"
          label="Discarded Date"
        />

        <ButtonDecision
          expanded
          isDisable={!valid || submitting}
          {...this.props}
        />
      </Form>
    )
  }
}

const validate = values => {
  let errors = {}

  if (!values.dialyzer_brand_id) {
    errors.dialyzer_brand_id = 'Required'
  }

  if (!values.type) {
    errors.type = 'Required'
  }

  if (!values.dialyzer_size_id) {
    errors.dialyzer_size_id = 'Required'
  }

  if (!values.dialyzer_date) {
    errors.dialyzer_date = 'Required'
  }

  return errors
}

DialyzerForm = reduxForm({
  fields: [
    'dialyzer_brand_id'
    , 'type'
    , 'dialyzer_size_id'
    , 'dialyzer_date'
    , 'count'
    , 'd_t'
  ]
  , enableReinitialize: true
  , validate
})(DialyzerForm)

export { DialyzerForm }
