import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableHeader } from '../TableHeader';
import { DialyzerTableRow } from './DialyzerTableRow';

export const DialyzerTable = props => (
  <Table basic="very" compact="very" definition striped unstackable fixed>
    <TableHeader />

    <DialyzerTableRow {...props} />
  </Table>
);
