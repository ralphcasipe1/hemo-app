import { Button, Divider, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { fetchDialyzerBrands } from '../actions/dialyzerBrands';
import { fetchDialyzerSizes } from '../actions/dialyzerSizes';
import { createDialyzer } from '../actions/dialyzers';
import {
  fetchPatientTemplateDialyzers,
  selectPatientTemplateDialyzer,
} from '../actions/templateDialyzers';
import DialyzerForm from '../components/DialyzerForm';
import FieldSelect from 'FieldSelect';
import moment from 'moment';
import React, { Component } from 'react';

class DialyzerCreationContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      applyTemplate: false,
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(values) {
    this.props.createDialyzer(this.props.params.patientRegistryId, values);
  }

  componentDidMount() {
    this.props.fetchPatientTemplateDialyzers(this.props.params.patientId);
  }

  componentWillMount() {
    this.props.fetchDialyzerBrands();
    this.props.fetchDialyzerSizes();
  }

  componentWillReceiveProps(nextProps) {
    const { patientId, patientRegistryId } = this.props.router.params;
    if (!this.props.dialyzer && !!nextProps.dialyzer) {
      this.props.router.push(
        `/patients/${patientId}/patient_registries/${patientRegistryId}/dialyzers`
      );
    }
  }

  render() {
    const { applyTemplate } = this.state;

    const {
      error,
      isLoading,
      patientTemplateDialyzers,
    } = this.props.patientTemplateDialyzersList;

    console.log(this.props);
    return (
      <div>
        <Button
          color={applyTemplate ? 'instagram' : 'youtube'}
          content="Apply Standing Order"
          icon={
            <Icon
              rotated={applyTemplate ? null : 'counterclockwise'}
              name="thumb tack"
            />
          }
          onClick={() => {
            this.setState({
              applyTemplate: !this.state.applyTemplate,
            });
          }}
          disabled={
            !this.props.patientTemplateDialyzersList.patientTemplateDialyzers
          }
        />

        <Divider section />

        <DialyzerForm
          form="DialyzerForm"
          initialValues={
            applyTemplate
              ? {
                  dialyzer_brand_id: this.props.patientTemplateDialyzersList
                    .patientTemplateDialyzers.dialyzer_brand_id,
                  type: this.props.patientTemplateDialyzersList
                    .patientTemplateDialyzers.type,
                  dialyzer_size_id: this.props.patientTemplateDialyzersList
                    .patientTemplateDialyzers.dialyzer_size_id,
                  dialyzer_date: moment(
                    this.props.patientTemplateDialyzersList
                      .patientTemplateDialyzers.dialyzer_date
                  ).format('YYYY-MM-DD'),
                  count: this.props.patientTemplateDialyzersList
                    .patientTemplateDialyzers.count,
                  d_t: this.props.patientTemplateDialyzersList
                    .patientTemplateDialyzers.d_t,
                }
              : undefined
          }
          onCancel={() => {}}
          onFormSubmit={this.handleFormSubmit}
          dialyzerBrands={this.props.dialyzerBrandsList.dialyzerBrands}
          dialyzerSizes={this.props.dialyzerSizes}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dialyzerBrandsList: state.dialyzerBrands.dialyzerBrandsList,
  dialyzerSizes: state.dialyzerSizes.dialyzerSizes,
  patientTemplateDialyzersList:
    state.patientTemplateDialyzers.patientTemplateDialyzersList,
  selectedPatientTemplateDialyzer:
    state.patientTemplateDialyzers.selectedPatientTemplateDialyzer,
  dialyzer: state.dialyzerReducer.dialyzer,
});

export default connect(
  mapStateToProps,
  {
    fetchDialyzerBrands,
    createDialyzer,
    fetchDialyzerSizes,
    fetchPatientTemplateDialyzers,
    selectPatientTemplateDialyzer,
  }
)(DialyzerCreationContainer);
