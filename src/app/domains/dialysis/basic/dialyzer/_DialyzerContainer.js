import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Divider, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  deleteDialyzer,
  fetchDialyzer,
  updateDialyzer,
} from '../actions/dialyzers';
import DialyzerTable from 'DialyzerTable';

class DialyzerContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      readOnly: true,
    };

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleValueChange = this.handleValueChange.bind(this);
  }

  handleDeleteClick() {
    this.props.deleteDialyzer(
      this.props.params.patientRegistryId,
      this.props.dialyzer.dialyzer_id
    );
  }

  handleEditClick() {
    this.setState({
      readOnly: !this.state.readOnly,
    });
  }

  handleValueChange(e) {
    this.props.updateDialyzer(
      this.props.params.patientRegistryId,
      this.props.dialyzer.dialyzer_id,
      {
        [e.target.name]: e.target.value,
      }
    );
  }

  componentDidMount() {
    this.props.fetchDialyzer(this.props.params.patientRegistryId);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let dialyzer = this.props.dialyzer;
    let nextDialyzer = nextProps.dialyzer;

    if (!dialyzer && nextDialyzer) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/dialyzers`
      );
    }
  }

  render() {
    const { dialyzer, error, isLoading, flashMessage, children } = this.props;

    const { readOnly } = this.state;

    let disableByDate;
    if (!this.props.selectedPatientRegister.patientRegister) {
      disableByDate = false;
    } else {
      disableByDate =
        moment(
          this.props.selectedPatientRegister.patientRegister.created_at
        ).format('ll') !== moment().format('ll');
    }

    return (
      <div>
        <Header content="Dialyzer" />

        {!dialyzer ? (
          <Button
            color="blue"
            content="Add Dialyzer"
            icon="plus"
            disabled={disableByDate}
            onClick={() =>
              this.props.router.push(
                `/patients/${this.props.params.patientId}/patient_registries/${
                  this.props.params.patientRegistryId
                }/dialyzers/create`
              )
            }
          />
        ) : (
          ''
        )}

        <Divider />

        <DialyzerTable
          error={error}
          isLoading={isLoading}
          onDeleteClick={this.handleDeleteClick}
          onEditClick={this.handleEditClick}
          onValueChange={this.handleValueChange}
          patientRegistryDialyzer={dialyzer}
          readOnly={readOnly}
        />

        {children}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dialyzer: state.dialyzerReducer.dialyzer,
  isLoading: state.dialyzerReducer.isLoading,
  error: state.dialyzerReducer.error,
  flashMessage: state.dialyzerReducer.flashMessage,
  selectedPatientRegister: state.patientRegisters.selectedPatientRegister,
  // getPatientRegistryDialyzer: state.dialyzers.getPatientRegistryDialyzer,
});

export default connect(
  mapStateToProps,
  {
    deleteDialyzer,
    fetchDialyzer,
    updateDialyzer,
  }
)(DialyzerContainer);
