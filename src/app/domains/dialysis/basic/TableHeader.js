import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const TableHeader = ({ content }) => (
  <Table.Header>
    <Table.Row>
      <Table.HeaderCell />
      <Table.HeaderCell textAlign="center" content={content} />
    </Table.Row>
  </Table.Header>
);

TableHeader.propTypes = {
  content: PropTypes.string.isRequired,
};

TableHeader.defaultProps = {
  content: 'Result',
};

export { TableHeader };
