import {
  CREATE_VACCINE,
  CREATE_VACCINE_FAILURE,
  CREATE_VACCINE_SUCCESS,
  DELETE_VACCINE,
  DELETE_VACCINE_FAILURE,
  DELETE_VACCINE_SUCCESS,
  FETCH_VACCINE,
  FETCH_VACCINE_FAILURE,
  FETCH_VACCINE_SUCCESS,
  UPDATE_VACCINE,
  UPDATE_VACCINE_FAILURE,
  UPDATE_VACCINE_SUCCESS
} from '../constants/vaccine';

const initialState = {
  vaccine: null
  , error: null
  , isLoading: false
};

export const vaccineReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_VACCINE:
    case DELETE_VACCINE:
    case UPDATE_VACCINE:
    case FETCH_VACCINE:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_VACCINE_FAILURE:
    case DELETE_VACCINE_FAILURE:
    case UPDATE_VACCINE_FAILURE:
    case FETCH_VACCINE_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.payload.message
      };

    case FETCH_VACCINE_SUCCESS:
    case UPDATE_VACCINE_SUCCESS:
    case CREATE_VACCINE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , vaccine: action.payload
      };

    case DELETE_VACCINE_SUCCESS:
      return {
        ...state
        , isLoading: false
        , vaccine: null
      };

    default:
      return state;
  }
};
