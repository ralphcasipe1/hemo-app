import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { InputField } from 'app/common/forms/InputField'
import { TextAreaField } from 'app/common/forms/TextAreaField'

class VaccineForm extends PureComponent {
  render() {
    const { formLoading, handleSubmit, onFormSubmit } = this.props

    return (
      <Form loading={formLoading} onSubmit={handleSubmit(onFormSubmit)}>
        <Field
          component={InputField}
          name="first_dose_date"
          label="First Dose Date"
          type="date"
        />
        <Field
          component={InputField}
          name="second_dose_date"
          label="Second Dose Date"
          type="date"
        />
        <Field
          component={InputField}
          name="third_dose_date"
          label="Third Dose Date"
          type="date"
        />
        <Field
          component={InputField}
          name="booster_date"
          label="Booster Date"
          type="date"
        />
        <Field
          component={InputField}
          name="flu_date"
          label="FLU Date"
          type="date"
        />
        <Field
          component={TextAreaField}
          name="pneumonia_spec"
          label="Pneumonia (Specify)"
        />
        <Field
          component={InputField}
          name="pneumonia_date"
          label="Pneumonia Date"
          type="date"
        />
        <Field component={TextAreaField} name="remarks" label="Other Remarks" />
        <ButtonDecision expanded {...this.props} />
      </Form>
    )
  }
}

VaccineForm = reduxForm({
  fields: [
    'first_dose_date'
    , 'second_dose_date'
    , 'third_dose_date'
    , 'booster_date'
    , 'flu_date'
    , 'pneumonia_spec'
    , 'pneumonia_date'
    , 'remarks'
  ]
})(VaccineForm)

export { VaccineForm }
