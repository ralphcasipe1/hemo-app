import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableHeader } from '../TableHeader';
import { VaccineTableRow } from './VaccineTableRow';

export const VaccineTable = props => (
  <Table basic="very" compact="very" definition striped unstackable fixed>
    <TableHeader />

    <VaccineTableRow {...props} />
  </Table>
);
