import { connect } from 'react-redux';
import { createVaccine, deleteVaccine, fetchVaccine } from './actions/vaccine';
import { Vaccine } from './Vaccine';

const mapStateToProps = state => {
  const { error, isLoading, vaccine } = state.vaccineReducer;

  return {
    isLoading
    , error
    , data: vaccine
  };
};

export const VaccineContainer = connect(
  mapStateToProps,
  {
    create: createVaccine
    , deleteData: deleteVaccine
    , fetchData: fetchVaccine
  }
)(Vaccine);
