import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button, Divider, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';
import {
  deleteVaccine,
  updateVaccine,
  fetchVaccine
} from '../actions/vaccines';
import moment from 'moment';
import VaccineTable from 'VaccineTable';

class VaccineContainer extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(props) {
    super(props);

    this.state = {
      readOnly: true
    };

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleValueChange = this.handleValueChange.bind(this);
  }

  handleDeleteClick() {
    this.props.deleteVaccine(
      this.props.params.patientRegistryId,
      this.props.vaccine.vaccine_id
    );
  }

  handleEditClick() {
    this.setState({
      readOnly: !this.state.readOnly
    });
  }

  handleValueChange(e) {
    this.props.updateVaccine(
      this.props.params.patientRegistryId,
      this.props.vaccine.vaccine_id,
      {
        [e.target.name]: e.target.value
      }
    );
  }

  componentDidMount() {
    this.props.fetchVaccine(this.props.params.patientRegistryId);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let vaccine = this.props.vaccine;
    let nextVaccine = nextProps.vaccine;

    if (!vaccine && nextVaccine) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/vaccines`
      );
    }
  }

  render() {
    const { isLoading, vaccine, error, children } = this.props;

    const { readOnly } = this.state;

    let disableByDate;

    if (!this.props.selectedPatientRegister.patientRegister) {
      disableByDate = false;
    } else {
      disableByDate =
        moment(
          this.props.selectedPatientRegister.patientRegister.created_at
        ).format('ll') !== moment().format('ll');
    }

    return (
      <Fragment>
        <Header content="Vaccine" />

        {!vaccine ? (
          <Button
            color="blue"
            content="Add Vaccine"
            icon="plus"
            disabled={disableByDate}
            onClick={() =>
              this.props.router.push(
                `/patients/${this.props.params.patientId}/patient_registries/${
                  this.props.params.patientRegistryId
                }/vaccines/create`
              )
            }
          />
        ) : (
          ''
        )}

        <Divider />

        <VaccineTable
          error={error}
          isLoading={isLoading}
          onDeleteClick={this.handleDeleteClick}
          onEditClick={this.handleEditClick}
          onValueChange={this.handleValueChange}
          patientRegistryVaccine={vaccine}
          readOnly={readOnly}
        />

        {children}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  vaccine: state.vaccineReducer.vaccine
  , error: state.vaccineReducer.error
  , isLoading: state.vaccineReducer.isLoading
  , flashMessage: state.vaccineReducer.flashMessage
  , selectedPatientRegister: state.patientRegisters.selectedPatientRegister
});

export default connect(
  mapStateToProps,
  {
    deleteVaccine
    , updateVaccine
    , fetchVaccine
  }
)(VaccineContainer);
