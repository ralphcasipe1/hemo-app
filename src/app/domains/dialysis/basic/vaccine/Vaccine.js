import React, { Component } from 'react';
import { ApplyTemplate } from '../../ApplyTemplate';
import { BasicHemodialysisHOC } from '../BasicHemodialysisHOC';
import { VaccineForm } from './VaccineForm';
import { VaccineTable } from './VaccineTable';

class Vaccine extends Component {
  render() {
    const { data, handleSubmit } = this.props;

    return (
      <div>
        {!data ? (
          <div>
            <ApplyTemplate />

            <VaccineForm form="CreateVaccineForm" onFormSubmit={handleSubmit} />
          </div>
        ) : (
          <VaccineTable vaccine={data} dataId={data.vaccine_id} />
        )}
      </div>
    );
  }
}

Vaccine = BasicHemodialysisHOC(Vaccine, { title: 'Vaccine' });

export { Vaccine };
