import React from 'react';
import { Table } from 'semantic-ui-react';

export const VaccineTableRow = ({ vaccine }) => (
  <Table.Body>
    <Table.Row>
      <Table.Cell content="First Dose Date" />
      <Table.Cell textAlign="center">{vaccine.first_dose_date}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Second Dose Date" />
      <Table.Cell textAlign="center">{vaccine.second_dose_date}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Third Dose Date" />
      <Table.Cell textAlign="center">{vaccine.third_dose_date}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Booster Date" />
      <Table.Cell textAlign="center">{vaccine.booster_date}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="FLU Date" />
      <Table.Cell textAlign="center">{vaccine.flu_date}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Pneumonia" />
      <Table.Cell textAlign="center">{vaccine.pneumonia_spec}</Table.Cell>
    </Table.Row>

    <Table.Row>
      <Table.Cell content="Pneumonia Date" />
      <Table.Cell textAlign="center">{vaccine.pneumonia_date}</Table.Cell>
    </Table.Row>
  </Table.Body>
);
