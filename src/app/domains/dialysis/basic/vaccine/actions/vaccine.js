import {
  CREATE_VACCINE,
  CREATE_VACCINE_FAILURE,
  CREATE_VACCINE_SUCCESS,
  DELETE_VACCINE,
  DELETE_VACCINE_FAILURE,
  DELETE_VACCINE_SUCCESS,
  FETCH_VACCINE,
  FETCH_VACCINE_FAILURE,
  FETCH_VACCINE_SUCCESS,
  UPDATE_VACCINE,
  UPDATE_VACCINE_FAILURE,
  UPDATE_VACCINE_SUCCESS
} from '../constants/vaccine';
import { instance } from '../../../../../config/connection';

export const createVaccine = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_VACCINE
  });

  return instance
    .post(`/patient_registries/${patientRegistryId}/vaccines`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_VACCINE_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: CREATE_VACCINE_FAILURE
          , error: error.response
        })
    );
};

export const deleteVaccine = (patientRegistryId, vaccineId) => dispatch => {
  dispatch({
    type: DELETE_VACCINE
  });

  return instance
    .delete(`/patient_registries/${patientRegistryId}/vaccines/${vaccineId}`)
    .then(
      response =>
        dispatch({
          type: DELETE_VACCINE_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: DELETE_VACCINE_FAILURE
          , payload: error.response
        })
    );
};

export const fetchVaccine = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_VACCINE
  });

  return instance.get(`/patient_registries/${patientRegistryId}/vaccines`).then(
    response =>
      dispatch({
        type: FETCH_VACCINE_SUCCESS
        , payload: response.data.data
      }),
    error =>
      dispatch({
        type: FETCH_VACCINE_FAILURE
        , error: error.response
      })
  );
};

export const updateVaccine = (
  patientRegistryId,
  vaccineId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_VACCINE
  });

  return instance
    .patch(
      `/patient_registries/${patientRegistryId}/vaccines/${vaccineId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_VACCINE_SUCCESS
          , payload: response.data.data
        }),
      error =>
        dispatch({
          type: UPDATE_VACCINE_FAILURE
          , error: error.response
        })
    );
};
