import { connect } from 'react-redux';
import { createVaccine } from '../actions/vaccines';
import React, { Component } from 'react';
import VaccineForm from '../components/VaccineForm';

class VaccineCreationContainer extends Component {
  constructor() {
    super();

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(values) {
    this.props.createVaccine(this.props.params.patientRegistryId, values);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { patientId, patientRegistryId } = this.props.router.params;
    if (!this.props.vaccine && !!nextProps.vaccine) {
      this.props.router.push(
        `/patients/${patientId}/patient_registries/${patientRegistryId}/vaccines`
      );
    }
  }

  /* componentWillUnmount() {
    this.props.resetNewPatientRegistryVaccine();
  } */

  render() {
    return (
      <VaccineForm
        form="VaccineForm"
        onFormSubmit={this.handleFormSubmit}
        onCancel={() => {}}
      />
    );
  }
}

const mapStateToProps = state => ({
  vaccine: state.vaccineReducer.vaccine
});

export default connect(
  mapStateToProps,
  {
    createVaccine
  }
)(VaccineCreationContainer);
