import {
  CREATE_PARAMETER,
  CREATE_PARAMETER_FAILURE,
  CREATE_PARAMETER_SUCCESS,
  DELETE_PARAMETER,
  DELETE_PARAMETER_FAILURE,
  DELETE_PARAMETER_SUCCESS,
  FETCH_PARAMETER,
  FETCH_PARAMETER_FAILURE,
  FETCH_PARAMETER_SUCCESS,
  UPDATE_PARAMETER,
  UPDATE_PARAMETER_FAILURE,
  UPDATE_PARAMETER_SUCCESS,
} from '../constants/parameter';

const initialState = {
  parameter: null,
  error: null,
  isLoading: false,
};

export const parameterReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PARAMETER:
    case DELETE_PARAMETER:
    case FETCH_PARAMETER:
    case UPDATE_PARAMETER:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_PARAMETER_SUCCESS:
    case CREATE_PARAMETER_SUCCESS:
    case UPDATE_PARAMETER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        parameter: action.payload,
      };

    case DELETE_PARAMETER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        parameter: null,
      };

    case FETCH_PARAMETER_FAILURE:
    case CREATE_PARAMETER_FAILURE:
    case UPDATE_PARAMETER_FAILURE:
    case DELETE_PARAMETER_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    default:
      return state;
  }
};
