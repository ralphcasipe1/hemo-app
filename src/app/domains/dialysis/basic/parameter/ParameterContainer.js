import { connect } from 'react-redux'
import {
  createParameter,
  deleteParameter,
  fetchParameter
} from './actions/parameter'
import { Parameter } from './Parameter'

const mapStateToProps = state => ({
  data: state.parameterReducer.parameter
  , isLoading: state.parameterReducer.isLoading
})

export const ParameterContainer = connect(
  mapStateToProps,
  {
    create: createParameter
    , deleteParameter
    , fetchData: fetchParameter
  }
)(Parameter)
