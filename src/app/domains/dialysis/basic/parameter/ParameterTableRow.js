import React from 'react';
import { Table } from 'semantic-ui-react';

const { Body, Cell, Row } = Table;
export const ParameterTableRow = ({ parameter }) => (
  <Body>
    <Row>
      <Cell content="Duration" />
      <Cell content={`${parameter.duration} hours`} />
    </Row>

    <Row>
      <Cell content="Blood Flow Rate" />
      <Cell content={`${parameter.blood_flow_rate} ml/min`} />
    </Row>

    <Row>
      <Cell content="UFV Goal" />
      <Cell content={`${parameter.ufv_goal} liter/s`} />
    </Row>

    <Row>
      <Cell content="Dialysate Additive" />
      <Cell content={`${parameter.dialysate_additive} liter/s`} />
    </Row>

    <Row>
      <Cell content="Dialysate Bath" />
      <Cell content={`${parameter.dialysate_bath}`} />
    </Row>

    <Row>
      <Cell content="Dialysate Flow Rate" />
      <Cell content={`${parameter.dialysate_flow_rate}`} />
    </Row>
  </Body>
);
