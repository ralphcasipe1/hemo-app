import React, { Component, Fragment } from 'react';
import { ApplyTemplate } from '../../ApplyTemplate';
import { BasicHemodialysisHOC } from '../BasicHemodialysisHOC';
import { ParameterForm } from './ParameterForm';
import { ParameterTable } from './ParameterTable';

class Parameter extends Component {
  handleDelete = () => {
    const {
      data,
      match: {
        params: { patientRegistryId }
      }
    } = this.props;

    this.props.deleteParameter(patientRegistryId, data.parameter_id);
  };

  render() {
    const { data, handleSubmit } = this.props;

    return (
      <Fragment>
        {!data ? (
          <div>
            <ApplyTemplate />
            <ParameterForm
              form="CreateParameterForm"
              onFormSubmit={handleSubmit}
            />
          </div>
        ) : (
          <ParameterTable parameter={data} dataId={data.parameter_id} />
        )}
      </Fragment>
    );
  }
}

Parameter = BasicHemodialysisHOC(Parameter, { title: 'Parameter' });

export { Parameter };
