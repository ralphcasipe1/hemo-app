import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { toInt } from 'app/common/helpers/to-int'
import { SaveButton } from 'app/common/forms/SaveButton'
import { InputField } from 'app/common/forms/InputField'

class ParameterForm extends PureComponent {
  render() {
    const { loading, handleSubmit, onFormSubmit } = this.props

    return (
      <div>
        <Form
          className="attached"
          onSubmit={handleSubmit(onFormSubmit)}
          loading={loading}
          size="large"
        >
          <Field
            component={InputField}
            inputLabel="hours"
            label="Duration"
            labelPosition="right"
            name="duration"
            parse={toInt}
          />

          <Field
            component={InputField}
            inputLabel="ml/min"
            label="Qb (BFR)"
            name="blood_flow_rate"
            labelPosition="right"
            parse={toInt}
          />

          <Field
            component={InputField}
            inputLabel="liters"
            label="UFV Goal"
            labelPosition="right"
            name="ufv_goal"
          />

          <Field
            component={InputField}
            label="Dialysate Bath"
            name="dialysate_bath"
          />

          <Field
            component={InputField}
            label="Additives"
            name="dialysate_additive"
          />

          <Field
            component={InputField}
            label="Qd (DFR)"
            name="dialysate_flow_rate"
          />

          <SaveButton />
        </Form>
      </div>
    )
  }
}

const validate = values => {
  let errors = {}

  if (!values.duration) {
    errors.duration = 'Required'
  }

  if (!values.blood_flow_rate) {
    errors.blood_flow_rate = 'Required'
  }

  if (!values.ufv_goal) {
    errors.ufv_goal = 'Required'
  }

  if (!values.dialysate_bath) {
    errors.dialysate_bath = 'Required'
  }

  if (!values.dialysate_flow_rate) {
    errors.dialysate_flow_rate = 'Required'
  }

  return errors
}

ParameterForm = reduxForm({
  fields: [
    'duration'
    , 'blood_flow_rate'
    , 'ufv_goal'
    , 'dialysate_bath'
    , 'dialysate_additive'
    , 'dialysate_flow_rate'
  ]
  , enableReinitialize: true
  , validate
})(ParameterForm)

export { ParameterForm }
