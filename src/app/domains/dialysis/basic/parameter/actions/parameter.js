import {
  CREATE_PARAMETER,
  CREATE_PARAMETER_FAILURE,
  CREATE_PARAMETER_SUCCESS,
  DELETE_PARAMETER,
  DELETE_PARAMETER_FAILURE,
  DELETE_PARAMETER_SUCCESS,
  FETCH_PARAMETER,
  FETCH_PARAMETER_FAILURE,
  FETCH_PARAMETER_SUCCESS,
  UPDATE_PARAMETER,
  UPDATE_PARAMETER_FAILURE,
  UPDATE_PARAMETER_SUCCESS,
} from '../constants/parameter';
import { instance } from '../../../../../config/connection';

export const fetchParameter = (patientRegistryId) => (dispatch) => {
  dispatch({
    type: FETCH_PARAMETER,
  });
  instance.get(`/patient_registries/${patientRegistryId}/parameters`).then(
    (response) =>
      dispatch({
        type: FETCH_PARAMETER_SUCCESS,
        payload: response.data.data,
      }),
    (error) =>
      dispatch({
        type: FETCH_PARAMETER_FAILURE,
        error: error.response,
      })
  );
};

export const createParameter = (patientRegistryId, values) => (dispatch) => {
  dispatch({
    type: CREATE_PARAMETER,
  });

  instance
    .post(`/patient_registries/${patientRegistryId}/parameters`, values)
    .then(
      (response) =>
        dispatch({
          type: CREATE_PARAMETER_SUCCESS,
          payload: response.data.data,
        }),
      (error) =>
        dispatch({
          type: CREATE_PARAMETER_FAILURE,
          error: error.response,
        })
    );
};

export const updateWeight = (patientRegistryId, parameterId, values) => (
  dispatch
) => {
  dispatch({
    type: UPDATE_PARAMETER,
  });

  instance
    .patch(
      `/patient_registries/${patientRegistryId}/parameters/${parameterId}`,
      values
    )
    .then(
      (response) =>
        dispatch({
          type: UPDATE_PARAMETER_SUCCESS,
          payload: response.data.data,
        }),
      (error) =>
        dispatch({
          type: UPDATE_PARAMETER_FAILURE,
          error: error.response,
        })
    );
};

export const deleteParameter = (patientRegistryId, parameterId) => (
  dispatch
) => {
  dispatch({
    type: DELETE_PARAMETER,
  });

  instance
    .delete(
      `/patient_registries/${patientRegistryId}/parameters/${parameterId}`
    )
    .then(
      (response) =>
        dispatch({
          type: DELETE_PARAMETER_SUCCESS,
          payload: response.data.data,
        }),
      (error) =>
        dispatch({
          type: DELETE_PARAMETER_FAILURE,
          error: error.response,
        })
    );
};
