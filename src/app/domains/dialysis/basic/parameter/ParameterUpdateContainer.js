import { connect } from 'react-redux';
import { editParameter, fetchParameter } from './actions/parameters';
import { ParameterUpdateForm } from './ParameterUpdateForm';

const mapStateToProps = (state) => ({
  isLoading: state.parameters.isLoading,
  parameter: state.parameters.parameter,
});

export const EditParameterContainer = connect(
  mapStateToProps,
  {
    editParameter,
    fetchParameter,
  }
)(ParameterUpdateForm);
