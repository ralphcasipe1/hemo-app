import React, { Component } from 'react';
import ParameterForm from './ParameterForm';

class EditParameter extends Component {
  componentDidMount() {
    this.props.fetchParameter(this.props.match.params.patientRegistryId);
  }

  handleFormSubmit = (values) => {
    const {
      match: {
        params: { patientRegistryId, parameterId },
      },
    } = this.props;

    this.props.editParameter(patientRegistryId, parameterId, values);

    this.props.history.push(
      `/patient_registries/${patientRegistryId}/parameters`
    );
  };

  render() {
    const { isLoading, parameter } = this.props;

    if (!parameter) {
      return <div />;
    }

    return (
      <div>
        <ParameterForm
          form="EditParameterForm"
          loading={isLoading}
          onFormSubmit={this.handleFormSubmit}
          initialValues={{
            duration: parameter.duration,
            blood_flow_rate: parameter.blood_flow_rate,
            ufv_goal: parameter.ufv_goal,
            dialysate_bath: parameter.dialysate_bath,
            dialysate_additive: parameter.dialysate_additive,
            dialysate_flow_rate: parameter.dialysate_flow_rate,
          }}
        />
      </div>
    );
  }
}

export { EditParameter };
