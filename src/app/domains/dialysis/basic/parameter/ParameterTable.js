import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableHeader } from '../TableHeader';
import { ParameterTableRow } from './ParameterTableRow';

export const ParameterTable = props => (
  <Table basic="very" compact="very" definition striped unstackable fixed>
    <TableHeader />

    <ParameterTableRow {...props} />
  </Table>
);
