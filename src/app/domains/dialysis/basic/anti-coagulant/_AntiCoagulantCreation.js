import { Button, Divider, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import {
  createPatientRegistryAntiCoa,
  resetNewPatientRegistryAntiCoa,
} from '../actions/antiCoagulants';
import {
  fetchPatientTemplateAntiCoagulants,
  resetPatientTemplateAntiCoagulants,
  resetSelectedPatientTemplateAntiCoagulant,
  selectPatientTemplateAntiCoagulant,
} from '../actions/templateAntiCoagulant.js';
import { AntiCoagulantForm } from 'AntiCoagulantForm';
import React, { Component } from 'react';

class AntiCoagulantForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      applyTemplate: false,
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleCancel() {
    this.props.router.push(
      `/patients${this.props.params.patientId}/patient_registries/${
        this.props.params.patientRegistryId
      }`
    );
  }

  handleFormSubmit(values) {
    this.props.createPatientRegistryAntiCoa(
      this.props.params.patientRegistryId,
      values
    );
  }

  componentDidMount() {
    this.props.fetchPatientTemplateAntiCoagulants(this.props.params.patientId);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let antiCoagulant = this.props.getPatientRegistryAntiCoa
      .patientRegistryAntiCoa;
    let nextAntiCoagulant =
      nextProps.getPatientRegistryAntiCoa.patientRegistryAntiCoa;

    if (!antiCoagulant && nextAntiCoagulant) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/anti_coagulants`
      );
    }
  }
  componentWillUnmount() {
    this.props.resetPatientTemplateAntiCoagulants();
    this.props.resetNewPatientRegistryAntiCoa();
  }

  render() {
    const { applyTemplate } = this.state;

    /* const {
      error,
      isLoading,
      patientTemplateAntiCoagulants
    } = this.props.patientTemplateAntiCoagulantsList; */

    return (
      <div>
        <Button
          color={applyTemplate ? 'instagram' : 'youtube'}
          content="Apply Standing Order"
          icon={
            <Icon
              rotated={applyTemplate ? null : 'counterclockwise'}
              name="thumb tack"
            />
          }
          onClick={() => {
            this.setState({
              applyTemplate: !this.state.applyTemplate,
            });
          }}
          disabled={
            !this.props.patientTemplateAntiCoagulantsList
              .patientTemplateAntiCoagulants
          }
        />

        <Divider section />

        <AntiCoagulantForm
          form="AntiCoagulantForm"
          onCancel={this.handleCancel}
          onFormSubmit={this.handleFormSubmit}
          initialValues={
            applyTemplate
              ? {
                  nss_flushing: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.nss_flushing,
                  nss_flushing_every: this.props
                    .patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.nss_flushing_every,
                  lmwh_iv: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.lmwh_iv,
                  lmwh_iv_iu: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.lmwh_iv_iu,
                  ufh_iv: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.ufh_iv,
                  ufh_iv_iu: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.ufh_iv_iu,
                  ufh_iv_iu_every: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.ufh_iv_iu_every,
                  bleeding_desc: this.props.patientTemplateAntiCoagulantsList
                    .patientTemplateAntiCoagulants.bleeding_desc,
                }
              : undefined
          }
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  patientTemplateAntiCoagulantsList:
    state.patientTemplateAntiCoagulants.patientTemplateAntiCoagulantsList,
  selectedPatientTemplateAntiCoagulant:
    state.patientTemplateAntiCoagulants.selectedPatientTemplateAntiCoagulant,
  getPatientRegistryAntiCoa: state.antiCoagulants.getPatientRegistryAntiCoa,
});

export default connect(
  mapStateToProps,
  {
    createPatientRegistryAntiCoa,
    fetchPatientTemplateAntiCoagulants,
    resetNewPatientRegistryAntiCoa,
    resetPatientTemplateAntiCoagulants,
    resetSelectedPatientTemplateAntiCoagulant,
    selectPatientTemplateAntiCoagulant,
  }
)(AntiCoagulantCreationContainer);
