import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { toInt } from '../../../../common/helpers/to-int'
import { ButtonDecision } from 'app/common/forms/ButtonDecision'
import { InputField } from 'app/common/forms/InputField'
import { TextAreaField } from 'app/common/forms/TextAreaField'
import React, { PureComponent } from 'react'

class AntiCoagulantForm extends PureComponent {
  render() {
    const { handleSubmit, loading, onFormSubmit } = this.props

    return (
      <Form
        className="attached"
        onSubmit={handleSubmit(onFormSubmit)}
        loading={loading}
        size="large"
      >
        <Field
          component={InputField}
          inputLabel="ml"
          label="NSS Flushing"
          labelPosition="right"
          name="nss_flushing"
        />

        <Field
          component={InputField}
          inputLabel="mins"
          label="Every"
          labelPosition="right"
          name="nss_flushing_every"
          parse={toInt}
        />

        <Field component={InputField} label="LMWH (IV)" name="lmwh_iv" />

        <Field component={InputField} label="IU" name="lmwh_iv_iu" />

        <Field component={InputField} label="UFH (IV)" name="ufh_iv" />

        <Field component={InputField} label="IU" name="ufh_iv_iu" />

        <Field component={InputField} label="Every" name="ufh_iv_iu_every" />

        <Field
          component={TextAreaField}
          name="bleeding_desc"
          label="Bleeding description"
        />

        <ButtonDecision expanded {...this.props} />
      </Form>
    )
  }
}

AntiCoagulantForm = reduxForm({
  field: [
    'nss_flushing'
    , 'nss_flushing_every'
    , 'lmwh_iv'
    , 'lmwh_iv_iu'
    , 'ufh_iv'
    , 'ufh_iv_iu'
    , 'ufh_iv_iu_every'
    , 'bleeding_desc'
  ]
  , enableReinitialize: true
})(AntiCoagulantForm)

export { AntiCoagulantForm }
