import {
  CLEAR_NEW_ANTI_COAGULANT,
  CREATE_ANTI_COAGULANT,
  CREATE_ANTI_COAGULANT_FAILURE,
  CREATE_ANTI_COAGULANT_SUCCESS,
  DELETE_ANTI_COAGULANT,
  DELETE_ANTI_COAGULANT_FAILURE,
  DELETE_ANTI_COAGULANT_SUCCESS,
  FETCH_ANTI_COAGULANT,
  FETCH_ANTI_COAGULANT_FAILURE,
  FETCH_ANTI_COAGULANT_SUCCESS,
  UPDATE_ANTI_COAGULANT,
  UPDATE_ANTI_COAGULANT_FAILURE,
  UPDATE_ANTI_COAGULANT_SUCCESS,
} from '../constants/anti-coagulant';

const initialState = {
  antiCoagulant: null,
  isLoading: false,
  error: null,
};

export const antiCoagulantReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ANTI_COAGULANT:
    case DELETE_ANTI_COAGULANT:
    case FETCH_ANTI_COAGULANT:
    case UPDATE_ANTI_COAGULANT:
      return {
        ...state,
        isLoading: true,
      };

    case CREATE_ANTI_COAGULANT_SUCCESS:
    case FETCH_ANTI_COAGULANT_SUCCESS:
    case UPDATE_ANTI_COAGULANT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        antiCoagulant: action.payload,
      };

    case DELETE_ANTI_COAGULANT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        antiCoagulant: null,
      };

    case CREATE_ANTI_COAGULANT_FAILURE:
    case FETCH_ANTI_COAGULANT_FAILURE:
    case UPDATE_ANTI_COAGULANT_FAILURE:
    case DELETE_ANTI_COAGULANT_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case CLEAR_NEW_ANTI_COAGULANT:
      return {
        ...state,
        antiCoagulant: null,
        error: null,
        isLoading: false,
      };

    default:
      return state;
  }
};
