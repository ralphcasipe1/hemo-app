import { connect } from 'react-redux';
import {
  createAntiCoagulant,
  fetchAntiCoagulant,
} from './actions/anti-coagulant';
import { AntiCoagulant } from './AntiCoagulant';

const mapStateToProps = state => ({
  data: state.antiCoagulantReducer.antiCoagulant,
  isLoading: state.antiCoagulantReducer.isLoading,
  error: state.antiCoagulantReducer.error,
});

export const AntiCoagulantContainer = connect(
  mapStateToProps,
  {
    create: createAntiCoagulant,
    fetchData: fetchAntiCoagulant,
  }
)(AntiCoagulant);
