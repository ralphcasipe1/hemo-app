import {
  CLEAR_NEW_ANTI_COAGULANT,
  CREATE_ANTI_COAGULANT,
  CREATE_ANTI_COAGULANT_FAILURE,
  CREATE_ANTI_COAGULANT_SUCCESS,
  DELETE_ANTI_COAGULANT,
  DELETE_ANTI_COAGULANT_FAILURE,
  DELETE_ANTI_COAGULANT_SUCCESS,
  FETCH_ANTI_COAGULANT,
  FETCH_ANTI_COAGULANT_FAILURE,
  FETCH_ANTI_COAGULANT_SUCCESS,
  UPDATE_ANTI_COAGULANT,
  UPDATE_ANTI_COAGULANT_FAILURE,
  UPDATE_ANTI_COAGULANT_SUCCESS,
} from '../constants/anti-coagulant';
import { instance } from '../../../../../config/connection';

/**
 * @note AntiCoagulant -> AntiCoa
 * @param {*} id
 * @param {*} props
 */
export const createAntiCoagulant = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_ANTI_COAGULANT,
  });

  instance
    .post(`/patient_registries/${patientRegistryId}/anti_coagulants`, values)
    .then(
      response =>
        dispatch({
          type: CREATE_ANTI_COAGULANT_SUCCESS,
          payload: response.data.data,
        }),
      error =>
        dispatch({
          type: CREATE_ANTI_COAGULANT_FAILURE,
          error: error.response,
        })
    );
};

export const deleteAntiCoagulant = (
  patientRegistryId,
  antiCoagulantId
) => dispatch => {
  dispatch({
    type: DELETE_ANTI_COAGULANT,
  });

  instance
    .delete(
      `/patients_registries/${patientRegistryId}/anti_coagulants/${antiCoagulantId}`
    )
    .then(
      response =>
        dispatch({
          type: DELETE_ANTI_COAGULANT_SUCCESS,
          payload: [],
        }),
      error =>
        dispatch({
          type: DELETE_ANTI_COAGULANT_FAILURE,
          error: error.response,
        })
    );
};

export const updateAntiCoagulant = (
  patientRegistryId,
  antiCoagulantId,
  values
) => dispatch => {
  dispatch({
    type: UPDATE_ANTI_COAGULANT,
  });

  instance
    .patch(
      `/patients_registries/${patientRegistryId}/anti_coagulants/${antiCoagulantId}`,
      values
    )
    .then(
      response =>
        dispatch({
          type: UPDATE_ANTI_COAGULANT_SUCCESS,
          payload: response.data.data,
        }),

      error =>
        dispatch({
          type: UPDATE_ANTI_COAGULANT_FAILURE,
          error: error.response,
        })
    );
};

export const fetchAntiCoagulant = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_ANTI_COAGULANT,
  });
  instance.get(`/patient_registries/${patientRegistryId}/anti_coagulants`).then(
    response =>
      dispatch({
        type: FETCH_ANTI_COAGULANT_SUCCESS,
        payload: response.data.data,
      }),
    error =>
      dispatch({
        type: FETCH_ANTI_COAGULANT_FAILURE,
        error: error.response,
      })
  );
};

export const clearNewAntiCoagulant = () => ({ type: CLEAR_NEW_ANTI_COAGULANT });
