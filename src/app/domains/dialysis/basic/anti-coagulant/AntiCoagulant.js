import React, { Component, Fragment } from 'react';
import { ApplyTemplate } from '../../ApplyTemplate';
import { BasicHemodialysisHOC } from '../BasicHemodialysisHOC';
import { AntiCoagulantForm } from './AntiCoagulantForm';
import { AntiCoagulantTable } from './AntiCoagulantTable';

class AntiCoagulant extends Component {
  render() {
    const { data, handleSubmit } = this.props;

    return (
      <Fragment>
        {!data ? (
          <div>
            <ApplyTemplate />

            <AntiCoagulantForm
              form="CreateAntiCoagulantForm"
              onFormSubmit={handleSubmit}
            />
          </div>
        ) : (
          <AntiCoagulantTable
            antiCoagulant={data}
            dataId={data.anti_coagulant_id}
          />
        )}
      </Fragment>
    );
  }
}

AntiCoagulant = BasicHemodialysisHOC(AntiCoagulant, {
  title: 'Anti-coagulant'
});

export { AntiCoagulant };
