import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableHeader } from '../TableHeader';
import { AntiCoagulantTableRow } from './AntiCoagulantTableRow';

export const AntiCoagulantTable = props => (
  <Table basic="very" compact="very" definition striped unstackable fixed>
    <TableHeader />

    <AntiCoagulantTableRow {...props} />
  </Table>
);
