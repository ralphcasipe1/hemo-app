import React from 'react';
import { Table } from 'semantic-ui-react';

const { Body, Cell, Row } = Table;
export const AntiCoagulantTableRow = ({ antiCoagulant }) => (
  <Body>
    <Row>
      <Cell content="NSS Flushing" />
      <Cell>
        {antiCoagulant.nss_flushing} {antiCoagulant.nss_flushing_every}
      </Cell>
    </Row>

    <Row>
      <Cell content="LMWH (IV)" />
      <Cell>
        {antiCoagulant.lmwh_iv} {antiCoagulant.lmwh_iv_iu}
      </Cell>
    </Row>

    <Row>
      <Cell content="UFH (IV)" />
      <Cell>
        {antiCoagulant.ufh_iv} {antiCoagulant.ufh_iv_iu}{' '}
        {antiCoagulant.ufh_iv_iu_every}
      </Cell>
    </Row>

    <Row>
      <Cell content="Bleeding Description" />
      <Cell>{antiCoagulant.bleeding_desc}</Cell>
    </Row>
  </Body>
);
