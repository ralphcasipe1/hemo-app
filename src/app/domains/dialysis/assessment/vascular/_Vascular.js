import { connect } from 'react-redux'
import { Button, Divider, Header } from 'semantic-ui-react'
import { Link } from 'react-router'
import { fetchAbnormals, resetAbnormals } from '../actions/abnormals'
import {
  editPatientRegistryVascular,
  deletePatientRegistryVascular,
  fetchPatientRegistryVascular,
  resetDeletedPatientRegistryVascular,
  resetEditedPatientRegistryVascular,
  resetPatientRegistryVascular
} from '../actions/vasculars'
import { editAbnormalVascular } from '../actions/vascularAbnormals'
import VascularTable from '../components/VascularTable'
import React, { Component } from 'react'

class Vascular extends Component {
  constructor(props) {
    super(props)

    this.state = {
      readOnly: true
    }

    this.handleEditArterialLocation = this.handleEditArterialLocation.bind(
      this
    )
    this.handleEditNeedles = this.handleEditNeedles.bind(this)
    this.handleEditLocation = this.handleEditLocation.bind(this)
    this.handleEditType = this.handleEditType.bind(this)
    this.handleEditClick = this.handleEditClick.bind(this)
    this.handleValueChange = this.handleValueChange.bind(this)
    this.handleEditNormal = this.handleEditNormal.bind(this)
    this.handleSetAbnormals = this.handleSetAbnormals.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
  }

  componentDidMount() {
    fetchAbnormals()
    fetchPatientRegistryVascular(this.props.params.patientRegistryId)
  }

  componentWillUnmount() {
    resetAbnormals()
    resetPatientRegistryVascular()
    resetEditedPatientRegistryVascular()
    resetDeletedPatientRegistryVascular()
  }

  handleEditArterialLocation(patientRegistryId, vascularId, arterialLocation) {
    editPatientRegistryVascular(patientRegistryId, vascularId, {
      arterial_location: arterialLocation
    })
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.creation.successMessage &&
      nextProps.creation.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/vasculars`
      )
    }
  }

  handleEditClick() {
    this.setState({
      readOnly: !this.state.readOnly
    })
  }

  handleEditNeedles(patientRegistryId, vascularId, name, needle) {
    if (name === 'arterial_needle') {
      editPatientRegistryVascular(patientRegistryId, vascularId, {
        arterial_needle: needle
      })
    } else if (name === 'venous_needle') {
      editPatientRegistryVascular(patientRegistryId, vascularId, {
        venous_needle: needle
      })
    }
  }

  handleEditLocation(patientRegistryId, vascularId, location) {
    editPatientRegistryVascular(patientRegistryId, vascularId, { location })
  }

  handleEditType(patientRegistryId, vascularId, type) {
    editPatientRegistryVascular(patientRegistryId, vascularId, { type })
  }

  handleSetAbnormals(vascularId, values) {
    editAbnormalVascular(vascularId, { abnormal_id: values })
  }

  handleEditNormal({ name, checked }) {
    const {
      params: { patientRegistryId },
      getPatientRegistryVascular: { patientRegistryVascular }
    } = this.props

    editPatientRegistryVascular(
      patientRegistryId,
      patientRegistryVascular.vascular_id,
      {
        [name]: checked
      }
    )
  }

  handleValueChange({ target: { name, value } }) {
    const {
      params: { patientRegistryId },
      getPatientRegistryVascular: { patientRegistryVascular }
    } = this.props

    editPatientRegistryVascular(
      patientRegistryId,
      patientRegistryVascular.vascular_id,
      {
        [name]: value
      }
    )
  }

  handleDelete() {
    this.props.deletePatientRegistryVascular(
      this.props.params.patientRegistryId,
      this.props.getPatientRegistryVascular.patientRegistryVascular.vascular_id
    )
  }

  render() {
    const {
      children,
      getPatientRegistryVascular: { error, isLoading, patientRegistryVascular },
      abnormalsList: { abnormals }
    } = this.props

    const { readOnly } = this.state

    return (
      <div>
        <Header content="Vascular" />

        {!patientRegistryVascular ? (
          <Link
            to={`/patients/${this.props.params.patientId}/patient_registries/${
              this.props.params.patientRegistryId
            }/vasculars/create`}
          >
            <Button color="blue" content="Add Vascular" icon="plus" />
          </Link>
        ) : (
          ''
        )}

        <Divider />

        <VascularTable
          abnormals={abnormals}
          error={error}
          isLoading={isLoading}
          patientRegistryVascular={patientRegistryVascular}
          onDeleteClick={this.handleDelete}
          onEditArterialLocation={this.handleEditArterialLocation}
          onEditClick={this.handleEditClick}
          onEditNeedles={this.handleEditNeedles}
          onEditType={this.handleEditType}
          onEditLocation={this.handleEditLocation}
          onSetAbnormals={this.handleSetAbnormals}
          onValueChange={this.handleValueChange}
          onEditNormal={this.handleEditNormal}
          readOnly={readOnly}
        />

        {children}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  abnormalsList: state.abnormals.abnormalsList
  , getPatientRegistryVascular: state.vasculars.getPatientRegistryVascular
  , creation: state.vasculars.creation
})

export default connect(
  mapStateToProps,
  {
    editPatientRegistryVascular
    , fetchPatientRegistryVascular
    , resetEditedPatientRegistryVascular
    , resetPatientRegistryVascular
    , deletePatientRegistryVascular
    , resetDeletedPatientRegistryVascular
    , editAbnormalVascular
    , fetchAbnormals
    , resetAbnormals
  }
)(Vascular)
