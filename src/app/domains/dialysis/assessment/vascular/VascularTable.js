import React from 'react';
import { Table } from 'semantic-ui-react';
import { DefinitionTableHeader } from '../../../../common/display/DefinitionTableHeader';
import { VascularTableBody } from './VascularTableBody';

export const VascularTable = ({ vascular }) => (
  <Table basic="very" compact="very" definition striped unstackable>
    <DefinitionTableHeader />

    <VascularTableBody vascular={vascular} />
  </Table>
);
