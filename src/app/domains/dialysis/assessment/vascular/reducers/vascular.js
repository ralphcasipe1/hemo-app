import {
  CREATE_VASCULAR,
  CREATE_VASCULAR_SUCCESS,
  CREATE_VASCULAR_FAILURE,
  FETCH_VASCULAR,
  FETCH_VASCULAR_SUCCESS,
  FETCH_VASCULAR_FAILURE,
  UPDATE_VASCULAR,
  UPDATE_VASCULAR_SUCCESS,
  UPDATE_VASCULAR_FAILURE,
  DELETE_VASCULAR,
  DELETE_VASCULAR_SUCCESS,
  DELETE_VASCULAR_FAILURE
} from '../constants/vascular';

const initialState = {
  error: null
  , isLoading: false
  , vascular: null
};

export const vascularReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_VASCULAR:
    case DELETE_VASCULAR:
    case FETCH_VASCULAR:
    case UPDATE_VASCULAR:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_VASCULAR_FAILURE:
    case DELETE_VASCULAR_FAILURE:
    case FETCH_VASCULAR_FAILURE:
    case UPDATE_VASCULAR_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      };

    case CREATE_VASCULAR_SUCCESS:
    case FETCH_VASCULAR_SUCCESS:
    case UPDATE_VASCULAR_SUCCESS:
      return {
        ...state
        , isLoading: false
        , vascular: action.payload
      };

    case DELETE_VASCULAR_SUCCESS:
      return {
        ...state
        , isLoading: false
        , vascular: null
      };

    default:
      return state;
  }
};
