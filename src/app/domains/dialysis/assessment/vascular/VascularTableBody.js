import React from 'react';
import { List, Table } from 'semantic-ui-react';

const { Body, Cell, Row } = Table;

const renderAbnormals = abnormals => {
  if (abnormals.length === 0) {
    return null;
  }
  return abnormals.map(abnormal => (
    <List.Item key={abnormal.abnormal_id}>{abnormal.name}</List.Item>
  ));
};

const getArterialLocation = arterialLocation => {
  let arterialLocationName;

  if (arterialLocation === 1) {
    arterialLocationName = 'Radial';
  } else if (arterialLocation === 2) {
    arterialLocationName = 'Brachial';
  } else if (arterialLocation === 3) {
    arterialLocationName = 'Femoral';
  }

  return arterialLocationName;
};

const getArterialNeedle = arterialNeedle => {
  let arterialNeedleName;

  if (arterialNeedle === 1) {
    arterialNeedleName = 'Gauge 16';
  } else if (arterialNeedle === 2) {
    arterialNeedleName = 'Gauge 17';
  }

  return arterialNeedleName;
};

const getLocation = location => {
  let locationName;

  if (location === 1) {
    locationName = 'Left';
  } else if (location === 2) {
    locationName = 'Right';
  }

  return locationName;
};

const getType = type => {
  let typeName;
  if (type === 1) {
    typeName = 'AV Fistula';
  } else if (type === 2) {
    typeName = 'AV Graft';
  }
  return typeName;
};

const getVenousNeedle = venousNeedle => {
  let venousNeedleName;

  if (venousNeedle === 1) {
    venousNeedleName = 'Gauge 16';
  } else if (venousNeedle === 2) {
    venousNeedleName = 'Gauge 17';
  }

  return venousNeedleName;
};

export const VascularTableBody = ({ vascular }) => (
  <Body>
    <Row textAlign="center">
      <Cell content="Date operation" />
      <Cell content={vascular.operation_date} />
    </Row>

    <Row textAlign="center">
      <Cell content="Surgeon" />
      <Cell content={vascular.surgeon} />
    </Row>

    <Row textAlign="center">
      <Cell content="Type" />
      <Cell content={getType(vascular.type)} />
    </Row>

    <Row textAlign="center">
      <Cell content="Location" />
      <Cell
        content={`${getLocation(vascular.location)} ${getArterialLocation(
          vascular.arterial_location
        )}`}
      />
    </Row>

    <Row textAlign="center">
      <Cell content="Arterial Needle" />
      <Cell content={getArterialNeedle(vascular.arterial_needle)} />
    </Row>

    <Row textAlign="center">
      <Cell content="Venous Needle" />
      <Cell content={getVenousNeedle(vascular.venous_needle)} />
    </Row>

    <Row textAlign="center">
      <Cell content="Normal Assessments" />
      <Cell>
        <div>{vascular.is_bruit ? 'Bruit' : ''}</div>
        <div>{vascular.is_thrill ? 'Thrill' : ''}</div>
      </Cell>
    </Row>

    <Row textAlign="center">
      <Cell content="Abnormal Assessment" />
      <Cell>
        <List>{renderAbnormals(vascular.abnormals)}</List>
      </Cell>
    </Row>
  </Body>
);
