import {
  CREATE_VASCULAR,
  CREATE_VASCULAR_SUCCESS,
  CREATE_VASCULAR_FAILURE,
  FETCH_VASCULAR,
  FETCH_VASCULAR_SUCCESS,
  FETCH_VASCULAR_FAILURE,
  UPDATE_VASCULAR,
  UPDATE_VASCULAR_SUCCESS,
  UPDATE_VASCULAR_FAILURE,
  DELETE_VASCULAR,
  DELETE_VASCULAR_SUCCESS,
  DELETE_VASCULAR_FAILURE
} from '../constants/vascular';
import { instance } from '../../../../../config/connection';

export const fetchVascular = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_VASCULAR
  });

  instance.get(`/patient_registries/${patientRegistryId}/vasculars`).then(
    res =>
      dispatch({
        type: FETCH_VASCULAR_SUCCESS
        , payload: res.data.data
      }),
    err =>
      dispatch({
        type: FETCH_VASCULAR_FAILURE
        , payload: err.response.data
      })
  );
};

export const createVascular = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_VASCULAR
  });

  instance
    .post(`/patient_registries/${patientRegistryId}/vasculars`, values)
    .then(
      res =>
        dispatch({
          type: CREATE_VASCULAR_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: CREATE_VASCULAR_FAILURE
          , payload: err.response.data
        })
    );
};

export const updateVascular = (patientRegistryId, vascularId, values) => {
  return dispatch => {
    dispatch({
      type: UPDATE_VASCULAR
    });

    instance
      .patch(
        `/patient_registries/${patientRegistryId}/vasculars/${vascularId}`,
        values
      )
      .then(
        res =>
          dispatch({
            type: UPDATE_VASCULAR_SUCCESS
            , payload: res.data.data
          }),
        err =>
          dispatch({
            type: UPDATE_VASCULAR_FAILURE
            , payload: err.response.data
          })
      );
  };
};

export const deleteVascular = (patientRegistry, vascularId) => dispatch => {
  dispatch({
    type: DELETE_VASCULAR
  });

  instance
    .delete(`/patient_registries/${patientRegistry}/vasculars/${vascularId}`)
    .then(
      res =>
        dispatch({
          type: DELETE_VASCULAR_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: DELETE_VASCULAR_FAILURE
          , payload: err.response.data
        })
    );
};
