import { connect } from 'react-redux'
import VascularForm from 'VascularForm'
import React, { Component } from 'react'
import { fetchAbnormals, resetAbnormals } from '../actions/abnormals'
/*import {
  createVascularNormals,
  resetNewVascularNormals
} from '../actions/vascularNormals';*/
import {
  createPatientRegistryVascular,
  resetNewPatientRegistryVascular
} from '../actions/vasculars'

class VascularCreation extends Component {
  constructor(props) {
    super(props)

    this.handleCancel = this.handleCancel.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  handleCancel() {
    this.props.router.goBack()
  }

  handleFormSubmit(values) {
    this.props.createPatientRegistryVascular(
      this.props.patientRegistryId,
      values
    )
  }

  componentDidMount() {
    this.props.fetchAbnormals()
  }

  UNSAFE_componentWillUnmount() {
    this.props.resetAbnormals()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.creation.successMessage &&
      nextProps.creation.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/vasculars`
      )
    }
  }

  render() {
    return (
      <div>
        <VascularForm
          abnormals={this.props.abnormalsList.abnormals}
          onCancel={this.handleCancel}
          onFormSubmit={this.handleFormSubmit}
          form="VascularForm"
        />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  abnormalsList: state.abnormals.abnormalsList
  , patientRegistryId: ownProps.params.patientRegistryId
  , creation: state.vasculars.creation
})

export default connect(
  mapStateToProps,
  {
    createPatientRegistryVascular
    , resetNewPatientRegistryVascular
    , fetchAbnormals
    , resetAbnormals
  }
)(VascularCreation)
