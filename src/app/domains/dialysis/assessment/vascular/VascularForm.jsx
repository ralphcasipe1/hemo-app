import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Header, Form } from 'semantic-ui-react'
import { InputField } from 'app/common/forms/InputField'
import { CheckboxGroup } from 'app/common/forms/CheckboxGroup'
import { RadioButton } from 'app/common/forms/RadioButton'
import { SaveButton } from 'app/common/forms/SaveButton'

class VascularForm extends PureComponent {
  renderAbnormals = abnormals => {
    return abnormals.map(abnormal => ({
      key: abnormal.abnormal_id
      , name: abnormal.name
    }))
  };

  render() {
    const { abnormals, handleSubmit, onFormSubmit } = this.props

    let types
    let location
    let arterial_location
    let arterial_needle
    let venous_needle

    types = [ { key: 1, name: 'AV Fistula' }, { key: 2, name: 'AV Graft' } ]

    location = [ { key: 1, name: 'Left' }, { key: 2, name: 'Right' } ]

    arterial_location = [
      { key: 1, name: 'Radial' }
      , { key: 2, name: 'Brachial' }
      , { key: 3, name: 'Femoral' }
    ]

    arterial_needle = [ { key: 1, name: 'G:16' }, { key: 2, name: 'G:17' } ]

    venous_needle = [ { key: 1, name: 'G:16' }, { key: 2, name: 'G:17' } ]

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field
          component={InputField}
          label="Date of Operation"
          name="operation_date"
          type="date"
        />

        {/* Free text */}
        <Field
          component={InputField}
          inputLabel="Dr."
          label="Surgeon"
          name="surgeon"
          required
        />

        <Header as="h4" content="Type" dividing />

        <Field component={RadioButton} name="type" options={types} />

        <Header as="h4" content="Location" dividing />
        <Form.Group inline widths="equal">
          <Field component={RadioButton} name="location" options={location} />

          <Field
            component={RadioButton}
            name="arterial_location"
            options={arterial_location}
          />
        </Form.Group>
        <Header as="h4" content="Assessment Finding" dividing />

        <b>Normal Assessment</b>
        {/*<Field
          component={CheckboxGroup}
          name="normal_assess"
          options={[
            {key: 1, name: 'Bruit'},
            {key: 2, name: 'Thrill'}
          ]}
        />*/}
        <div className="inline field">
          <div className="ui checkbox">
            <Field
              name="is_bruit"
              id="is_bruit"
              component="input"
              type="checkbox"
              className="hidden"
            />
            <label htmlFor="is_bruit">Bruit</label>
          </div>
          <div className="ui checkbox">
            <Field
              name="is_thrill"
              id="is_thrill"
              component="input"
              type="checkbox"
              className="hidden"
            />
            <label htmlFor="is_thrill">Thrill</label>
          </div>
        </div>

        <b>Abnormal Assessment</b>
        <Field
          component={CheckboxGroup}
          name="abnormal_id"
          options={this.renderAbnormals(abnormals)}
        />

        <Header as="h4" content="Needles" dividing />
        <b>Arterial</b>
        <Field
          component={RadioButton}
          name="arterial_needle"
          options={arterial_needle}
        />
        <b>Venous</b>
        <Field
          component={RadioButton}
          name="venous_needle"
          options={venous_needle}
        />

        <SaveButton />
      </Form>
    )
  }
}

VascularForm = reduxForm({
  fields: [
    'type'
    , 'operation_date'
    , 'surgeon'
    , 'location'
    , 'arterial_location'
    , 'arterial_needle'
    , 'venous_needle'
    , 'abnormal_id'
    , 'is_bruit'
    , 'is_thrill'
  ]
})(VascularForm)

export { VascularForm }
