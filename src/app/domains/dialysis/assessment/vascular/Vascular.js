import React, { Component, Fragment } from 'react';
import { Header } from 'semantic-ui-react';
import { VascularForm } from './VascularForm';
import { VascularTable } from './VascularTable';

class Vascular extends Component {
  componentDidMount() {
    this.props.fetchVascular(this.props.match.params.patientRegistryId);
    this.props.fetchAllAbnormals();
  }

  handleSubmit = values =>
    this.props.createVascular(
      this.props.match.params.patientRegistryId,
      values
    );

  render() {
    const { abnormals, isLoading, vascular } = this.props;

    if (isLoading) {
      return <h1>Loading</h1>;
    }

    return (
      <Fragment>
        <Header as="h3" content="Vascular" />

        {!vascular ? (
          <VascularForm
            abnormals={abnormals}
            form="CreateVascularForm"
            onFormSubmit={this.handleSubmit}
          />
        ) : (
          <VascularTable vascular={vascular} />
        )}
      </Fragment>
    );
  }
}

export { Vascular };
