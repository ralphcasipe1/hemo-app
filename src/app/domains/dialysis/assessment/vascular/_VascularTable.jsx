import React, { PureComponent } from 'react';
import {
  Button,
  Confirm,
  Input,
  Checkbox,
  Dropdown,
  Label,
  Loader,
  Radio,
  Table
} from 'semantic-ui-react';
import moment from 'moment';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';

class VascularTable extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      edit: false
      , typeVal: undefined
      , locationVal: undefined
      , arterialLocationVal: undefined
      , arterialNeedleVal: undefined
      , venousNeedleVal: undefined
      , abnormalVals: []
      , isBruit: false
      , isThrill: false
    };

    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleArterialLocationChange = this.handleArterialLocationChange.bind(
      this
    );
    this.handleNeedleChange = this.handleNeedleChange.bind(this);
    this.handleNormalChange = this.handleNormalChange.bind(this);
    this.handleAbnormalsChange = this.handleAbnormalsChange.bind(this);
  }

  handleTypeChange(e, { value }) {
    const {
      params: { patientRegistryId },
      patientRegistryVascular
    } = this.props;

    this.setState({ typeVal: value });
    this.props.onEditType(
      patientRegistryId,
      patientRegistryVascular.vascular_id,
      value
    );
  }

  handleLocationChange(e, { value }) {
    const {
      params: { patientRegistryId },
      patientRegistryVascular
    } = this.props;

    this.setState({
      locationVal: value
    });

    this.props.onEditLocation(
      patientRegistryId,
      patientRegistryVascular.vascular_id,
      value
    );
  }

  handleArterialLocationChange(e, { value }) {
    const {
      params: { patientRegistryId },
      patientRegistryVascular
    } = this.props;

    this.setState({
      arterialLocationVal: value
    });

    this.props.onEditArterialLocation(
      patientRegistryId,
      patientRegistryVascular.vascular_id,
      value
    );
  }

  handleNeedleChange(e, { name, value }) {
    const {
      params: { patientRegistryId },
      patientRegistryVascular
    } = this.props;

    if (name === 'arterial_needle') {
      this.setState({
        arterialNeedleVal: value
      });
    } else if (name === 'venous_needle') {
      this.setState({
        venousNeedleVal: value
      });
    }

    this.props.onEditNeedles(
      patientRegistryId,
      patientRegistryVascular.vascular_id,
      name,
      value
    );
  }

  handleAbnormalsChange(e, { value }) {
    const { patientRegistryVascular } = this.props;

    this.props.onSetAbnormals(patientRegistryVascular.vascular_id, value);
  }

  handleNormalChange(e, { name, checked }) {
    if (name === 'is_bruit') {
      this.setState({
        isBruit: checked
      });
    } else if (name === 'is_thrill') {
      this.setState({
        isThrill: checked
      });
    }

    this.props.onEditNormal({ name, checked });
  }

  /*UNSAFE_componentWillUpdate(nextProps, nextState) {
    const { patientRegistryVascular } = nextProps;

    if (
      !this.props.patientRegistryVascular &&
      nextProps.patientRegistryVascular
    ) {
      this.setState({
        typeVal: patientRegistryVascular.type
        , locationVal: patientRegistryVascular.location
        , arterialLocationVal: patientRegistryVascular.arterial_location
        , arterialNeedleVal: patientRegistryVascular.arterial_needle
        , venousNeedleVal: patientRegistryVascular.venous_needle
        , abnormalVals: patientRegistryVascular.Abnormals.map(
          abnormal => abnormal.abnormal_id
        )
        , isBruit: patientRegistryVascular.is_bruit
        , isThrill: patientRegistryVascular.is_thrill
      });
    }
  }*/

  render() {
    const {
      abnormals,
      error,
      isLoading,
      onDeleteClick,
      onEditClick,
      onValueChange,
      patientRegistryVascular,
      readOnly
    } = this.props;

    const {
      typeVal,
      locationVal,
      arterialLocationVal,
      arterialNeedleVal,
      venousNeedleVal,
      // normalVals,
      abnormalVals,
      isBruit,
      isThrill
    } = this.state;

    if (isLoading) {
      return <Loader active inline="centered" />;
    } else if (error) {
      return <ExceptionMessage exception={error} />;
    } else if (!patientRegistryVascular) {
      return <RecommendMessage header="No Vascular" />;
    }

    let arterial_location;
    let arterial_needle;
    let location;
    let type;
    let venous_needle;

    if (patientRegistryVascular.arterial_location === 1) {
      arterial_location = 'Radial';
    } else if (patientRegistryVascular.arterial_location === 2) {
      arterial_location = 'Brachial';
    } else if (patientRegistryVascular.arterial_location === 3) {
      arterial_location = 'Femoral';
    }

    if (patientRegistryVascular.arterial_needle === 1) {
      arterial_needle = 'Gauge 16';
    } else if (patientRegistryVascular.arterial_needle === 2) {
      arterial_needle = 'Gauge 17';
    }

    if (patientRegistryVascular.location === 1) {
      location = 'Left';
    } else if (patientRegistryVascular.location === 2) {
      location = 'Right';
    }

    if (patientRegistryVascular.type === 1) {
      type = 'AV Fistula';
    } else if (patientRegistryVascular.type === 2) {
      type = 'AV Graft';
    }

    if (patientRegistryVascular.venous_needle === 1) {
      venous_needle = 'Gauge 16';
    } else if (patientRegistryVascular.venous_needle === 2) {
      venous_needle = 'Gauge 17';
    }

    return (
      <div>
        <Table
          basic="very"
          color="blue"
          compact="very"
          definition
          striped
          unstackable
        >
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell textAlign="center" content="Result" />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row textAlign="center">
              <Table.Cell content="Date of Operation" />
              <Table.Cell
                content={
                  readOnly ? (
                    `${moment(patientRegistryVascular.operation_date).format(
                      'll'
                    )}`
                  ) : (
                    <Input
                      defaultValue={patientRegistryVascular.operation_date}
                      fluid
                      icon="edit"
                      iconPosition="left"
                      name="operation_date"
                      onBlur={onValueChange}
                      type="date"
                      transparent
                    />
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Surgeon" />
              <Table.Cell
                content={
                  readOnly ? (
                    `Dr. ${patientRegistryVascular.surgeon}`
                  ) : (
                    <div>
                      <Input
                        defaultValue={patientRegistryVascular.surgeon}
                        fluid
                        icon="edit"
                        iconPosition="left"
                        name="surgeon"
                        onBlur={onValueChange}
                        transparent
                      />
                      <Label
                        basic
                        color="blue"
                        content="No need to put 'Dr.'"
                      />
                    </div>
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Type" />
              <Table.Cell
                content={
                  readOnly ? (
                    `${type}`
                  ) : (
                    <div>
                      <Radio
                        checked={typeVal === 1}
                        label="AV Fistula"
                        name="type"
                        onChange={this.handleTypeChange}
                        value={1}
                      />{' '}
                      <Radio
                        checked={typeVal === 2}
                        label="AV Graft"
                        name="type"
                        onChange={this.handleTypeChange}
                        value={2}
                      />
                    </div>
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Location" />
              <Table.Cell
                content={
                  readOnly ? (
                    `${location} ${arterial_location}`
                  ) : (
                    <div>
                      <div>
                        <Radio
                          checked={locationVal === 1}
                          label="Left"
                          name="location"
                          onChange={this.handleLocationChange}
                          value={1}
                        />{' '}
                        <Radio
                          checked={locationVal === 2}
                          label="Right"
                          name="location"
                          onChange={this.handleLocationChange}
                          value={2}
                        />
                      </div>
                      <div>
                        <Radio
                          checked={arterialLocationVal === 1}
                          label="Radial"
                          name="aterial_location"
                          onChange={this.handleArterialLocationChange}
                          value={1}
                        />{' '}
                        <Radio
                          checked={arterialLocationVal === 2}
                          label="Brachial"
                          name="aterial_location"
                          onChange={this.handleArterialLocationChange}
                          value={2}
                        />{' '}
                        <Radio
                          checked={arterialLocationVal === 3}
                          label="Femoral"
                          name="aterial_location"
                          onChange={this.handleArterialLocationChange}
                          value={3}
                        />
                      </div>
                    </div>
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Arterial Needle" />
              <Table.Cell
                content={
                  readOnly ? (
                    `${arterial_needle}`
                  ) : (
                    <div>
                      <Radio
                        checked={arterialNeedleVal === 1}
                        label="G:16"
                        name="arterial_needle"
                        onChange={this.handleNeedleChange}
                        value={1}
                      />{' '}
                      <Radio
                        checked={arterialNeedleVal === 2}
                        label="G:17"
                        name="arterial_needle"
                        onChange={this.handleNeedleChange}
                        value={2}
                      />
                    </div>
                  )
                }
              />
            </Table.Row>
            <Table.Row textAlign="center">
              <Table.Cell content="Venous Needle" />
              <Table.Cell
                content={
                  readOnly ? (
                    venous_needle
                  ) : (
                    <div>
                      <Radio
                        checked={venousNeedleVal === 1}
                        label="G:16"
                        name="venous_needle"
                        onChange={this.handleNeedleChange}
                        value={1}
                      />{' '}
                      <Radio
                        checked={venousNeedleVal === 2}
                        label="G:17"
                        name="venous_needle"
                        onChange={this.handleNeedleChange}
                        value={2}
                      />
                    </div>
                  )
                }
              />
            </Table.Row>
            <Table.Row textAlign="center">
              <Table.Cell content="Normal Assessments" />
              <Table.Cell>
                {readOnly ? (
                  <div>
                    <div>{patientRegistryVascular.is_bruit ? 'Bruit' : ''}</div>
                    <div>
                      {patientRegistryVascular.is_thrill ? 'Thrill' : ''}
                    </div>
                  </div>
                ) : (
                  <div className="inline field">
                    <Checkbox
                      name="is_bruit"
                      type="checkbox"
                      checked={isBruit}
                      label="Bruit"
                      onClick={this.handleNormalChange}
                    />
                    <Checkbox
                      name="is_thrill"
                      type="checkbox"
                      checked={isThrill}
                      onClick={this.handleNormalChange}
                      label="Thrill"
                    />
                  </div>
                )}
              </Table.Cell>
            </Table.Row>
            {/* <Table.Row textAlign="center">
                            <Table.Cell content="Normal Assessments" />
                            <Table.Cell>
                                {readOnly ?
                                    <ul className="table-list">
                                        {patientRegistryVascular.normalAssessments.map(na => (
                                            <li key={na.vascular_normal_assessment_id}>
                                                {na.normal_assess === 1 ? 'Bruit' : 'Thrill'}
                                            </li>
                                        ))}
                                    </ul> :
                                    <div>
                                        <Checkbox
                                            checked={normalVals.indexOf(1) !== -1}
                                            label="Bruit"
                                            value={1}
                                            onChange={this.handleNormalsChange}

                                        />
                                        <Checkbox
                                            checked={normalVals.indexOf(2) !== -1}
                                            label="Thrill"
                                            value={2}
                                            onChange={this.handleNormalsChange}
                                        />
                                    </div>
                                }
                                
                            </Table.Cell>
                        </Table.Row> */}
            <Table.Row textAlign="center">
              <Table.Cell content="Abnormal Assessments" />
              <Table.Cell>
                {readOnly ? (
                  <ul className="table-list">
                    {patientRegistryVascular.Abnormals.map((ab, index) => (
                      <li key={index}>{ab.name}</li>
                    ))}
                  </ul>
                ) : (
                  <Dropdown
                    fluid
                    multiple
                    options={abnormals.map(abnormal => ({
                      key: abnormal.abnormal_id
                      , value: abnormal.abnormal_id
                      , text: abnormal.name
                    }))}
                    search
                    selection
                    defaultValue={abnormalVals}
                    onChange={this.handleAbnormalsChange}
                    name="abnormal_id"
                  />
                )}
              </Table.Cell>
            </Table.Row>
          </Table.Body>
          {onEditClick ? (
            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell
                  textAlign="right"
                  content={
                    <Button
                      basic
                      color="red"
                      compact
                      icon="trash"
                      content="Delete"
                      onClick={() =>
                        this.setState({
                          openConfirm: true
                        })
                      }
                      disabled={
                        moment(patientRegistryVascular.created_at).format(
                          'll'
                        ) !== moment().format('ll')
                      }
                    />
                  }
                />
                <Table.HeaderCell
                  content={
                    <Button
                      basic={this.state.edit ? false : true}
                      color={this.state.edit ? 'red' : 'green'}
                      compact
                      icon={this.state.edit ? 'delete' : 'edit'}
                      content={this.state.edit ? 'Close' : 'Edit'}
                      onClick={() => {
                        this.setState({
                          edit: !this.state.edit
                        });
                        this.props.onEditClick();
                      }}
                      disabled={
                        moment(patientRegistryVascular.created_at).format(
                          'll'
                        ) !== moment().format('ll')
                      }
                    />
                  }
                />
              </Table.Row>
            </Table.Footer>
          ) : null}
        </Table>
        <Confirm
          open={this.state.openConfirm}
          onCancel={() => this.setState({ openConfirm: false })}
          onConfirm={() => {
            onDeleteClick();
            this.setState({
              openConfirm: false
            });
          }}
        />
      </div>
    );
  }
}

export { VascularTable };
