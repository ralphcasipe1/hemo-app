import { connect } from 'react-redux';
import { fetchAllAbnormals } from '../../../admin/abnormal/actions/abnormal';
import { createVascular, fetchVascular } from './actions/vascular';
import { Vascular } from './Vascular';

const mapStateToProps = state => ({
  abnormals: state.abnormalReducer.abnormals
  , isLoading: state.vascularReducer.isLoading
  , vascular: state.vascularReducer.vascular
});

export const VascularContainer = connect(
  mapStateToProps,
  {
    createVascular
    , fetchAllAbnormals
    , fetchVascular
  }
)(Vascular);
