import {
  Button,
  Checkbox,
  Confirm,
  Dropdown,
  Input,
  Label,
  List,
  Loader,
  Table,
  Radio
} from 'semantic-ui-react';
import RecommendMessage from 'RecommendMessage';
import ExceptionMessage from 'ExceptionMessage';
import moment from 'moment';
import React from 'react';

class CatheterTable extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      abnormalVals: []
      , edit: false
      , typeVal: undefined
      , positionVal: undefined
      , locationVal: undefined
      , isRedPortIn: undefined
      , isRedPortOut: undefined
      , isBluePortIn: undefined
      , isBluePortOut: undefined
    };

    this.handleChange = this.handleChange.bind(this);
  }

  renderListItem(abnormal) {
    return <List.Item key={abnormal.abnormal_id}>{abnormal.name}</List.Item>;
  }

  /*UNSAFE_componentWillUpdate(nextProps, nextState) {
    const { patientRegistryCatheter } = nextProps;

    if (!this.props.patientRegistryCatheter && patientRegistryCatheter) {
      this.setState({
        typeVal: patientRegistryCatheter.type
        , positionVal: patientRegistryCatheter.position
        , locationVal: patientRegistryCatheter.location
        , isRedPortIn: patientRegistryCatheter.is_red_port_inflow
        , isRedPortOut: patientRegistryCatheter.is_red_port_outflow
        , isBluePortIn: patientRegistryCatheter.is_blue_port_inflow
        , isBluePortOut: patientRegistryCatheter.is_blue_port_outflow
        , abnormalVals: patientRegistryCatheter.Abnormals.map(
          abnormal => abnormal.abnormal_id
        )
      });
    }
  }*/

  handleChange(e, { name, value, checked }) {
    const {
      isRedPortIn,
      isRedPortOut,
      isBluePortIn,
      isBluePortOut
    } = this.state;

    if (name === 'type') {
      this.setState({
        typeVal: value
      });
    }

    if (name === 'position') {
      this.setState({
        positionVal: value
      });
    }

    if (name === 'location') {
      this.setState({
        locationVal: value
      });
    }

    if (name === 'is_red_port_inflow') {
      this.setState({
        isRedPortIn: !isRedPortIn
      });
    }

    if (name === 'is_red_port_outflow') {
      this.setState({
        isRedPortOut: !isRedPortOut
      });
    }

    if (name === 'is_blue_port_inflow') {
      this.setState({
        isBluePortIn: !isBluePortIn
      });
    }

    if (name === 'is_blue_port_outflow') {
      this.setState({
        isBluePortOut: !isBluePortOut
      });
    }

    this.props.onCheckChange({ name, value, checked });
  }

  render() {
    let element;

    const {
      abnormals,
      error,
      isLoading,
      onDeleteClick,
      onEditClick,
      onInputChange,
      patientRegistryCatheter,
      readOnly
    } = this.props;

    const {
      abnormalVals,
      typeVal,
      positionVal,
      locationVal,
      isRedPortIn,
      isRedPortOut,
      isBluePortIn,
      isBluePortOut
    } = this.state;

    if (isLoading) {
      element = <Loader active inline="centered" />;
    } else if (!patientRegistryCatheter) {
      element = <RecommendMessage header="No Catheter" />;
    } else if (error) {
      element = <ExceptionMessage exception={error} />;
    } else {
      let location;
      let position;
      let type;

      if (patientRegistryCatheter.type === 1) {
        type = 'Percutaneous / Temporary';
      } else if (patientRegistryCatheter.type === 2) {
        type = 'Tunelled / Permanent';
      }

      if (patientRegistryCatheter.position === 1) {
        position = 'Right';
      } else if (patientRegistryCatheter.position === 2) {
        position = 'Left';
      }

      if (patientRegistryCatheter.location === 1) {
        location = 'Intrajugular';
      } else if (patientRegistryCatheter.location === 2) {
        location = 'Subclavian';
      } else if (patientRegistryCatheter.location === 3) {
        location = 'Femoral';
      }

      element = (
        <Table basic="very" compact="very" definition striped unstackable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell textAlign="center" content="Result" />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row textAlign="center">
              <Table.Cell content="Date Operation" />
              <Table.Cell
                content={
                  readOnly ? (
                    moment(patientRegistryCatheter.operation_date).format('ll')
                  ) : (
                    <Input
                      defaultValue={patientRegistryCatheter.operation_date}
                      name="operation_date"
                      onBlur={onInputChange}
                      transparent
                      icon="edit"
                      iconPosition="left"
                      type="date"
                      fluid
                    />
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Physician" />
              <Table.Cell
                content={
                  readOnly ? (
                    patientRegistryCatheter.physician
                  ) : (
                    <Input
                      defaultValue={patientRegistryCatheter.physician}
                      name="physician"
                      onBlur={onInputChange}
                      transparent
                      icon="edit"
                      iconPosition="left"
                      fluid
                    />
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Days in Situ" />
              <Table.Cell
                content={
                  readOnly ? (
                    patientRegistryCatheter.days_situ
                  ) : (
                    <Input
                      defaultValue={patientRegistryCatheter.days_situ}
                      name="days_situ"
                      onBlur={onInputChange}
                      transparent
                      icon="edit"
                      iconPosition="left"
                      fluid
                    />
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Type" />
              <Table.Cell
                content={
                  readOnly ? (
                    type
                  ) : (
                    <div>
                      <Radio
                        checked={typeVal === 1}
                        label="Percutaneous / Temporary"
                        name="type"
                        onChange={this.handleChange}
                        value={1}
                      />{' '}
                      <Radio
                        checked={typeVal === 2}
                        label="Tunelled / Permanent"
                        name="type"
                        onChange={this.handleChange}
                        value={2}
                      />
                    </div>
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Location" />
              <Table.Cell
                content={
                  readOnly ? (
                    `${position} ${location}`
                  ) : (
                    <div>
                      <div>
                        <Radio
                          checked={positionVal === 1}
                          label="Right"
                          name="position"
                          onChange={this.handleChange}
                          value={1}
                        />{' '}
                        <Radio
                          checked={positionVal === 2}
                          label="Left"
                          name="position"
                          onChange={this.handleChange}
                          value={2}
                        />
                      </div>
                      <div>
                        <Radio
                          checked={locationVal === 1}
                          label="Intrajugular"
                          name="location"
                          onChange={this.handleChange}
                          value={1}
                        />{' '}
                        <Radio
                          checked={locationVal === 2}
                          label="Subclavian"
                          name="location"
                          onChange={this.handleChange}
                          value={2}
                        />
                        <Radio
                          checked={locationVal === 3}
                          label="Femoral"
                          name="location"
                          onChange={this.handleChange}
                          value={3}
                        />
                      </div>
                    </div>
                  )
                }
              />
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Normal Assessment" />
              <Table.Cell>
                {readOnly ? (
                  <div>
                    {patientRegistryCatheter.is_red_port_inflow === true ? (
                      <Label color="red">Red Port Inflow</Label>
                    ) : (
                      ''
                    )}
                    {patientRegistryCatheter.is_red_port_outflow === true ? (
                      <Label color="red">Red Port Outflow</Label>
                    ) : (
                      ''
                    )}
                    {patientRegistryCatheter.is_blue_port_inflow === true ? (
                      <Label color="blue">Blue Port Inflow</Label>
                    ) : (
                      ''
                    )}
                    {patientRegistryCatheter.is_blue_port_outflow === true ? (
                      <Label color="blue">Blue Port Outflow</Label>
                    ) : (
                      ''
                    )}
                  </div>
                ) : (
                  <div>
                    <div>
                      <Checkbox
                        checked={isRedPortIn}
                        name="is_red_port_inflow"
                        label="Red Port Inflow"
                        onClick={this.handleChange}
                      />

                      <Checkbox
                        checked={isRedPortOut}
                        name="is_red_port_outflow"
                        label="Red Port Outflow"
                        onClick={this.handleChange}
                      />
                    </div>
                    <div>
                      <Checkbox
                        checked={isBluePortIn}
                        name="is_blue_port_inflow"
                        label="Blue Port Inflow"
                        onClick={this.handleChange}
                      />

                      <Checkbox
                        checked={isBluePortOut}
                        name="is_blue_port_outflow"
                        label="Blue Port Outflow"
                        onClick={this.handleChange}
                      />
                    </div>
                  </div>
                )}
              </Table.Cell>
            </Table.Row>

            <Table.Row textAlign="center">
              <Table.Cell content="Abnormal Assessment" />
              <Table.Cell>
                {readOnly ? (
                  patientRegistryCatheter.Abnormals.map(abnormalAssessment =>
                    this.renderListItem(abnormalAssessment)
                  )
                ) : (
                  <Dropdown
                    fluid
                    multiple
                    options={abnormals.map(abnormal => ({
                      key: abnormal.abnormal_id
                      , value: abnormal.abnormal_id
                      , text: abnormal.name
                    }))}
                    search
                    selection
                    defaultValue={abnormalVals}
                    name="abnormal_id"
                    onChange={this.handleChange}
                  />
                )}
              </Table.Cell>
            </Table.Row>
          </Table.Body>
          {onEditClick ? (
            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell
                  textAlign="right"
                  content={
                    <Button
                      basic
                      color="red"
                      compact
                      icon="trash"
                      content="Delete"
                      onClick={() =>
                        this.setState({
                          openConfirm: true
                        })
                      }
                      disabled={
                        moment(patientRegistryCatheter.created_at).format(
                          'll'
                        ) !== moment().format('ll')
                      }
                    />
                  }
                />
                <Table.HeaderCell
                  content={
                    <Button
                      basic={this.state.edit ? false : true}
                      color={this.state.edit ? 'red' : 'green'}
                      compact
                      icon={this.state.edit ? 'delete' : 'edit'}
                      content={this.state.edit ? 'Close' : 'Edit'}
                      onClick={() => {
                        this.setState({
                          edit: !this.state.edit
                        });
                        this.props.onEditClick();
                      }}
                      disabled={
                        moment(patientRegistryCatheter.created_at).format(
                          'll'
                        ) !== moment().format('ll')
                      }
                    />
                  }
                />
              </Table.Row>
            </Table.Footer>
          ) : null}
        </Table>
      );
    }

    return (
      <div>
        {element}
        <Confirm
          open={this.state.openConfirm}
          onCancel={() => this.setState({ openConfirm: false })}
          onConfirm={() => {
            onDeleteClick();
            this.setState({
              openConfirm: false
            });
          }}
        />
      </div>
    );
  }
}

export { CatheterTable };
