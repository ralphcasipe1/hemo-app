import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Header, Form } from 'semantic-ui-react'
import { Checkbox } from 'app/common/forms/Checkbox'
import { CheckboxGroup } from 'app/common/forms/CheckboxGroup'
import { InputField } from 'app/common/forms/InputField'
import { RadioButton } from 'app/common/forms/RadioButton'
import { SaveButton } from 'app/common/forms/SaveButton'

class CatheterForm extends PureComponent {
  static defaultProps = {
    abnormals: []
    , loading: false
  };

  renderAbnormals = abnormals => {
    return abnormals.map(abnormal => ({
      key: abnormal.abnormal_id
      , name: abnormal.name
    }))
  };

  render() {
    const { abnormals, handleSubmit, loading, onFormSubmit } = this.props

    let type
    let position
    let location

    type = [
      { key: 1, name: 'Percutaneous / Temporary' }
      , { key: 2, name: 'Tunelled / Permanent' }
    ]

    position = [ { key: 1, name: 'Right' }, { key: 2, name: 'Left' } ]

    location = [
      { key: 1, name: 'Intrajugular' }
      , { key: 2, name: 'Subclavian' }
      , { key: 3, name: 'Femoral' }
    ]

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={loading}>
        <Field
          component={InputField}
          type="date"
          name="operation_date"
          label="Date of Operation"
        />

        <Field
          component={InputField}
          inputLabel="Dr."
          label="Physician"
          name="physician"
          required
        />

        <Field component={InputField} name="days_situ" label="Days in situ" />
        <Header as="h4" content="Type" dividing />
        <Field component={RadioButton} name="type" options={type} />

        <Header as="h4" content="Location" dividing />

        <Form.Group inline widths="equal">
          <Field component={RadioButton} name="position" options={position} />
          <Field component={RadioButton} name="location" options={location} />
        </Form.Group>

        <Header as="h4" content="Assessment Findings" dividing />

        <Header as="h5" content="Normal" />

        <b>Red Port</b>
        <Field
          component={Checkbox}
          name="is_red_port_inflow"
          label="Inflow"
          identity="is_red_port_inflow"
        />
        <Field
          component={Checkbox}
          name="is_red_port_outflow"
          label="Outflow"
          identity="is_red_port_outflow"
        />

        <b>Blue Port</b>
        <Field
          component={Checkbox}
          name="is_blue_port_inflow"
          label="Inflow"
          identity="is_blue_port_inflow"
        />
        <Field
          component={Checkbox}
          name="is_blue_port_outflow"
          label="Outflow"
          identity="is_blue_port_outflow"
        />

        <Header as="h5" content="Abnormal" />

        <Field
          component={CheckboxGroup}
          name="abnormal_id"
          options={this.renderAbnormals(abnormals)}
        />

        <SaveButton />
      </Form>
    )
  }
}

CatheterForm = reduxForm({
  fields: [
    'operation_date'
    , 'physician'
    , 'days_situ'
    , 'type'
    , 'position'
    , 'location'
    , 'is_red_port_inflow'
    , 'is_red_port_outflow'
    , 'is_blue_port_inflow'
    , 'is_blue_port_outflow'
  ]
  , enableReinitialize: true
})(CatheterForm)

export { CatheterForm }
