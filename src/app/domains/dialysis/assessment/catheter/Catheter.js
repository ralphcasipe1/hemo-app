import React, { Component, Fragment } from 'react'
import { Header }                     from 'semantic-ui-react'
import { CatheterForm }               from './CatheterForm'
import { CatheterTable }              from './CatheterTable'

class Catheter extends Component {
  componentDidMount() {
    this.props.fetchCatheter(this.props.match.params.patientRegistryId)
    this.props.fetchAllAbnormals()
  }

  handleSubmit = values => {
    this.props.createCatheter(
      this.props.match.params.patientRegistryId,
      values
    )
    this.props.fetchCatheter(this.props.match.params.patientRegistryId)
  }


  render() {
    const { abnormals, catheter, isLoading } = this.props

    if (isLoading) {
      return <h1>Loading</h1>
    }

    return (
      <Fragment>
        <Header as="h3" content="Catheter" />

        {!catheter ? (
          <CatheterForm
            abnormals={abnormals}
            form="CreateCatheterForm"
            onFormSubmit={this.handleSubmit}
          />
        ) : (
          <CatheterTable catheter={catheter} />
        )}
      </Fragment>
    )
  }
}

export { Catheter }
