import React from 'react';
import { Table } from 'semantic-ui-react';
import { DefinitionTableHeader } from '../../../../common/display/DefinitionTableHeader';
import { CatheterTableBody } from './CatheterTableBody';

export const CatheterTable = ({ catheter }) => (
  <Table basic="very" compact="very" definition striped unstackable>
    <DefinitionTableHeader />

    <CatheterTableBody catheter={catheter} />
  </Table>
);
