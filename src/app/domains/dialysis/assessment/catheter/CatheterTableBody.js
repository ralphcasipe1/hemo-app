import React from 'react';
import { Label, List, Table } from 'semantic-ui-react';

const { Body, Cell, Row } = Table;
const renderAbnormals = abnormals => {
  if (abnormals.length === 0) {
    return null;
  }
  return abnormals.map(abnormal => (
    <List.Item key={abnormal.abnormal_id}>{abnormal.name}</List.Item>
  ));
};

const getPosition = position => {
  let positionName;
  if (position === 1) {
    positionName = 'Right';
  } else if (position === 2) {
    positionName = 'Left';
  }
  return positionName;
};

const getLocation = location => {
  let locationName;

  if (location === 1) {
    locationName = 'Intrajugular';
  } else if (location === 2) {
    locationName = 'Subclavian';
  } else if (location === 3) {
    locationName = 'Femoral';
  }

  return locationName;
};

const getType = type => {
  let typeName;
  if (type === 1) {
    typeName = 'Percutaneous / Temporary';
  } else if (type === 2) {
    typeName = 'Tunelled / Permanent';
  }

  return typeName;
};

export const CatheterTableBody = ({ catheter }) => (
  <Body>
    <Row textAlign="center">
      <Cell content="Date operation" />
      <Cell content={catheter.operation_date} />
    </Row>

    <Row textAlign="center">
      <Cell content="Physician" />
      <Cell content={catheter.physician} />
    </Row>

    <Row textAlign="center">
      <Cell content="Days in Situ" />
      <Cell content={`${catheter.days_situ} Day(s)`} />
    </Row>

    <Row textAlign="center">
      <Cell content="Type" />
      <Cell content={getType(catheter.type)} />
    </Row>

    <Row textAlign="center">
      <Cell content="Location" />
      <Cell
        content={`${getPosition(catheter.position)} ${getLocation(
          catheter.location
        )}`}
      />
    </Row>

    <Row textAlign="center">
      <Cell content="Normal Assessment" />
      <Cell>
        {catheter.is_red_port_inflow ? (
          <Label color="red">Red Port Inflow</Label>
        ) : null}
        {catheter.is_red_port_outflow ? (
          <Label color="red">Red Port Outflow</Label>
        ) : null}
        {catheter.is_blue_port_inflow ? (
          <Label color="blue">Blue Port Inflow</Label>
        ) : null}
        {catheter.is_blue_port_outflow ? (
          <Label color="blue">Blue Port Outflow</Label>
        ) : null}
      </Cell>
    </Row>

    <Row textAlign="center">
      <Cell content="Abnormal Assessment" />
      <Cell>
        <List>{renderAbnormals(catheter.abnormals)}</List>
      </Cell>
    </Row>
  </Body>
);
