import {
  CREATE_CATHETER,
  CREATE_CATHETER_SUCCESS,
  CREATE_CATHETER_FAILURE,
  FETCH_CATHETER,
  FETCH_CATHETER_SUCCESS,
  FETCH_CATHETER_FAILURE,
  UPDATE_CATHETER,
  UPDATE_CATHETER_SUCCESS,
  UPDATE_CATHETER_FAILURE,
  DELETE_CATHETER,
  DELETE_CATHETER_SUCCESS,
  DELETE_CATHETER_FAILURE
} from '../constants/catheter';

const initialState = {
  catheter: null
  , error: null
  , isLoading: false
};

export const catheterReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_CATHETER:
    case DELETE_CATHETER:
    case FETCH_CATHETER:
    case UPDATE_CATHETER:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_CATHETER_FAILURE:
    case DELETE_CATHETER_FAILURE:
    case FETCH_CATHETER_FAILURE:
    case UPDATE_CATHETER_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      };

    case CREATE_CATHETER_SUCCESS:
    case FETCH_CATHETER_SUCCESS:
    case UPDATE_CATHETER_SUCCESS:
      return {
        ...state
        , catheter: action.payload
        , isLoading: false
      };

    case DELETE_CATHETER_SUCCESS:
      return {
        ...state
        , catheter: null
        , isLoading: false
      };

    default:
      return state;
  }
};
