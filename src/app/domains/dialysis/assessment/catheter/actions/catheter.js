import {
  CREATE_CATHETER,
  CREATE_CATHETER_SUCCESS,
  CREATE_CATHETER_FAILURE,
  FETCH_CATHETER,
  FETCH_CATHETER_SUCCESS,
  FETCH_CATHETER_FAILURE,
  UPDATE_CATHETER,
  UPDATE_CATHETER_SUCCESS,
  UPDATE_CATHETER_FAILURE,
  DELETE_CATHETER,
  DELETE_CATHETER_SUCCESS,
  DELETE_CATHETER_FAILURE
} from '../constants/catheter';
import { instance } from '../../../../../config/connection';

export const createCatheter = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_CATHETER
  });

  instance
    .post(`patient_registries/${patientRegistryId}/catheters`, values)
    .then(
      res =>
        dispatch({
          type: CREATE_CATHETER_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: CREATE_CATHETER_FAILURE
          , payload: err.response
        })
    );
};

export const deleteCatheter = (patientRegistryId, catheterId) => dispatch => {
  dispatch({
    type: DELETE_CATHETER
  });

  instance
    .delete(`patient_registries/${patientRegistryId}/catheters/${catheterId}`)
    .then(
      res =>
        dispatch({
          type: DELETE_CATHETER_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: DELETE_CATHETER_FAILURE
          , payload: err.response.data
        })
    );
};

export const fetchCatheter = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_CATHETER
  });
  instance.get(`patient_registries/${patientRegistryId}/catheters`).then(
    res =>
      dispatch({
        type: FETCH_CATHETER_SUCCESS
        , payload: res.data.data
      }),
    err =>
      dispatch({
        type: FETCH_CATHETER_FAILURE
        , payload: err.response.data
      })
  );
};

export const updateCatheter = (
  patientRegistryId,
  catheterId,
  props
) => dispatch => {
  dispatch({
    type: UPDATE_CATHETER
  });

  instance
    .patch(
      `patient_registries/${patientRegistryId}/catheters/${catheterId}`,
      props
    )
    .then(
      res =>
        dispatch({
          type: UPDATE_CATHETER_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: UPDATE_CATHETER_FAILURE
          , payload: err
        })
    );
};
