import { connect } from 'react-redux';
import { fetchAllAbnormals } from '../../../admin/abnormal/actions/abnormal';
import { createCatheter, fetchCatheter } from './actions/catheter';
import { Catheter } from './Catheter';

const mapStateToProps = state => ({
  abnormals: state.abnormalReducer.abnormals
  , catheter: state.catheterReducer.catheter
  , isLoading: state.catheterReducer.isLoading
});

export const CatheterContainer = connect(
  mapStateToProps,
  {
    createCatheter
    , fetchAllAbnormals
    , fetchCatheter
  }
)(Catheter);
