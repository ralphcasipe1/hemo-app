import { List } from 'semantic-ui-react';
import PostSubjectiveAssessmentsListItem from './PostSubjectiveAssessmentsListItem';
import PropTypes from 'prop-types';
import React from 'react';

const PostSubjectiveAssessmentsList = ({ patientRegistryPostSubjs }) => (
  <List>
    {patientRegistryPostSubjs.map(subjective => (
      <PostSubjectiveAssessmentsListItem
        subjective={subjective}
        key={subjective.subjective_id}
      />
    ))}
  </List>
);

PostSubjectiveAssessmentsList.propTypes = {
  patientRegistryPostSubjs: PropTypes.array
};

export default PostSubjectiveAssessmentsList;
