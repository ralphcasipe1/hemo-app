import { Button, Header } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import SubjectiveAssessmentForm from './SubjectiveAssessmentForm';
import SubjectiveAssessmentList from './SubjectiveAssessmentList';
import SubjectiveAssessmentRemark from './SubjectiveAssessmentRemark';

class SubjectiveAssessment extends PureComponent {
  constructor() {
    super();

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    const { patientRegistryId } = this.props.params;
    this.onInit(this.props, patientRegistryId);
  }

  onInit = (props, id) => {
    props.fetchPatientRegistrySubjs(id);
    props.getSubjectiveRemark(id);
  };

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createPatientRegistrySubjs(patientRegistryId, values);
    this.props.createSubjectiveRemark(patientRegistryId, values);
  }

  handleDelete() {
    const { subjectiveRemark, patientRegistrySubjs } = this.props;
    const { patientRegistryId } = this.props.params;

    if (!_.isEmpty(patientRegistrySubjs)) {
      this.props.deletePatientRegistrySubjs(patientRegistryId);
    }

    if (subjectiveRemark) {
      this.props.deleteSubjectiveRemark(
        patientRegistryId,
        subjectiveRemark.subjective_remark_id
      );
    }
  }

  render() {
    const { patientRegistrySubjs, subjectiveRemark } = this.props;

    return (
      <div>
        <Header as="h3" content="Subjective Assessment" />
        {_.isEmpty(patientRegistrySubjs) && !subjectiveRemark ? (
          <SubjectiveAssessmentForm
            form="SubjectiveAssessmentForm"
            onFormSubmit={this.handleFormSubmit}
            {...this.props}
          />
        ) : (
          <div>
            <SubjectiveAssessmentList
              patientRegistrySubjs={patientRegistrySubjs}
            />
            <SubjectiveAssessmentRemark subjectiveRemark={subjectiveRemark} />
            <Button
              content="Delete"
              color="red"
              icon="trash"
              onClick={this.handleDelete}
            />
          </div>
        )}
      </div>
    );
  }
}

SubjectiveAssessment.propTypes = {
  createPatientRegistrySubjs: PropTypes.func.isRequired
  , createSubjectiveRemark: PropTypes.func.isRequired
  , deletePatientRegistrySubjs: PropTypes.func.isRequired
  , fetchSubjectives: PropTypes.func.isRequired
  , fetchPatientRegistrySubjs: PropTypes.func.isRequired
  , deleteSubjectiveRemark: PropTypes.func.isRequired
  , subjectiveRemark: PropTypes.object
};

export default withRouter(SubjectiveAssessment);
