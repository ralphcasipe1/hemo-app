import { List } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const { Item } = List;
const PostSubjectiveAssessmentsListItem = ({ subjective }) => (
  <Item content={subjective.name} />
);

PostSubjectiveAssessmentsListItem.propTypes = {
  subjective: PropTypes.shape({
    name: PropTypes.func.isRequired
  })
};

export default PostSubjectiveAssessmentsListItem;
