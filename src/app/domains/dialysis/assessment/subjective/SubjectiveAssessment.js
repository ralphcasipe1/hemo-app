import React, { Component, Fragment } from 'react'
import { Header }                     from 'semantic-ui-react'
import { SubjectiveAssessmentForm }   from './SubjectiveAssessmentForm'
import { SubjectiveAssessmentList }   from './SubjectiveAssessmentList'
import { SubjectiveAssessmentRemark } from './SubjectiveAssessmentRemark'

class SubjectiveAssessment extends Component {
  componentDidMount() {
    this.props.fetchAllSubjectiveAssessments(
      this.props.match.params.patientRegistryId
    )
    this.props.fetchAllSubjectives()
  }

  handleSubmit = values =>
    this.props.createSubjectiveAssessments(
      this.props.match.params.patientRegistryId,
      values
    );

  render() {
    const { isLoading, subjectives, subjectiveAssessments } = this.props
    if (isLoading) {
      return <h1>Loading</h1>
    }

    return (
      <Fragment>
        <Header as="h3" content="Subjective Assessment" />
        { subjectiveAssessments.length === 0 ?
            <SubjectiveAssessmentForm
              form="CreateSubjectiveAssessmentForm"
              subjectives={subjectives}
              onFormSubmit={this.handleSubmit}
            /> :
            <div>
              <SubjectiveAssessmentList
                subjectiveAssessments={subjectiveAssessments}
              />
              <SubjectiveAssessmentRemark subjectiveRemark={null} />
            </div>
        }
      </Fragment>
    )
  }
}

export { SubjectiveAssessment }
