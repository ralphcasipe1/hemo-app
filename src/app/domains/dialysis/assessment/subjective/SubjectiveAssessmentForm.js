import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { CheckboxGroup } from 'app/common/forms/CheckboxGroup'
import { SaveButton } from 'app/common/forms/SaveButton'
import { TextAreaField } from 'app/common/forms/TextAreaField'

class SubjectiveAssessmentForm extends PureComponent {
  renderSubjectives = subjectives =>
    subjectives.map(subjective => ({
      key: subjective.subjective_id
      , name: subjective.name
    }));

  render() {
    const { subjectives, handleSubmit, formLoading, onFormSubmit } = this.props

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Field
          component={CheckboxGroup}
          name="subjective_id"
          options={this.renderSubjectives(subjectives)}
        />

        <Field component={TextAreaField} name="remark" label="Other Remarks" />

        <SaveButton />
      </Form>
    )
  }
}

SubjectiveAssessmentForm = reduxForm({
  fields: [ 'subjective_assessment_id', 'remark' ]
})(SubjectiveAssessmentForm)

export { SubjectiveAssessmentForm }
