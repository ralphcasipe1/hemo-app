import PropTypes from 'prop-types'
import React     from 'react'

const SubjectiveAssessmentRemark = ({ subjectiveRemark }) => (
  <div>{!subjectiveRemark ? 'No remarks' : subjectiveRemark.remark}</div>
)

SubjectiveAssessmentRemark.propTypes = {
  subjectiveRemark: PropTypes.shape({
    remark: PropTypes.string
  })
}

export { SubjectiveAssessmentRemark }
