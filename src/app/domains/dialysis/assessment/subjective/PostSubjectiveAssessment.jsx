import { Button, Header } from 'semantic-ui-react';
import _ from 'lodash';
import PostSubjectiveAssessmentForm from './PostSubjectiveAssessmentForm';
import PostSubjectiveAssessmentsList from './PostSubjectiveAssessmentsList';
import PostSubjectiveAssessmentRemark from './PostSubjectiveAssessmentRemark';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PostSubjectiveAssessment extends PureComponent {
  constructor() {
    super();

    this.handleDelete = this.handleDelete.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    const { patientRegistryId } = this.props.params;
    this.onInit(this.props, patientRegistryId);
  }

  onInit(props, id) {
    props.fetchPatientRegistryPostSubjs(id);
    props.getPostSubjectiveRemark(id);
  }

  handleDelete() {
    const { postSubjectiveRemark, patientRegistryPostSubjs } = this.props;
    const { patientRegistryId } = this.props.params;

    if (!_.isEmpty(patientRegistryPostSubjs)) {
      this.props.deletePatientRegistryPostSubjs(patientRegistryId);
    }

    if (postSubjectiveRemark) {
      this.props.deletePostSubjectiveRemark(
        patientRegistryId,
        postSubjectiveRemark.post_subjective_remark_id
      );
    }
  }

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createPatientRegistryPostSubjs(patientRegistryId, values);
    this.props.createPostSubjectiveRemark(patientRegistryId, values);
  }

  render() {
    const { patientRegistryPostSubjs, postSubjectiveRemark } = this.props;

    return (
      <div>
        <Header as="h3" content="Post Subjective Assessment" />
        {_.isEmpty(patientRegistryPostSubjs) && !postSubjectiveRemark ? (
          <PostSubjectiveAssessmentForm
            form="PostSubjectiveAssessmentForm"
            onFormSubmit={this.handleFormSubmit}
            {...this.props}
          />
        ) : (
          <div>
            <PostSubjectiveAssessmentsList
              patientRegistryPostSubjs={patientRegistryPostSubjs}
            />
            <PostSubjectiveAssessmentRemark
              postSubjectiveRemark={postSubjectiveRemark}
            />
            <Button
              content="Delete"
              color="red"
              icon="trash"
              onClick={this.handleDelete}
            />
          </div>
        )}
      </div>
    );
  }
}

PostSubjectiveAssessment.propTypes = {
  createPatientRegistryPostSubjs: PropTypes.func.isRequired
  , fetchPatientRegistryPostSubjs: PropTypes.func.isRequired
  , deletePatientRegistryPostSubjs: PropTypes.func.isRequired
  , patientRegistryPostSubjs: PropTypes.array
  , createPostSubjectiveRemark: PropTypes.func.isRequired
  , getPostSubjectiveRemark: PropTypes.func.isRequired
  , deletePostSubjectiveRemark: PropTypes.func.isRequired
  , postSubjectiveRemark: PropTypes.object
};

export default PostSubjectiveAssessment;
