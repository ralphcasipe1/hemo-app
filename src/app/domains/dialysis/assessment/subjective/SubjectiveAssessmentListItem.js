import React    from 'react'
import { List } from 'semantic-ui-react'

const { Item } = List
const SubjectiveAssessmentListItem = ({ subjectiveAssessment }) => (
  <Item content={subjectiveAssessment.name} />
)

export { SubjectiveAssessmentListItem }
