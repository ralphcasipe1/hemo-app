import {
  CREATE_SUBJECTIVE_ASSESSMENTS,
  CREATE_SUBJECTIVE_ASSESSMENTS_FAILURE,
  CREATE_SUBJECTIVE_ASSESSMENTS_SUCCESS,
  DELETE_SUBJECTIVE_ASSESSMENTS,
  DELETE_SUBJECTIVE_ASSESSMENTS_FAILURE,
  DELETE_SUBJECTIVE_ASSESSMENTS_SUCCESS,
  FETCH_ALL_SUBJECTIVE_ASSESSMENTS,
  FETCH_ALL_SUBJECTIVE_ASSESSMENTS_FAILURE,
  FETCH_ALL_SUBJECTIVE_ASSESSMENTS_SUCCESS
} from '../constants/subjective-assessment';

const initialState = {
  subjectiveAssessments: []
  , isLoading: false
  , error: null
};

export const subjectiveAssessmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_SUBJECTIVE_ASSESSMENTS:
    case DELETE_SUBJECTIVE_ASSESSMENTS:
    case FETCH_ALL_SUBJECTIVE_ASSESSMENTS:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_SUBJECTIVE_ASSESSMENTS_FAILURE:
    case DELETE_SUBJECTIVE_ASSESSMENTS_FAILURE:
    case FETCH_ALL_SUBJECTIVE_ASSESSMENTS_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      };

    case CREATE_SUBJECTIVE_ASSESSMENTS_SUCCESS:
    case DELETE_SUBJECTIVE_ASSESSMENTS_SUCCESS:
    case FETCH_ALL_SUBJECTIVE_ASSESSMENTS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , subjectiveAssessments: action.payload
      };

    default:
      return state;
  }
};
