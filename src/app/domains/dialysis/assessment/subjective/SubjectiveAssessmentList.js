import React                            from 'react'
import { List }                         from 'semantic-ui-react'
import { SubjectiveAssessmentListItem } from './SubjectiveAssessmentListItem'

const SubjectiveAssessmentList = ({ subjectiveAssessments }) => (
  <List>
    {subjectiveAssessments.map(subjectiveAssessment => (
      <SubjectiveAssessmentListItem
        subjectiveAssessment={subjectiveAssessment}
        key={subjectiveAssessment.subjective_id}
      />
    ))}
  </List>
)
export { SubjectiveAssessmentList }
