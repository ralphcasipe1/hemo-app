import PropTypes from 'prop-types';
import React from 'react';

const PostSubjectiveAssessmentRemark = ({ postSubjectiveRemark }) => (
  <div>
    {!postSubjectiveRemark ? 'No Remarks' : postSubjectiveRemark.remark}
  </div>
);

PostSubjectiveAssessmentRemark.propTypes = {
  postSubjectiveRemark: PropTypes.shape({
    name: PropTypes.string
  })
};

export default PostSubjectiveAssessmentRemark;
