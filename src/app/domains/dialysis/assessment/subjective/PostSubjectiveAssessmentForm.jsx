import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import ButtonDecision from 'ButtonDecision';
import CheckboxGroup from './HtmlComponents/CheckboxGroup';
import TextAreaField from './HtmlComponents/TextAreaField';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PostSubjectiveAssessmentForm extends PureComponent {
  static defaultProps = {
    subjectives: []
  };

  static propTypes = {
    subjectives: PropTypes.array
    , error: PropTypes.string
    , isLoading: PropTypes.bool
  };

  renderSubj(subjectives) {
    return subjectives.map(subjective => ({
      key: subjective.subjective_id
      , name: subjective.name
    }));
  }

  render() {
    const { subjectives, handleSubmit, formLoading, onFormSubmit } = this.props;

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Form.Group>
          <Field
            component={CheckboxGroup}
            name="subjective_id"
            options={this.renderSubj(subjectives)}
          />
        </Form.Group>

        <Field component={TextAreaField} name="remark" label="Other Remarks" />

        <ButtonDecision expanded {...this.props} />
      </Form>
    );
  }
}

export default reduxForm({
  fields: [ 'subjective_assessment_id', 'remark' ]
})(PostSubjectiveAssessmentForm);
