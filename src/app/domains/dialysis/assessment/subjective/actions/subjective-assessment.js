import {
  CREATE_SUBJECTIVE_ASSESSMENTS,
  CREATE_SUBJECTIVE_ASSESSMENTS_FAILURE,
  CREATE_SUBJECTIVE_ASSESSMENTS_SUCCESS,
  DELETE_SUBJECTIVE_ASSESSMENTS,
  DELETE_SUBJECTIVE_ASSESSMENTS_FAILURE,
  DELETE_SUBJECTIVE_ASSESSMENTS_SUCCESS,
  FETCH_ALL_SUBJECTIVE_ASSESSMENTS,
  FETCH_ALL_SUBJECTIVE_ASSESSMENTS_FAILURE,
  FETCH_ALL_SUBJECTIVE_ASSESSMENTS_SUCCESS
} from '../constants/subjective-assessment';
import { instance } from '../../../../../config/connection';

export const fetchAllSubjectiveAssessments = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_ALL_SUBJECTIVE_ASSESSMENTS
  });
  instance
    .get(`/patient_registries/${patientRegistryId}/subjective_assessments`)
    .then(
      res =>
        dispatch({
          type: FETCH_ALL_SUBJECTIVE_ASSESSMENTS_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: FETCH_ALL_SUBJECTIVE_ASSESSMENTS_FAILURE
          , error: err.response
        })
    );
};

export const createSubjectiveAssessments = (
  patientRegistryId,
  props
) => dispatch => {
  dispatch({
    type: CREATE_SUBJECTIVE_ASSESSMENTS
  });

  instance
    .post(
      `/patient_registries/${patientRegistryId}/subjective_assessments`,
      props
    )
    .then(
      res =>
        dispatch({
          type: CREATE_SUBJECTIVE_ASSESSMENTS_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: CREATE_SUBJECTIVE_ASSESSMENTS_FAILURE
          , error: err.response
        })
    );
};

export const deleteSubjectiveAssessments = patientRegistryId => dispatch => {
  dispatch({
    type: DELETE_SUBJECTIVE_ASSESSMENTS
  });

  instance
    .delete(`/patient_registries/${patientRegistryId}/subjective_assessments`)
    .then(
      res =>
        dispatch({
          type: DELETE_SUBJECTIVE_ASSESSMENTS_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: DELETE_SUBJECTIVE_ASSESSMENTS_FAILURE
          , payload: err.response
        })
    );
};
