import { connect }             from 'react-redux'
import { fetchAllSubjectives } from 'app/domains/admin/subjective/actions/subjective'
import {
  createSubjectiveAssessments,
  fetchAllSubjectiveAssessments
} from './actions/subjective-assessment'
import { SubjectiveAssessment } from './SubjectiveAssessment'

const mapStateToProps = state => ({
	isLoading: state.subjectiveAssessmentReducer.isLoading
  , subjectives: state.subjectiveReducer.subjectives
  , subjectiveAssessments: state.subjectiveAssessmentReducer.subjectiveAssessments
})

export const SubjectiveAssessmentContainer = connect(
  mapStateToProps,
  {
    createSubjectiveAssessments
    , fetchAllSubjectiveAssessments
    , fetchAllSubjectives
  }
)(SubjectiveAssessment)
