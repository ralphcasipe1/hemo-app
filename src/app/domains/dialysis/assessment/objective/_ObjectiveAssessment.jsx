import { Button, Header } from 'semantic-ui-react';
import _ from 'lodash';
import ObjectiveAssessmentForm from './ObjectiveAssessmentForm';
import ObjectiveAssessmentList from './ObjectiveAssessmentList';
import ObjectiveAssessmentRemark from './ObjectiveAssessmentRemark';
import React, { PureComponent } from 'react';

class ObjectiveAssessment extends PureComponent {
  constructor() {
    super();

    this.handleDelete = this.handleDelete.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    const { patientRegistryId } = this.props.params;
    this.onInit(this.props, patientRegistryId);
  }

  onInit(props, id) {
    props.fetchPatientRegistryObjs(id);
    props.getObjectiveRemark(id);
  }

  handleDelete() {
    const { objectiveRemark, patientRegistryObjs } = this.props;
    const { patientRegistryId } = this.props.params;

    if (!_.isEmpty(patientRegistryObjs)) {
      this.props.deletePatientRegistryObjs(patientRegistryId);
    }

    if (objectiveRemark) {
      this.props.deleteObjectiveRemark(
        patientRegistryId,
        objectiveRemark.objective_remark_id
      );
    }
  }

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createPatientRegistryObjs(patientRegistryId, values);
    this.props.createObjectiveRemark(patientRegistryId, values);
  }

  render() {
    const { patientRegistryObjs, objectiveRemark } = this.props;
    return (
      <div>
        <Header as="h3" content="Objective Assessment" />
        {_.isEmpty(patientRegistryObjs) && !objectiveRemark ? (
          <ObjectiveAssessmentForm
            form="CreateObjectiveAssessmentForm"
            onFormSubmit={this.handleFormSubmit}
            {...this.props}
          />
        ) : (
          <div>
            <ObjectiveAssessmentList
              patientRegistryObjs={patientRegistryObjs}
            />
            <ObjectiveAssessmentRemark objectiveRemark={objectiveRemark} />
            <Button
              content="Delete"
              color="red"
              icon="trash"
              onClick={this.handleDelete}
            />
          </div>
        )}
      </div>
    );
  }
}

export default ObjectiveAssessment;
