import {
  CREATE_OBJECTIVE_ASSESSMENTS,
  CREATE_OBJECTIVE_ASSESSMENTS_FAILURE,
  CREATE_OBJECTIVE_ASSESSMENTS_SUCCESS,
  DELETE_OBJECTIVE_ASSESSMENTS,
  DELETE_OBJECTIVE_ASSESSMENTS_FAILURE,
  DELETE_OBJECTIVE_ASSESSMENTS_SUCCESS,
  FETCH_ALL_OBJECTIVE_ASSESSMENTS,
  FETCH_ALL_OBJECTIVE_ASSESSMENTS_FAILURE,
  FETCH_ALL_OBJECTIVE_ASSESSMENTS_SUCCESS
} from '../constants/objective-assessment'

const initialState = {
  objectiveAssessments: []
  , error: null
  , isLoading: false
}

export const objectiveAssessmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_OBJECTIVE_ASSESSMENTS:
    case DELETE_OBJECTIVE_ASSESSMENTS:
    case FETCH_ALL_OBJECTIVE_ASSESSMENTS:
      return {
        ...state
        , isLoading: true
      }

    case CREATE_OBJECTIVE_ASSESSMENTS_FAILURE:
    case DELETE_OBJECTIVE_ASSESSMENTS_FAILURE:
    case FETCH_ALL_OBJECTIVE_ASSESSMENTS_FAILURE:
      return {
        ...state
        , isLoading: false
        , error: action.error
      }

    case CREATE_OBJECTIVE_ASSESSMENTS_SUCCESS:
    case DELETE_OBJECTIVE_ASSESSMENTS_SUCCESS:
    case FETCH_ALL_OBJECTIVE_ASSESSMENTS_SUCCESS:
      return {
        ...state
        , isLoading: false
        , objectiveAssessments: action.payload
      }

    default:
      return state
  }
}
