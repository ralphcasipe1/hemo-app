import { Button, Header } from 'semantic-ui-react';
import _ from 'lodash';
import PostObjectiveAssessmentForm from './PostObjectiveAssessmentForm';
import PostObjectiveAssessmentsList from './PostObjectiveAssessmentsList';
import PostObjectiveAssessmentRemark from './PostObjectiveAssessmentRemark';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PostObjectiveAssessment extends PureComponent {
  constructor() {
    super();

    this.handleDelete = this.handleDelete.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    const { patientRegistryId } = this.props.params;
    this.onInit(this.props, patientRegistryId);
  }

  onInit(props, id) {
    props.fetchPatientRegistryPostObjs(id);
    props.getPostObjectiveRemark(id);
  }

  handleDelete() {
    const { postObjectiveRemark, patientRegistryPostObjs } = this.props;
    const { patientRegistryId } = this.props.params;

    if (!_.isEmpty(patientRegistryPostObjs)) {
      this.props.deletePatientRegistryPostObjs(patientRegistryId);
    }

    if (postObjectiveRemark) {
      this.props.deletePostObjectiveRemark(
        patientRegistryId,
        postObjectiveRemark.post_objective_remark_id
      );
    }
  }

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createPatientRegistryPostObjs(patientRegistryId, values);
    this.props.createPostObjectiveRemark(patientRegistryId, values);
  }

  render() {
    const { patientRegistryPostObjs, postObjectiveRemark } = this.props;
    return (
      <div>
        <Header as="h3" content="Post Objective Assessment" />
        {_.isEmpty(patientRegistryPostObjs) && !postObjectiveRemark ? (
          <PostObjectiveAssessmentForm
            form="CreatePostObjectiveAssessmentForm"
            onFormSubmit={this.handleFormSubmit}
            {...this.props}
          />
        ) : (
          <div>
            <PostObjectiveAssessmentsList
              patientRegistryPostObjs={patientRegistryPostObjs}
            />
            <PostObjectiveAssessmentRemark
              postObjectiveRemark={postObjectiveRemark}
            />
            <Button
              content="Delete"
              color="red"
              icon="trash"
              onClick={this.handleDelete}
            />
          </div>
        )}
      </div>
    );
  }
}

PostObjectiveAssessment.propTypes = {
  createPatientRegistryPostObjs: PropTypes.func.isRequired
  , fetchPatientRegistryPostObjs: PropTypes.func.isRequired
  , deletePatientRegistryPostObjs: PropTypes.func.isRequired
  , createPostObjectiveRemark: PropTypes.func.isRequired
  , getPostObjectiveRemark: PropTypes.func.isRequired
  , fetchObjectives: PropTypes.func.isRequired
  , deletePostObjectiveRemark: PropTypes.func.isRequired
  , patientRegistryPostObjs: PropTypes.array
  , postObjectiveRemark: PropTypes.object
};

export default PostObjectiveAssessment;
