import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import { CheckboxGroup } from '../../../../common/forms/CheckboxGroup';
import { SaveButton } from '../../../../common/forms/SaveButton';
import { TextAreaField } from '../../../../common/forms/TextAreaField';

class ObjectiveAssessmentForm extends PureComponent {
  renderObjectives = objectives => {
    return objectives.map(objective => ({
      key: objective.objective_id
      , name: objective.name
    }));
  };

  render() {
    const { objectives, handleSubmit, formLoading, onFormSubmit } = this.props;

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Form.Group>
          <Field
            component={CheckboxGroup}
            name="objective_id"
            options={this.renderObjectives(objectives)}
          />
        </Form.Group>

        <Field component={TextAreaField} name="remark" label="Other Remarks" />

        <SaveButton />
      </Form>
    );
  }
}

ObjectiveAssessmentForm = reduxForm({
  fields: [ 'objective_assessment_id', 'remark' ]
})(ObjectiveAssessmentForm);

export { ObjectiveAssessmentForm };
