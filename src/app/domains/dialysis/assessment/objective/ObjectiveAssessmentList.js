import React                           from 'react'
import { List }                        from 'semantic-ui-react'
import { ObjectiveAssessmentListItem } from './ObjectiveAssessmentListItem'


const ObjectiveAssessmentList = ({ objectiveAssessments }) => (
  <List>
    {objectiveAssessments.map(objective => (
      <ObjectiveAssessmentListItem
        objective={objective}
        key={objective.objective_id}
      />
    ))}
  </List>
)

export { ObjectiveAssessmentList }
