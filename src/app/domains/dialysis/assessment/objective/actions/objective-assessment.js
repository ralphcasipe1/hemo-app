import {
  CREATE_OBJECTIVE_ASSESSMENTS,
  CREATE_OBJECTIVE_ASSESSMENTS_FAILURE,
  CREATE_OBJECTIVE_ASSESSMENTS_SUCCESS,
  DELETE_OBJECTIVE_ASSESSMENTS,
  DELETE_OBJECTIVE_ASSESSMENTS_FAILURE,
  DELETE_OBJECTIVE_ASSESSMENTS_SUCCESS,
  FETCH_ALL_OBJECTIVE_ASSESSMENTS,
  FETCH_ALL_OBJECTIVE_ASSESSMENTS_FAILURE,
  FETCH_ALL_OBJECTIVE_ASSESSMENTS_SUCCESS
} from '../constants/objective-assessment'
import { instance } from 'app/config/connection'

export const fetchAllObjectiveAssessments = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_ALL_OBJECTIVE_ASSESSMENTS
  })
  instance
    .get(`/patient_registries/${patientRegistryId}/objective_assessments`)
    .then(
      res =>
        dispatch({
          type: FETCH_ALL_OBJECTIVE_ASSESSMENTS_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: FETCH_ALL_OBJECTIVE_ASSESSMENTS_FAILURE
          , error: err.response
        })
    )
}

export const createObjectiveAssessments = (
  patientRegistryId,
  values
) => dispatch => {
  dispatch({
    type: CREATE_OBJECTIVE_ASSESSMENTS
  })

  instance
    .post(
      `/patient_registries/${patientRegistryId}/objective_assessments`,
      values
    )
    .then(
      res =>
        dispatch({
          type: CREATE_OBJECTIVE_ASSESSMENTS_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: CREATE_OBJECTIVE_ASSESSMENTS_FAILURE
          , error: err.response
        })
    )
}

export const deleteObjectiveAssessments = patientRegistryId => dispatch => {
  dispatch({
    type: DELETE_OBJECTIVE_ASSESSMENTS
  })

  instance
    .delete(`/patient_registries/${patientRegistryId}/objective_assessments`)
    .then(
      res =>
        dispatch({
          type: DELETE_OBJECTIVE_ASSESSMENTS_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: DELETE_OBJECTIVE_ASSESSMENTS_FAILURE
          , error: err.response
        })
    )
}
