import { List } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const { Item } = List;
const PostObjectiveAssessmentsListItem = ({ objective }) => (
  <Item content={objective.name} />
);

PostObjectiveAssessmentsListItem.propTypes = {
  objective: PropTypes.shape({
    name: PropTypes.string.isRequired
  })
};

export default PostObjectiveAssessmentsListItem;
