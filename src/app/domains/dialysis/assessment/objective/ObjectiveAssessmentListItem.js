import React     from 'react'
import { List }  from 'semantic-ui-react'

const { Item } = List
const ObjectiveAssessmentListItem = ({ objective }) => (
  <Item content={objective.name} />
)

export { ObjectiveAssessmentListItem }
