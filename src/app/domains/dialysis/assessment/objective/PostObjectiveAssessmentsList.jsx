import { List } from 'semantic-ui-react';
import PostObjectiveAssessmentsListItem from './PostObjectiveAssessmentsListItem';
import PropTypes from 'prop-types';
import React from 'react';

const PostObjectiveAssessmentsList = ({ patientRegistryPostObjs }) => (
  <List>
    {patientRegistryPostObjs.map(objective => (
      <PostObjectiveAssessmentsListItem
        objective={objective}
        key={objective.objective_id}
      />
    ))}
  </List>
);

PostObjectiveAssessmentsList.propTypes = {
  patientRegistryPostObjs: PropTypes.array
};

export default PostObjectiveAssessmentsList;
