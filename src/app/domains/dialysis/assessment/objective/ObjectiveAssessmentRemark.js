import PropTypes from 'prop-types'
import React     from 'react'

const ObjectiveAssessmentRemark = ({ objectiveRemark }) => (
  <div>{!objectiveRemark ? 'No Remarks' : objectiveRemark.remark}</div>
)

ObjectiveAssessmentRemark.propTypes = {
  objectiveRemark: PropTypes.shape({
    remark: PropTypes.string
  })
}

export { ObjectiveAssessmentRemark }
