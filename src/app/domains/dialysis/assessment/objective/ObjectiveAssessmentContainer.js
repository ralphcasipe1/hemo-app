import { connect }            from 'react-redux'
import { fetchAllObjectives } from '../../../admin/objective/actions/objective'
import {
  createObjectiveAssessments,
  fetchAllObjectiveAssessments
} from './actions/objective-assessment'
import { ObjectiveAssessment } from './ObjectiveAssessment'

const mapStateToProps = state => ({
  objectives: state.objectiveReducer.objectives
  , objectiveAssessments: state.objectiveAssessmentReducer.objectiveAssessments
})

export const ObjectiveAssessmentContainer = connect(
  mapStateToProps,
  {
    createObjectiveAssessments
    , fetchAllObjectiveAssessments
    , fetchAllObjectives
  }
)(ObjectiveAssessment)
