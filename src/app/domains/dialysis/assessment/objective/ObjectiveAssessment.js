import React, { Component, Fragment } from 'react'
import { Header }                     from 'semantic-ui-react'
import { ObjectiveAssessmentForm }    from './ObjectiveAssessmentForm'
import { ObjectiveAssessmentList }    from './ObjectiveAssessmentList'
import { ObjectiveAssessmentRemark }  from './ObjectiveAssessmentRemark'

class ObjectiveAssessment extends Component {
  componentDidMount() {
    this.props.fetchAllObjectiveAssessments(
      this.props.match.params.patientRegistryId
    )
    this.props.fetchAllObjectives()
  }

  handleSubmit = values =>
    this.props.createObjectiveAssessments(
      this.props.match.params.patientRegistryId,
      values
    );

  render() {
    const { objectives, objectiveAssessments } = this.props

    return (
      <Fragment>
        <Header as="h3" content="Objective Assessment" />
        { objectiveAssessments.length === 0 ?
          <ObjectiveAssessmentForm
            form="CreateObjectiveAssessmentForm"
            objectives={objectives}
            onFormSubmit={this.handleSubmit}
          /> :
          <div>
            <ObjectiveAssessmentList objectiveAssessments={objectiveAssessments} />
            <ObjectiveAssessmentRemark objectiveRemark={null} />
          </div>
        }
      </Fragment>
    )
  }
}

export { ObjectiveAssessment }
