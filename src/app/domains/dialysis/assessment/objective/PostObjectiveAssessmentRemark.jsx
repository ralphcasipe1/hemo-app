import PropTypes from 'prop-types';
import React from 'react';

const PostObjectiveAssessmentRemark = ({ postObjectiveRemark }) => (
  <div>{!postObjectiveRemark ? 'No Remarks' : postObjectiveRemark.remark}</div>
);

PostObjectiveAssessmentRemark.propTypes = {
  postObjectiveRemark: PropTypes.shape({
    name: PropTypes.string
  })
};

export default PostObjectiveAssessmentRemark;
