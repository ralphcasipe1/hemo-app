import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import ButtonDecision from './ButtonDecision';
import CheckboxGroup from './HtmlComponents/CheckboxGroup';
import TextAreaField from './HtmlComponents/TextAreaField';
import React, { PureComponent } from 'react';

class PostObjectiveAssessmentForm extends PureComponent {
  renderObj(objectives) {
    return objectives.map(objective => ({
      key: objective.objective_id
      , name: objective.name
    }));
  }

  render() {
    const { objectives, handleSubmit, formLoading, onFormSubmit } = this.props;

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Form.Group>
          <Field
            component={CheckboxGroup}
            name="objective_id"
            options={this.renderObj(objectives)}
          />
        </Form.Group>

        <Field component={TextAreaField} name="remark" label="Other Remarks" />

        <ButtonDecision expanded {...this.props} />
      </Form>
    );
  }
}

export default reduxForm({
  fields: [ 'objective_assessment_id', 'remark' ]
})(PostObjectiveAssessmentForm);
