import React, { PureComponent } from 'react'
import { Field, reduxForm }     from 'redux-form'
import { Form }                 from 'semantic-ui-react'
import { FieldSelect }          from 'app/common/forms/FieldSelect'
import { InputField }           from 'app/common/forms/InputField'
import { SaveButton }           from 'app/common/forms/SaveButton'

class PreAssessmentForm extends PureComponent {
  renderMobility(mobilities) {
    return mobilities.map(mobility => ({
      key: mobility.mobility_id
      , text: mobility.name
      , value: mobility.mobility_id
    }))
  }

  render() {
    const { handleSubmit, mobilities, onFormSubmit } = this.props

    const antigens = [
      { key: 0, text: 'Non-reactive (-)', value: 0 }
      , { key: 1, text: 'Reactive (+)', value: 1 }
    ]

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)}>
        <Field
          component={FieldSelect}
          items={antigens}
          name="hbs_ag"
          label="HBsAg"
          loader={false}
        />

        <Field
          component={FieldSelect}
          items={antigens}
          name="anti_hbs"
          label="Anti-HBs"
          loader={false}
        />

        <Field
          component={FieldSelect}
          items={antigens}
          name="anti_hcv"
          label="Anti-HCV"
          loader={false}
        />

        <Field
          component={FieldSelect}
          items={antigens}
          name="anti_hiv"
          label="Anti-HIV"
          loader={false}
        />

        <Field
          component={InputField}
          name="antigen_date"
          label="Date"
          type="date"
        />

        <Field
          component={FieldSelect}
          items={this.renderMobility(mobilities)}
          name="mobility_id"
          label="Mobility"
          loader={false}
        />

        <SaveButton />
      </Form>
    )
  }
}

/*const validate = values => {
  let errors = {};

  if (!values.mobility_id) {
    errors = 'Required';
  }

  return errors;
};*/

PreAssessmentForm = reduxForm({
  fields: [
    'hbs_ag'
    , 'anti_hbs'
    , 'anti_hcv'
    , 'anti_hiv'
    , 'antigen_date'
    , 'mobility_id'
  ]
})(PreAssessmentForm)

export { PreAssessmentForm }
