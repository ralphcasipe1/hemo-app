import { Table } from 'semantic-ui-react';
import moment from 'moment';
import React, { PureComponent } from 'react';

class PreAssessmentTable extends PureComponent {
  render() {
    const { error, isLoading, preAssessment } = this.props;

    if (isLoading) {
      return <div>Loading</div>;
    } else if (error) {
      return <div>{error.message}</div>;
    } else if (!preAssessment) {
      return <div />;
    }

    let hbs_ag;
    let anti_hbs;
    let anti_hcv;
    let anti_hiv;

    if (preAssessment.hbs_ag === 0) {
      hbs_ag = 'HBsAg Non-reactive (-)';
    } else {
      hbs_ag = 'HBsAg Reactive (+)';
    }

    if (preAssessment.anti_hbs === 0) {
      anti_hbs = 'Anti-HBs Non-reactive (-)';
    } else {
      anti_hbs = 'Anti-HBs Reactive (+)';
    }

    if (preAssessment.anti_hcv === 0) {
      anti_hcv = 'Anti-HCV Non-reactive (-)';
    } else {
      anti_hcv = 'Anti-HCV Reactive (+)';
    }

    if (preAssessment.anti_hiv === 0) {
      anti_hiv = 'Anti-HIV Non-reactive (-)';
    } else {
      anti_hiv = 'Anti-HIV Reactive (+)';
    }

    return (
      <Table basic="very" color="blue" definition fixed striped unstackable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell textAlign="center" content="Result" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row textAlign="center">
            <Table.Cell content="Antigens" />
            <Table.Cell>
              <div>{hbs_ag}</div>
              <div>{anti_hbs}</div>
              <div>{anti_hcv}</div>
              <div>{anti_hiv}</div>
              {moment(preAssessment.antigen_date).format('ll')}
            </Table.Cell>
          </Table.Row>
          <Table.Row textAlign="center">
            <Table.Cell content="Mobility" />
            <Table.Cell>{preAssessment.mobility.name}</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    );
  }
}

export { PreAssessmentTable };
