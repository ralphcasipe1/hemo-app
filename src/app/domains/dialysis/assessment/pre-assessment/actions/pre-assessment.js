import {
  CREATE_PRE_ASSESSMENT,
  CREATE_PRE_ASSESSMENT_FAILURE,
  CREATE_PRE_ASSESSMENT_SUCCESS,
  FETCH_PRE_ASSESSMENT,
  FETCH_PRE_ASSESSMENT_FAILURE,
  FETCH_PRE_ASSESSMENT_SUCCESS
} from '../constants/pre-assessment';
import { instance } from '../../../../../config/connection';

export const fetchPreAssessment = patientRegistryId => dispatch => {
  dispatch({
    type: FETCH_PRE_ASSESSMENT
  });

  instance.get(`/patient_registries/${patientRegistryId}/pre_assessments`).then(
    res =>
      dispatch({
        type: FETCH_PRE_ASSESSMENT_SUCCESS
        , payload: res.data.data
      }),
    err =>
      dispatch({
        type: FETCH_PRE_ASSESSMENT_FAILURE
        , payload: err.response.data
      })
  );
};

export const createPreAssessment = (patientRegistryId, values) => dispatch => {
  dispatch({
    type: CREATE_PRE_ASSESSMENT
  });
  instance
    .post(`/patient_registries/${patientRegistryId}/pre_assessments`, values)
    .then(
      res =>
        dispatch({
          type: CREATE_PRE_ASSESSMENT_SUCCESS
          , payload: res.data.data
        }),
      err =>
        dispatch({
          type: CREATE_PRE_ASSESSMENT_FAILURE
          , payload: err.response.data
        })
    );
};
