import {
  CREATE_PRE_ASSESSMENT,
  CREATE_PRE_ASSESSMENT_FAILURE,
  CREATE_PRE_ASSESSMENT_SUCCESS,
  FETCH_PRE_ASSESSMENT,
  FETCH_PRE_ASSESSMENT_FAILURE,
  FETCH_PRE_ASSESSMENT_SUCCESS
} from '../constants/pre-assessment';

const initialState = {
  error: null
  , isLoading: false
  , preAssessment: null
};

export const preAssessmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PRE_ASSESSMENT:
    case FETCH_PRE_ASSESSMENT:
      return {
        ...state
        , isLoading: true
      };

    case CREATE_PRE_ASSESSMENT_FAILURE:
    case FETCH_PRE_ASSESSMENT_FAILURE:
      return {
        ...state
        , error: action.error
        , isLoading: false
      };

    case CREATE_PRE_ASSESSMENT_SUCCESS:
    case FETCH_PRE_ASSESSMENT_SUCCESS:
      return {
        ...state
        , isLoading: false
        , preAssessment: action.payload
      };

    default:
      return state;
  }
};
