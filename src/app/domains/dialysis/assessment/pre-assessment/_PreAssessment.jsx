import { withRouter } from 'react-router';
import PreAssessmentForm from './PreAssessmentForm';
import PreAssessmentTable from './PreAssessmentTable';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PreAssessment extends PureComponent {
  constructor() {
    super();

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    const { patientRegistryId } = this.props.params;
    this.onInit(this.props, patientRegistryId);
  }

  onInit(props, id) {
    props.fetchMobilities();
    props.fetchPreAssessments(id);
  }

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createPreAssessment(patientRegistryId, values);
  }

  render() {
    const { preAssessment } = this.props;
    return (
      <div>
        {!preAssessment ? (
          <PreAssessmentForm
            form="CreatePreAssessmentForm"
            onFormSubmit={this.handleFormSubmit}
            {...this.props}
          />
        ) : (
          <PreAssessmentTable preAssessment={preAssessment} />
        )}
      </div>
    );
  }
}

PreAssessment.propTypes = {
  fetchMobilities: PropTypes.func.isRequired
  , createPreAssessment: PropTypes.func.isRequired
  , preAssessment: PropTypes.object
};

export default withRouter(PreAssessment);
