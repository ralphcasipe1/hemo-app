import React, { Component, Fragment } from 'react'
import { Header }                     from 'semantic-ui-react'
import { PreAssessmentForm }          from './PreAssessmentForm'
import { PreAssessmentTable }         from './PreAssessmentTable'

class PreAssessment extends Component {
  componentDidMount() {
    this.props.fetchAllMobilities()
    this.props.fetchPreAssessment(this.props.match.params.patientRegistryId)
  }
  handleSubmit = values =>
    this.props.createPreAssessment(
      this.props.match.params.patientRegistryId,
      values
    );

  render() {
    const { mobilities, preAssessment } = this.props

    return (
      <Fragment>
        <Header as="h3" content="Pre-Assessment" />
        {!preAssessment ? (
          <PreAssessmentForm
            form="CreatePreAssessmentForm"
            mobilities={mobilities}
            onFormSubmit={this.handleSubmit}
          />
        ) : (
          <PreAssessmentTable preAssessment={preAssessment} />
        )}
      </Fragment>
    )
  }
}

export { PreAssessment }
