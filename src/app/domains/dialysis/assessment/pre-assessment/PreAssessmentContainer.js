import { connect } from 'react-redux';
import { fetchAllMobilities } from '../../../admin/mobility/actions/mobility';
import {
  createPreAssessment,
  fetchPreAssessment
} from './actions/pre-assessment';
import { PreAssessment } from './PreAssessment';

const mapStateToProps = state => ({
  isLoading: state.preAssessmentReducer.isLoading
  , mobilities: state.mobilityReducer.mobilities
  , preAssessment: state.preAssessmentReducer.preAssessment
});

export const PreAssessmentContainer = connect(
  mapStateToProps,
  {
    createPreAssessment
    , fetchAllMobilities
    , fetchPreAssessment
  }
)(PreAssessment);
