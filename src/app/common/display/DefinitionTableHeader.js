import React from 'react';
import { Table } from 'semantic-ui-react';

const { Header, HeaderCell, Row } = Table;
export const DefinitionTableHeader = () => (
  <Header>
    <Row>
      <HeaderCell />
      <HeaderCell textAlign="center" content="Result" />
    </Row>
  </Header>
);
