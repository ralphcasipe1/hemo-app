import React from 'react'
import './CardList.css'

export const CardList = ({ children }) => (
  <div className="card-list-wrapper">{children}</div>
)
