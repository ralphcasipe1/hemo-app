import React, { PureComponent } from 'react';
import './CardListItem.css';

class CardListItem extends PureComponent {
  render() {
    const { label, value } = this.props;

    return (
      <React.Fragment>
        <div className="card-list-item-wrapper">
          <div className="card-list-item-label">{label}:</div>
          <div className="card-list-item-value">{value}</div>
        </div>
      </React.Fragment>
    );
  }
}

export { CardListItem };
