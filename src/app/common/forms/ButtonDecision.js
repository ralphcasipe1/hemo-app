import React, { PureComponent }           from 'react'
import type { Element }                   from 'react'
import { Button, Divider, Icon, Segment } from 'semantic-ui-react'

type PropType = {
  buttonName: string,
  disableCancel: boolean,
  expanded: boolean,
  simple: boolean,
  onCancel: () => object,
  isDisable: boolean
};
class ButtonDecision extends PureComponent<PropType> {
  static defaultProps = {
    expanded: false
    , simple: false
  };

  render(): Element<'div'> {
    let element: Element<'div'>

    const {
      buttonName,
      disableCancel,
      expanded,
      onCancel,
      isDisable,
      simple
    } = this.props

    if (simple) {
      element = (
        <Button.Group>
          <Button
            basic
            icon="cancel"
            negative
            onClick={onCancel}
            type="button"
          />
          <Button primary icon="check" type="submit" disabled={isDisable} />
        </Button.Group>
      )
    } else if (expanded) {
      element = (
        <Segment basic>
          <Button fluid primary type="submit" disabled={isDisable}>
            <Icon name="check" />
            {buttonName || 'Save'}
          </Button>
          <Divider horizontal>Or</Divider>
          <Button
            basic
            fluid
            negative
            onClick={onCancel}
            type="button"
            disabled={disableCancel}
          >
            <Icon name="cancel" />
            Cancel
          </Button>
        </Segment>
      )
    } else {
      element = (
        <Button.Group>
          <Button basic negative onClick={onCancel} type="button">
            <Icon name="cancel" />
            Cancel
          </Button>
          <Button primary type="submit" disabled={isDisable}>
            <Icon name="check" />
            {buttonName || 'Save'}
          </Button>
        </Button.Group>
      )
    }

    return element
  }
}

export { ButtonDecision }
