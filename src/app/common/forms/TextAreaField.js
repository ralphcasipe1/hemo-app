// @flow
import React            from 'react'
import type { Element } from 'react'

type PropType = {
  input: object,
  label: string,
  placeholder: string
};

/**
 * [TextAreaField description] No autoheight
 * @type {[type]}
 */
export const TextAreaField = ({
  input,
  label,
  placeholder
}: PropType): Element<'div'> => (
  <div className="ui-form">
    <div className="field">
      <label>{label}</label>
      <textarea placeholder={placeholder} {...input} />
    </div>
  </div>
)
