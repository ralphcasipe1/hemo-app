import React, { PureComponent } from 'react'
import type { Element }         from 'react'
import { Form, Input, Label }   from 'semantic-ui-react'

interface Meta {
  touched: boolean,
  error: string,
  warning: string
}

type PropsType = {
  type: string,
  required: boolean,
  meta: Meta
};

class InputField extends PureComponent<PropsType> {
  static defaultProps = {
    type: 'text'
    , required: false
  };

  render(): Element<'div'> {
    const {
      meta: { touched, error, warning }
    } = this.props

    return (
      <Form.Field
        error={touched && !!error}
        width={this.props.width}
        required={this.props.required}
      >
        <label>{this.props.label}</label>
        <Input
          disabled={this.props.disabled}
          hidden={this.props.hidden}
          label={this.props.inputLabel}
          labelPosition={this.props.labelPosition}
          placeholder={this.props.placeholder}
          transparent={this.props.transparent}
          readOnly={this.props.readOnly}
          type={this.props.type}
          {...this.props.input}
        />

        {touched &&
          ((error && (
            <Label basic color="red" pointing>
              {error}
            </Label>
          )) ||
            (warning && (
              <Label basic color="yellow" pointing>
                {warning}
              </Label>
            )))}
      </Form.Field>
    )
  }
}

export { InputField }
