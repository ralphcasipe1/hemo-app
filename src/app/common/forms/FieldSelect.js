import React, { PureComponent } from 'react'
import type { Element }         from 'react'
import _                        from 'lodash'
import classnames               from 'classnames'

interface Item {
  key: number,
  value: number | string,
  text: string
}

type PropType = {
  defaultName: string,
  items: Array<Item>,
  input: object,
  label: string,
  onChange: (e: SyntheticInputEvent<HTMLSelectElement>) => any,
  required: boolean
};

class FieldSelect extends PureComponent<PropType, void> {
  static defaultProps = {
    defaultName: 'Default'
    , items: []
  };

  render(): Element<'div'> {
    const { defaultName, items, input, label, onChange, required } = this.props

    let isRequired = classnames({
      required: required
      , '': !required
    })

    return (
      <div className={`field ${isRequired}`}>
        <label>{label}</label>
        <select
          className="ui fluid dropdown"
          {...input}
          onChange={onChange ?
            onChange :
            (e: SyntheticInputEvent<HTMLSelectElement>): SyntheticInputEvent => input.onChange(e.target.value)}
        >
          <option>{_.startCase(defaultName)}</option>
          {items.map((item: Item): Element<'option'> => (
            <option key={item.key} value={item.value}>
              {item.text}
            </option>
          ))}
        </select>
      </div>
    )
  }
}

export { FieldSelect }
