// @flow
import React            from 'react'
import type { Element } from 'react'

type PropType = {
  content: string
};

const SaveButton = ({ content }: PropType): Element<'button'> => (
  <button className="fluid ui primary button" type="submit">
    <i className="check icon" />
    {content}
  </button>
)

SaveButton.defaultProps = {
  content: 'Save'
}

export { SaveButton }
