import React from 'react'

const CheckboxGroup = ({ label, options, input }) => {
  const checkboxGroup = () =>
    options.map((option, index) => (
      <div className="field" key={index}>
        <div className="ui checkbox">
          <input
            type="checkbox"
            name={`${input.name}[${index}]`}
            value={option.key}
            checked={input.value.indexOf(option.key) !== -1}
            onChange={e => {
              const newValue = [...input.value]
              if (e.target.checked) {
                newValue.push(option.key)
              } else {
                newValue.splice(newValue.indexOf(option.key), 1)
              }
              return input.onChange(newValue)
            }}
            id={`${input.name}[${index}]`}
          />
          <label htmlFor={`${input.name}[${index}]`}>{option.name}</label>
        </div>
      </div>
    ))
  return <div className="grouped fields">{checkboxGroup()}</div>
}

export { CheckboxGroup }
