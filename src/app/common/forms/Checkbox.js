import React            from 'react'
import type { Element } from 'react'
import { Form }         from 'semantic-ui-react'

type PropType = {
  label: string,
  input: object,
  identity: string
};
const Checkbox = ({ label, input, identity }: PropType): Element<'div'>  => {
  return (
    <Form.Field>
      <div className="ui checkbox">
        <input type="checkbox" {...input} id={identity} checked={input.value} />
        <label htmlFor={identity}>{label}</label>
      </div>
    </Form.Field>
  )
}

export { Checkbox }
