import React, { PureComponent } from 'react'
import type { Element }         from 'react'
import { Radio }                from 'semantic-ui-react'

interface Option {
  key: number,
  name: string
}

type PropsType = {
  label: string,
  options: Array<Option>,
  input: object
};

class RadioButton extends PureComponent<PropsType> {
  radioButtonGroup(): Array<Option> {
    let { label, options, input } = this.props

    return options.map((option: Option, index: number) =>
      <div className="radio" key={index}>
        <label>
          <Radio
            type="radio"
            name={`${input.name}`}
            value={option.key}
            checked={input.value === option.key}
            onChange={() => input.onChange(option.key)}
          />
          {option.name}
        </label>
      </div>
    )
  }

  render(): Element<'div'> {
    return <div>{this.radioButtonGroup()}</div>
  }
}

export { RadioButton }
