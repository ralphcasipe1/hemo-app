export const isAggregatedValues = (valOne: number, valTwo: number): boolean => {
  if (valOne && !valTwo) {
    return false
  } else if (!valOne && valTwo) {
    return false
  }

  return true
}
