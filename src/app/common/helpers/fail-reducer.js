export const failReducer = (state: object, action: object): object => ({
  ...state
  , isLoading: false
  , error: action.error
})
