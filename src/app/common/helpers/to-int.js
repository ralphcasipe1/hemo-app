export const toInt = (value: number): number => parseInt(value) || 0
