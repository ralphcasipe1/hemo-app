import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

class MobileSidebar extends Component {
  render() {
    const { close, isAuthenticated, isAdmin, onLogout } = this.props;

    return (
      <Menu vertical icon="labeled" fluid>
        <Menu.Item
          as={Link}
          content="Home"
          icon="home"
          name="home"
          onClick={close}
          to="/"
        />
        {isAdmin ? (
          <Menu.Item
            as={Link}
            content="Admin"
            icon="universal access"
            name="admin"
            onClick={close}
            to="/admins"
          />
        ) : (
          ''
        )}

        <Menu.Item icon="settings" name="settings" onClick={close} disabled />
        <Menu.Item icon="bell" name="notifications" onClick={close} disabled />

        <Menu.Item icon="user" name="profile" onClick={close} disabled />

        <Menu.Item
          name={isAuthenticated ? 'logout' : 'login'}
          icon={isAuthenticated ? 'sign out' : 'sign in'}
          to={'/login'}
          onClick={isAuthenticated ? onLogout : close}
        />
      </Menu>
    );
  }
}
/*class MobileSidebar extends Component {
  render() {
    const { visible, close, isAuthenticated, isAdmin, onLogout } = this.props;

    return (
      <Sidebar
        as={Menu}
        animation="push"
        color="blue"
        icon="labeled"
        inverted
        vertical
        visible={visible}
        width="thin"
        fixed="left"
      >
        <Menu.Item
          as={Link}
          content="Home"
          icon="home"
          name="home"
          onClick={close}
          to="/"
        />
        {isAdmin ? (
          <Menu.Item
            as={Link}
            content="Admin"
            icon="universal access"
            name="admin"
            onClick={close}
            to="/admins"
          />
        ) : (
          ''
        )}

        <Menu.Item icon="settings" name="settings" onClick={close} disabled />
        <Menu.Item icon="bell" name="notifications" onClick={close} disabled />

        <Menu.Item icon="user" name="profile" onClick={close} disabled />

        <Menu.Item
          name={isAuthenticated ? 'logout' : 'login'}
          icon={isAuthenticated ? 'sign out' : 'sign in'}
          to={'/login'}
          onClick={isAuthenticated ? onLogout : close}
        />
      </Sidebar>
    );
  }
}*/

export default MobileSidebar;
