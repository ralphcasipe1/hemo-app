import { Dropdown, Icon, Image } from 'semantic-ui-react'
import { Link, withRouter }      from 'react-router-dom'
import _                         from 'lodash'
import PropTypes                 from 'prop-types'
import React                     from 'react'

const userOptions = [
  { key: 'users', text: 'Profile', icon: 'id card', disabled: true }
  , { key: 'settings', text: 'Settings', icon: 'settings', disabled: true }
]

/* const infoOptions = [
  { key: 'changelogs', text: 'Changelogs', icon: 'info' },
  { key: 'help', text: 'Help', icon: 'help' },
  { key: 'sign_out', text: 'Log Out', icon: 'sign out' },
]; */
const UserAvatar = ({ image, user, onLogout }) => (
  <Dropdown
    trigger={
      <span>
        {image ? (
          <Image avatar src={image} spaced />
        ) : (
          <Icon name="user" circular inverted color="teal" fitted />
        )}
        {user}
      </span>
    }
    pointing="top right"
    icon={null}
  >
    <Dropdown.Menu>
      <Dropdown.Header icon="user" content="User" />
      {userOptions.map(opt => (
        <Dropdown.Item
          as={Link}
          key={opt.key}
          text={opt.text}
          icon={opt.icon}
          to={`/${_.lowerCase(opt.text)}`}
          disabled={opt.disabled}
        />
      ))}
      <Dropdown.Menu scrolling>
        <Dropdown.Item
          key="log out"
          text="Log out"
          icon="sign out"
          onClick={onLogout}
        />
      </Dropdown.Menu>
    </Dropdown.Menu>
  </Dropdown>
)

UserAvatar.propTypes = {
  image: PropTypes.string
  , user: PropTypes.string.isRequired
}

export default withRouter(UserAvatar)
