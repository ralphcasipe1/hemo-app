import { combineReducers }                    from 'redux'
import { reducer as formReducer }             from 'redux-form'
import { authenticationReducer }              from 'app/domains/account/authentication/reducers/authentication'
import { abnormalReducer }                    from 'app/domains/admin/abnormal/reducers/abnormal'
import { dialyzerBrandReducer }               from 'app/domains/admin/dialyzer-brand/reducers/dialyzer-brand'
import { dialyzerSizeReducer }                from 'app/domains/admin/dialyzer-size/reducers/dialyzer-size'
import { mobilityReducer }                    from 'app/domains/admin/mobility/reducers/mobility'
import { subjectiveReducer }                  from 'app/domains/admin/subjective/reducers/subjective'
import { objectiveReducer }                   from 'app/domains/admin/objective/reducers/objective'
import { userReducer }                        from 'app/domains/admin/user/reducers/user'

import { bizboxPatientReducer }               from 'app/domains/patient/bizbox/reducers/bizbox-patient'
import { patientReducer }                     from 'app/domains/patient/local/reducers/patient'
import { patientRegistryReducer }             from 'app/domains/patient/registry/reducers/patient-registry'
import { antiCoagulantReducer }               from 'app/domains/dialysis/basic/anti-coagulant/reducers/anti-coagulant'
import { dialyzerReducer }                    from 'app/domains/dialysis/basic/dialyzer/reducers/dialyzer'
import { parameterReducer }                   from 'app/domains/dialysis/basic/parameter/reducers/parameter'
import { vaccineReducer }                     from 'app/domains/dialysis/basic/vaccine/reducers/vaccine'
import { weightReducer }                      from 'app/domains/dialysis/basic/weight/reducers/weight'
import { medicationReducer }                  from 'app/domains/dialysis/monitor/medication/reducers/medication'
import { vitalSignReducer }                   from 'app/domains/dialysis/monitor/vital-sign/reducers/vital-sign'
import { nurseNoteReducer }                   from 'app/domains/dialysis/monitor/nurse-note/reducers/nurse-note'

import { catheterReducer }                    from 'app/domains/dialysis/assessment/catheter/reducers/catheter'
import { objectiveAssessmentReducer }         from 'app/domains/dialysis/assessment/objective/reducers/objective-assessment'
import { preAssessmentReducer }               from 'app/domains/dialysis/assessment/pre-assessment/reducers/pre-assessment'
import { subjectiveAssessmentReducer }        from 'app/domains/dialysis/assessment/subjective/reducers/subjective-assessment'
import { vascularReducer }                    from 'app/domains/dialysis/assessment/vascular/reducers/vascular'
import { standingOrderAntiCoagulantReducer }  from 'app/domains/standing-order/anti-coagulant/reducers'
import { standingOrderDialyzerReducer }       from 'app/domains/standing-order/dialyzer/reducers'
import { standingOrderParameterReducer }      from 'app/domains/standing-order/parameter/reducers'
import { standingOrderWeightReducer }         from 'app/domains/standing-order/weight/reducers'
import { standingOrderMedicationReducer }     from 'app/domains/standing-order/medication/reducers/standing-order-medication'
import { standingOrderPhysicianOrderReducer } from 'app/domains/standing-order/physician-order/reducers/standing-order-physician-order'

export const rootReducer = combineReducers({
  authenticationReducer
  , form: formReducer

  // Build Files
  , abnormalReducer
  , dialyzerBrandReducer
  , dialyzerSizeReducer
  , mobilityReducer
  , objectiveReducer
  , subjectiveReducer
  , userReducer
  // , bizboxPhysicianReducer
  // , physicians: physiciansReducer

  , bizboxPatientReducer
  , patientReducer
  , patientRegistryReducer

  // Parameters
  , antiCoagulantReducer
  , dialyzerReducer
  , parameterReducer
  , vaccineReducer
  , weightReducer

  // Monitor
  , medicationReducer
  , vitalSignReducer
  , nurseNoteReducer

  // Monitor Statistics
  , catheterReducer
  , objectiveAssessmentReducer
  , preAssessmentReducer
  , vascularReducer
  , subjectiveAssessmentReducer

  // Templates
  , standingOrderAntiCoagulantReducer
  , standingOrderDialyzerReducer
  , standingOrderParameterReducer
  , standingOrderWeightReducer
  , standingOrderPhysicianOrderReducer
  , standingOrderMedicationReducer
})
