import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import App from './containers/App'
import { LoginContainer } from './app/domains/account/authentication/LoginContainer'
import { ProfileContainer } from './app/domains/account/profile/ProfileContainer'
import { AdminDashboard } from './app/domains/admin/AdminDashboard'
import { BizboxPatientContainer } from './app/domains/patient/bizbox/BizboxPatientContainer'
import { PatientContainer } from './app/domains/patient/local/PatientContainer'
import { PatientDetailContainer } from './app/domains/patient/local/PatientDetailContainer'
import { RegistrySummaryContainer } from './app/domains/patient/registry/RegistrySummaryContainer'

export const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route component={LoginContainer} exact path="/login" strict />
        <App>
          <Switch>
            <Route exact component={PatientContainer} path="/" />

            <Route exact component={BizboxPatientContainer} path="/bizbox" />

            <Route
              component={RegistrySummaryContainer}
              path="/patients/:patientId/patient_registries/:patientRegistryId"
            />

            <Route
              component={PatientDetailContainer}
              path="/patients/:patientId"
            />

            <Route component={ProfileContainer} path="/profile" />

            <Route component={AdminDashboard} path="/admins" />

            <Route render={() => <h1>Page Not Found</h1>} />
          </Switch>
        </App>
      </Switch>
    </Router>
  </Provider>
)
