import React                      from 'react'
import { render }                 from 'react-dom'
import jwtDecode                  from 'jwt-decode'
import { setAuthenticationToken } from './app/domains/account/authentication/helpers/set-authentication-token'
import { setCurrentUser }         from './app/domains/account/authentication/actions/set-current-user'
import { Root }                   from './Root'
import { configureStore }         from './store/configureStore'
import './index.css'
import 'semantic-ui-css/semantic.min.css'

const store = configureStore()

if (localStorage.accessToken) {
  setAuthenticationToken(localStorage.accessToken)
  store.dispatch(setCurrentUser(jwtDecode(localStorage.accessToken)))
}

render(<Root store={store} />, document.getElementById('root'))
