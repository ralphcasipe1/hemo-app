import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import { rootReducer } from '../reducers/index'

export const configureStore = preloadedState => {
  let middleware = [thunk]

  if (process.env.NODE_ENV !== 'production') {
    middleware = [ ...middleware, createLogger ]
  }

  const createStoreWithMiddleware = compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )(createStore)

  const store = createStoreWithMiddleware(rootReducer, preloadedState)

  return store
}
