export const labelColor = (status) => {
  let color;
  if (status === 'pending') {
    color = 'orange';
  } else if (status === 'approved') {
    color = 'green';
  } else if (status === 'completed') {
    color = 'blue';
  } else {
    color = null;
  }
  return color;
};
