import _ from 'lodash';
import { Icon, List, Segment } from 'semantic-ui-react';
import React from 'react';

export function mustHaveSpecChar(str) {
  let pattern = /[~`!@#$%\^&*+=\-\[\]\\;,/{}|\\":<>\?]/g;
  if (pattern.test(str)) {
    return false;
  }
  return true;
}

export function mustHaveNumber(str) {
  if (/\d/.test(str)) {
    return false;
  }
  return true;
}

/**
 * Thank you Internet!
 * @link https://stackoverflow.com/questions/32852339/password-validation-with-sequential-letters-and-numbers-regex
 * @param {String} str
 */
/* export function checkSequence(str) {

    for(var i in str)
        if (+str[+i+1] == +str[i]+1 &&
            +str[+i+2] == +str[i]+2) return false;


    for(var i in str)
        if (String.fromCharCode(str.charCodeAt(i)+1) == str[+i+1] &&
            String.fromCharCode(str.charCodeAt(i)+2) == str[+i+2]) return false;
    return true;
} */

export function toInt(value) {
  return parseInt(value) || 0;
}

export function toFloat(value) {
  if (!value) {
    return value;
  }

  let floatVal = +value.replace(/./, '.');

  return floatVal;
}

export function aggregateValues(valOne, valTwo) {
  if (valOne && !valTwo) {
    return false;
  } else if (!valOne && valTwo) {
    return false;
  }

  return true;
}
/**
 * @flow
 */

type Configuration = {
  location: string,
  patientId: ?string,
  registryId: ?string,
};

export function subHeader(config: Configuration): string {
  let title: string;

  switch (config.location) {
    case '/':
      title = 'Patients';
      break;

    case `patients/${String(config.patientId)}`:
      title = 'Preparatory';
      break;

    case `/patients/${String(config.patientId)}/template_parameters`:
    case `/patients/${String(config.patientId)}/template_weights`:
    case `/patients/${String(config.patientId)}/template_anti_coagulants`:
    case `/patients/${String(config.patientId)}/template_dialyzers`:
    case `/patients/${String(config.patientId)}/template_medications`:
    case `/patients/${String(config.patientId)}/template_physician_orders`:
      title = 'Standing Order';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}`:
      title = 'Summary';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/parameters`:
      title = 'Parameters';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/weights`:
      title = 'Weight';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/anti_coagulants`:
      title = 'Anti-Coagulant';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/dialyzers`:
      title = 'Dialyzers';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/vaccines`:
      title = 'Vaccines';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/operate_assessments`:
    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/vasculars`:
    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/catheters`:
      title = 'Assessments';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/physician_orders`:
      title = 'Orders';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/medications`:
      title = 'Medications';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/nurse_notes`:
      title = 'Notes';
      break;

    case `/patients/${String(config.patientId)}/patient_registries/${String(
      config.registryId
    )}/vital_signs`:
      title = 'Vital Signs';
      break;

    default:
      title = '';
      break;
  }

  return title;
}

type MessageSettings = {
  success: ?string,
  error: {
    status_code: ?number,
    message: string,
  },
  loading: boolean,
};

type CustomMessageSettings = {
  customHeader: ?string,
  customContent: ?string,
};

type Message = {
  icon: string,
  content: ?string,
  header: ?string,
};

export function message(
  config: MessageSettings,
  custom: CustomMessageSettings
): Message {
  // let msg : Message;
  let msg: Message = {
    icon: '',
    content: '',
    header: '',
  };

  if (config.success) {
    msg.icon = 'check';
    msg.header = `Status code: 400`;
    msg.content = String(config.success);
  } else if (config.error) {
    msg.icon = 'warning circle';
    msg.header = `Status code: ${String(config.error.status_code)}`;
    msg.content = config.error.message;
  } else if (config.loading) {
    msg.icon = 'circle notched';
    msg.header = 'Loading..';
    msg.content = 'Please wait a moment';
  } else {
    msg.icon = 'info';
    msg.header = custom.customHeader;
    msg.content = custom.customContent;
  }

  return msg;
}

type Options = {
  id: number,
  attribute: string,
};

export function viewMoreControl(
  stateName,
  option: {
    id: null,
    attributes: [],
  }
) {
  if (stateName.indexOf(option.id) !== -1) {
    return (
      <div>
        {option.attributes.map((attribute, index) => (
          <List key={index} size="large" relaxed>
            <List.Item>
              <List.Content>
                <List.Header>
                  <Icon name="hashtag" color="blue" /> {attribute.as}
                </List.Header>
                <List.Description>
                  <Segment>
                    {attribute.field
                      ? attribute.field.split('\n').map((field, index) => (
                          <div key={index}>
                            {field}
                            <br />
                          </div>
                        ))
                      : null}
                  </Segment>
                </List.Description>
              </List.Content>
            </List.Item>
          </List>
        ))}
      </div>
    );
  }

  return option.attributes.map((attribute, index) => (
    <List key={index} size="large" relaxed>
      <List.Item>
        <List.Content>
          <List.Header>
            <Icon name="hashtag" color="blue" /> {attribute.as}
          </List.Header>
          <List.Description>
            <Segment>
              {attribute.field
                ? _.truncate(attribute.field, {
                    length: 200,
                    omission: '[...]',
                  })
                    .split('\n')
                    .map((field, index) => (
                      <div key={index}>
                        {field}
                        <br />
                      </div>
                    ))
                : null}
            </Segment>
          </List.Description>
        </List.Content>
      </List.Item>
    </List>
  ));
}
