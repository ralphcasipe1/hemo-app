import moment from 'moment';

export const now = () => moment().format('ll');

export const formatDate = (date) => moment(date).format('ll');

export const dateWithTime = (date) => moment(date).format('lll');

export const ternary = (state, success, fail) => (state ? success : fail);
