import React from 'react';
import { Button } from 'semantic-ui-react';

export const AddOrBack = ({ openForm, onClick }) => (
  <Button
    color={openForm ? 'red' : 'blue'}
    content={openForm ? 'Back' : 'Add'}
    icon={openForm ? 'arrow left' : 'plus'}
    onClick={onClick}
  />
);
