import { Field, reduxForm } from 'redux-form';
import { Form, Icon, Message } from 'semantic-ui-react';
import ButtonDecision from './ButtonDecision';
import InputField from './HtmlComponents/InputField';
import FieldSelect from 'FieldSelect';
import TextAreaField from './HtmlComponents/TextAreaField';
import React, { PureComponent } from 'react';

class SOPhysicianOrder extends PureComponent {
  render() {
    const { handleSubmit, onFormSubmit, loading, submitting } = this.props;

    const options = [
      { key: 1, value: 1, text: 'Laboratory' },
      { key: 2, value: 2, text: 'X-Ray' },
    ];

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={loading}>
        <Field
          component={TextAreaField}
          type=""
          name="assessment"
          label="Assessment"
        />
        <Field
          component={TextAreaField}
          name="medication"
          label="Medications"
        />
        <Field component={TextAreaField} name="procedure" label="Procedures" />
        <Field
          component={TextAreaField}
          name="diagnostic_test"
          label="Diagnostic Tests"
          items={options}
        />
        <Field
          component={TextAreaField}
          name="other_remark"
          label="Other Remarks"
        />

        <ButtonDecision expanded {...this.props} />
      </Form>
    );
  }
}

export default reduxForm({
  fields: [
    'assessment',
    'medication',
    'procedure',
    'diagnostic_test',
    'other_remark',
  ],
})(SOPhysicianOrder);
