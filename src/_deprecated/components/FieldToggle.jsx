import { Checkbox } from 'semantic-ui-react';
import React, { PureComponent } from 'react';

class FieldToggle extends PureComponent {
  render() {
    const { label } = this.props;

    return <Checkbox toggle label={label} />;
  }
}

export default FieldToggle;
