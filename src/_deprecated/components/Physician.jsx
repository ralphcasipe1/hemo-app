import React, { Component } from 'react';
import { Confirm, Header, Segment } from 'semantic-ui-react';
import { BizboxPhysicianList } from './BizboxPhysicianList';
import { PhysicianList } from './PhysicianList';
import { PhysicianMenu } from './PhysicianMenu';

export class Physician extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 'view physicians',
      openConfirm: false,
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleListItemClick = this.handleListItemClick.bind(this);
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
  }

  componentDidMount() {
    this.props.fetchAllBizboxPhysicians();
    this.props.fetchPhysicians();
  }

  handleMenuItemClick(e, { name }) {
    this.setState({
      active: name,
    });
  }

  handleCancel() {
    this.setState({
      openConfirm: false,
    });
  }

  handleConfirm() {
    this.setState({
      openConfirm: false,
    });

    const {
      PK_emdDoctors,
      firstname,
      lastname,
      birthdate,
      specialization,
    } = this.props.bizboxPhysician;

    this.props.createPhysician({
      bizbox_physician_no: PK_emdDoctors,
      fname: firstname,
      lname: lastname,
      date_of_birth: birthdate,
      specialization,
    });
  }

  handleListItemClick(id) {
    this.setState({
      openConfirm: true,
    });

    this.props.fetchBizboxPhysician(id);
  }

  renderContent() {
    const { bizboxPhysicians, physicians } = this.props;
    const { active } = this.state;

    if (active === 'view bizbox physicians') {
      return (
        <BizboxPhysicianList
          bizboxPhysicians={bizboxPhysicians}
          onClick={this.handleListItemClick}
        />
      );
    } else {
      return <PhysicianList physicians={physicians} />;
    }
  }

  render() {
    const { isLoading } = this.props;
    const { active, openConfirm } = this.state;

    return (
      <Segment loading={isLoading}>
        <PhysicianMenu
          active={active}
          onMenuItemClick={this.handleMenuItemClick}
        />

        {this.renderContent()}
        <Confirm
          open={openConfirm}
          content={
            <Header
              size="small"
              content="Do you want to migrate this physician to your LOCAL DATABASE"
            />
          }
          onCancel={this.handleCancel}
          onConfirm={this.handleConfirm}
        />
      </Segment>
    );
  }
}
