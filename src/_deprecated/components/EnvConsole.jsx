import PropTypes from 'prop-types';
import React from 'react';

const EnvConsole = ({ env, apiConn, status, platform }) => (
  <div>
    {env}
    {apiConn}
    {status}
    {platform}
  </div>
);

EnvConsole.propTypes = {
  env: PropTypes.string,
  apiConn: PropTypes.string,
  status: PropTypes.string,
  platform: PropTypes.string,
};

export default EnvConsole;
