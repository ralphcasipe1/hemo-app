import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';
import { VitalSignMenu } from './VitalSignMenu';
import { VitalSignTable } from './VitalSignTable';
import VitalSignForm from './VitalSignForm';

export class VitalSign extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 'view vital signs',
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
  }

  handleMenuItemClick(e, { name }) {
    this.setState({
      active: name,
    });
  }

  handleFormSubmit(values) {
    this.props.createVitalSign(this.props.patientRegistryId, values);

    this.setState({
      active: 'view vital signs',
    });
  }

  componentDidMount() {
    this.props.fetchAllVitalSigns(this.props.patientRegistryId);
  }

  renderContent() {
    const { active } = this.state;
    const { vitalSigns } = this.props;

    if (active === 'add vital sign') {
      return (
        <VitalSignForm
          form="CreateVitalSignForm"
          onFormSubmit={this.handleFormSubmit}
        />
      );
    } else {
      return <VitalSignTable vitalSigns={vitalSigns} />;
    }
  }

  render() {
    const { isLoading } = this.props;
    const { active } = this.state;
    console.log(this.props);
    return (
      <Segment basic loading={isLoading}>
        <VitalSignMenu
          active={active}
          onMenuItemClick={this.handleMenuItemClick}
        />
        {this.renderContent()}
      </Segment>
    );
  }
}
