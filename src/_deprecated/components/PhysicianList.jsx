import React from 'react';
import { List } from 'semantic-ui-react';
import { PhysicianListItem } from './PhysicianListItem';

export const PhysicianList = ({ physicians }) => (
  <List>
    {physicians.map((physician) => (
      <PhysicianListItem key={physician.physician_id} physician={physician} />
    ))}
  </List>
);
