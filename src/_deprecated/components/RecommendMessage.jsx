import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class RecommendMessage extends PureComponent {
  static defaultProps = {
    content: 'No data found please create',
    header: 'None',
  };

  static propTypes = {
    content: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
  };

  render() {
    const { content, header } = this.props;

    return <Message header={header} content={content} error icon="warning" />;
  }
}

export default RecommendMessage;
