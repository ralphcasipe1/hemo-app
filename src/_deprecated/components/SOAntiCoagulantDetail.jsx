import { Button, Divider, List, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import ExceptionMessage from 'ExceptionMessage';
import moment from 'moment';
import PropTypes from 'prop-types';
import RecommendMessage from 'RecommendMessage';
import React, { PureComponent } from 'react';
import SOAntiCoagulantForm from './SOAntiCoagulantForm';

class SOAntiCoagulantDetail extends PureComponent {
  static defaultProps = {
    error: null,
    isLoading: false,
    templateAntiCoagulant: null,
  };

  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    templateAntiCoagulant: PropTypes.shape({
      nss_flushing: PropTypes.string,
      nss_flushing_every: PropTypes.string,
      lmwh_iv: PropTypes.string,
      lmwh_iv_iu: PropTypes.string,
      ufh_iv: PropTypes.string,
      ufh_iv_iu: PropTypes.string,
      ufh_iv_iu_every: PropTypes.string,
      bleeding_desc: PropTypes.string,
    }),
  };

  constructor() {
    super();

    this.state = {
      edit: false,
      askDelete: false,
    };
  }

  render() {
    const { error, isLoading, templateAntiCoagulant } = this.props;

    if (isLoading) {
      return <Loader active inline="center" />;
    } else if (error) {
      return <ExceptionMessage exception={error} />;
    } else if (!templateAntiCoagulant) {
      return <RecommendMessage header="No Standing Order for Anti-Coagulant" />;
    }

    return (
      <div>
        {this.state.askDelete ? (
          <div>
            Are you sure?{' '}
            <Button
              basic
              color="blue"
              content="Cancel"
              icon="delete"
              onClick={() => this.setState({ askDelete: false })}
            />
            <Button
              color="red"
              content="Yes"
              icon="checkmark"
              onClick={() =>
                this.props.onDelete(
                  this.props.params.patientId,
                  templateAntiCoagulant.template_anti_coagulant_id
                )
              }
            />
          </div>
        ) : (
          <div>
            {this.state.edit ? (
              <SOAntiCoagulantForm
                form={`EditTemplateAntiCoagulantForm_${
                  templateAntiCoagulant.template_anti_coagulant_id
                }`}
                initialValues={{
                  nss_flushing: templateAntiCoagulant.nss_flushing,
                  nss_flushing_every: templateAntiCoagulant.nss_flushing_every,
                  lmwh_iv: templateAntiCoagulant.lmwh_iv,
                  lmwh_iv_iu: templateAntiCoagulant.lmwh_iv_iu,
                  ufh_iv: templateAntiCoagulant.ufh_iv,
                  ufh_iv_iu: templateAntiCoagulant.ufh_iv_iu,
                  ufh_iv_iu_every: templateAntiCoagulant.ufh_iv_iu_every,
                  bleeding_desc: templateAntiCoagulant.bleeding_desc,
                }}
                onCancel={() => this.setState({ edit: false })}
                onFormSubmit={(values) => {
                  this.props.onEditSubmit(
                    this.props.params.patientId,
                    templateAntiCoagulant.template_anti_coagulant_id,
                    values
                  );

                  this.setState({
                    edit: false,
                  });
                }}
              />
            ) : (
              <div>
                {templateAntiCoagulant.nss_flushing ? (
                  <div>
                    <div>
                      <b>NSS Flushing</b> {templateAntiCoagulant.nss_flushing}{' '}
                      <i>ml</i> <b>Every</b>{' '}
                      {templateAntiCoagulant.nss_flushing_every} <i>mins</i>
                    </div>
                  </div>
                ) : (
                  ''
                )}

                {templateAntiCoagulant.lmwh_iv ? (
                  <div>
                    <div>
                      <b>LMWH (IV)</b> {templateAntiCoagulant.lmwh_iv}{' '}
                      {templateAntiCoagulant.lmwh_iv_iu} <i>IU</i>
                    </div>
                  </div>
                ) : (
                  ''
                )}

                {templateAntiCoagulant.ufh_iv ? (
                  <div>
                    <div>
                      <b>UFH (IV)</b>
                      {templateAntiCoagulant.ufh_iv}{' '}
                      {templateAntiCoagulant.ufh_iv_iu}{' '}
                      {templateAntiCoagulant.ufh_iv_iu_every}
                    </div>
                  </div>
                ) : (
                  ''
                )}

                {templateAntiCoagulant.bleeding_desc ? (
                  <div>
                    <div>
                      <b>Bleeding Desc</b> {templateAntiCoagulant.bleeding_desc}
                    </div>
                  </div>
                ) : (
                  ''
                )}

                <Divider />

                <Button
                  basic
                  color="red"
                  content="Delete"
                  icon="delete"
                  onClick={() =>
                    this.setState({
                      askDelete: true,
                    })
                  }
                />

                <Button
                  basic
                  color="green"
                  content="Edit"
                  floated="right"
                  icon="edit"
                  onClick={() => this.setState({ edit: true })}
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(SOAntiCoagulantDetail);
