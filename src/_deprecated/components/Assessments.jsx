import { Icon, Tab } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import ObjectiveAssessment from './ObjectiveAssessment';
import PostObjectiveAssessment from './PostObjectiveAssessment';
import PostSubjectiveAssessment from './PostSubjectiveAssessment';
import PreAssessment from './PreAssessment';
import SubjectiveAssessment from './SubjectiveAssessment';

const { Pane } = Tab;

class Assessments extends PureComponent {
  componentDidMount() {
    this.onInit(this.props);
  }

  onInit = (props) => {
    props.fetchObjectives();
    props.fetchSubjectives();
  };

  render() {
    const panes = [
      {
        menuItem: {
          key: 1,
          icon: 'treatment',
          content: 'Subjective Assessment',
        },
        render: () => (
          <Pane>
            <SubjectiveAssessment {...this.props} />
          </Pane>
        ),
      },
      {
        menuItem: { key: 2, icon: 'medkit', content: 'Objective Assessment' },
        render: () => (
          <Pane>
            <ObjectiveAssessment {...this.props} />
          </Pane>
        ),
      },
      {
        menuItem: { key: 3, icon: 'medkit', content: 'Pre-Assessment' },
        render: () => <PreAssessment {...this.props} />,
      },
      {
        menuItem: {
          key: 4,
          icon: (
            <Icon.Group>
              <Icon name="treatment" />
              <Icon corner name="share" />
            </Icon.Group>
          ),
          content: 'Post Subjective Assessment',
        },
        render: () => (
          <Pane>
            <PostSubjectiveAssessment {...this.props} />
          </Pane>
        ),
      },
      {
        menuItem: {
          key: 5,
          icon: (
            <Icon.Group>
              <Icon name="medkit" />
              <Icon corner name="share" />
            </Icon.Group>
          ),
          content: 'Post Objective Assessment',
        },
        render: () => (
          <Pane>
            <PostObjectiveAssessment {...this.props} />
          </Pane>
        ),
      },
    ];

    const menu = {
      fluid: true,
      widths: 5,
      stackable: true,
      pointing: true,
      secondary: true,
    };

    return <Tab menu={menu} panes={panes} />;
  }
}

Assessments.propTypes = {
  fetchObjectives: PropTypes.func.isRequired,
  fetchSubjectives: PropTypes.func.isRequired,
};
export default Assessments;
