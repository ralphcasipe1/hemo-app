import { Tab } from 'semantic-ui-react';
import Timeline from './Timeline';
import Tasks from './Tasks';
import React from 'react';

const ProfileTab = (props) => {
  const panes = [
    {
      menuItem: 'Timeline',
      render: () => (
        <Tab.Pane attached={false}>
          <Timeline {...props} />
        </Tab.Pane>
      ),
    },
    {
      menuItem: 'Tasks',
      render: () => (
        <Tab.Pane attached={false}>
          <Tasks {...props} />
        </Tab.Pane>
      ),
    },
    {
      menuItem: 'Orders',
      render: () => (
        <Tab.Pane attached={false}>
          <h1>Orders</h1>
        </Tab.Pane>
      ),
    },
  ];

  return <Tab menu={{ secondary: true, pointing: true }} panes={panes} />;
};

export default ProfileTab;
