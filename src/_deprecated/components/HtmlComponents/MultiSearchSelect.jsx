import React from 'react';
import { Dropdown, Form } from 'semantic-ui-react';
const MultiSearchSelect = ({
  label,
  input,
  options,
  loading,
  meta: { touched, error },
}) => {
  return (
    <Form.Field>
      <label>{label}</label>
      <Dropdown
        {...input}
        placeholder={label}
        options={options}
        value={[...input.value]}
        onChange={(param, data) => input.onChange(data.value)}
        onBlur={(data) => input.onChange(data.value)}
        selection
        fluid
        multiple
        search
        loading={loading}
      />
    </Form.Field>
  );
};
export default MultiSearchSelect;
