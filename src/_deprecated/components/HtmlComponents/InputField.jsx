import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Label } from 'semantic-ui-react';

class InputField extends PureComponent {
  static defaultProps = {
    type: 'text',
    required: false,
  };

  static propTypes = {
    inputLabel: PropTypes.string,
    type: PropTypes.string,
    label: PropTypes.any,
    labelPosition: PropTypes.oneOf([
      'left',
      'right',
      'letft corder',
      'right corner',
    ]),
    disabled: PropTypes.bool,
    transparent: PropTypes.bool,
    width: PropTypes.number,
    readOnly: PropTypes.bool,
    hidden: PropTypes.bool,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
  };

  render() {
    const {
      meta: { touched, error, warning },
    } = this.props;

    return (
      <Form.Field
        error={touched && !!error}
        width={this.props.width}
        required={this.props.required}
      >
        <label>{this.props.label}</label>
        <Input
          disabled={this.props.disabled}
          hidden={this.props.hidden}
          label={this.props.inputLabel}
          labelPosition={this.props.labelPosition}
          placeholder={this.props.placeholder}
          transparent={this.props.transparent}
          readOnly={this.props.readOnly}
          type={this.props.type}
          {...this.props.input}
        />

        {touched &&
          ((error && (
            <Label basic color="red" pointing>
              {error}
            </Label>
          )) ||
            (warning && (
              <Label basic color="yellow" pointing>
                {warning}
              </Label>
            )))}
      </Form.Field>
    );
  }
}

export default InputField;
