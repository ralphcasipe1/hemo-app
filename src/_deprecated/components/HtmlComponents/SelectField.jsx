import React from 'react';
import { Select, Form } from 'semantic-ui-react';
const SelectField = ({
  label,
  input,
  loading,
  options,
  meta: { touched, error },
}) => {
  return (
    <Form.Field>
      <label>{label}</label>
      <Select
        placeholder={label}
        selection
        {...input}
        options={options}
        value={[input.value]}
        onChange={(param, data) => input.onChange(data.value)}
        onBlur={(data) => input.onChange(data.value)}
        loading={loading}
      />
    </Form.Field>
  );
};
export default SelectField;
