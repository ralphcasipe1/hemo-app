import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';

class ArrayCheckbox extends Component {
  checkboxGroup() {
    let { label, required, options, input, meta } = this.props;

    return options.map((option, index) => {
      return (
        <div className="checkbox" key={index}>
          <label>
            <input
              type="checkbox"
              name={`${input.name}[${index}]`}
              value={option.key}
              checked={input.value.indexOf(option.key) !== -1}
              onChange={(event) => {
                const newValue = [...input.value];
                if (event.target.checked) {
                  newValue.push(option.key);
                } else {
                  newValue.splice(newValue.indexOf(option.key), 1);
                }

                return input.onChange(newValue);
              }}
            />
            {option.name}
          </label>
        </div>
      );
    });
  }
  render() {
    return <div>{this.checkboxGroup()}</div>;
  }
}

export default ArrayCheckbox;
