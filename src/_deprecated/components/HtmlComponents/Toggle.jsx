import { Form, Radio } from 'semantic-ui-react';
import React, { PureComponent } from 'react';

class Toggle extends PureComponent {
  render() {
    console.log(this.props.input);
    return (
      <Form.Field>
        <label onClick={this.handleChange}>{this.props.label}</label>
        <Radio toggle value="1" {...this.props.input} />
      </Form.Field>
    );
  }
}

export default Toggle;
