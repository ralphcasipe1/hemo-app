import { Button, Header, Segment } from 'semantic-ui-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TaskList from './TaskList';
import { setTimeout } from 'timers';

class Tasks extends Component {
  constructor() {
    super();

    this.handleCheck = this.handleCheck.bind(this);
  }
  componentDidMount() {
    this.props.fetchTasks();
  }

  handleCheck(id) {
    setTimeout(() => this.props.completeTask(id), 1000);
  }
  render() {
    return (
      <div>
        <Header content="Tasks" icon="tasks" attached="top" inverted />
        <Segment attached="bottom" basic>
          <Button
            content="Add New Task"
            icon="plus"
            color="blue"
            onClick={() => alert('Kalma ka!')}
          />
          <TaskList {...this.props} onCheck={this.handleCheck} />
        </Segment>
      </div>
    );
  }
}

Tasks.PropTypes = {
  fetchTasks: PropTypes.func.isRequired,
  completeTask: PropTypes.func.isRequired,
};

export default Tasks;
