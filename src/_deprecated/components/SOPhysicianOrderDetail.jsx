import { Button, Divider, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';
import React, { PureComponent } from 'react';
import SOPhysicianOrderForm from './SOPhysicianOrderForm';

class SOPhysicianOrderDetail extends PureComponent {
  static defaultProps = {
    templatePhysicianOrder: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      askDelete: false,
    };
  }

  render() {
    const { error, isLoading, templatePhysicianOrder } = this.props;

    if (isLoading) {
      return <Loader active inline="center" />;
    } else if (error) {
      return <ExceptionMessage exception={error} />;
    } else if (!templatePhysicianOrder) {
      return (
        <RecommendMessage header="No Standing Order for Physician's Order" />
      );
    }

    return (
      <div>
        {this.state.edit ? (
          <SOPhysicianOrderForm
            form={`EditTemplatePhysicianOrder_${
              templatePhysicianOrder.template_physician_order_id
            }`}
            initialValues={{
              assessment: templatePhysicianOrder.assessment,
              medication: templatePhysicianOrder.medication,
              procedure: templatePhysicianOrder.procedure,
              diagnostic_test: templatePhysicianOrder.diagnostic_test,
              other_remark: templatePhysicianOrder.other_remark,
            }}
            onCancel={() => this.setState({ edit: false })}
            onFormSubmit={(values) => {
              this.props.onEditSubmit(
                this.props.params.patientId,
                templatePhysicianOrder.template_physician_order_id,
                values
              );
              this.setState({
                edit: false,
              });
            }}
          />
        ) : (
          <div>
            <div>
              <b>Assessment</b> {templatePhysicianOrder.assessment}
            </div>
            <div>
              <b>Medication</b> {templatePhysicianOrder.medication}
            </div>
            <div>
              <b>Procedure</b> {templatePhysicianOrder.procedure}
            </div>
            <div>
              <b>Diagnostic Test</b> {templatePhysicianOrder.diagnostic_test}
            </div>
            <div>
              <b>Other Remarks</b> {templatePhysicianOrder.other_remark}
            </div>

            <Divider />

            <Button
              basic
              color="red"
              content="Delete"
              icon="delete"
              onClick={() =>
                this.props.onDelete(
                  this.props.params.patientId,
                  templatePhysicianOrder.template_physician_order_id
                )
              }
            />

            <Button
              basic
              color="green"
              content="Edit"
              floated="right"
              icon="edit"
              onClick={() => this.setState({ edit: true })}
            />
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(SOPhysicianOrderDetail);
