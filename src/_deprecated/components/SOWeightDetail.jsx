import { Button, Divider, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import moment from 'moment';
import ExceptionMessage from 'ExceptionMessage';
import PropTypes from 'prop-types';
import RecommendMessage from 'RecommendMessage';
import SOWeightForm from './SOWeightForm';
import React, { PureComponent } from 'react';

class SOWeightDetail extends PureComponent {
  static defaultProps = {
    error: null,
    isLoading: false,
    templateWeight: null,
  };

  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    templateWeight: PropTypes.shape({
      dry_weight: PropTypes.number.isRequired,
    }),
  };

  constructor() {
    super();

    this.state = {
      edit: false,
      askDelete: false,
    };
  }

  render() {
    const { error, isLoading, templateWeight } = this.props;

    if (isLoading) {
      return <Loader active inline="centered" />;
    } else if (error) {
      return <ExceptionMessage exception={error} />;
    } else if (!templateWeight) {
      return <RecommendMessage header="No Standing Order for Weight" />;
    }

    return (
      <div>
        {this.state.askDelete ? (
          <div>
            Are you sure?{' '}
            <Button
              basic
              color="blue"
              content="Cancel"
              icon="delete"
              onClick={() => this.setState({ askDelete: false })}
            />
            <Button
              color="red"
              content="Yes"
              icon="checkmark"
              onClick={() =>
                this.props.onDelete(
                  this.props.params.patientId,
                  templateWeight.template_weight_id
                )
              }
            />
          </div>
        ) : (
          <div>
            {this.state.edit === true ? (
              <SOWeightForm
                form={`EditTemplateWeightForm_${
                  templateWeight.template_weight_id
                }`}
                onCancel={() => this.setState({ edit: false })}
                onFormSubmit={(values) => {
                  this.props.onEditSubmit(
                    this.props.params.patientId,
                    templateWeight.template_weight_id,
                    values
                  );
                  this.setState({
                    edit: false,
                  });
                }}
                initialValues={{
                  dry_weight: templateWeight.dry_weight,
                }}
              />
            ) : (
              <div>
                <div>
                  <b>DRY Weight:</b> {templateWeight.dry_weight} <i>kg</i>
                </div>

                <Divider />

                <Button
                  basic
                  color="red"
                  content="Delete"
                  icon="delete"
                  onClick={() => this.setState({ askDelete: true })}
                />

                <Button
                  basic
                  color="green"
                  content="Edit"
                  floated="right"
                  icon="edit"
                  onClick={() => this.setState({ edit: true })}
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(SOWeightDetail);
