import React, { PureComponent } from 'react';

export class ChangeLog extends PureComponent {
  render() {
    return (
      <div>
        <h1 id="changelogsv100beta">Changelogs v.1.0.0.beta</h1>

        <h3 id="adjustments">Adjustments</h3>

        <ul>
          <li>Adjust sidebar transition</li>
          <li>
            Adjust <strong>Patient Registry</strong> tab menu
          </li>
          <li>
            Adjust interface in <strong>Admin/Physicians</strong>
          </li>
          <li>
            Change icon for <strong>Admin/Abnormals</strong>
          </li>
          <li>Change default nurse notes interface</li>
        </ul>

        <h3 id="fixes">Fixes</h3>

        <ul>
          <li>
            Fix <strong>Standing Orders</strong>
          </li>
          <li>
            Fix <strong>Basic Parameters</strong>
          </li>
          <li>
            Fix <strong>Nurse Notes</strong>
          </li>
          <li>
            Fix automated local patients and bizbox's patients comparison at the
            start of the app
          </li>
          <li>Fix patient registry fetcher at the patient registry module</li>
        </ul>

        <h3 id="improvements">Improvements</h3>

        <ul>
          <li>Performance benchark and initial loading for patients</li>
          <li>Improve tab navigation in Admin modules and it's sub-modules</li>
        </ul>

        <h3 id="disabledmodulesandfunctionstemporary">
          Disabled Modules and Functions(Temporary)
        </h3>

        <ul>
          <li>Standing order for Medications</li>
          <li>Update and delete for standing orders</li>
          <li>
            Application of standing orders to actual assessments of basic
            parameters
          </li>
          <li>Update and delete in Vital Signs</li>
          <li>Update and delete in Medications</li>
          <li>Update and delete in Nurse notes</li>
        </ul>
      </div>
    );
  }
}
