import React from 'react';
import { Header, Icon, List } from 'semantic-ui-react';

export const BizboxPhysicianListItem = ({ bizboxPhysician, onClick }) => (
  <List.Item>
    <List.Content floated="right">
      <Icon color="blue" link name="external" onClick={onClick} size="large" />
    </List.Content>
    <List.Content>
      <Header
        as="h5"
        icon="doctor"
        content={`${bizboxPhysician.firstname} ${bizboxPhysician.lastname}`}
        subheader={bizboxPhysician.specialization}
      />
    </List.Content>
  </List.Item>
);
