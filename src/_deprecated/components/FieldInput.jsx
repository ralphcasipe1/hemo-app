import _ from 'lodash';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class FieldInput extends PureComponent {
  static defaultProps = {
    type: 'text',
  };

  static proptypes = {
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    type: PropTypes.string.isRequired,
  };

  render() {
    const {
      input,
      label,
      meta: { touched, error, warning },
      placeholder,
      required,
      type,
    } = this.props;

    let isRequired = classnames({
      required: required,
      '': !required,
    });

    return (
      <div className={`field ${isRequired}`}>
        <label>{_.startCase(label)}</label>
        <input placeholder={placeholder} type={type} {...input} />
        {touched &&
          ((error && (
            <div className="ui compact bottom attached message negative">
              <i className="icon warning circle" />
              {error}
            </div>
          )) ||
            (warning && (
              <div className="ui compact bottom attached message warning">
                <i className="icon warning" />
                {warning}
              </div>
            )))}
      </div>
    );
  }
}

export default FieldInput;
