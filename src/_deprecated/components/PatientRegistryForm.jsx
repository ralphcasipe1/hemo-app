import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import ButtonDecision from 'ButtonDecision';
import InputField from './HtmlComponents/InputField';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class PatientRegistryForm extends PureComponent {
  static propTypes = {
    onFormSubmit: PropTypes.func.isRequired,
  };

  render() {
    const { formLoading, handleSubmit, onFormSubmit } = this.props;

    return (
      <Form onSubmit={handleSubmit(onFormSubmit)} loading={formLoading}>
        <Field component={InputField} label="Registry No." name="registry_no" />
        <ButtonDecision expanded {...this.props} />
      </Form>
    );
  }
}

export default reduxForm({
  fields: ['id_number'],
})(PatientRegistryForm);
