import React from 'react';
import { List } from 'semantic-ui-react';

export const PhysicianListItem = ({ physician }) => (
  <List.Item>
    {physician.fname} {physician.lname}
  </List.Item>
);
