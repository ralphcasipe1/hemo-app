import { Grid } from 'semantic-ui-react';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ProfileCard from './ProfileCard';
import ProfileTab from './ProfileTab';

const propTypes = {
  user: PropTypes.object,
  isLoading: PropTypes.bool,
};

class Profile extends PureComponent {
  render() {
    const { user } = this.props;
    return (
      <Grid columns={2} doubling divided="vertically" inverted padded>
        <Grid.Column width={6}>
          <ProfileCard user={user} />
        </Grid.Column>
        <Grid.Column width={10}>
          <ProfileTab {...this.props} />
        </Grid.Column>
      </Grid>
    );
  }
}

Profile.propTypes = propTypes;
export default Profile;
