import { Feed } from 'semantic-ui-react';
import React from 'react';
import TimelineEvent from './TimelineEvent';
import PropTypes from 'prop-types';

const TimelineFeed = ({ timelines }) => (
  <Feed>
    {timelines.map((timeline) => (
      <TimelineEvent key={timeline.timeline_id} timeline={timeline} />
    ))}
  </Feed>
);
TimelineFeed.propTypes = {
  timelines: PropTypes.array,
};
export default TimelineFeed;
