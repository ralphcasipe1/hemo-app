import { List } from 'semantic-ui-react';
import _ from 'lodash';
import React from 'react';
import TaskListItem from './TaskListItem';
const TaskList = (props) => (
  <List divided relaxed="very" selection ordered>
    <List.Item>
      <List.Content>
        <List.Header content="Ongoing" />
        <List.List>
          {_.isEmpty(props.tasks) ? (
            <h1>No Task(s) Available</h1>
          ) : (
            props.tasks.map((task) => (
              <TaskListItem key={task.task_id} task={task} {...props} />
            ))
          )}
        </List.List>
      </List.Content>
    </List.Item>
    <List.Item>
      <List.Content>
        <List.Header content="Completed" />
        <List.List>
          {_.isEmpty(props.completedTasks) ? (
            <h1>No Completed Task(s)</h1>
          ) : (
            props.completedTasks.map((task) => (
              <TaskListItem key={task.task_id} task={task} {...props} />
            ))
          )}
        </List.List>
      </List.Content>
    </List.Item>
  </List>
);

export default TaskList;

/* {props.tasks.map(task => <TaskListItem key={task.task_id} task={task} {...props}/>)} */
