import React from 'react';
import { Icon, Menu } from 'semantic-ui-react';

export const PhysicianMenu = ({ active, onMenuItemClick }) => (
  <Menu fluid tabular icon widths={2}>
    <Menu.Item
      name="view physicians"
      active={active === 'view physicians'}
      onClick={onMenuItemClick}
    >
      <Icon name="doctor" color="blue" />
    </Menu.Item>
    <Menu.Item
      name="view bizbox physicians"
      active={active === 'view bizbox physicians'}
      onClick={onMenuItemClick}
    >
      <Icon name="server" color="blue" />
    </Menu.Item>
  </Menu>
);
