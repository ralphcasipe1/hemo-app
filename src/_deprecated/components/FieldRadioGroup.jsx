import React, { PureComponent } from 'react';

class FieldRadioGroup extends PureComponent {
  render() {
    const { items, input } = this.props;

    return (
      <div className="field">
        <div className="ui radio checkbox">
          {items.map((item) => (
            <div key={item.key}>
              <input
                type="radio"
                {...input}
                onChange={(event, value) => input.onChange(value)}
                id={item.text}
              />
              <label htmlFor={item.text}>{item.text}</label>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default FieldRadioGroup;
