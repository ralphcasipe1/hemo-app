import { Button, Confirm, Header, Icon, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import { MODULE } from '../constants/ModuleConstants';
import _ from 'lodash';
import AddButton from './Common/AddButton';
import NoDataMessage from './Common/NoDataMessage';
import ParameterForm from './ParameterForm';
import ParameterTable from './ParameterTable';
import React, { PureComponent } from 'react';

class Parameter extends PureComponent {
  constructor() {
    super();

    this.state = {
      DOMContent: MODULE.VIEW,
      toggled: false,
      confirmDelete: false,
      applyTemplate: false,
    };

    this.handleAdd = this.handleAdd.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.openConfirmDelete = this.openConfirmDelete.bind(this);
    this.closeConfirmDelete = this.closeConfirmDelete.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.toggleField = this.toggleField.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.openView = this.openView.bind(this);
  }

  componentDidMount() {
    const { patientRegistryId } = this.props.params;
    this.props.fetchParameter(patientRegistryId);
  }

  handleAdd() {
    this.setState({
      DOMContent: MODULE.CREATE,
    });
  }

  handleFormSubmit(values) {
    const { patientRegistryId } = this.props.params;
    this.props.createParameter(patientRegistryId, values);
    this.setState({
      DOMContent: MODULE.VIEW,
    });
  }

  openConfirmDelete() {
    this.setState({
      confirmDelete: true,
    });
  }

  closeConfirmDelete() {
    this.setState({
      confirmDelete: false,
    });
  }

  handleConfirm() {
    const { parameter } = this.props;
    const { patientRegistryId } = this.props.params;
    this.props.deleteParameter(patientRegistryId, parameter.parameter_id);
    this.setState({
      confirmDelete: false,
    });
  }

  toggleField() {
    this.setState({
      toggled: !this.state.toggled,
    });
  }

  handleChange(e) {
    const { parameter } = this.props;
    const { patientRegistryId } = this.props.params;
    const values = {
      [e.target.name]: e.target.value,
    };
    _.debounce(
      () =>
        this.props.editParameter(
          patientRegistryId,
          parameter.parameter_id,
          values
        ),
      1000
    )();
  }

  openView() {
    this.setState({
      DOMContent: MODULE.VIEW,
    });
  }

  render() {
    const { isLoading, parameter, templateParameter } = this.props;
    const { DOMContent, toggled, confirmDelete, applyTemplate } = this.state;

    if (isLoading) {
      return <Loader active inline="centered" />;
    } else if (!parameter) {
      return (
        <div>
          <Header content="Parameter" />
          {DOMContent === 'create' ? (
            <div>
              <Button
                color={applyTemplate ? 'instagram' : 'youtube'}
                content="Apply Standing Order"
                icon={
                  <Icon
                    rotated={applyTemplate ? null : 'counterclockwise'}
                    name="thumb tack"
                  />
                }
                onClick={() => {
                  this.setState({
                    applyTemplate: !this.state.applyTemplate,
                  });
                }}
                disabled={!templateParameter}
              />
              <ParameterForm
                form="ParameterForm"
                onFormSubmit={this.handleFormSubmit}
                onCancel={this.openView}
                initialValues={
                  applyTemplate
                    ? {
                        duration: templateParameter.duration,
                        blood_flow_rate: templateParameter.blood_flow_rate,
                        dialysate_bath: templateParameter.dialysate_bath,
                        dialysate_additive:
                          templateParameter.dialysate_additive,
                        dialysate_flow_rate:
                          templateParameter.dialysate_flow_rate,
                      }
                    : null
                }
              />
            </div>
          ) : (
            <div>
              <AddButton onAdd={this.handleAdd} />
              <NoDataMessage />
            </div>
          )}
        </div>
      );
    }

    return (
      <div>
        <Header content="Parameter" />

        <ParameterTable
          onChange={this.handleChange}
          onDelete={this.openConfirmDelete}
          onEdit={this.toggleField}
          parameter={parameter}
          toggled={toggled}
          {...this.props}
        />

        <Confirm
          open={confirmDelete}
          onCancel={this.closeConfirmDelete}
          onConfirm={this.handleConfirm}
          content="Are you sure you want to DELETE it?"
        />
      </div>
    );
  }
}

export default withRouter(Parameter);
