import { Button, Divider, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import ExceptionMessage from 'ExceptionMessage';
import moment from 'moment';
import PropTypes from 'prop-types';
import RecommendMessage from 'RecommendMessage';
import React, { PureComponent } from 'react';
import SODialyzerForm from './SODialyzerForm';

class SODialyzerDetail extends PureComponent {
  static defaultProps = {
    error: null,
    isLoading: false,
    templateDialyzer: null,
  };

  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    templateDialyzer: PropTypes.shape({
      d_t: PropTypes.string.isRequired,
      dialyzer_date: PropTypes.string.isRequired,
    }),
  };

  constructor() {
    super();

    this.state = {
      edit: false,
      askDelete: false,
    };
  }

  render() {
    const { error, isLoading, templateDialyzer } = this.props;

    if (isLoading) {
      return <Loader active inline="center" />;
    } else if (error) {
      return <ExceptionMessage exception={error.message} />;
    } else if (!templateDialyzer) {
      return <RecommendMessage header="No Standing Order for Dialyzer" />;
    }

    return (
      <div>
        {this.state.askDelete ? (
          <div>
            Are you sure?{' '}
            <Button
              basic
              color="blue"
              content="Cancel"
              icon="delete"
              onClick={() => this.setState({ askDelete: false })}
            />
            <Button
              color="red"
              content="Yes"
              icon="checkmark"
              onClick={() =>
                this.props.onDelete(
                  this.props.params.patientId,
                  templateDialyzer.template_dialyzer_id
                )
              }
            />
          </div>
        ) : (
          <div>
            {this.state.edit ? (
              <SODialyzerForm
                brands={this.props.brands}
                form={`EditTemplateDialyzerForm_${
                  templateDialyzer.template_dialyzer_id
                }`}
                initialValues={{
                  dialyzer_brand_id: templateDialyzer.dialyzer_brand_id,
                  type: templateDialyzer.type,
                  dialyzer_size_id: templateDialyzer.dialyzer_size_id,
                  dialyzer_date: templateDialyzer.dialyzer_date,
                  count: templateDialyzer.count,
                  d_t: templateDialyzer.d_t,
                }}
                onCancel={() => this.setState({ edit: false })}
                onFormSubmit={(values) => {
                  this.props.onEditSubmit(
                    this.props.params.patientId,
                    templateDialyzer.template_dialyzer_id,
                    values
                  );

                  this.setState({
                    edit: false,
                  });
                }}
                sizes={this.props.sizes}
              />
            ) : (
              <div>
                <div>
                  <div>
                    <b>Brand</b> {templateDialyzer.dialyzerBrand.brand_name}
                  </div>
                  <div>
                    <b>Type</b> {templateDialyzer.type}
                  </div>
                  <div>
                    <b>Size</b> {templateDialyzer.dialyzerSize.size_name}
                  </div>
                </div>
                <div>
                  <b>First use date</b>{' '}
                  {moment(templateDialyzer.dialyzer_date).format('ll')}
                </div>
                <div>
                  <b>Count</b>: {templateDialyzer.count}
                </div>
                <div>
                  <b>Discarded Date</b>
                  {templateDialyzer.d_t
                    ? moment(templateDialyzer.d_t).format('ll')
                    : ''}
                </div>

                <Divider />

                <Button
                  basic
                  color="red"
                  content="Delete"
                  icon="delete"
                  onClick={() => this.setState({ askDelete: true })}
                />

                <Button
                  basic
                  color="green"
                  content="Edit"
                  floated="right"
                  icon="edit"
                  onClick={() => this.setState({ edit: true })}
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(SODialyzerDetail);
