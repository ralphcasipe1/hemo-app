import { Table } from 'semantic-ui-react';
import React, { PureComponent } from 'react';
import { VitalSignTableHeader } from './VitalSignTableHeader';
import { VitalSignTableBody } from './VitalSignTableBody';

class VitalSignTable extends PureComponent {
  render() {
    const { vitalSigns } = this.props;

    return (
      <Table fixed compact="very" collapsing unstackable>
        <VitalSignTableHeader />

        <VitalSignTableBody vitalSigns={vitalSigns} />
      </Table>
    );
  }
}

export { VitalSignTable };
