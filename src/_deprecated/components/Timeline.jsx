import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TimelineFeed from './TimelineFeed';

const propTypes = {
  fetchTimelines: PropTypes.func.isRequired,
};

class Timeline extends PureComponent {
  componentDidMount() {
    this.props.fetchTimelines();
  }

  render() {
    return (
      <div>
        <TimelineFeed {...this.props} />
      </div>
    );
  }
}
Timeline.propTypes = propTypes;
export default Timeline;
