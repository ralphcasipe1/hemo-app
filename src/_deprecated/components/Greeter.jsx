import { Icon, Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class Greeter extends PureComponent {
  constructor() {
    super();

    this.state = {
      visible: true,
    };

    this.handleDismiss = this.handleDismiss.bind(this);
  }

  static propTypes = {
    user: PropTypes.shape({
      lname: PropTypes.string,
      fname: PropTypes.string,
    }),
  };

  handleDismiss() {
    this.setState({
      visible: false,
    });
  }

  render() {
    const { visible } = this.state;

    const { user } = this.props;

    return (
      <Message
        info
        hidden={!visible}
        onDismiss={this.handleDismiss}
        size="large"
        color="blue"
      >
        <Icon name="smile" size="large" />
        Welcome {user.is_doctor ? 'Dr.' : ''} <b>{user.lname}</b>, {user.fname}&nbsp;<Icon name="exclamation" />
      </Message>
    );
  }
}

export default Greeter;
