import { Button, Divider, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import _ from 'lodash';
import ExceptionMessage from 'ExceptionMessage';
import moment from 'moment';
import PropTypes from 'prop-types';
import RecommendMessage from 'RecommendMessage';
import SOMedicationForm from './SOMedicationForm';
import React, { PureComponent } from 'react';

class SOMedicationDetail extends PureComponent {
  static defaultProps = {
    templateMedication: [],
  };

  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    medications: PropTypes.shape({
      generic: PropTypes.string,
      brand: PropTypes.string,
      preparation: PropTypes.string,
      dosage: PropTypes.string,
      route: PropTypes.string,
    }),
  };

  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      askDelete: false,
    };
  }

  renderMedItem(templateMedication) {
    return (
      <div key={templateMedication.template_medication_id}>
        <div>
          {_.lowerCase(templateMedication.generic)}{' '}
          {_.startCase(_.lowerCase(templateMedication.brand))}
        </div>
        <div>
          <b>Preparation</b> {templateMedication.preparation}
        </div>
        <div>
          <b>Dosage</b> {templateMedication.dosage}
        </div>
        <div>
          <b>Route</b> {templateMedication.route}
        </div>
        <div>
          <b>Timing</b> {templateMedication.timing}
        </div>
        <Divider />

        <Button
          basic
          color="red"
          content="Delete"
          icon="delete"
          onClick={() =>
            this.props.onDelete(
              this.props.params.patientId,
              templateMedication.template_medication_id
            )
          }
        />

        <Button
          basic
          color="green"
          content="Edit"
          floated="right"
          icon="edit"
          onClick={() => this.setState({ edit: true })}
        />
      </div>
    );
  }

  render() {
    const { error, isLoading, templateMedications } = this.props;

    if (isLoading) {
      return <Loader active />;
    } else if (error) {
      return <ExceptionMessage exception={error} />;
    } else if (!templateMedications) {
      return <RecommendMessage header="No Standing Order for Medication" />;
    }

    return (
      <div>
        {templateMedications.map((templateMedication) =>
          this.renderMedItem(templateMedication)
        )}
      </div>
    );
    /* return (
      <div>
        {this.state.edit ? (
          <TemplateMedicationForm
            form={`EditMedicationForm_${templateMedication.template_medication_id}`}
            initialValues={{
              brand: templateMedication.brand,
              generic: templateMedication.generic,
              preparation: templateMedication.preparation,
              dosage: templateMedication.dosage,
              route: templateMedication.route
            }}
            onCancel={() => this.setState({ edit: false })}
            onFormSubmit={values => {
              this.props.onEditSubmit(
                this.props.params.patientId,
                templateMedication.template_medication_id,
                values
              );

              this.setState({
                edit: false
              });
            }}
          />
        ) : (
          
        )}
      </div>
    ); */
  }
}

export default withRouter(SOMedicationDetail);
