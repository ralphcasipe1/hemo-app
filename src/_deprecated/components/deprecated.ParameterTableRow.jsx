import { Table, Input } from 'semantic-ui-react';
import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  toggled: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  parameter: PropTypes.shape({
    duration: PropTypes.number,
    blood_flow_rate: PropTypes.number,
    ufv_goal: PropTypes.number,
    dialysate_additive: PropTypes.number,
    dialysate_bath: PropTypes.string,
    dialysate_flow_rate: PropTypes.number,
    created_at: PropTypes.string,
    updated_at: PropTypes.string,
  }),
};

const defaultProps = {
  toggled: false,
};

const { Body, Row, Cell } = Table;
const ParameterTableRow = ({
  onChange,
  toggled,
  parameter: {
    duration,
    blood_flow_rate,
    ufv_goal,
    dialysate_additive,
    dialysate_bath,
    dialysate_flow_rate,
  },
}) => (
  <Body>
    <Row>
      <Cell content="Duration" />
      <Cell textAlign="center">
        {toggled ? (
          <Input
            name="duration"
            defaultValue={duration}
            onChange={onChange}
            transparent
            icon="edit"
            iconPosition="left"
            type="number"
          />
        ) : (
          `${duration} hours`
        )}
      </Cell>
    </Row>
    <Row>
      <Cell content="Blood Flow Rate" />
      <Cell textAlign="center">
        {toggled ? (
          <Input
            name="blood_flow_rate"
            defaultValue={blood_flow_rate}
            onChange={onChange}
            transparent
            icon="edit"
            iconPosition="left"
            type="number"
          />
        ) : (
          `${blood_flow_rate} ml/min`
        )}
      </Cell>
    </Row>
    <Row>
      <Cell content="UFV Goal" />
      <Cell textAlign="center">
        {toggled ? (
          <Input
            name="ufv_goal"
            defaultValue={ufv_goal}
            onChange={onChange}
            transparent
            icon="edit"
            iconPosition="left"
            type="number"
          />
        ) : (
          `${ufv_goal} liter/s`
        )}
      </Cell>
    </Row>
    <Row>
      <Cell content="Dialysate Additive" />
      <Cell textAlign="center">
        {toggled ? (
          <Input
            name="dialysate_additive"
            defaultValue={dialysate_additive}
            onChange={onChange}
            transparent
            icon="edit"
            iconPosition="left"
            type="number"
          />
        ) : (
          `${dialysate_additive} liter/s`
        )}
      </Cell>
    </Row>
    <Row>
      <Cell content="Dialysate Bath" />
      <Cell textAlign="center">
        {toggled ? (
          <Input
            name="dialysate_bath"
            defaultValue={dialysate_bath}
            onChange={onChange}
            transparent
            icon="edit"
            iconPosition="left"
          />
        ) : (
          dialysate_bath
        )}
      </Cell>
    </Row>
    <Row>
      <Cell content="Dialysate Flow Rate" />
      <Cell textAlign="center">
        {toggled ? (
          <Input
            name="dialysate_flow_rate"
            defaultValue={dialysate_flow_rate}
            onChange={onChange}
            transparent
            icon="edit"
            iconPosition="left"
            type="number"
          />
        ) : (
          dialysate_flow_rate
        )}
      </Cell>
    </Row>
  </Body>
);

ParameterTableRow.propTypes = propTypes;
ParameterTableRow.defaultProps = defaultProps;

export default ParameterTableRow;
