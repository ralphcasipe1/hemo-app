import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class DeleteMessage extends PureComponent {
  static defaultProps = {
    content: 'No data found please create',
    header: 'Deleted successfully',
  };

  static propTypes = {
    content: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
  };

  render() {
    const { content, header } = this.props;

    return <Message header={header} content={content} error icon="x" />;
  }
}

export default DeleteMessage;
