import { Icon, Header, List, Segment } from 'semantic-ui-react';
import { Link, withRouter } from 'react-router';
import React from 'react';

class MobileTemplateArea extends React.PureComponent {
  render() {
    /* return (
            <div>
                <Header as="h3" content="Standing Orders" dividing/>
                <Segment.Group>
                <Header as="h5" content="Parameters" attached="top"/>
                    <Segment attached>
                        <Link to={`/patients/${patientId}/template/parameters`}>Parameters</Link>
                    </Segment>
                    <Segment attached><Link to={`/patients/${patientId}/template/weights`}>Weight</Link></Segment>
                    <Segment attached><Link to={`/patients/${patientId}/template/anti_coagulants`}>Anti-Coagulant</Link></Segment>
                    <Segment attached><Link to={`/patients/${patientId}/template/dialyzers`}>Dialyzers</Link></Segment>
                    <Segment attached><Link to={`/patients/${patientId}/template/vaccines`}>Vaccines</Link></Segment>
                    <Header content="Medications" attached/>
                    <Segment attached><Link to={`/patients/${patientId}/template/medicatoins`}>Medications</Link></Segment>
                    <Segment attached><Link to={`/patients/${patientId}/template/physician_orders`}>Physician Orders</Link></Segment>
                </Segment.Group>
            </div>
        ); */

    return (
      <div>
        <Segment>
          <Header as="h4" content="Standing Orders" dividing color="grey" />
          <List celled divided verticalAlign="bottom" relaxed="very">
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_parameters`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Parameters</List.Header>
              </List.Content>
            </List.Item>
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_weights`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Weight</List.Header>
              </List.Content>
            </List.Item>
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_anti_coagulants`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Anti-Coagulants</List.Header>
              </List.Content>
            </List.Item>
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_dialyzers`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Dialyzers</List.Header>
              </List.Content>
            </List.Item>
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_vaccines`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Vaccine</List.Header>
              </List.Content>
            </List.Item>
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_medications`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Medications</List.Header>
              </List.Content>
            </List.Item>
            <List.Item
              as={Link}
              to={`/patients/${this.props.patientId}/template_physician_orders`}
            >
              <List.Content floated="right">
                <Icon name="chevron right" inverted />
              </List.Content>
              <List.Content>
                <List.Header>Physician Orders</List.Header>
              </List.Content>
            </List.Item>
          </List>
        </Segment>
      </div>
    );
  }
}

MobileTemplateArea.propTypes = {};

export default withRouter(MobileTemplateArea);
