import { Checkbox, List } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const { Content, Header, Item } = List;
const TaskListItem = ({ task, onCheck }) => (
  <Item>
    <Content>
      <Header>
        {task.task_status === 'completed' ? (
          task.task_desc
        ) : (
          <Checkbox
            label={task.task_desc}
            fitted
            onChange={() => onCheck(task.task_id)}
          />
        )}
      </Header>
    </Content>
  </Item>
);
TaskListItem.propTypes = {
  task: PropTypes.shape({
    task_desc: PropTypes.string,
  }),
  onCheck: PropTypes.func.isRequired,
};
export default TaskListItem;
