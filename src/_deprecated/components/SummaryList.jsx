import { Header, List, Message } from 'semantic-ui-react';
import { Link, withRouter } from 'react-router';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import SummaryDetail from 'SummaryDetail';

class SummaryList extends PureComponent {
  static propTypes = {
    exclude: PropTypes.number.isRequired,
  };

  constructor() {
    super();

    this.state = {
      open: false,
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
  }

  handleClose() {
    this.setState({
      open: false,
    });
  }

  handleOpen() {
    this.setState({
      open: true,
    });
  }

  render() {
    const { exclude, items, onClickList } = this.props;

    return (
      <div>
        <Header as="h4" content="Recent Registry Nos." />
        {_.isEmpty(
          items.filter((item) => item.patient_registry_id !== exclude)
        ) ? (
          <Message
            icon="warning sign"
            content="No other patient registry nos."
            warning
          />
        ) : (
          <List divided relaxed selection link>
            {items
              .filter((item) => item.patient_registry_id !== exclude)
              .map((item) => (
                <List.Item
                  key={item.patient_registry_id}
                  as={Link}
                  to={`/patients/${this.props.params.patientId}/summary/${
                    item.patient_registry_id
                  }`}
                >
                  <List.Content>
                    <List.Header>Registry No: {item.registry_no}</List.Header>
                    <List.Description>
                      {moment(item.created_at).format('lll')}
                    </List.Description>
                  </List.Content>
                </List.Item>
              ))}
          </List>
        )}
      </div>
    );
  }
}

export default withRouter(SummaryList);
