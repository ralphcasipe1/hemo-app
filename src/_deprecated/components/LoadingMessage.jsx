import { Icon, Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class LoadingMessage extends PureComponent {
  static propTypes = {
    content: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
  };

  render() {
    const { content, header } = this.props;

    return (
      <Message icon>
        <Icon name="circle notched" loading />
        <Message.Content>
          <Message.Header>{header}</Message.Header>
          {content}
        </Message.Content>
      </Message>
    );
  }
}

export default LoadingMessage;
