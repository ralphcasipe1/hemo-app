import { Button, Divider, Loader } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import _ from 'lodash';
import ExceptionMessage from 'ExceptionMessage';
import moment from 'moment';
import RecommendMessage from 'RecommendMessage';
import React, { PureComponent } from 'react';
import TemplateParameterForm from 'TemplateParameterForm';

class SOParameterDetail extends PureComponent {
  constructor() {
    super();

    this.state = {
      edit: false,
      askDelete: false,
    };
  }

  render() {
    const { error, isLoading, sOParameter } = this.props;

    if (isLoading) {
      return <Loader active inline="centered" />;
    } else if (error) {
      return <ExceptionMessage exception={error} />;
    } else if (!sOParameter) {
      return <RecommendMessage header="No Standing Order for Parameter" />;
    }

    return (
      <div>
        {this.state.askDelete ? (
          <div>
            Are you sure?{' '}
            <Button
              basic
              color="blue"
              content="Cancel"
              icon="delete"
              onClick={() => this.setState({ askDelete: false })}
            />
            <Button
              color="red"
              content="Yes"
              icon="checkmark"
              onClick={() =>
                this.props.onDelete(
                  this.props.params.patientId,
                  templateParameter.template_parameter_id
                )
              }
            />
          </div>
        ) : (
          <div>
            {this.state.edit ? (
              <TemplateParameterForm
                form={`EditTemplateParameterForm_${
                  templateParameter.template_parameter_id
                }`}
                initialValues={{
                  duration: templateParameter.duration,
                  blood_flow_rate: templateParameter.blood_flow_rate,
                  dialysate_bath: templateParameter.dialysate_bath,
                  dialysate_additive: templateParameter.dialysate_additive,
                  dialysate_flow_rate: templateParameter.dialysate_flow_rate,
                }}
                onCancel={() => this.setState({ edit: false })}
                onFormSubmit={(values) => {
                  this.props.onEditSubmit(
                    this.props.params.patientId,
                    templateParameter.template_parameter_id,
                    values
                  );
                  this.setState({
                    edit: false,
                  });
                }}
              />
            ) : (
              <div>
                <div>
                  <b>Duration:</b> {templateParameter.duration} <i>hours</i>
                </div>
                <div>
                  <b>Blood Flow Rate:</b> {templateParameter.blood_flow_rate}{' '}
                  <i>ml/min</i>
                </div>
                <div>
                  <b>Dialysate Bath:</b> {templateParameter.dialysate_bath}
                </div>
                <div>
                  <b>Dialysate Additive:</b>{' '}
                  {templateParameter.dialysate_additive}
                </div>
                <div>
                  <b>Dialysate Flow Rate:</b>{' '}
                  {templateParameter.dialysate_flow_rate}
                </div>

                <Divider />

                <Button
                  basic
                  color="red"
                  content="Delete"
                  icon="delete"
                  onClick={() => this.setState({ askDelete: true })}
                />

                <Button
                  basic
                  color="green"
                  content="Edit"
                  floated="right"
                  icon="edit"
                  onClick={() => this.setState({ edit: true })}
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(SOParameterDetail);
