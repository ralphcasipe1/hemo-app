import { Header, Image } from 'semantic-ui-react';
import userImage from '../assets/defaultMale.jpg';
import PropTypes from 'prop-types';
import React from 'react';

const propTypes = {
  user: PropTypes.shape({
    fname: PropTypes.string,
    lname: PropTypes.string,
  }),
};

const ProfileAvatar = ({ user }) => (
  <div>
    <Header as="h3" icon textAlign="center" dividing>
      <Image className="ProfileImage" src={userImage} circular centered />
      <Header.Content>
        {user.fname} {user.lname}
      </Header.Content>
      <Header.Subheader>Head Nurse</Header.Subheader>
    </Header>
  </div>
);
ProfileAvatar.propTypes = propTypes;
export default ProfileAvatar;
