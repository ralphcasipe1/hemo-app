import { Header, List } from 'semantic-ui-react';
import React from 'react';

const ProfileInfo = ({ user }) => (
  <div>
    <Header content="Basic Information" size="small" />
    <List relaxed="very" verticalAlign="middle" className="ProfileListInfo">
      <List.Item>
        <List.Content>
          <List.Header>ID Number</List.Header>
          <List.Description>{user.id_number}</List.Description>
        </List.Content>
      </List.Item>
    </List>
  </div>
);

export default ProfileInfo;
