import { Segment } from 'semantic-ui-react';
import ProfileInfo from './ProfileInfo';
import ProfileAvatar from './ProfileAvatar';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class ProfileCard extends PureComponent {
  static propTypes = {
    user: PropTypes.shape({
      lname: PropTypes.string.isRequired,
      fname: PropTypes.string.isRequired,
      mname: PropTypes.string,
    }),
  };

  render() {
    const { error, isLoading, user } = this.props;

    if (isLoading) {
      return <span>Loading</span>;
    } else if (error) {
      return <span>{error}</span>;
    } else if (!user) {
      return <span />;
    }

    return (
      <Segment>
        <ProfileAvatar {...this.props} />

        <ProfileInfo {...this.props} />
      </Segment>
    );
  }
}

export default ProfileCard;
