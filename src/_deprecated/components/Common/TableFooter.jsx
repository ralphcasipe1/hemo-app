import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button, Table } from 'semantic-ui-react';


const { Footer, HeaderCell, Row } = Table;

export default class TableFooter extends PureComponent {
  constructor() {
    super();

    this.state = {
      isEdit: false,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      isEdit: !this.state.isEdit,
    });

    this.props.onEdit();
  }

  render() {
    const { onDelete, disabled } = this.props;
    const { isEdit } = this.state;
    return (
      <Footer fullWidth>
        <Row>
          <HeaderCell textAlign="right">
            <Button
              basic
              color="red"
              compact
              disabled={disabled}
              onClick={onDelete}
              content="Delete"
              icon="trash"
            />
          </HeaderCell>
          <HeaderCell>
            <Button
              basic
              color={isEdit ? null : 'green'}
              compact
              content={isEdit ? 'Close' : 'Edit'}
              disabled={disabled}
              icon={isEdit ? 'delete' : 'edit'}
            />
          </HeaderCell>
        </Row>
      </Footer>
    );
  }
}

TableFooter.propTypes = {
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

TableFooter.defaultProps = {
  disabled: false,
};
