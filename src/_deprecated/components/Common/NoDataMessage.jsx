import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const NoDataMessage = ({ content, header }) => (
  <Message content={content} header={header} error icon="warning" />
);

NoDataMessage.propTypes = {
  content: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
};

NoDataMessage.defaultProps = {
  content: 'No data found please create',
  header: 'None',
};

export default NoDataMessage;
