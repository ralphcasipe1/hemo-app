import React from 'react';
import { shallow } from 'enzyme';
import AddButton from './AddButton';

describe('Common component AddButton', () => {
  it('Must have a content/name', () => {
    const content = 'Add';
  });

  it('AddButton render without crashing', () => {
    const component = shallow(<AddButton />);
    expect(component.exists()).toEqual(true);
  });
});
