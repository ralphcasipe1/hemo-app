import { Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const AddButton = ({ content, onAdd }) => (
  <Button color="blue" content={content} icon="plus" onClick={onAdd} />
);

AddButton.propTypes = {
  content: PropTypes.string.isRequired,
  onAdd: PropTypes.func.isRequired,
};

AddButton.defaultProps = {
  content: 'Add',
};

export default AddButton;
