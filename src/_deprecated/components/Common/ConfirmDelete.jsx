import { Container, Header, Button, Divider } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const ConfirmDelete = ({ content, onCancel, onConfirm }) => (
  <Container textAlign="right">
    <Header content={content} icon="trash" color="red" />
    <Divider section />
    <Button content="Cancel" icon="delete" onClick={onCancel} />
    <Button content="Yes" icon="checkmark" color="blue" onClick={onConfirm} />
  </Container>
);

ConfirmDelete.propTypes = {
  content: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

ConfirmDelete.defaultProps = {
  content: 'You want to delete it?',
};

export default ConfirmDelete;
