import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const SuccessMessage = ({ header, content }) => (
  <Message header={header} content={content} success icon="checkmark" />
);

SuccessMessage.propTypes = {
  header: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

SuccessMessage.defaultProps = {
  header: "It's a success",
};

export default SuccessMessage;
