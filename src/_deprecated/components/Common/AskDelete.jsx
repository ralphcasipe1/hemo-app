import { Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const AskDelete = ({ onCancel, onYes }) => (
  <div style={{ float: 'right' }}>
    <label>Are you sure? </label>
    <Button
      basic
      content="Cancel"
      icon="delete"
      onClick={onCancel}
      size="small"
    />
    <Button
      color="red"
      content="Yes"
      icon="check"
      onClick={onYes}
      size="small"
    />
  </div>
);

AskDelete.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onYes: PropTypes.func.isRequired,
};

export default AskDelete;
