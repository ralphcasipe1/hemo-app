import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const ExceptionMessage = ({ header, content }) => (
  <Message header={header} content={content} error icon="warning sign" />
);

ExceptionMessage.propTypes = {
  header: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

ExceptionMessage.defaultProps = {
  header: 'An Error Occurred',
};

export default ExceptionMessage;
