import { Menu } from 'semantic-ui-react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class Pagination extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { pager: {} };
  }

  componentWillMount() {
    // set page if items array isn't empty
    if (this.props.items && this.props.items.length) {
      this.setPage(this.props.initialPage);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // reset page if items array has changed
    if (this.props.items !== prevProps.items) {
      this.setPage(this.props.initialPage);
    }
  }

  setPage(page) {
    var items = this.props.items;
    var pager = this.state.pager;

    if (page < 1 || page > pager.totalPages) {
      return;
    }

    // get new pager object for specified page
    pager = this.getPager(items.length, page);

    // get new page of items from items array
    var pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

    // update state
    this.setState({ pager: pager });

    // call change page function in parent component
    this.props.onChangePage(pageOfItems);
  }

  getPager(totalItems, currentPage, pageSize) {
    // default to first page
    currentPage = currentPage || 1;

    // default page size is 10
    pageSize = pageSize || 10;

    // calculate total pages
    var totalPages = Math.ceil(totalItems / pageSize);

    var startPage, endPage;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    var startIndex = (currentPage - 1) * pageSize;
    var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    var pages = _.range(startPage, endPage + 1);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages,
    };
  }

  render() {
    let pager = this.state.pager;

    if (!pager.pages || pager.pages.length <= 1) {
      return null;
    }

    return (
      <Menu pagination>
        <Menu.Item content="First" onClick={() => this.setPage(1)} />
        <Menu.Item
          content="Prev"
          onClick={() => this.setPage(pager.currentPage - 1)}
        />
        {pager.pages.map((page, index) => (
          <Menu.Item
            key={index}
            name={`${page}`}
            onClick={() => this.setPage(page)}
          />
        ))}
        <Menu.Item
          content="Next"
          onClick={() => this.setPage(pager.currentPage + 1)}
        />
        <Menu.Item
          content="Last"
          onClick={() => this.setPage(pager.totalPages)}
        />
      </Menu>
    );
  }
}

Pagination.defaultProps = {
  initialPage: 1,
};

Pagination.propTypes = {
  items: PropTypes.array.isRequired,
  onChangePage: PropTypes.func.isRequired,
  initialPage: PropTypes.number,
};
/* class Pagination extends PureComponent {
  render() {
    const {pages} = this.props
    let content = []
    let page = 1
    for(page; page <= pages; page++) {
      content.push(page)
    }
    
    
    let items = content.map(c => {
      if( c > 3 && c < pages) {
        return <Menu.Item content="..."/>
      }
      return <Menu.Item name={c}/>
    })
    
    
    return (
      <Menu pagination>
        {items}
      </Menu>
    )
  }
} */

export default Pagination;
