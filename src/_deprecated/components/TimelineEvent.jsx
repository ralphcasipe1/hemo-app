import { Feed, Icon } from 'semantic-ui-react';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';

const { Event, Label, Content, Summary, Date, Extra, Meta } = Feed;

const TimelineEvent = ({ timeline }) => {
  let icon;
  if (timeline.timeline_status === 'added') {
    icon = 'plus';
  } else if (timeline.timeline_status === 'updated') {
    icon = 'edit';
  } else if (timeline.timeline_status === 'deleted') {
    icon = 'delete';
  } else {
    icon = 'info';
  }
  return (
    <Event>
      <Label>
        <Icon name={icon} />
      </Label>
      <Content>
        <Summary>
          {timeline.activity.name} {timeline.timeline_status}
          <Date>{moment(timeline.created_at).fromNow()}</Date>
        </Summary>
        {!timeline.description ? null : <Extra>{timeline.description}</Extra>}
        <Meta>{moment(timeline.created_at).format('LT')}</Meta>
      </Content>
    </Event>
  );
};

TimelineEvent.propTypes = {
  timeline: PropTypes.shape({
    activity_id: PropTypes.number,
    timeline_status: PropTypes.string,
    description: PropTypes.string,
    created_at: PropTypes.string,
  }),
};

export default TimelineEvent;
