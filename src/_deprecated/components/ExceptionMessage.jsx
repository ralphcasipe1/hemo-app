import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import React from 'react';

class ExceptionMessage extends React.PureComponent {
  render() {
    const { exception } = this.props;

    return (
      <Message
        icon="warning"
        header={exception.status}
        content={_.capitalize(exception.message)}
        error
      />
    );
  }
}

ExceptionMessage.propTypes = {
  exception: PropTypes.shape({
    errors: PropTypes.object,
    message: PropTypes.string,
    status_code: PropTypes.number,
  }),
};

export default ExceptionMessage;
