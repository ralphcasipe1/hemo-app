import { Field, reduxForm } from 'redux-form';
import { Form } from 'semantic-ui-react';
import ButtonDecision from './ButtonDecision';
import InputField from './HtmlComponents/InputField';
import TextAreaField from './HtmlComponents/TextAreaField';
import React, { PureComponent } from 'react';

class VitalSignForm extends PureComponent {
  render() {
    return (
      <Form
        loading={this.props.formLoading}
        onSubmit={this.props.handleSubmit(this.props.onFormSubmit)}
      >
        <Field
          component={InputField}
          name="blood_pressure"
          label="Blood Pressure"
          inputLabel="mmHg"
          labelPosition="right"
        />
        <Field
          component={InputField}
          name="cardiac_arrest"
          label="Cardiac Rate"
          inputLabel="bpm"
          labelPosition="right"
        />
        <Field
          component={InputField}
          name="respiratory_rate"
          label="Respiratory Rate"
          inputLabel="cpm"
          labelPosition="right"
        />
        <Field
          component={InputField}
          name="temperature"
          label="Temperature"
          inputLabel="&#x002DA;c"
          labelPosition="right"
          required
        />
        <Field
          component={InputField}
          name="oxygen_saturation"
          label="Oxygen Saturation"
          inputLabel="%"
          labelPosition="right"
          required
        />
        <Field
          component={InputField}
          name="arterial_pressure"
          label="Arterial Pressure"
        />
        <Field
          component={InputField}
          name="venous_pressure"
          label="Venous Pressure"
        />
        <Field
          component={InputField}
          name="transmembrane_pressure"
          label="Transmembrane Pressure"
        />
        <Field
          component={InputField}
          name="blood_flow_rate"
          label="Blood Flow Rate"
        />
        <Field
          component={InputField}
          name="blood_glucose"
          label="Blood Glucose"
          inputLabel="mg/dL"
          labelPosition="right"
        />

        <Field component={TextAreaField} name="remarks" label="Other Remarks" />
        <ButtonDecision {...this.props} expanded />
      </Form>
    );
  }
}

export default reduxForm({
  fields: [
    'blood_pressure',
    'cardiac_arrest',
    'respiratory_rate',
    'temperature',
    'oxygen_saturation',
    'arterial_pressure',
    'venous_pressure',
    'transmembrane_pressure',
    'blood_flow_rate',
    'blood_glucose',
    'remarks',
  ],
})(VitalSignForm);
