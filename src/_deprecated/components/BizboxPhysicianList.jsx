import React, { PureComponent } from 'react';
import { List } from 'semantic-ui-react';
import { BizboxPhysicianListItem } from './BizboxPhysicianListItem';

export class BizboxPhysicianList extends PureComponent {
  render() {
    const { bizboxPhysicians, onClick } = this.props;

    return (
      <List divided relaxed>
        {bizboxPhysicians.map((bizboxPhysician) => (
          <BizboxPhysicianListItem
            key={bizboxPhysician.PK_emdDoctors}
            bizboxPhysician={bizboxPhysician}
            onClick={() => onClick(bizboxPhysician.PK_emdDoctors)}
          />
        ))}
      </List>
    );
  }
}
