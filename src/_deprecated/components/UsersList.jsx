import { Button, Icon, List, Loader, Segment } from 'semantic-ui-react';
import _ from 'lodash';
import UserForm from 'UserForm';
import ExceptionMessage from 'ExceptionMessage';
import RecommendMessage from 'RecommendMessage';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class UsersList extends PureComponent {
  static defaultProps = {
    users: [],
  };

  static propTypes = {
    error: PropTypes.object,
    isLoading: PropTypes.bool,
    users: PropTypes.arrayOf(
      PropTypes.shape({
        user_id: PropTypes.number.isRequired,
        lname: PropTypes.string.isRequired,
        mname: PropTypes.string,
        fname: PropTypes.string.isRequired,
      })
    ),
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
  };

  constructor() {
    super();

    this.state = {
      userId: [],
      deleteUsers: [],
    };
  }

  renderListItem(user) {
    if (this.state.userId.indexOf(user.user_id) !== -1) {
      return (
        <List.Item key={user.user_id}>
          <div style={{ backgroundColor: '#ecf0f1', padding: '2px 0' }}>
            <UserForm
              form={`EditUserForm${user.user_id}`}
              onCancel={() =>
                this.setState({
                  userId: this.state.userId.filter((id) => id !== user.user_id),
                })
              }
              onFormSubmit={(values) => {
                this.props.onEditClick(user.user_id, values);
                this.setState({
                  userId: this.state.userId.filter((id) => id !== user.user_id),
                });
              }}
              initialValues={{
                fname: user.fname,
                mname: user.mname,
                lname: user.lname,
                id_number: user.id_number,
                password: user.password,
              }}
              simple
            />
          </div>
        </List.Item>
      );
    } else if (this.state.deleteUsers.indexOf(user.user_id) !== -1) {
      return (
        <List.Item key={user.user_id}>
          <List.Content floated="right" verticalAlign="middle">
            <Button
              content="Yes"
              color="blue"
              floated="right"
              color="red"
              icon="checkmark"
              onClick={() => this.props.onDeleteClick(user.user_id)}
            />
            <Button
              content="No"
              color="blue"
              floated="right"
              basic
              icon="cancel"
              onClick={() =>
                this.setState({
                  deleteUsers: this.state.deleteUsers.filter(
                    (id) => id !== user.user_id
                  ),
                })
              }
            />
          </List.Content>
          <Icon name="warning sign" color="yellow" />
          <List.Content>
            <List.Header>
              You want to delete {user.fname} {user.lname}?
            </List.Header>
          </List.Content>
        </List.Item>
      );
    } else {
      return (
        <List.Item key={user.user_id}>
          <List.Content floated="right">
            {user.is_admin ? (
              ''
            ) : (
              <Icon
                color="red"
                link
                name="trash"
                onClick={() =>
                  this.setState({
                    deleteUsers: [...this.state.deleteUsers, user.user_id],
                  })
                }
              />
            )}
            <Icon
              color="green"
              link
              name="edit"
              onClick={() =>
                this.setState({
                  userId: [...this.state.userId, user.user_id],
                })
              }
            />
          </List.Content>
          <List.Content>
            <List.Header>
              {user.id_number} {user.lname}, {user.fname} {user.mname}
            </List.Header>
            <List.Description>
              {moment(user.created_at).format('ll')}
            </List.Description>
          </List.Content>
        </List.Item>
      );
    }
  }
  render() {
    if (this.props.isLoading) {
      return <Loader active inline="centered" />;
    } else if (this.props.error) {
      return <ExceptionMessage exception={this.props.error} />;
    } else if (_.isEmpty(this.props.users)) {
      return <RecommendMessage header="No Users" />;
    }

    return (
      <List divided relaxed>
        {this.props.users.map((user) => this.renderListItem(user))}
      </List>
    );
  }
}

export default UsersList;
