import { Header } from 'semantic-ui-react';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

class SummaryDetail extends PureComponent {
  static propTypes = {
    header: PropTypes.string.isRequired,
  };

  render() {
    const { children, error, header, isLoading, patientRegistry } = this.props;

    if (isLoading) {
      return <div>Loading</div>;
    } else if (error) {
      return <div>{error.message}</div>;
    } else if (!patientRegistry) {
      return <div>None</div>;
    }

    return (
      <div>
        {moment(patientRegistry.created_at).format('ll')}
        <Header>{header}</Header>
        <div>{patientRegistry.registry_no}</div>
        <div>
          {patientRegistry.parameter ? patientRegistry.parameter : 'None'}
        </div>
        <div>{patientRegistry.weight ? patientRegistry.weight : 'None'}</div>
        <div>
          {patientRegistry.antiCoagulant
            ? patientRegistry.antiCoagulant
            : 'None'}
        </div>
        <div>
          {patientRegistry.dialyzer ? patientRegistry.dialyzer : 'None'}
        </div>
        <div>{patientRegistry.vaccine ? patientRegistry.vaccine : 'None'}</div>
        <div>
          {patientRegistry.vascular ? patientRegistry.vascular : 'None'}
        </div>
        <div>
          {patientRegistry.catheter ? patientRegistry.catheter : 'None'}
        </div>
      </div>
    );
  }
}

export default SummaryDetail;
