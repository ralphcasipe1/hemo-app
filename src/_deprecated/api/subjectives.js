import { instance } from './config';

export default {
  fetchAll: () =>
    instance
      .get('/subjectives')
      .then((res) => res.data, (err) => err.response.data),
};
