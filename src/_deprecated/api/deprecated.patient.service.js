import { instance } from './config';

export default {
  fetchAll: () =>
    instance
      .get(`patients`)
      .then((res) => res.data, (err) => err.response.data),

  fetch: (id) => instance.get(`patients/${id}`).then((res) => res.data),

  create: (values) => instance.post('patients', values).then((res) => res.data),
};
