import { bizbox } from './config';

export const bizboxPhysicianService = {
  fetchAll: () =>
    bizbox
      .get('physicians')
      .then((res) => res.data, (err) => err.response.data),

  fetch: (id) =>
    bizbox
      .get(`physicians/${id}`)
      .then((res) => res.data, (err) => err.response.data),
};
