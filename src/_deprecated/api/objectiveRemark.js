import { instance } from './config';

export default {
  create: (id, values) =>
    instance
      .post(`patient_registries/${id}/objective_remarks`, values)
      .then((res) => res.data),
  fetch: (id) =>
    instance
      .get(`patient_registries/${id}/objective_remarks`)
      .then((res) => res.data),
  delete: (id, remarkId) =>
    instance
      .delete(`/patient_registries/${id}/objective_remarks/${remarkId}`)
      .then((res) => res.data),
};
