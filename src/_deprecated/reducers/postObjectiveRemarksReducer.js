import * as types from '../constants/PostObjectiveRemarkConstants';

const initialState = {
  selectedPostObjectiveRemark: {
    postObjectiveRemark: null,
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.GET_POST_OBJECTIVE_REMARK:
      return {
        ...state,
        selectedPostObjectiveRemark: {
          postObjectiveRemark: null,
          error: null,
          isLoading: true,
        },
      };

    case types.GET_POST_OBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        selectedPostObjectiveRemark: {
          postObjectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.GET_POST_OBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        selectedPostObjectiveRemark: {
          postObjectiveRemark: null,
          error: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_GET_POST_OBJECTIVE_REMARK:
      return {
        ...state,
        selectedPostObjectiveRemark: {
          postObjectiveRemark: null,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_POST_OBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };

    case types.CREATE_POST_OBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Post objective remark successfully added',
          errorMessage: null,
          isLoading: false,
        },
        selectedPostObjectiveRemark: {
          postObjectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_POST_OBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_NEW_POST_OBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
