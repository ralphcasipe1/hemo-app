import * as types from '../constants/PhysicianConstants';

const INITIAL_STATE = {
  physiciansList: {
    physicians: [],
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.CREATE_PHYSICIAN:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };
    case types.CREATE_PHYSICIAN_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Physician successfully migrated',
          errorMessage: null,
          isLoading: false,
        },
        physiciansList: {
          physicians: [action.payload.data, ...state.physiciansList.physicians],
          error: null,
          isLoading: false,
        },
      };
    case types.CREATE_PHYSICIAN_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_NEW_PHYSICIAN:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    case types.FETCH_PHYSICIANS:
      return {
        ...state,
        physiciansList: {
          physicians: [],
          error: null,
          isLoading: true,
        },
      };
    case types.FETCH_PHYSICIANS_SUCCESS:
      return {
        ...state,
        physiciansList: {
          physicians: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.FETCH_PHYSICIANS_FAILURE:
      return {
        ...state,
        physiciansList: {
          physicians: [],
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_PHYSICIANS:
      return {
        ...state,
        physiciansList: {
          physicians: [],
          error: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
