import * as types from '../constants/SubjectiveRemarkConstants';

const initialState = {
  selectedSubjectiveRemark: {
    subjectiveRemark: null,
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.GET_SUBJECTIVE_REMARK:
      return {
        ...state,
        selectedSubjectiveRemark: {
          subjectiveRemark: null,
          error: null,
          isLoading: true,
        },
      };

    case types.GET_SUBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        selectedSubjectiveRemark: {
          subjectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.GET_SUBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        selectedSubjectiveRemark: {
          subjectiveRemark: null,
          error: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_GET_SUBJECTIVE_REMARK:
      return {
        ...state,
        selectedSubjectiveRemark: {
          subjectiveRemark: null,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_SUBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };

    case types.CREATE_SUBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Subjective remark successfully added',
          errorMessage: null,
          isLoading: false,
        },
        selectedSubjectiveRemark: {
          subjectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_SUBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_NEW_SUBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
