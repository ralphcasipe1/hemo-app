import * as types from '../constants/PostSubjectiveAssessmentConstants';

/**
 * @note: SubjectiveAssessments -> Subjs
 */
const INITIAL_STATE = {
  patientRegistryPostSubjsList: {
    patientRegistryPostSubjs: [],
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_PATIENT_REGISTRY_POST_SUBJS:
      return {
        ...state,
        patientRegistryPostSubjsList: {
          patientRegistryPostSubjs: [],
          error: null,
          isLoading: true,
        },
      };
    case types.FETCH_PATIENT_REGISTRY_POST_SUBJS_SUCCESS:
      return {
        ...state,
        patientRegistryPostSubjsList: {
          patientRegistryPostSubjs: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.FETCH_PATIENT_REGISTRY_POST_SUBJS_FAILURE:
      return {
        ...state,
        patientRegistryPostSubjsList: {
          patientRegistryPostSubjs: [],
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_PATIENT_REGISTRY_POST_SUBJS:
      return {
        ...state,
        patientRegistryPostSubjsList: {
          patientRegistryPostSubjs: [],
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_PATIENT_REGISTRY_POST_SUBJ:
      return {
        ...state,
        creation: {
          successMessage: null,
          error: null,
          isLoading: true,
        },
      };

    case types.CREATE_PATIENT_REGISTRY_POST_SUBJ_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Post subjective assessment added successfully',
          errorMessage: null,
          isLoading: false,
        },
        patientRegistryPostSubjsList: {
          patientRegistryPostSubjs: action.payload.data,
          errorMessage: null,
          isLoading: false,
        },
      };

    case types.CREATE_PATIENT_REGISTRY_POST_SUBJ_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_NEW_PATIENT_REGISTRY_POST_SUBJ:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
