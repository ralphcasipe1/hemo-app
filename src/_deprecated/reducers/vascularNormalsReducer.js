import * as types from '../constants/VascularNormalConstants';

const initialState = {
  vascularNormalsList: {
    vascularNormals: [],
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.CREATE_VASCULAR_NORMALS:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };

    case types.CREATE_VASCULAR_NORMALS_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Successfully added',
          errorMessage: null,
          isLoading: false,
        },
      };

    case types.CREATE_VASCULAR_NORMALS_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_NEW_VASCULAR_NORMALS:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    case types.FETCH_VASCULAR_NORMALS:
      return {
        ...state,
        vascularNormalsList: {
          vascularNormals: [],
          error: null,
          isLoading: true,
        },
      };

    case types.FETCH_VASCULAR_NORMALS_SUCCESS:
      return {
        ...state,
        vascularNormalsList: {
          vascularNormals: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.FETCH_VASCULAR_NORMALS_FAILURE:
      return {
        ...state,
        vascularNormalsList: {
          vascularNormals: [],
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_VASCULAR_NORMALS:
      return {
        ...state,
        vascularNormalsList: {
          vascularNormals: [],
          error: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
