import {
  FETCH_BIZBOX_PHYSICIAN,
  FETCH_BIZBOX_PHYSICIAN_FAILURE,
  FETCH_BIZBOX_PHYSICIAN_SUCCESS,
  FETCH_BIZBOX_PHYSICIANS,
  FETCH_BIZBOX_PHYSICIANS_FAILURE,
  FETCH_BIZBOX_PHYSICIANS_SUCCESS,
} from '../constants/bizboxPhysician.constant';

const initialState = {
  bizboxPhysicians: [],
  bizboxPhysician: null,
  error: null,
  isLoading: false,
};

export const bizboxPhysicianReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_BIZBOX_PHYSICIAN:
    case FETCH_BIZBOX_PHYSICIANS:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_BIZBOX_PHYSICIAN_FAILURE:
    case FETCH_BIZBOX_PHYSICIANS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.message,
      };

    case FETCH_BIZBOX_PHYSICIANS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        bizboxPhysicians: action.payload.data,
      };

    case FETCH_BIZBOX_PHYSICIAN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        bizboxPhysician: action.payload.data,
      };

    default:
      return state;
  }
};

/* 
const INITIAL_STATE = {
  bizboxPhysiciansList: {
    bizboxPhysicians: [],
    error: null,
    isLoading: false,
    limit: NaN,
    offset: NaN,
    end: NaN,
    page: 1,
    pages: NaN
  },
  selectedBizboxPhysician: {
    bizboxPhysician: null,
    error: null,
    isLoading: false
  }
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_BIZBOX_PHYSICIANS:
      return {
        ...state,
        bizboxPhysiciansList: {
          ...state.bizboxPhysiciansList,
          bizboxPhysicians: [],
          error: null,
          isLoading: true
        }
      };

    case FETCH_BIZBOX_PHYSICIANS_SUCCESS:
      return {
        ...state,
        bizboxPhysiciansList: {
          ...state.bizboxPhysiciansList,
          bizboxPhysicians: action.payload.data,
          error: null,
          isLoading: false,
          limit: action.payload.limit,
          offset: action.payload.offset,
          end: action.payload.end,
          pages: action.payload.pages
        }
      };
    case FETCH_BIZBOX_PHYSICIANS_FAILURE:
      return {
        ...state,
        bizboxPhysiciansList: {
          bizboxPhysicians: [],
          error: action.payload,
          isLoading: false
        }
      };
    case RESET_BIZBOX_PHYSICIANS:
      return {
        ...state,
        bizboxPhysiciansList: {
          bizboxPhysicians: [],
          error: null,
          isLoading: false,
          start: NaN,
          end: NaN,
          page: 1,
          pages: NaN
        }
      };

    case SELECT_BIZBOX_PHYSICIAN:
      return {
        ...state,
        selectedBizboxPhysician: {
          bizboxPhysician: null,
          error: null,
          isLoading: true
        }
      };
    case SELECT_BIZBOX_PHYSICIAN_SUCCESS:
      return {
        ...state,
        selectedBizboxPhysician: {
          bizboxPhysician: action.payload.data,
          error: null,
          isLoading: false
        }
      };
    case SELECT_BIZBOX_PHYSICIAN_FAILURE:
      return {
        ...state,
        selectedBizboxPhysician: {
          bizboxPhysician: null,
          error: action.payload,
          isLoading: false
        }
      };
    case RESET_SELECTED_BIZBOX_PHYSICIAN:
      return {
        ...state,
        selectedBizboxPhysician: {
          bizboxPhysician: null,
          error: null,
          isLoading: false
        }
      };

    case NEXT_PAGE_BIZBOX_PHYSICIAN:
      return {
        ...state,
        bizboxPhysiciansList: {
          ...state.bizboxPhysiciansList,
          page: state.bizboxPhysiciansList.page + 1,
          offset: state.bizboxPhysiciansList.offset + 10
        }
      };
    case PREV_PAGE_BIZBOX_PHYSICIAN:
      return {
        ...state,
        bizboxPhysiciansList: {
          ...state.bizboxPhysiciansList,
          page: state.bizboxPhysiciansList.page - 1,
          offset: state.bizboxPhysiciansList.offset - 10
        }
      };
    default:
      return state;
  }
}
 */
