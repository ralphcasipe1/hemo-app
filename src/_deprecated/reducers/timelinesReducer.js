import * as types from '../constants/TimelineConstants';
import { updateObject } from '../utils/reducerHelpers';

const initialState = {
  timelines: [],
  isLoading: false,
  error: null,
};
const timeline = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_TIMELINE:
      return updateObject(state, {
        isLoading: true,
      });
    case types.FETCH_TIMELINE_SUCCESS:
      return updateObject(state, {
        timelines: action.payload.data,
        isLoading: false,
      });

    case types.FLASH_ERROR:
      return updateObject(state, {
        error: action.payload.message,
      });
    default:
      return state;
  }
};

export default timeline;
