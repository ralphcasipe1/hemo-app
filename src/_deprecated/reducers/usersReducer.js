import * as types from '../constants/UserConstants';
import { updateObject } from '../utils/reducerHelpers';

const initialState = {
  visibilityFilter: 'SHOW_ALL',
  users: [],
  error: null,
  isLoading: false,
  nextPage: null,
  prevPage: null,
  message: null,
  type: null,
};

const users = (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_USER:
    case types.DELETE_USER:
    case types.UPDATE_USER:
    case types.FETCH_USERS:
      return updateObject(state, {
        isLoading: true,
      });

    case types.FETCH_USERS_SUCCESS:
      return updateObject(state, {
        users: action.payload.data,
        isLoading: false,
        message: 'Users successfully fetched',
      });

    case types.CREATE_USER_SUCCESS:
      return updateObject(state, {
        users: [action.payload.data, ...state.users],
        isLoading: false,
        message: 'User successfully added',
      });

    case types.DELETE_USER_SUCCESS:
      return updateObject(state, {
        users: state.users.filter(
          (user) => user.user_id !== action.payload.data.user_id
        ),
        isLoading: false,
        message: 'User successfully deleted',
      });

    case types.UPDATE_USER_SUCCESS:
      const {
        fname,
        mname,
        lname,
        id_number,
        password,
        user_id,
      } = action.payload.data;
      const newUsers = state.users.map((user) => {
        if (user.user_id !== user_id) {
          return user;
        }
        return updateObject(user, {
          fname,
          mname,
          lname,
          id_number,
          password,
        });
      });

      return updateObject(state, {
        users: newUsers,
        isLoading: false,
        message: 'User successfully updated',
      });

    case types.CREATE_USER_FAILURE:
    case types.DELETE_USER_FAILURE:
    case types.UPDATE_USER_FAILURE:
    case types.FETCH_USERS_FAILURE:
      return updateObject(state, {
        error: action.payload,
        isLoading: false,
      });

    case types.RESET_USER_MESSAGE:
      return updateObject(state, { message: null });

    default:
      return state;
  }
};

export default users;
