import * as types from '../constants/PostSubjectiveRemarkConstants';

const initialState = {
  selectedPostSubjectiveRemark: {
    postSubjectiveRemark: null,
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.GET_POST_SUBJECTIVE_REMARK:
      return {
        ...state,
        selectedPostSubjectiveRemark: {
          postSubjectiveRemark: null,
          error: null,
          isLoading: true,
        },
      };

    case types.GET_POST_SUBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        selectedPostSubjectiveRemark: {
          postSubjectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.GET_POST_SUBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        selectedPostSubjectiveRemark: {
          postSubjectiveRemark: null,
          error: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_GET_POST_SUBJECTIVE_REMARK:
      return {
        ...state,
        selectedPostSubjectiveRemark: {
          postSubjectiveRemark: null,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_POST_SUBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };

    case types.CREATE_POST_SUBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Post subjective remark successfully added',
          errorMessage: null,
          isLoading: false,
        },
        selectedPostSubjectiveRemark: {
          postSubjectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_POST_SUBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_NEW_POST_SUBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
