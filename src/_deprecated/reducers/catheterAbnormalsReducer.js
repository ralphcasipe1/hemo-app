import * as types from '../constants/AbnormalAssessmentConstants';

const INITIAL_STATE = {
  catheterAbnormalsList: {
    catheterAbnormals: [],
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_CATHETER_ABNORMALS:
      return {
        ...state,
        catheterAbnormalsList: {
          catheterAbnormals: [],
          error: null,
          isLoading: true,
        },
      };
    case types.FETCH_CATHETER_ABNORMALS_SUCCESS:
      return {
        ...state,
        catheterAbnormalsList: {
          catheterAbnormals: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.FETCH_CATHETER_ABNORMALS_FAILURE:
      return {
        ...state,
        catheterAbnormalsList: {
          catheterAbnormals: [],
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_CATHETER_ABNORMALS:
      return {
        ...state,
        catheterAbnormalsList: {
          catheterAbnormals: [],
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_CATHETER_ABNORMAL:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };
    case types.CREATE_CATHETER_ABNORMAL_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Abnormal assessment added successfully',
          errorMessage: null,
          isLoading: false,
        },

        catheterAbnormalsList: {
          catheterAbnormals: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_CATHETER_ABNORMAL_FAILURE:
      return {
        ...state,
        newPatientObj: {
          successMessage: null,
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_NEW_CATHETER_ABNORMAL:
      return {
        ...state,
        newPatientObj: {
          successMessage: null,
          error: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
