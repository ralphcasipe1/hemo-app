import * as types from '../constants/TaskConstants';
import { updateObject } from '../utils/reducerHelpers';

const initialState = {
  tasks: [],
  completedTasks: [],
};

const tasks = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_TASKS:
      return updateObject(state, {
        tasks: action.payload.data.filter(
          (task) => task.task_status !== 'completed'
        ),
        completedTasks: action.payload.data.filter(
          (task) => task.task_status !== 'pending'
        ),
      });

    case types.COMPLETE_TASK:
      return updateObject(state, {
        tasks: state.tasks.filter(
          (task) => task.task_id !== action.payload.data.task_id
        ),
        completedTasks: [...state.completedTasks, action.payload.data],
      });
    default:
      return state;
  }
};

export default tasks;
