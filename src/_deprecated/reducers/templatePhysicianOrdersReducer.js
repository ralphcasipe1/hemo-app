import * as types from '../constants/TemplatePhysicianOrderContants';

const INITIAL_STATE = {
  templatePhysicianOrdersList: {
    templatePhysicianOrders: null,
    error: null,
    isLoading: false,
  },
  selectedTemplatePhysicianOrder: {
    patientTemplateParameter: null,
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
  edition: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
  deletion: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case types.CREATE_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };
    case types.CREATE_TEMPLATE_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Standing Order Physician Order added successfully',
          errorMessage: null,
          isLoading: false,
        },
        templatePhysicianOrdersList: {
          templatePhysicianOrders: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.CREATE_TEMPLATE_PHYSICIAN_ORDER_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload.message,
          isLoading: false,
        },
      };
    case types.RESET_NEW_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    case types.FETCH_TEMPLATE_PHYSICIAN_ORDERS:
      return {
        ...state,
        templatePhysicianOrdersList: {
          templatePhysicianOrders: null,
          error: null,
          isLoading: true,
        },
      };
    case types.FETCH_TEMPLATE_PHYSICIAN_ORDERS_SUCCESS:
      return {
        ...state,
        templatePhysicianOrdersList: {
          templatePhysicianOrders: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.FETCH_TEMPLATE_PHYSICIAN_ORDERS_FAILURE:
      return {
        ...state,
        templatePhysicianOrdersList: {
          templatePhysicianOrders: null,
          error: action.paylaod,
          isLoading: false,
        },
      };
    case types.RESET_TEMPLATE_PHYSICIAN_ORDERS:
      return {
        ...state,
        templatePhysicianOrdersList: {
          templatePhysicianOrders: null,
          error: null,
          isLoading: false,
        },
      };

    case types.SELECT_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        selectedTemplatePhysicianOrder: {
          patientTemplateParameter: null,
          error: null,
          isLoading: true,
        },
      };
    case types.SELECT_TEMPLATE_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state,
        selectedTemplatePhysicianOrder: {
          patientTemplateParameter: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.SELECT_TEMPLATE_PHYSICIAN_ORDER_FAILURE:
      return {
        ...state,
        selectedTemplatePhysicianOrder: {
          patientTemplateParameter: null,
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_SELECTED_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        selectedTemplatePhysicianOrder: {
          patientTemplateParameter: null,
          error: null,
          isLoading: false,
        },
      };

    case types.UPDATE_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        edition: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };
    case types.UPDATE_TEMPLATE_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state,
        edition: {
          successMessage: 'Standing order physician order successfully updated',
          errorMessage: null,
          isLoading: false,
        },
        templatePhysicianOrdersList: {
          templatePhysicianOrders: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.UPDATE_TEMPLATE_PHYSICIAN_ORDER_FAILURE:
      return {
        ...state,
        edition: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_UPDATED_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        edition: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    case types.DELETE_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        deletion: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };

    case types.DELETE_TEMPLATE_PHYSICIAN_ORDER_SUCCESS:
      return {
        ...state,
        deletion: {
          successMessage: 'Standing order physician order sucessfully deleted',
          errorMessage: null,
          isLoading: false,
        },
        templatePhysicianOrdersList: {
          templatePhysicianOrders: null,
          error: null,
          isLoading: false,
        },
      };
    case types.DELETE_TEMPLATE_PHYSICIAN_ORDER_FAILURE:
      return {
        ...state,
        deletion: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_DELETED_TEMPLATE_PHYSICIAN_ORDER:
      return {
        ...state,
        deletion: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
