import * as types from '../constants/ObjectiveRemarkConstants';

const initialState = {
  selectedObjectiveRemark: {
    objectiveRemark: null,
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.GET_OBJECTIVE_REMARK:
      return {
        ...state,
        selectedObjectiveRemark: {
          objectiveRemark: null,
          error: null,
          isLoading: true,
        },
      };

    case types.GET_OBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        selectedObjectiveRemark: {
          objectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.GET_OBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        selectedObjectiveRemark: {
          objectiveRemark: null,
          error: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_GET_OBJECTIVE_REMARK:
      return {
        ...state,
        selectedObjectiveRemark: {
          objectiveRemark: null,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_OBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };

    case types.CREATE_OBJECTIVE_REMARK_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Objective remark successfully added',
          errorMessage: null,
          isLoading: false,
        },
        selectedObjectiveRemark: {
          objectiveRemark: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_OBJECTIVE_REMARK_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };

    case types.RESET_NEW_OBJECTIVE_REMARK:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
