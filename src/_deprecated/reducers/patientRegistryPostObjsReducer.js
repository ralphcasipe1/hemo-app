import * as types from '../constants/PostObjectiveAssessmentConstants';

const INITIAL_STATE = {
  patientRegistryPostObjsList: {
    patientRegistryPostObjs: [],
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
  edition: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
  deletion: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_PATIENT_REGISTRY_POST_OBJS:
      return {
        ...state,
        patientRegistryPostObjsList: {
          patientRegistryPostObjs: [],
          error: null,
          isLoading: true,
        },
      };
    case types.FETCH_PATIENT_REGISTRY_POST_OBJS_SUCCESS:
      return {
        ...state,
        patientRegistryPostObjsList: {
          patientRegistryPostObjs: action.payload.data,
          error: null,
          isLoading: false,
        },
      };
    case types.FETCH_PATIENT_REGISTRY_POST_OBJS_FAILURE:
      return {
        ...state,
        patientRegistryPostObjsList: {
          patientRegistryPostObjs: [],
          error: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_PATIENT_REGISTRY_POST_OBJS:
      return {
        ...state,
        patientRegistryPostObjsList: {
          patientRegistryPostObjs: [],
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_PATIENT_REGISTRY_POST_OBJ:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: true,
        },
      };
    case types.CREATE_PATIENT_REGISTRY_POST_OBJ_SUCCESS:
      return {
        ...state,
        creation: {
          successMessage: 'Post objective assessment added successfully',
          errorMessage: null,
          isLoading: false,
        },
        patientRegistryPostObjsList: {
          patientRegistryPostObjs: action.payload.data,
          error: null,
          isLoading: false,
        },
      };

    case types.CREATE_PATIENT_REGISTRY_POST_OBJ_FAILURE:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: action.payload,
          isLoading: false,
        },
      };
    case types.RESET_NEW_PATIENT_REGISTRY_POST_OBJ:
      return {
        ...state,
        creation: {
          successMessage: null,
          errorMessage: null,
          isLoading: false,
        },
      };

    default:
      return state;
  }
}
