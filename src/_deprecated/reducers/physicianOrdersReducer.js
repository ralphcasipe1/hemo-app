import * as types from '../constants/PhysicianOrderConstants';
import { updateObject } from '../utils/reducerHelpers';

/**
 * @note: PhysicianOrders -> PhyOrders
 */
const INITIAL_STATE = {
  patientRegistryPhyOrdersList: {
    patientRegistryPhyOrders: [],
    error: null,
    isLoading: false,
  },
  creation: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
  edition: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
  deletion: {
    successMessage: null,
    errorMessage: null,
    isLoading: false,
  },
};

const initialState = {
  physicianOrders: [],
  physicianOrder: null,
  error: null,
  isLoading: false,
  message: null,
};

const physicianOrder = (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_PATIENT_REGISTRY_PHY_ORDER:
    case types.FETCH_PATIENT_REGISTRY_PHY_ORDERS:
    case types.DELETE_PATIENT_REGISTRY_PHY_ORDER:
    case types.UPDATE_PATIENT_REGISTRY_PHY_ORDER:
      return updateObject(state, {
        isLoading: true,
      });

    case types.FETCH_PATIENT_REGISTRY_PHY_ORDERS_FAILURE:
      return updateObject(state, {
        error: action.payload,
        isLoading: false,
      });
    case types.FETCH_PATIENT_REGISTRY_PHY_ORDERS_SUCCESS:
      return updateObject(state, {
        physicianOrders: action.payload.data,
        isLoading: false,
      });
    case types.CREATE_PATIENT_REGISTRY_PHY_ORDER_SUCCESS:
      return updateObject(state, {
        physicianOrders: [...state.physicianOrders, action.payload.data],
        isLoading: false,
      });

    case types.UPDATE_PATIENT_REGISTRY_PHY_ORDER_SUCCESS:
      const {
        physician_order_id,
        assessment,
        medication,
        procedure,
        diagnostic_test,
        other_remark,
        physician_order_status,
        updated_at,
      } = action.payload.data;

      const newPhysicianOrders = state.physicianOrders.map((po) => {
        if (po.physician_order_id !== physician_order_id) {
          return po;
        }

        return updateObject(po, {
          assessment,
          medication,
          procedure,
          diagnostic_test,
          other_remark,
          physician_order_status,
          updated_at,
        });
      });

      return updateObject(state, {
        physicianOrders: newPhysicianOrders,
        isLoading: false,
      });

    case types.DELETE_PATIENT_REGISTRY_PHY_ORDER_SUCCESS:
      return updateObject(state, {
        physicianOrders: state.physicianOrders.filter(
          (po) =>
            po.physician_order_id !== action.payload.data.physician_order_id
        ),
        isLoading: false,
        message: action.payload,
      });
    default:
      return state;
  }
};

export default physicianOrder;
