export const FETCH_TASKS = 'FETCH_TASKS';
export const FLASH_ERROR = 'FLASH_ERROR';
export const CREATE_TASK = 'CREATE_TASK';
export const COMPLETE_TASK = 'COMPLETE_TASK';
