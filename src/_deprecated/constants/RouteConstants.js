export const APP = '/';
export const LOGIN = '/login';
export const PROFILE = 'profile';
export const PATIENT_CREATION = 'patients/create';
export const CHANGELOGS = 'it/changelogs';
export const HELP = 'it/help';
export const PATIENT = 'patients/:patientId';
export const REGISTRY_CREATION = 'registries/create';
export const REGISTRY_LIST = 'registries/list';
export const SO_ANTICOAGULANT = 'template_anti_coagulants';
export const SO_DIALYZER = 'template_dialyzers';
export const SO_PARAMETER = 'template_parameters';
export const SO_WEIGHT = 'template_weights';
export const SO_PHYSICIAN_ORDER = 'template_physician_orders';
export const SO_MEDICATION = 'template_medications';
export const SUMMARY = 'summaries';
export const PATIENT_PREPARATION =
  'patients/:patientId/patient_registries/:patientRegistryId';
export const ANTI_COAGULANTS = 'anti_coagulants';
export const DIALYZERS = 'dialyzers';
export const PARAMETERS = 'parameters';
export const VACCINES = 'vaccines';
export const WEIGHTS = 'weights';
export const NURSE_NOTES = 'nurse_notes';
export const VASCULARS = 'vasculars';
export const CATHETERS = 'catheters';
export const VITAL_SIGNS = 'vital_signs';
export const MEDICATIONS = 'medications';
export const PHYSICIAN_ORDERS = 'physician_orders';
export const ADMIN = 'admin';
export const ABNORMALS = 'abnormals';
export const DIALYZER_BRANDS = 'dialyzer_brands';
export const DIALYZER_SIZES = 'dialyzer_sizes';
export const MOBILITIES = 'mobilities';
export const OBJECTIVES = 'objectives';
export const SUBJECTIVES = 'subjectives';
export const USERS = 'users';
export const PHYSICIANS = 'physicians';
export const INITIAL_ROUTE = {
  keys: {},
  options: {},
  path: '',
};
