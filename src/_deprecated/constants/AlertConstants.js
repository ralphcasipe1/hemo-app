export const SUCCESS_ALERT = 'Success';

export const ERROR_ALERT = 'Failed';

export const CLEAR_ALERT = 'Cleared';
