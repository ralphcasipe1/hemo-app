import { connect } from 'react-redux'
import {
  editPatientRegistryMedication,
  selectPatientRegistryMedication,
  resetEditedPatientRegistryMedication,
  resetSelectedPatientRegistryMedication
} from '../actions/medications'
import MedicationForm from 'MedicationForm'
import React, { Component } from 'react'

class MedicationEdit extends Component {
  constructor() {
    super()

    this.handleCancel = this.handleCancel.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  componentDidMount() {
    this.props.selectPatientRegistryMedication(
      this.props.patientRegistryId,
      this.props.medicationId
    )
  }

  componentWillUnmount() {
    this.props.resetEditedPatientRegistryMedication()
    this.props.resetSelectedPatientRegistryMedication()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.edition.successMessage &&
      nextProps.edition.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.patientId}/patient_registries/${
          this.props.patientRegistryId
        }/medications`
      )
    }
  }

  handleCancel() {
    this.props.router.push(
      `/patients/${this.props.patientId}/patient_registries/${
        this.props.patientRegistryId
      }/medications`
    )
  }

  handleFormSubmit(values) {
    this.props.editPatientRegistryMedication(
      this.props.patientRegistryId,
      this.props.medicationId,
      values
    )
  }

  render() {
    const {
      selectedPatientRegistryMedication: {
        error,
        isLoading,
        patientRegistryMedication
      }
    } = this.props

    if (isLoading) {
      return <span>Loading</span>
    } else if (error) {
      return <span>{error.message}</span>
    } else if (!patientRegistryMedication) {
      return <span />
    }

    return (
      <MedicationForm
        form={`EditMedicationForm_${this.props.params.medicationId}`}
        expanded
        onCancel={this.handleCancel}
        onFormSubmit={this.handleFormSubmit}
        initialValues={{
          generic: patientRegistryMedication.generic
          , brand: patientRegistryMedication.brand
          , preparation: patientRegistryMedication.preparation
          , dosage: patientRegistryMedication.dosage
          , route: patientRegistryMedication.route
        }}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId
  , patientRegistryId: ownProps.params.patientRegistryId
  , medicationId: ownProps.params.medicationId
  , selectedPatientRegistryMedication:
    state.medications.selectedPatientRegistryMedication
  , edition: state.medications.edition
})

export default connect(
  mapStateToProps,
  {
    editPatientRegistryMedication
    , selectPatientRegistryMedication
    , resetEditedPatientRegistryMedication
    , resetSelectedPatientRegistryMedication
  }
)(MedicationEdit)
