import React, { Fragment, Component } from 'react'
import type { Node }                  from 'react'
import { withRouter }                 from 'react-router-dom'
import { connect }                    from 'react-redux'
import { Header, Menu, Modal }        from 'semantic-ui-react'
import { logout }                     from '../app/domains/account/authentication/actions/log-out'
import UserAvatar                     from '../components/UserAvatar'
import MobileSidebar                  from '../components/MobileSidebar'

type AuthenticationType = {
  isAuthenticated: boolean,
  user: Object
};

type PropType = {
  children: Node,
  authentication: AuthenticationType
};

type StateType = {
  width: string,
  height: string,
  visible: boolean
};

class App extends Component<PropType, StateType> {
  state = {
    width: '0'
    , height: '0'
    , visible: false
  };

  componentDidMount() {
    if (!this.props.authentication.isAuthenticated) {
      this.props.history.push('/login')
    }
  }

  componentDidUpdate(prevProps: PropType) {
    if (prevProps.authentication.user !== this.props.authentication.user) {
      this.props.history.push('/login')
    }
  }

  toggleSidebar = () => {
    this.setState({ visible: !this.state.visible })
  };

  handleLogout = () => {
    this.props.logout()
    this.setState({ visible: false })
  };

  render(): Node {
    const { children } = this.props
    const { isAuthenticated, user } = this.props.authentication

    const { visible } = this.state

    return (
      <Fragment>
        <Modal
          dimmer="blurring"
          onClose={this.toggleSidebar}
          open={visible}
          closeIcon
          size="fullscreen"
        >
          <Header content="Navigations" />
          <Modal.Content>
            <MobileSidebar
              close={this.toggleSidebar}
              fixed="left"
              isAuthenticated={isAuthenticated}
              isAdmin={user.is_admin}
              onLogout={this.handleLogout}
            />
          </Modal.Content>
        </Modal>

        <Menu borderless className="top attached" color="grey" inverted>
          <Menu.Item icon="content" link onClick={this.toggleSidebar} />
          <Menu.Item>HEMO</Menu.Item>

          <Menu.Menu position="right">
            <Menu.Item>
              <UserAvatar
                user={`${user.first_name} ${user.last_name}`}
                onLogout={this.handleLogout}
              />
            </Menu.Item>
          </Menu.Menu>
        </Menu>

        <div className="full-height">{children}</div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  authentication: state.authenticationReducer
})

export default withRouter(
  connect(
    mapStateToProps,
    {
      logout
    }
  )(App)
)
