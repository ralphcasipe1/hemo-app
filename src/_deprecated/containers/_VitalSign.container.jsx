import { connect } from 'react-redux'
import { VitalSign } from '../components/VitalSign'
import { createVitalSign, fetchAllVitalSigns } from '../actions/vitalSigns'

const mapStateToProps = (state, props) => ({
  patientRegistryId: props.params.patientRegistryId
  , vitalSigns: state.vitalSignReducer.vitalSigns
  , isLoading: state.vitalSignReducer.isLoading
})

export const VitalSignContainer = connect(
  mapStateToProps,
  {
    createVitalSign
    , fetchAllVitalSigns
  }
)(VitalSign)
/* import { connect } from 'react-redux';
import { Button, Divider, Header, Icon, Table } from 'semantic-ui-react';
import {
  createVitalSign,
  fetchVitalSigns,
} from '../actions/vitalSigns';
import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import VitalSignsTable from 'VitalSignsTable';

class VitalSignContainer extends Component {
  constructor() {
    super();

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }

  componentDidMount() {
    this.props.fetchPatientRegistryVitalSigns(
      this.props.params.patientRegistryId
    );
  }

  render() {
    const {
      children,
      patientRegistryVitalSignsList: { error, isLoading, vitalSigns }
    } = this.props;

    let disableByDate;
    if (!this.props.selectedPatientRegister.patientRegister) {
      disableByDate = false;
    } else {
      disableByDate =
        moment(
          this.props.selectedPatientRegister.patientRegister.created_at
        ).format('ll') !== moment().format('ll');
    }

    return (
      <div>
        <Header content="Vital Signs" />

        <Button
          color="blue"
          content="Add Vital Signs"
          icon="plus"
          disabled={disableByDate}
          onClick={() =>
            this.props.router.push(
              `/patients/${this.props.params.patientId}/patient_registries/${
                this.props.params.patientRegistryId
              }/vital_signs/create`
            )
          }
        />

        <Divider />

        <VitalSignsTable
          error={error}
          isLoading={isLoading}
          vitalSigns={vitalSigns}
          onDeleteClick={this.handleDeleteClick}
          isDisabled={disableByDate}
        />

        {children}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  patientRegistryVitalSignsList: state.vitalSigns.patientRegistryVitalSignsList,
  selectedPatientRegister: state.patientRegisters.selectedPatientRegister,
  deletion: state.vitalSigns.deletion
});

export default connect(mapStateToProps, {
  createVitalSign,
  fetchVitalSigns,
})(VitalSignContainer);
 */
