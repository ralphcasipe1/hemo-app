import { connect } from 'react-redux'
import { fetchDialyzerBrands } from '../actions/dialyzerBrands'
import { fetchDialyzerSizes } from '../actions/dialyzerSizes'
import {
  createStandingOrderDialyzer,
  fetchAllStandingOrderDialyzers,
  fetchStandingOrderDialyzer,
  updateStandingOrderDialyzer,
  deleteStandingOrderDialyzer
} from '../actions/standing-order-dialyzer'
import { SODialyzer } from '../components/SODialyzer'

const mapStateToProps = (state, props) => {
  const {
    error,
    isLoading,
    standingOrderDialyzer,
    standingOrderDialyzers
  } = state.standingOrderDialyzerReducer

  return {
    patientId: props.match.params.patientId
    , error
    , isLoading
    , standingOrderDialyzer
    , standingOrderDialyzers
    , dialyzerBrands: state.dialyzerBrands.dialyzerBrandsList.dialyzerBrands
    , dialyzerSizes: state.dialyzerSizes.dialyzerSizes
  }
}

export const SODialyzerContainer = connect(
  mapStateToProps,
  {
    createStandingOrderDialyzer
    , fetchAllStandingOrderDialyzers
    , fetchStandingOrderDialyzer
    , updateStandingOrderDialyzer
    , deleteStandingOrderDialyzer

    , fetchDialyzerBrands

    , fetchDialyzerSizes
  }
)(SODialyzer)

/* class SODialyzerContainer extends Component {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleCancel() {
    this.props.router.push(`/patients/${this.props.patientId}`);
  }

  handleDelete(patientId, templateDialyzerId) {
    this.props.deletePatientTemplateDialyzer(patientId, templateDialyzerId);
  }

  handleEditSubmit(patientId, templateDialyzerId, values) {
    this.props.editPatientTemplateDialyzer(
      patientId,
      templateDialyzerId,
      values
    );
  }

  handleFormSubmit(values) {
    this.props.createPatientTemplateDialyzer(this.props.patientId, values);
  }

  componentDidMount() {
    this.props.fetchDialyzerBrands();
    this.props.fetchDialyzerSizes();
    this.props.fetchPatientTemplateDialyzers(this.props.patientId);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => (
          <Tab.Pane loading={this.props.patientTemplateDialyzersList.isLoading}>
            {this.props.patientTemplateDialyzersList
              .patientTemplateDialyzers ? (
              <SODialyzerDetail
                brands={this.props.dialyzerBrandsList.dialyzerBrands}
                error={this.props.patientTemplateDialyzersList.error}
                isLoading={this.props.patientTemplateDialyzersList.isLoading}
                templateDialyzer={
                  this.props.patientTemplateDialyzersList
                    .patientTemplateDialyzers
                }
                sizes={this.props.dialyzerSizesList.dialyzerSizes}
                onDelete={this.handleDelete}
                onEditSubmit={this.handleEditSubmit}
              />
            ) : (
              <SODialyzerForm
                brands={this.props.dialyzerBrandsList.dialyzerBrands}
                form="TemplateDialyzerForm"
                onCancel={this.handleCancel}
                onFormSubmit={this.handleFormSubmit}
                sizes={this.props.dialyzerSizesList.dialyzerSizes}
              />
            )}
          </Tab.Pane>
        )
      },
      {
        menuItem: 'Logs',
        render: () => (
          <Tab.Pane>
            <Header
              icon="wrench"
              content="Under const."
              subheader="Sorry for inconvenience"
            />
          </Tab.Pane>
        )
      }
    ];

    return (
      <div>
        <Header content="Standing Order Dialyzers" />
        <Tab panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  dialyzerBrandsList: state.dialyzerBrands.dialyzerBrandsList,
  dialyzerSizesList: state.dialyzerSizes,
  patientTemplateDialyzersList:
    state.patientTemplateDialyzers.patientTemplateDialyzersList
});

export default connect(mapStateToProps, {
  fetchDialyzerBrands,
  fetchDialyzerSizes,
  createPatientTemplateDialyzer,
  deletePatientTemplateDialyzer,
  editPatientTemplateDialyzer,
  fetchPatientTemplateDialyzers
})(SODialyzerContainer);
 */
