import { connect } from 'react-redux';
import {
  createPatientRegistryPhyOrder,
  deletePatientRegistryPhyOrder,
  editPatientRegistryPhyOrder,
  fetchPatientRegistryPhyOrders,
} from '../actions/physicianOrders';
import { fetchPhysicians } from '../actions/physicians';
import {
  getLoading,
  getPhysicianOrders,
} from '../selectors/PhysicianOrderSelectors';
import PhysicianOrders from '../components/PhysicianOrders';
import React from 'react';

const PhysicianOrderContainer = (props) => <PhysicianOrders {...props} />;

const mapStateToProps = (state) => ({
  physicianOrders: getPhysicianOrders(state),
  isLoading: getLoading(state),
  physicians: state.physicians.physiciansList.physicians,
});

export default connect(
  mapStateToProps,
  {
    createPatientRegistryPhyOrder,
    deletePatientRegistryPhyOrder,
    editPatientRegistryPhyOrder,
    fetchPatientRegistryPhyOrders,
    fetchPhysicians,
  }
)(PhysicianOrderContainer);
