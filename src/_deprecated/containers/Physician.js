import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import BizboxPhysiciansList from './BizboxPhysiciansList';
import PhysiciansList from './PhysiciansList';
import React, { Component } from 'react';

class Physician extends Component {
  render() {
    const panes = [
      {
        menuItem: 'Local',
        render: () => (
          <Tab.Pane>
            <PhysiciansList />
          </Tab.Pane>
        ),
      },
      {
        menuItem: 'Bizbox',
        render: () => (
          <Tab.Pane>
            <BizboxPhysiciansList />
          </Tab.Pane>
        ),
      },
    ];
    return (
      <div>
        <Header content="Physicians" />
        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

export default connect(
  null,
  {}
)(Physician);
