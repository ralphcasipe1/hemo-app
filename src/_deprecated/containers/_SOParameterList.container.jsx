import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import SOParameterList from '../components/SOParameterList'
import { fetchAllSOParameters } from '../actions/sOParameter.action'

class SOParameterListContainer extends Component {
  componentDidMount() {
    this.props.fetchAllSOParameters(this.props.params.patientId)
  }

  render() {
    const { sOParameters } = this.props

    if (sOParameters.length === 0) {
      return <div>No standing order for parameter</div>
    }

    return <SOParameterList sOParameters={sOParameters} />
  }
}

const mapStateToProps = state => ({
  sOParameters: state.sOParameters.sOParameters
})

export default withRouter(
  connect(
    mapStateToProps,
    {
      fetchAllSOParameters
    }
  )(SOParameterListContainer)
)
