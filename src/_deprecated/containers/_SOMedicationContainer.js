import { connect } from 'react-redux'
import {
  createStandingOrderMedication,
  deleteStandingOrderMedication,
  fetchAllStandingOrderMedication,
  fetchStandingOrderMedication,
  updateStandingOrderMedication
} from '../actions/standing-order-medication'
import { SOMedication } from '../components/SOMedication'

const mapStateToProps = (state, props) => {
  const {
    error,
    isLoading,
    standingOrderMedication,
    standingOrderMedications
  } = state.standingOrderMedicationReducer
  return {
    patientId: props.params.patientId
    , error
    , isLoading
    , standingOrderMedication
    , standingOrderMedications
  }
}

export const SOMedicationContainer = connect(
  mapStateToProps,
  {
    createStandingOrderMedication
    , deleteStandingOrderMedication
    , fetchAllStandingOrderMedication
    , fetchStandingOrderMedication
    , updateStandingOrderMedication
  }
)(SOMedication)

/* import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import React from 'react';
import {
  createTemplateMedication,
  deleteTemplateMedication,
  editTemplateMedication,
  fetchTemplateMedications,
  selectTemplateMedication,
  resetDeletedTemplateMedication,
  resetEditedTemplateMedication,
  resetNewTemplateMedication,
  resetTemplateMedications
} from '../actions/templateMedications';
import {
  getLoading,
  getTemplateMedications,
  getTemplateMedication
} from '../selectors/TemplateMedicationSelectors';
import _ from 'lodash';
import SOMedications from '../components/SOMedications';

const SOMedicationContainer = props => <SOMedications {...props} />;

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  templateMedications: getTemplateMedications(state),
  templateMedication: getTemplateMedication(state),
  isLoading: getLoading(state)
});

export default connect(mapStateToProps, {
  fetchTemplateMedications,
  createTemplateMedication,
  editTemplateMedication,
  deleteTemplateMedication,
  selectTemplateMedication,
  resetTemplateMedications
})(SOMedicationContainer); */
