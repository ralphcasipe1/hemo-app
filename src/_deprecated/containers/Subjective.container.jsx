import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import _ from 'lodash';
import {
  deleteSubjective,
  editSubjective,
  createSubjective,
  fetchSubjectives,
  resetDeletedSubjective,
  resetEditSubjective,
  resetNewSubjective,
  resetSubjectives,
} from '../actions/subjectives';
import SubjectiveForm from 'SubjectiveForm';
import SubjectivesList from 'SubjectivesList';

class SubjectiveContainer extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchSubjectives();
  }

  componentWillUnmount() {
    this.props.resetDeletedSubjective();
    this.props.resetEditSubjective();
    this.props.resetSubjectives();
    this.props.resetNewSubjective();
  }

  handleCancel() {
    this.props.router.push('/admin');
  }

  handleDeleteClick(id) {
    this.props.deleteSubjective(id);
  }

  handleEditClick(id, values) {
    this.props.editSubjective(id, values);
  }

  handleSubmit(values) {
    this.props.createSubjective(values);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane>
              <SubjectiveForm
                onFormSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
                form="SubjectiveForm"
              />
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'List',
        render: () => {
          return (
            <Tab.Pane>
              <SubjectivesList
                error={this.props.error}
                isLoading={this.props.isLoading}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
                subjectives={this.props.subjectives}
              />
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div>
        <Header content="Subjectives" />

        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  subjectives: state.subjectives.subjectives,
  error: state.subjectives.error,
  isLoading: state.subjectives.isLoading,
});

export default connect(
  mapStateToProps,
  {
    deleteSubjective,
    editSubjective,
    createSubjective,
    fetchSubjectives,
    resetDeletedSubjective,
    resetEditSubjective,
    resetNewSubjective,
    resetSubjectives,
  }
)(SubjectiveContainer);
