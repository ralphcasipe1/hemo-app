import { connect } from 'react-redux'
import { Button, Divider, Header } from 'semantic-ui-react'
import { fetchAbnormals, resetAbnormals } from '../actions/abnormals'
import {
  deletePatientRegistryCatheter,
  editPatientRegistryCatheter,
  fetchPatientRegistryCatheter,
  resetDeletePatientRegistryCatheter,
  resetEditedPatientRegistryCatheter,
  resetPatientRegistryCatheter
} from '../actions/catheters'
import { setCatheterAbnormal } from '../actions/catheterAbnormals'
import { Link } from 'react-router'
import CatheterTable from '../components/CatheterTable'

import PropTypes from 'prop-types'
import React from 'react'

class Catheter extends React.Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(props) {
    super(props)

    this.state = {
      readOnly: true
    }

    this.handleDeleteClick = this.handleDeleteClick.bind(this)
    this.handleEditClick = this.handleEditClick.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleCheckChange = this.handleCheckChange.bind(this)
  }

  handleEditClick() {
    this.setState({
      readOnly: !this.state.readOnly
    })

    this.props.fetchPatientRegistryCatheter(
      this.props.params.patientRegistryId
    )
  }

  handleDeleteClick() {
    const {
      params: { patientRegistryId },
      getPatientRegistryCatheter: { patientRegistryCatheter }
    } = this.props

    this.props.deletePatientRegistryCatheter(
      patientRegistryId,
      patientRegistryCatheter.catheter_id
    )
  }

  handleInputChange({ target: { name, value } }) {
    const {
      params: { patientRegistryId },
      getPatientRegistryCatheter: { patientRegistryCatheter }
    } = this.props

    editPatientRegistryCatheter(
      patientRegistryId,
      patientRegistryCatheter.catheter_id,
      {
        [name]: value
      }
    )
  }

  handleCheckChange({ name, value, checked }) {
    const {
      params: { patientRegistryId },
      getPatientRegistryCatheter: { patientRegistryCatheter }
    } = this.props

    let flows = [
      'is_red_port_inflow'
      , 'is_red_port_outflow'
      , 'is_blue_port_inflow'
      , 'is_blue_port_outflow'
    ]

    if (name !== 'abnormal_id') {
      editPatientRegistryCatheter(
        patientRegistryId,
        patientRegistryCatheter.catheter_id,
        {
          [name]: flows.indexOf(name) !== -1 ? checked : value
        }
      )
    } else {
      setCatheterAbnormal(patientRegistryCatheter.catheter_id, {
        [name]: value
      })
    }
  }

  componentDidMount() {
    this.props.fetchPatientRegistryCatheter(
      this.props.params.patientRegistryId
    )
    this.props.fetchAbnormals()
  }

  componentWillUnmount() {
    this.props.resetDeletePatientRegistryCatheter()
    this.props.resetEditedPatientRegistryCatheter()
    this.props.resetPatientRegistryCatheter()
    this.props.resetAbnormals()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.creation.successMessage &&
      nextProps.creation.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/catheters`
      )
    }
  }

  render() {
    const {
      children,
      getPatientRegistryCatheter: { error, isLoading, patientRegistryCatheter },
      abnormalsList: { abnormals }
    } = this.props

    const { readOnly } = this.state

    return (
      <div>
        <Header content="Catheter" />

        {!patientRegistryCatheter ? (
          <Link
            to={`/patients/${this.props.params.patientId}/patient_registries/${
              this.props.params.patientRegistryId
            }/catheters/create`}
          >
            <Button color="blue" content="Add Catheter" icon="plus" />
          </Link>
        ) : (
          ''
        )}

        <Divider />

        <CatheterTable
          abnormals={abnormals}
          error={error}
          isLoading={isLoading}
          onDeleteClick={this.handleDeleteClick}
          onEditClick={this.handleEditClick}
          onInputChange={this.handleInputChange}
          onCheckChange={this.handleCheckChange}
          patientRegistryCatheter={patientRegistryCatheter}
          readOnly={readOnly}
        />

        {children}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  abnormalsList: state.abnormals.abnormalsList
  , getPatientRegistryCatheter: state.catheters.getPatientRegistryCatheter
  , creation: state.catheters.creation
})

export default connect(
  mapStateToProps,
  {
    fetchAbnormals
    , resetAbnormals
    , deletePatientRegistryCatheter
    , editPatientRegistryCatheter
    , fetchPatientRegistryCatheter
    , resetDeletePatientRegistryCatheter
    , resetEditedPatientRegistryCatheter
    , resetPatientRegistryCatheter
    , setCatheterAbnormal
  }
)(Catheter)
