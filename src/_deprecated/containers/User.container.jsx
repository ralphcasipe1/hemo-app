import { connect } from 'react-redux';
import {
  createUser,
  deleteUser,
  editUser,
  fetchUsers,
  selectUser,
  resetUserMessage,
} from '../actions/users';
import { Header, Tab } from 'semantic-ui-react';
import React, { Component } from 'react';
import UserForm from 'UserForm';
import UsersList from 'UsersList';

class User extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  onInit = (props) => props.fetchUsers();

  componentDidMount() {
    this.onInit(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.users.message && nextProps.users.message) {
      setTimeout(() => this.props.resetUserMessage(), 3000);
    }
  }

  handleCancel() {
    this.props.router.goBack();
  }

  handleDeleteClick(id) {
    this.props.deleteUser(id);
  }

  handleEditClick(id, values) {
    this.props.editUser(id, values);
  }

  handleFormSubmit(values) {
    this.props.createUser(values);
  }

  render() {
    const { error, isLoading, users, message } = this.props.users;

    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane>
              <UserForm
                onCancel={this.handleCancel}
                onFormSubmit={this.handleFormSubmit}
                form="UserForm"
              />
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'List',
        render: () => {
          return (
            <Tab.Pane>
              <UsersList
                error={error}
                isLoading={isLoading}
                users={users}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
              />
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div>
        <Header content="Users" />

        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
});

export default connect(
  mapStateToProps,
  {
    createUser,
    deleteUser,
    editUser,
    fetchUsers,
    selectUser,
    resetUserMessage,
  }
)(User);
