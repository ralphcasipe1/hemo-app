import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import React, { Component } from 'react';
import {
  createTemplatePhysicianOrder,
  deleteTemplatePhysicianOrder,
  editTemplatePhysicianOrder,
  fetchTemplatePhysicianOrders,
  resetDeletedTemplatePhysicianOrder,
  resetEditedTemplatePhysicianOrder,
  resetNewTemplatePhysicianOrder,
  resetTemplatePhysicianOrders,
} from '../actions/templatePhysicianOrders';
import SOPhysicianOrderForm from '../components/SOPhysicianOrderForm';
import SOPhysicianOrderDetail from '../components/SOPhysicianOrderDetail';

class SOPhysicianOrderContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchTemplatePhysicianOrders(this.props.patientId);
  }

  componentWillUnmount() {
    this.props.resetNewTemplatePhysicianOrder();
    this.props.resetTemplatePhysicianOrders();
  }

  handleCancel() {
    this.props.router.push(`/patients/${this.props.patientId}`);
  }

  handleDelete(patientId, templatePhysicianOrderId) {
    this.props.deleteTemplatePhysicianOrder(
      patientId,
      templatePhysicianOrderId
    );
  }

  handleEditSubmit(patientId, templatePhysicianOrderId, values) {
    this.props.editTemplatePhysicianOrder(
      patientId,
      templatePhysicianOrderId,
      values
    );
  }

  handleFormSubmit(values) {
    this.props.createTemplatePhysicianOrder(this.props.patientId, values);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane
              loading={this.props.templatePhysicianOrdersList.isLoading}
            >
              {!!this.props.templatePhysicianOrdersList
                .templatePhysicianOrders ? (
                <SOPhysicianOrderDetail
                  error={this.props.templatePhysicianOrdersList.error}
                  isLoading={this.props.templatePhysicianOrdersList.isLoading}
                  templatePhysicianOrder={
                    this.props.templatePhysicianOrdersList
                      .templatePhysicianOrders
                  }
                  onDelete={this.handleDelete}
                  onEditSubmit={this.handleEditSubmit}
                />
              ) : (
                <SOPhysicianOrderForm
                  form="TemplatePhysicianOrderForm"
                  onCancel={this.handleCancel}
                  onFormSubmit={this.handleFormSubmit}
                />
              )}
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'Logs',
        render: () => {
          return <Tab.Pane />;
        },
      },
    ];

    return (
      <div>
        <Header content="Standing Order Physician Orders" />
        <Tab panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  templatePhysicianOrdersList:
    state.templatePhysicianOrders.templatePhysicianOrdersList,
});

export default connect(
  mapStateToProps,
  {
    createTemplatePhysicianOrder,
    deleteTemplatePhysicianOrder,
    editTemplatePhysicianOrder,
    fetchTemplatePhysicianOrders,
    resetDeletedTemplatePhysicianOrder,
    resetEditedTemplatePhysicianOrder,
    resetNewTemplatePhysicianOrder,
    resetTemplatePhysicianOrders,
  }
)(SOPhysicianOrderContainer);
