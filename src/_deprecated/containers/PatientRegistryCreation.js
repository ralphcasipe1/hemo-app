import { connect } from 'react-redux';
import {
  createPatientRegistry,
  resetNewPatientRegistry,
} from '../actions/patients/patientRegistries';
import React, { Component } from 'react';
import PatientRegistryForm from 'PatientRegistryForm';

class PatientRegistryCreation extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleCancel() {
    this.props.router.goBack();
  }

  handleFormSubmit(values) {
    this.props.createPatientRegistry(this.props.patientId, values);
  }

  render() {
    return (
      <div>
        <PatientRegistryForm
          onCancel={this.handleCancel}
          onFormSubmit={this.handleFormSubmit}
          form="PatientRegistryForm"
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
});

export default connect(
  mapStateToProps,
  {
    createPatientRegistry,
    resetNewPatientRegistry,
  }
)(PatientRegistryCreation);
