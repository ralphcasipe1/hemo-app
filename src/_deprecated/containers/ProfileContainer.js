import { connect } from 'react-redux';
import { getTimelines } from '../selectors/TimelineSelectors';
import { fetchTimelines } from '../actions/timelines';
import { getTasks, getCompletedTasks } from '../selectors/TaskSelectors';
import { fetchTasks, completeTask } from '../actions/tasks';
import React from 'react';
import Profile from '../components/Profile';

const ProfileContainer = (props) => <Profile {...props} />;

const mapStateToProps = (state) => ({
  user: state.auth.user,
  timelines: getTimelines(state),
  tasks: getTasks(state),
  completedTasks: getCompletedTasks(state),
  isLoading: state.timelines.isLoading,
});

export default connect(
  mapStateToProps,
  {
    fetchTimelines,

    fetchTasks,
    completeTask,
  }
)(ProfileContainer);
