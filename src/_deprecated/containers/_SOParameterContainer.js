import { connect } from 'react-redux'
import {
  createStandingOrderParameter,
  fetchAllStandingOrderParameters
} from '../actions/standing-order-parameter'
import SOParameter from '../components/SOParameter'

const mapStateToProps = (state, props) => {
  const {
    isLoading,
    standingOrderParameters
  } = state.standingOrderParameterReducer

  return {
    patientId: props.match.params.patientId
    , standingOrderParameters
    , isLoading
  }
}

export const SOParameterContainer = connect(
  mapStateToProps,
  {
    createStandingOrderParameter
    , fetchAllStandingOrderParameters
  }
)(SOParameter)
