import { connect } from 'react-redux'
import { NurseNote } from '../components/NurseNote'
import {
  createPatientRegistryNurseNote,
  deletePatientRegistryNurseNote,
  editPatientRegistryNurseNote,
  fetchPatientRegistryNurseNotes,
  resetNurseNoteMessage
} from '../actions/nurseNotes'

const mapStateToProps = (state, props) => ({
  patientRegistryId: props.params.patientRegistryId
  , nurseNotes: state.nurseNotes.nurseNotes
  , isLoading: state.nurseNotes.isLoading
})

export const NurseNoteContainer = connect(
  mapStateToProps,
  {
    createPatientRegistryNurseNote
    , deletePatientRegistryNurseNote
    , editPatientRegistryNurseNote
    , fetchPatientRegistryNurseNotes
    , resetNurseNoteMessage
  }
)(NurseNote)

/* class NurseNoteContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeMenu: 'view notes'
    }
  }


  render() {
    const { nurseNotes, isLoading, patientRegistryId } = this.props;
    
    if (isLoading) {
      return <h1>Loading</h1>;
    }

    return (
      <div>
        <Header content="Nurse's Notes"/>
        
        <NurseNote
          nurseNotes={nurseNotes}
          patientRegistryId={this.props.patientRegistry}
        />
        
      </div>
    );
  }
} */
