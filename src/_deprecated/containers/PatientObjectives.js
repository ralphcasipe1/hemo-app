import React from 'react';
import { connect } from 'react-redux';
import { Header, List } from 'semantic-ui-react';
import {
  fetchPatientObjs,
  resetPatientObjs,
  createPatientObj,
  resetNewPatientObj,
} from '../actions/patients/patientObjs';
import { fetchObjectives, resetObjectives } from '../actions/objectives';

import PatientObjectiveForm from '../components/Forms/PatientObjectiveForm';

class PatientObjectives extends React.Component {
  constructor(props) {
    super(props);
    this.handleObjectiveSubmit = this.handleObjectiveSubmit.bind(this);
  }
  componentWillMount() {
    this.props.fetchObjectives();
    this.props.fetchPatientObjs(this.props.patientId);
  }

  componentWillUnmount() {
    this.props.resetObjectives();
    this.props.resetPatientObjs();
    this.props.resetNewPatientObj();
  }

  handleObjectiveSubmit(values) {
    this.props.createPatientObj(this.props.patientId, values);
  }

  render() {
    return (
      <div>
        <Header>Objective Assessment</Header>
        <PatientObjectiveForm
          onFormSubmit={this.handleObjectiveSubmit}
          objectives={this.props.objectivesList.objectives}
          form="PatientObjectiveForm"
        />
        <List />
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  objectivesList: state.objectives.objectivesList,
});
export default connect(
  mapStateToProps,
  {
    fetchObjectives,
    resetObjectives,
    fetchPatientObjs,
    resetPatientObjs,
    createPatientObj,
    resetNewPatientObj,
  }
)(PatientObjectives);
