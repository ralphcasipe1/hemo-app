import { connect } from 'react-redux';
import { fetchObjectives } from '../actions/objectives';
import { fetchSubjectives } from '../actions/subjectives';
import { fetchMobilities } from '../actions/mobilities';
import {
  createPatientRegistryObjs,
  fetchPatientRegistryObjs,
  deletePatientRegistryObjs,
} from '../actions/objectiveAssessments';
import {
  createObjectiveRemark,
  getObjectiveRemark,
  deleteObjectiveRemark,
} from '../actions/objectiveRemarks';
import {
  createPatientRegistrySubjs,
  fetchPatientRegistrySubjs,
  deletePatientRegistrySubjs,
} from '../actions/subjectiveAssessments';
import {
  createSubjectiveRemark,
  getSubjectiveRemark,
  deleteSubjectiveRemark,
} from '../actions/subjectiveRemarks';
import {
  createPreAssessment,
  fetchPreAssessments,
} from '../actions/preAssessments';
import {
  createPatientRegistryPostObjs,
  fetchPatientRegistryPostObjs,
  deletePatientRegistryPostObjs,
} from '../actions/postObjectiveAssessments';
import {
  createPostObjectiveRemark,
  getPostObjectiveRemark,
  deletePostObjectiveRemark,
} from '../actions/postObjectiveRemarks';
import {
  createPatientRegistryPostSubjs,
  fetchPatientRegistryPostSubjs,
  deletePatientRegistryPostSubjs,
} from '../actions/postSubjectiveAssessments';
import {
  createPostSubjectiveRemark,
  getPostSubjectiveRemark,
  deletePostSubjectiveRemark,
} from '../actions/postSubjectiveRemarks';
import { getMobilities } from '../selectors/MobilitySelectors';
import { getObjectives } from '../selectors/ObjectiveSelectors';
import { getSubjectives } from '../selectors/SubjectiveSelectors';
import { getPreAssessment } from '../selectors/PreAssessmentSelectors';
import {
  getPatientRegistryObjs,
  getObjectiveAssessmentRemark,
} from '../selectors/ObjectiveAssessmentSelectors';
import {
  getPatientRegistrySubjs,
  getSubjectiveAssessmentRemark,
} from '../selectors/SubjectiveAssessmentSelectors';
import {
  getPatientRegistryPostObjs,
  getPostObjectiveAssessmentRemark,
} from '../selectors/PostObjectiveAssessmentSelectors';
import {
  getPatientRegistryPostSubjs,
  getPostSubjectiveAssessmentRemark,
} from '../selectors/PostSubjectiveAssessmentSelectors';
import React from 'react';
import Assessments from '../components/Assessments';

const AssessmentContainer = (props) => <Assessments {...props} />;

const mapStateToProps = (state) => ({
  mobilities: getMobilities(state),
  objectives: getObjectives(state),
  subjectives: getSubjectives(state),
  preAssessment: getPreAssessment(state),
  patientRegistrySubjs: getPatientRegistrySubjs(state),
  subjectiveRemark: getSubjectiveAssessmentRemark(state),
  patientRegistryObjs: getPatientRegistryObjs(state),
  objectiveRemark: getObjectiveAssessmentRemark(state),
  patientRegistryPostSubjs: getPatientRegistryPostSubjs(state),
  postSubjectiveRemark: getPostSubjectiveAssessmentRemark(state),
  patientRegistryPostObjs: getPatientRegistryPostObjs(state),
  postObjectiveRemark: getPostObjectiveAssessmentRemark(state),
});

export default connect(
  mapStateToProps,
  {
    fetchMobilities,
    fetchObjectives,
    fetchSubjectives,
    createPreAssessment,
    fetchPreAssessments,
    createPatientRegistryObjs,
    fetchPatientRegistryObjs,
    deletePatientRegistryObjs,
    createObjectiveRemark,
    getObjectiveRemark,
    deleteObjectiveRemark,
    createPatientRegistrySubjs,
    deletePatientRegistrySubjs,
    fetchPatientRegistrySubjs,
    createSubjectiveRemark,
    getSubjectiveRemark,
    deleteSubjectiveRemark,
    createPatientRegistryPostObjs,
    fetchPatientRegistryPostObjs,
    deletePatientRegistryPostObjs,
    createPostObjectiveRemark,
    getPostObjectiveRemark,
    deletePostObjectiveRemark,
    createPatientRegistryPostSubjs,
    fetchPatientRegistryPostSubjs,
    deletePatientRegistryPostSubjs,
    createPostSubjectiveRemark,
    getPostSubjectiveRemark,
    deletePostSubjectiveRemark,
  }
)(AssessmentContainer);
