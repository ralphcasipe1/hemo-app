import { connect } from 'react-redux'
import { Header, Tab } from 'semantic-ui-react'
import {
  createAbnormal,
  deleteAbnormal,
  editAbnormal,
  fetchAbnormals,
  resetAbnormals,
  resetDeletedAbnormal,
  resetEditAbnormal,
  resetNewAbnormal
} from '../actions/abnormals'
import AbnormalForm from 'AbnormalForm'
import AbnormalsList from 'AbnormalsList'
import React, { Component } from 'react'

class AbnormalContainer extends Component {
  constructor() {
    super()

    this.handleCancel = this.handleCancel.bind(this)
    this.handleDeleteClick = this.handleDeleteClick.bind(this)
    this.handleEditClick = this.handleEditClick.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  handleCancel() {
    this.props.router.push('/admin')
  }

  handleDeleteClick(id) {
    this.props.deleteAbnormal(id)
  }

  handleEditClick(id, values) {
    this.props.editAbnormal(id, values)
  }

  handleFormSubmit(values) {
    this.props.createAbnormal(values)
  }

  componentDidMount() {
    this.props.fetchAbnormals()
  }

  componentWillUnmount() {
    this.props.resetAbnormals()
    this.props.resetNewAbnormal()
  }

  render() {
    const panes = [
      {
        menuItem: 'Form'
        , render: () => {
          return (
            <Tab.Pane>
              <AbnormalForm
                onFormSubmit={this.handleFormSubmit}
                hasCancel
                onCancel={this.handleCancel}
                form="AbnormalForm"
              />
            </Tab.Pane>
          )
        }
      }
      , {
        menuItem: 'List'
        , render: () => {
          return (
            <Tab.Pane>
              <AbnormalsList
                abnormals={this.props.abnormals}
                error={this.props.error}
                isLoading={this.props.isLoading}
                onDeleteClick={this.handleDeleteClick}
                onEditClick={this.handleEditClick}
              />
            </Tab.Pane>
          )
        }
      }
    ]

    return (
      <div>
        <Header content="Abnormals" />
        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  abnormals: state.abnormals.abnormals
  , isLoading: state.abnormals.isLoading
  , error: state.abnormals.error
})

export default connect(
  mapStateToProps,
  {
    createAbnormal
    , deleteAbnormal
    , editAbnormal
    , fetchAbnormals
    , resetAbnormals
    , resetDeletedAbnormal
    , resetEditAbnormal
    , resetNewAbnormal
  }
)(AbnormalContainer)
