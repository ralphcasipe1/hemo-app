import { connect } from 'react-redux'
import { Medication } from '../components/Medication'
import { createMedication, fetchAllMedications } from '../actions/medications'

const mapStateToProps = (state, props) => ({
  patientRegistryId: props.params.patientRegistryId
  , medications: state.medicationReducer.medications
  , isLoading: state.medicationReducer.isLoading
})

export const MedicationContainer = connect(
  mapStateToProps,
  {
    createMedication
    , fetchAllMedications
  }
)(Medication)
