import { connect } from 'react-redux';
import { Physician } from '../components/Physician';
import {
  fetchAllBizboxPhysicians,
  fetchBizboxPhysician,
} from '../actions/bizboxPhysician.action';
import { createPhysician, fetchPhysicians } from '../actions/physicians';

const mapStateToProps = (state) => ({
  bizboxPhysicians: state.bizboxPhysicianReducer.bizboxPhysicians,
  bizboxPhysician: state.bizboxPhysicianReducer.bizboxPhysician,
  isLoading: state.bizboxPhysicianReducer.isLoading,
  physicians: state.physicians.physiciansList.physicians,
  error: state.bizboxPhysicianReducer.error,
});

export const PhysicianContainer = connect(
  mapStateToProps,
  {
    createPhysician,
    fetchAllBizboxPhysicians,
    fetchBizboxPhysician,
    fetchPhysicians,
  }
)(Physician);
