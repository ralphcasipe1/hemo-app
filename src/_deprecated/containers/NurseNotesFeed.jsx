import { connect } from 'react-redux';
import { Feed, Header, Icon, TextArea, List, Button } from 'semantic-ui-react';
import {
  deletePatientRegistryNurseNote,
  editPatientRegistryNurseNote,
  fetchPatientRegistryNurseNotes,
} from '../actions/nurseNotes';
import { createTimeline } from '../actions/timelines';
import {
  getNurseNotes,
  getLoading,
  getError,
} from '../selectors/NurseNoteSelectors';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

class NurseNotesFeed extends Component {
  static propTypes = {
    patientRegistryId: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      id: [],
      deleteNote: [],
    };
  }

  toggleInput(id) {
    if (this.state.id.indexOf(id) === -1) {
      this.setState({
        id: [...this.state.id, id],
      });
    } else if (this.state.id.indexOf(id) !== -1) {
      this.setState({
        id: this.state.id.filter((col) => col !== id),
      });
    }
  }

  handleValueChange(e, id) {
    this.setState({
      id: this.state.id.filter((col) => col !== id),
    });

    this.props.editPatientRegistryNurseNote(this.props.patientRegistryId, id, {
      [e.target.name]: e.target.value,
    });
  }

  handleDeleteClick(id) {
    this.props.deletePatientRegistryNurseNote(this.props.patientRegistryId, id);
    this.props.createTimeline({
      activity_id: 1,
      timeline_status: 'deleted',
    });
  }

  componentDidMount() {
    this.props.fetchPatientRegistryNurseNotes(this.props.patientRegistryId);
  }

  renderItems(note) {
    return (
      <List.Item key={note.user_id}>
        <Icon color="blue" name="user" size="large" />
        <List.Content>
          <List.Header>{`${note.fname} ${note.lname}'s`} Notes</List.Header>
          <List.Description>
            <Feed size="small">
              {note.nurseNotes.map((noteData) => {
                return (
                  <Feed.Event key={noteData.nurse_note_id}>
                    <Feed.Label>
                      <Icon color="blue" name="pencil" />
                    </Feed.Label>
                    <Feed.Content>
                      <Feed.Summary>
                        {noteData.created_at !== noteData.updated_at
                          ? 'Update this note @ '
                          : 'Added a note @'}
                        <Feed.Date>
                          {moment(noteData.updated_at).format('lll')}
                        </Feed.Date>
                      </Feed.Summary>
                      <Feed.Extra text>
                        {this.state.id.indexOf(noteData.nurse_note_id) !==
                        -1 ? (
                          <TextArea
                            autoHeight
                            defaultValue={noteData.note_desc}
                            onBlur={(e) =>
                              this.handleValueChange(e, noteData.nurse_note_id)
                            }
                            name="note_desc"
                          />
                        ) : (
                          noteData.note_desc
                            .split('\n')
                            .map((field, index) => (
                              <div key={index}>{field}</div>
                            ))
                        )}
                      </Feed.Extra>
                      {this.state.deleteNote.indexOf(noteData.nurse_note_id) !==
                      -1 ? (
                        <Feed.Meta>
                          Are you sure?
                          <Button
                            basic
                            color="blue"
                            content="Cancel"
                            icon="delete"
                            onClick={() =>
                              this.setState({
                                deleteNote: this.state.deleteNote.filter(
                                  (id) => id !== noteData.nurse_note_id
                                ),
                              })
                            }
                          />
                          <Button
                            color="red"
                            content="Yes"
                            icon="checkmark"
                            onClick={() =>
                              this.handleDeleteClick(noteData.nurse_note_id)
                            }
                          />
                        </Feed.Meta>
                      ) : (
                        <Feed.Meta>
                          {noteData.created_at !== noteData.updated_at
                            ? 'Updated '
                            : ''}
                          {moment(noteData.updated_at).fromNow()}

                          {this.props.auth.user.user_id === note.user_id ? (
                            <div>
                              <a>
                                <Button
                                  basic
                                  color="red"
                                  onClick={() =>
                                    this.setState({
                                      deleteNote: [
                                        ...this.state.deleteNote,
                                        noteData.nurse_note_id,
                                      ],
                                    })
                                  }
                                  disabled={
                                    moment(noteData.created_at).format('ll') !==
                                    moment().format('ll')
                                  }
                                  size="tiny"
                                >
                                  Delete
                                </Button>
                              </a>
                              ||
                              <a>
                                <Button
                                  basic
                                  color="blue"
                                  onClick={
                                    this.state.id.indexOf(
                                      noteData.nurse_note_id
                                    ) !== -1
                                      ? () =>
                                          this.setState({
                                            id: this.state.id.filter(
                                              (id) =>
                                                id !== noteData.nurse_note_id
                                            ),
                                          })
                                      : () =>
                                          this.toggleInput(
                                            noteData.nurse_note_id
                                          )
                                  }
                                  size="tiny"
                                  disabled={
                                    moment(noteData.created_at).format('ll') !==
                                    moment().format('ll')
                                  }
                                >
                                  {this.state.id.indexOf(
                                    noteData.nurse_note_id
                                  ) !== -1
                                    ? 'OK'
                                    : 'Edit'}
                                </Button>
                              </a>
                            </div>
                          ) : (
                            ''
                          )}
                        </Feed.Meta>
                      )}
                    </Feed.Content>
                  </Feed.Event>
                );
              })}
            </Feed>
          </List.Description>
        </List.Content>
      </List.Item>
    );
  }

  render() {
    const { isLoading, error, nurseNotes } = this.props;

    if (isLoading) {
      return <div>Loading</div>;
    } else if (error) {
      return <div>{error}</div>;
    } else if (nurseNotes.length === 0) {
      return <div>No notes. please add new note</div>;
    }

    return (
      <List divided relaxed>
        {nurseNotes.map((note) => this.renderItems(note))}
      </List>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  nurseNotes: getNurseNotes(state),
  isLoading: getLoading(state),
  error: getError(state),
});

export default connect(
  mapStateToProps,
  {
    deletePatientRegistryNurseNote,
    editPatientRegistryNurseNote,
    fetchPatientRegistryNurseNotes,
    createTimeline,
  }
)(NurseNotesFeed);
