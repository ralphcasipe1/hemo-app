import { connect } from 'react-redux'
import {
  createStandingOrderAntiCoagulant,
  fetchAllStandingOrderAntiCoagulants,
  fetchStandingAntiCoagulant,
  updateStandingOrderAntiCoagulant,
  deleteStandingOrderAntiCoagulant
} from '../actions/standing-order-anti-coagulant'
import SOAntiCoagulant from '../components/SOAntiCoagulant'

const mapStateToProps = (state, props) => {
  const {
    error,
    isLoading,
    standingOrderAntiCoagulant,
    standingOrderAntiCoagulants
  } = state.standingOrderAntiCoagulantReducer

  return {
    patientId: props.match.params.patientId
    , error
    , isLoading
    , standingOrderAntiCoagulant
    , standingOrderAntiCoagulants
  }
}

export const SOAntiCoagulantContainer = connect(
  mapStateToProps,
  {
    createStandingOrderAntiCoagulant
    , fetchAllStandingOrderAntiCoagulants
    , fetchStandingAntiCoagulant
    , updateStandingOrderAntiCoagulant
    , deleteStandingOrderAntiCoagulant
  }
)(SOAntiCoagulant)

/* import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, Tab } from 'semantic-ui-react';
import {
  createPatientTemplateAntiCoagulant,
  deletePatientTemplateAntiCoagulant,
  editPatientTemplateAntiCoagulant,
  fetchPatientTemplateAntiCoagulants
} from '../actions/templateAntiCoagulant.js';
import SOAntiCoagulantForm from '../components/SOAntiCoagulantForm';
import SOAntiCoagulantDetail from '../components/SOAntiCoagulantDetail';

class SOAntiCoagulantContainer extends Component {
  constructor(props) {
    super(props);

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchPatientTemplateAntiCoagulants(this.props.patientId);
  }

  handleCancel() {
    this.props.router.push(`/patients/${this.props.patientId}`);
  }

  handleDelete(patientId, templateAntiCoagulantId) {
    this.props.deletePatientTemplateAntiCoagulant(
      patientId,
      templateAntiCoagulantId
    );
  }

  handleEditSubmit(patientId, templateAntiCoagulantId, values) {
    this.props.editPatientTemplateAntiCoagulant(
      patientId,
      templateAntiCoagulantId,
      values
    );
  }

  handleFormSubmit(values) {
    this.props.createPatientTemplateAntiCoagulant(
      this.props.params.patientId,
      values
    );
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane
              loading={this.props.patientTemplateAntiCoagulantsList.isLoading}
            >
              {this.props.patientTemplateAntiCoagulantsList
                .patientTemplateAntiCoagulants ? (
                <SOAntiCoagulantDetail
                  error={this.props.patientTemplateAntiCoagulantsList.error}
                  isLoading={
                    this.props.patientTemplateAntiCoagulantsList.isLoading
                  }
                  templateAntiCoagulant={
                    this.props.patientTemplateAntiCoagulantsList
                      .patientTemplateAntiCoagulants
                  }
                  onDelete={this.handleDelete}
                  onEditSubmit={this.handleEditSubmit}
                />
              ) : (
                <SOAntiCoagulantForm
                  form="TemplateAntiCoagulantForm"
                  onCancel={this.handleCancel}
                  onFormSubmit={this.handleFormSubmit}
                />
              )}
            </Tab.Pane>
          );
        }
      },
      {
        menuItem: 'Logs',
        render: () => {
          return (
            <Tab.Pane>
              <Header
                icon="wrench"
                content="Under const."
                subheader="Sorry for inconvenience"
              />
            </Tab.Pane>
          );
        }
      }
    ];

    return (
      <div>
        <Header content="Standing Order Anti-coagulants" />
        <Tab panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  patientTemplateAntiCoagulantsList:
    state.patientTemplateAntiCoagulants.patientTemplateAntiCoagulantsList
});

export default connect(mapStateToProps, {
  createPatientTemplateAntiCoagulant,
  deletePatientTemplateAntiCoagulant,
  editPatientTemplateAntiCoagulant,
  fetchPatientTemplateAntiCoagulants
})(SOAntiCoagulantContainer);
 */
