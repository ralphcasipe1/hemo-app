import { connect } from 'react-redux';
import {
  fetchPatientRegistries,
  resetPatientRegistries,
} from '../actions/patients/patientRegistries';
import React, { Component } from 'react';
import SummaryList from '../components/SummaryList';

class PatientDashboard extends Component {
  componentWillMount() {
    this.props.fetchPatientRegistries(this.props.patientId);
  }

  componentWillUnmount() {
    this.props.resetPatientRegistries();
  }

  render() {
    return (
      <SummaryList
        exclude={+this.props.patientRegistryId}
        items={this.props.patientRegistersList.patientRegisters}
        onClickList={this.props.patientRegistersList.patientRegisters}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  patientRegistryId: ownProps.params.patientRegistryId,
  patientRegistersList: state.patientRegisters.patientRegistersList,
});

export default connect(
  mapStateToProps,
  {
    fetchPatientRegistries,
    resetPatientRegistries,
  }
)(PatientDashboard);
