import { connect } from 'react-redux'
import CatheterForm from 'CatheterForm'
import React, { Component } from 'react'
import {
  fetchPatientRegistryCatheter,
  resetPatientRegistryCatheter,
  resetUpdatedPatientRegistryCatheter,
  updatePatientRegistryCatheter
} from '../actions/catheters'

class CatheterEdition extends Component {
  constructor(props) {
    super(props)

    this.handleCancel = this.handleCancel.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  handleCancel() {
    this.props.router.goBack()
  }

  handleFormSubmit(values) {
    this.props.updatePatientRegistryCatheter(
      this.props.params.patientRegistryId,
      this.props.getPatientRegistryCatheter.patientRegistryCatheter.catheter_id,
      values
    )
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.edition.successMessage &&
      nextProps.edition.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/catheters`
      )
    }
  }

  render() {
    const { patientRegistryCatheter } = this.props.getPatientRegistryCatheter

    return (
      <div>
        <CatheterForm
          form="EditCatheterForm"
          loading={this.props.isLoading}
          onFormSubmit={this.handleFormSubmit}
          onCancel={this.handleCancel}
          initialValues={patientRegistryCatheter}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  getPatientRegistryCatheter: state.catheters.getPatientRegistryCatheter
  , edition: state.catheters.edition
})

export default connect(
  mapStateToProps,
  {
    fetchPatientRegistryCatheter
    , resetPatientRegistryCatheter
    , resetUpdatedPatientRegistryCatheter
    , updatePatientRegistryCatheter
  }
)(CatheterEdition)
