import { connect } from 'react-redux';
import {
  editPatientRegistryVitalSigns,
  selectPatientRegistryVitalSigns,
  resetEditedPatientRegistryVitalSign,
  resetSelectedPatientRegistryVitalSign,
} from '../actions/vitalSigns';
import React, { Component } from 'react';
import VitalSignForm from 'VitalSignForm';

class VitalSignEdit extends Component {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    this.props.selectPatientRegistryVitalSigns(
      this.props.patientRegistryId,
      this.props.vitalSignId
    );
  }

  componentWillUnmount() {
    this.props.resetEditedPatientRegistryVitalSign();
    this.props.resetSelectedPatientRegistryVitalSign();
  }

  componentWillReceiveProps(nextProps) {
    if (
      !this.props.edition.successMessage &&
      nextProps.edition.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.patientId}/patient_registries/${
          this.props.patientRegistryId
        }/vital_signs`
      );
    }
  }

  handleCancel() {
    this.props.router.push(
      `/patients/${this.props.patientId}/patient_registries/${
        this.props.patientRegistryId
      }/vital_signs`
    );
  }

  handleFormSubmit(values) {
    this.props.editPatientRegistryVitalSigns(
      this.props.patientRegistryId,
      this.props.vitalSignId,
      values
    );
  }

  render() {
    const {
      selectedPatientRegistryVitalSign: { error, isLoading, vitalSign },
    } = this.props;

    if (isLoading) {
      return <span>Loading</span>;
    } else if (error) {
      return <span>{error.message}</span>;
    } else if (!vitalSign) {
      return <span />;
    }

    return (
      <VitalSignForm
        expanded
        form={`EditVitalSignForm_${this.props.vitalSignId}`}
        initialValues={{
          blood_pressure: vitalSign.blood_pressure,
          cardiac_arrest: vitalSign.cardiac_arrest,
          respiratory_rate: vitalSign.respiratory_rate,
          temperature: vitalSign.temperature,
          oxygen_saturation: vitalSign.oxygen_saturation,
          arterial_pressure: vitalSign.arterial_pressure,
          venous_pressure: vitalSign.venous_pressure,
          transmembrane_pressure: vitalSign.transmembrane_pressure,
          blood_flow_rate: vitalSign.blood_flow_rate,
          blood_glucose: vitalSign.blood_glucose,
          remarks: vitalSign.remarks,
        }}
        onCancel={this.handleCancel}
        onFormSubmit={this.handleFormSubmit}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  patientRegistryId: ownProps.params.patientRegistryId,
  vitalSignId: ownProps.params.vitalSignId,
  edition: state.vitalSigns.edition,
  selectedPatientRegistryVitalSign:
    state.vitalSigns.selectedPatientRegistryVitalSign,
});

export default connect(
  mapStateToProps,
  {
    editPatientRegistryVitalSigns,
    selectPatientRegistryVitalSigns,
    resetEditedPatientRegistryVitalSign,
    resetSelectedPatientRegistryVitalSign,
  }
)(VitalSignEdit);
