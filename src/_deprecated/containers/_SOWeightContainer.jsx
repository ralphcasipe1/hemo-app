import { connect } from 'react-redux'
import {
  createStandingOrderWeight,
  fetchAllStandingOrderWeights,
  fetchStandingOrderWeight,
  updateStandingOrderWeight,
  deleteStandingOrderWeight
} from '../actions/standing-order-weight'
import SOWeight from '../components/SOWeight'

const mapStateToProps = (state, props) => {
  const {
    error,
    isLoading,
    standingOrderWeight,
    standingOrderWeights
  } = state.standingOrderWeightReducer

  return {
    patientId: props.match.params.patientId
    , error
    , isLoading
    , standingOrderWeight
    , standingOrderWeights
  }
}

export const SOWeightContainer = connect(
  mapStateToProps,
  {
    createStandingOrderWeight
    , fetchAllStandingOrderWeights
    , fetchStandingOrderWeight
    , updateStandingOrderWeight
    , deleteStandingOrderWeight
  }
)(SOWeight)
/*
class SOWeightContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };

    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchPatientTemplateWeights(this.props.patientId);
  }

  componentWillUnmount() {
    this.props.resetDeletedPatientTemplateWeight();
    this.props.resetEditedPatientTemplateWeight();
    this.props.resetNewPatientTemplateWeight();
    this.props.resetPatientTemplateWeights();
  }

  handleCancel() {
    this.props.router.push(`/patients/${this.props.patientId}`);
  }

  handleDelete(patientId, templateWeightId) {
    this.props.deletePatientTemplateWeight(patientId, templateWeightId);
  }

  handleEditSubmit(patientId, templateWeightId, values) {
    this.props.editPatientTemplateWeight(patientId, templateWeightId, values);
  }

  handleFormSubmit(values) {
    this.props.createPatientTemplateWeight(this.props.patientId, values);
  }

  render() {
    const panes = [
      {
        menuItem: 'Form',
        render: () => {
          return (
            <Tab.Pane loading={this.props.patientTemplateWeightsList.isLoading}>
              {this.props.patientTemplateWeightsList.patientTemplateWeights ? (
                <SOWeightDetail
                  error={this.props.patientTemplateWeightsList.error}
                  isLoading={this.props.patientTemplateWeightsList.isLoading}
                  templateWeight={
                    this.props.patientTemplateWeightsList.patientTemplateWeights
                  }
                  onDelete={this.handleDelete}
                  onEditSubmit={this.handleEditSubmit}
                />
              ) : (
                <SOWeightForm
                  form="TemplateWeightForm"
                  onCancel={this.handleCancel}
                  onFormSubmit={this.handleFormSubmit}
                />
              )}
            </Tab.Pane>
          );
        }
      },
      {
        menuItem: 'Logs',
        render: () => {
          return (
            <Tab.Pane>
              <Header
                icon="wrench"
                content="Under const."
                subheader="Sorry for inconvenience"
              />
            </Tab.Pane>
          );
        }
      }
    ];

    return (
      <div>
        <Header content="Standing Order Weight" />
        <Tab panes={panes} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  patientId: ownProps.params.patientId,
  patientTemplateWeightsList:
    state.patientTemplateWeights.patientTemplateWeightsList
});

export default connect(mapStateToProps, {
  createPatientTemplateWeight,
  deletePatientTemplateWeight,
  editPatientTemplateWeight,
  fetchPatientTemplateWeights,
  resetDeletedPatientTemplateWeight,
  resetEditedPatientTemplateWeight,
  resetPatientTemplateWeights,
  resetNewPatientTemplateWeight
})(SOWeightContainer);
 */
