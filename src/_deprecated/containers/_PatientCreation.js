import { connect } from 'react-redux'
import { message } from '../utils/appUtils'
import { createPatient, resetNewPatient } from '../actions/patients/patients'
import { fetchPhysicians, resetPhysicians } from '../actions/physicians'
import PatientForm from 'PatientForm'
import React, { Component } from 'react'

class PatientCreation extends Component {
  constructor(props) {
    super(props)

    this.handleCancel = this.handleCancel.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  componentDidMount() {
    this.props.fetchPhysicians()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      (!this.props.newPatient.patientMessage &&
        nextProps.newPatient.patientMessage) ||
      (!this.props.newPatient.error && nextProps.newPatient.error)
    ) {
      setTimeout(() => {
        this.props.resetNewPatient()
      }, 5000)
    }
  }

  componentWillUnmount() {
    this.props.resetNewPatient()
    this.props.resetPhysicians()
  }

  handleCancel() {
    this.props.router.push('/')
  }

  handleFormSubmit(values) {
    this.props.createPatient(values)
  }

  render() {
    const { patientMessage, error, isLoading } = this.props.newPatient

    let msg = message(
      {
        success: patientMessage
        , error
        , loading: isLoading
      },
      {
        customHeader: 'New patient'
        , customContent: 'Create a patient'
      }
    )

    return (
      <PatientForm
        attached
        form="PatientForm"
        loading={isLoading}
        onCancel={this.handleCancel}
        onFormSubmit={this.handleFormSubmit}
        physicians={this.props.physiciansList.physicians}
        isSuccess={!!patientMessage}
        isError={!!error}
        msgIcon={msg.icon}
        msgHeader={msg.header}
        msgContent={msg.content}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    newPatient: state.patients.newPatient
    , physiciansList: state.physicians.physiciansList
  }
}

export default connect(
  mapStateToProps,
  {
    createPatient
    , resetNewPatient
    , fetchPhysicians
    , resetPhysicians
  }
)(PatientCreation)
