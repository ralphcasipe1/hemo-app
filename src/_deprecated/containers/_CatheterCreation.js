import { connect } from 'react-redux'
import {
  createPatientRegistryCatheter,
  resetNewPatientRegistryCatheter
} from '../actions/catheters'
import { fetchAbnormals, resetAbnormals } from '../actions/abnormals'
import { fetchPhysicians, resetPhysicians } from '../actions/physicians'
import CatheterForm from 'CatheterForm'
import React, { Component } from 'react'

class CatheterCreation extends Component {
  constructor(props) {
    super(props)

    this.handleCancel = this.handleCancel.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  handleCancel() {
    this.props.router.goBack()
  }

  handleFormSubmit(values) {
    this.props.createPatientRegistryCatheter(
      this.props.params.patientRegistryId,
      values
    )
  }

  componentDidMount() {
    this.props.fetchAbnormals()
    this.props.fetchPhysicians()
  }

  componentWillUnmount() {
    this.props.resetAbnormals()
    this.props.resetPhysicians()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.creation.successMessage &&
      nextProps.creation.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/catheters`
      )
    }
  }

  render() {
    return (
      <div>
        <CatheterForm
          abnormals={this.props.abnormalsList.abnormals}
          onCancel={this.handleCancel}
          onFormSubmit={this.handleFormSubmit}
          physicians={this.props.physiciansList.physicians}
          form="CatheterForm"
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  abnormalsList: state.abnormals.abnormalsList
  , physiciansList: state.physicians.physiciansList
  , creation: state.catheters.creation
})

export default connect(
  mapStateToProps,
  {
    fetchAbnormals
    , resetAbnormals
    , createPatientRegistryCatheter
    , resetNewPatientRegistryCatheter
    , fetchPhysicians
    , resetPhysicians
  }
)(CatheterCreation)
