import { connect } from 'react-redux'
import {
  createPatientRegistryMedication,
  resetNewPatientRegistryMedication
} from '../actions/medications'
import MedicationForm from 'MedicationForm'
import React, { Component } from 'react'

class MedicationCreation extends Component {
  constructor() {
    super()

    this.handleCancel = this.handleCancel.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      !this.props.creation.successMessage &&
      nextProps.creation.successMessage
    ) {
      this.props.router.push(
        `/patients/${this.props.params.patientId}/patient_registries/${
          this.props.params.patientRegistryId
        }/medications`
      )
    }
  }

  handleCancel() {
    this.props.router.push(
      `/patients/${this.props.params.patientId}/patient_registries/${
        this.props.params.patientRegistryId
      }/medications`
    )
  }

  handleFormSubmit(values) {
    this.props.createPatientRegistryMedication(
      this.props.params.patientRegistryId,
      values
    )
  }

  render() {
    return (
      <MedicationForm
        expanded
        onFormSubmit={this.handleFormSubmit}
        onCancel={this.handleCancel}
        form="MedicationForm"
      />
    )
  }
}

const mapStateToProps = state => ({
  creation: state.medications.creation
})

export default connect(
  mapStateToProps,
  {
    createPatientRegistryMedication
    , resetNewPatientRegistryMedication
  }
)(MedicationCreation)
