import * as types from '../constants/PostObjectiveAssessmentConstants';
import { instance } from './config';

/**
 * @note: PostObjectiveAssessments -> PostObjs
 * @param {*} id
 */
export const fetchPatientRegistryPostObjs = (id) => (dispatch) => {
  dispatch({
    type: types.FETCH_PATIENT_REGISTRY_POST_OBJS,
  });
  instance.get(`/patient_registries/${id}/post_objective_assessments`).then(
    (res) =>
      dispatch({
        type: types.FETCH_PATIENT_REGISTRY_POST_OBJS_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.FETCH_PATIENT_REGISTRY_POST_OBJS_FAILURE,
        payload: err.response.data,
      })
  );
};

/**
 * @note: PostObjectiveAssessments -> PostObjs
 */
export const resetPatientRegistryPostObjs = () => ({
  type: types.RESET_PATIENT_REGISTRY_POST_OBJS,
});

/**
 * @note: PostObjectiveAssessments -> PostObjs
 * @param {*} id
 * @param {*} props
 */
export const createPatientRegistryPostObjs = (id, props) => (dispatch) => {
  dispatch({
    type: types.CREATE_PATIENT_REGISTRY_POST_OBJ,
  });

  instance
    .post(`/patient_registries/${id}/post_objective_assessments`, props)
    .then(
      (res) =>
        dispatch({
          type: types.CREATE_PATIENT_REGISTRY_POST_OBJ_SUCCESS,
          payload: res.data,
        }),
      (err) =>
        dispatch({
          type: types.CREATE_PATIENT_REGISTRY_POST_OBJ_FAILURE,
          payload: err.response.data,
        })
    );
};

/**
 * @note: PostObjectiveAssessments -> PostObjs
 */
export const resetNewPatientRegistryPostObjs = () => ({
  type: types.RESET_NEW_PATIENT_REGISTRY_POST_OBJ,
});

export const deletePatientRegistryPostObjs = (id, remarkId) => (dispatch) => {
  dispatch({
    type: types.DELETE_POST_OBJS,
  });

  instance.delete(`/patient_registries/${id}/post_objective_assessments`).then(
    (res) =>
      dispatch({
        type: types.DELETE_POST_OBJS_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.DELETE_POST_OBJS_FAILURE,
        payload: err.response.data,
      })
  );
};
