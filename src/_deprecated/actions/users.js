import * as types from '../constants/UserConstants';
import userApi from '../api/user';

export const viewProfile = () => (dispatch) => {
  dispatch({
    type: types.VIEW_PROFILE,
  });

  userApi
    .profile()
    .then((user) =>
      dispatch({
        type: types.VIEW_PROFILE_SUCCESS,
        payload: user,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.VIEW_PROFILE_FAILURE,
        payload: err.response.data,
      })
    );
};

export const createUser = (values) => (dispatch) => {
  dispatch({
    type: types.CREATE_USER,
  });

  userApi
    .create(values)
    .then((user) =>
      dispatch({
        type: types.CREATE_USER_SUCCESS,
        payload: user,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.CREATE_USER_FAILURE,
        payload: err.response.data,
      })
    );
};

export const deleteUser = (id) => (dispatch) => {
  dispatch({
    type: types.DELETE_USER,
  });

  userApi
    .delete(id)
    .then((user) =>
      dispatch({
        type: types.DELETE_USER_SUCCESS,
        payload: user,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.DELETE_USER_FAILURE,
        payload: err.response.data,
      })
    );
};

export const editUser = (id, values) => (dispatch) => {
  dispatch({
    type: types.UPDATE_USER,
  });

  userApi
    .update(id, values)
    .then((user) =>
      dispatch({
        type: types.UPDATE_USER_SUCCESS,
        payload: user,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.UPDATE_USER_FAILURE,
        payload: err.response.data,
      })
    );
};

export const fetchUsers = () => (dispatch) => {
  dispatch({
    type: types.FETCH_USERS,
  });

  userApi
    .fetchAll()
    .then((users) =>
      dispatch({
        type: types.FETCH_USERS_SUCCESS,
        payload: users,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.FETCH_USERS_FAILURE,
        payload: err,
      })
    );
};

export const selectUser = (id) => (dispatch) => {
  dispatch({
    type: types.SELECT_USER,
  });

  userApi
    .fetch(id)
    .then((res) =>
      dispatch({
        type: types.SELECT_USER_SUCCESS,
        payload: res.data,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.SELECT_USER_FAILURE,
        payload: err.response.data,
      })
    );
};

export const resetUserMessage = () => ({ type: types.RESET_USER_MESSAGE });
