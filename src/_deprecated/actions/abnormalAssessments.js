import * as types from '../constants/AbnormalAssessmentConstants';
import { instance } from './config';

/**
 * @note: AbnormalAssessments -> Abnormals
 * @param {*} id
 */
export const fetchCatheterAbnormals = (id) => (dispatch) => {
  dispatch({
    type: types.FETCH_CATHETER_ABNORMALS,
  });
  instance.get(`/catheters/${id}/abnormal_assessments`).then(
    (response) =>
      dispatch({
        type: types.FETCH_CATHETER_ABNORMALS_SUCCESS,
        payload: response.data,
      }),
    (error) =>
      dispatch({
        type: types.FETCH_CATHETER_ABNORMALS_FAILURE,
        error: error.response,
      })
  );
};

/**
 * @note: AbnormalAssessments -> Abnormals
 */
export const resetCatheterAbnormals = () => ({
  type: types.RESET_CATHETER_ABNORMALS,
});

/**
 * @note: AbnormalAssessment -> Abnormal
 * @param {*} id
 * @param {*} props
 */
export const createCatheterAbnormal = (id, props) => (dispatch) => {
  dispatch({
    type: types.CREATE_CATHETER_ABNORMAL,
  });

  instance.post(`/catheters/${id}/abnormal_assessments`, props).then(
    (res) =>
      dispatch({
        type: types.CREATE_CATHETER_ABNORMAL_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.CREATE_CATHETER_ABNORMAL_FAILURE,
        payload: err.response.data,
      })
  );
};

/**
 * @note: AbnormalAssessment -> Abnormal
 */
export const resetNewCatheterAbnormal = () => (dispatch) => ({
  type: types.RESET_NEW_CATHETER_ABNORMAL,
});
