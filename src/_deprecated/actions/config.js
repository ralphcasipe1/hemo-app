import axios from 'axios'

let baseURL

if (process.env.NODE_ENV === 'production') {
  // baseURL = 'http://192.168.7.4:8081/api';
  baseURL = 'http://192.168.110.14:8081/api'
} else if (process.env.NODE_ENV === 'development') {
  // baseURL = 'http://192.168.110.14:8081/api';
  baseURL = 'http://localhost:8081/api'
}

export let oauth = axios.create({
  baseURL
})

export let instance = axios.create({
  baseURL
})

export let bizbox = axios.create({
  baseURL: `${baseURL}/bizbox`
})
