import * as types from '../constants/TaskConstants';
import api from '../api/task';

export const fetchTasks = () => (dispatch) =>
  api.fetchAll().then(
    (tasks) =>
      dispatch({
        type: types.FETCH_TASKS,
        payload: tasks,
      }),
    (err) =>
      dispatch({
        type: types.FLASH_ERROR,
        payload: err.response.data,
      })
  );

/* export const createTask = values => dispatch => api.create(values).then(
  task => dispatch({
    type: types.CREATE_TASK
  }),
  err
) */

export const completeTask = (id) => (dispatch) =>
  api.update(id).then(
    (task) =>
      dispatch({
        type: types.COMPLETE_TASK,
        payload: task,
      }),
    (err) =>
      dispatch({
        type: types.FLASH_ERROR,
        payload: err.response.data,
      })
  );
