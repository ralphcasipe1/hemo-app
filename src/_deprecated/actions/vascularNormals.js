import * as types from '../constants/VascularNormalConstants';
import { instance } from './config';

export function createVascularNormals(id, props) {
  return function(dispatch) {
    dispatch({
      type: types.CREATE_VASCULAR_NORMALS,
    });

    instance.post(`/vasculars/${id}/abnormals`, props).then(
      (res) =>
        dispatch({
          type: types.CREATE_VASCULAR_NORMALS_SUCCESS,
          payload: res.data,
        }),
      (err) =>
        dispatch({
          type: types.CREATE_VASCULAR_NORMALS_FAILURE,
          payload: err.response.data,
        })
    );
  };
}

export function resetNewVascularNormals() {
  return {
    type: types.RESET_NEW_VASCULAR_NORMALS,
  };
}

export function fetchVacularNormals(id) {
  return function(dispatch) {
    dispatch({
      type: types.FETCH_VASCULAR_NORMALS,
    });

    instance.get(`/vasculars/${id}/abnormals`).then(
      (res) =>
        dispatch({
          type: types.FETCH_VASCULAR_NORMALS_SUCCESS,
          payload: res.data,
        }),
      (err) =>
        dispatch({
          type: types.FETCH_VASCULAR_NORMALS_FAILURE,
          payload: err.response.data,
        })
    );
  };
}

export function resetVascularNormals() {
  return {
    type: types.RESET_VASCULAR_NORMALS,
  };
}
