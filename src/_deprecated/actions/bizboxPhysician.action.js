import {
  FETCH_BIZBOX_PHYSICIAN,
  FETCH_BIZBOX_PHYSICIAN_FAILURE,
  FETCH_BIZBOX_PHYSICIAN_SUCCESS,
  FETCH_BIZBOX_PHYSICIANS,
  FETCH_BIZBOX_PHYSICIANS_FAILURE,
  FETCH_BIZBOX_PHYSICIANS_SUCCESS,
} from '../constants/bizboxPhysician.constant';
import { bizboxPhysicianService } from '../api/bizboxPhysician.service';

export const fetchAllBizboxPhysicians = () => (dispatch) => {
  dispatch({
    type: FETCH_BIZBOX_PHYSICIANS,
  });

  return bizboxPhysicianService.fetchAll().then(
    (res) =>
      dispatch({
        type: FETCH_BIZBOX_PHYSICIANS_SUCCESS,
        payload: res,
      }),
    (err) =>
      dispatch({
        type: FETCH_BIZBOX_PHYSICIANS_FAILURE,
        payload: err,
      })
  );
};

export const fetchBizboxPhysician = (id) => (dispatch) => {
  dispatch({
    type: FETCH_BIZBOX_PHYSICIAN,
  });

  return bizboxPhysicianService.fetch(id).then(
    (res) =>
      dispatch({
        type: FETCH_BIZBOX_PHYSICIAN_SUCCESS,
        payload: res,
      }),
    (err) =>
      dispatch({
        type: FETCH_BIZBOX_PHYSICIAN_FAILURE,
        payload: err,
      })
  );
};
