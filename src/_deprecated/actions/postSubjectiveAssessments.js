import * as types from '../constants/PostSubjectiveAssessmentConstants';
import { instance } from './config';

/**
 * @note: PostSubjectiveAssessments -> PostSubjs
 * @param {*} id
 */
export const fetchPatientRegistryPostSubjs = (id) => (dispatch) => {
  dispatch({
    type: types.FETCH_PATIENT_REGISTRY_POST_SUBJS,
  });
  instance.get(`/patient_registries/${id}/post_subjective_assessments`).then(
    (res) =>
      dispatch({
        type: types.FETCH_PATIENT_REGISTRY_POST_SUBJS_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.FETCH_PATIENT_REGISTRY_POST_SUBJS_FAILURE,
        payload: err.response.data,
      })
  );
};

/**
 * @note: PostSubjectiveAssessments -> PostSubjs
 */
export const resetPatientRegistryPostSubjs = () => ({
  type: types.RESET_PATIENT_REGISTRY_POST_SUBJS,
});

/**
 * @note: PostSubjectiveAssessments -> PostSubjs
 */
export const createPatientRegistryPostSubjs = (id, props) => (dispatch) => {
  dispatch({
    type: types.CREATE_PATIENT_REGISTRY_POST_SUBJ,
  });

  instance
    .post(`/patient_registries/${id}/post_subjective_assessments`, props)
    .then(
      (res) =>
        dispatch({
          type: types.CREATE_PATIENT_REGISTRY_POST_SUBJ_SUCCESS,
          payload: res.data,
        }),
      (err) =>
        dispatch({
          type: types.CREATE_PATIENT_REGISTRY_POST_SUBJ_FAILURE,
          payload: err.response.data,
        })
    );
};

/**
 * @note: PostSubjectiveAssessments -> PostSubjs
 */
export const resetNewPatientRegistryPostSubjs = () => ({
  type: types.RESET_NEW_PATIENT_REGISTRY_POST_SUBJ,
});

export const deletePatientRegistryPostSubjs = (id) => (dispatch) => {
  dispatch({
    type: types.DELETE_PATIENT_REGISTRY_POST_SUBJS,
  });

  instance.delete(`/patient_registries/${id}/post_subjective_assessments`).then(
    (res) =>
      dispatch({
        type: types.DELETE_PATIENT_REGISTRY_POST_SUBJS_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.DELETE_PATIENT_REGISTRY_POST_SUBJS_FAILURE,
        payload: err.response.data,
      })
  );
};
