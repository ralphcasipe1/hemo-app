import { instance } from './config';
import * as types from '../constants/TemplateParameterLogConstants';

export function createTemplateParameterLog(
  patientId,
  templateParameterId,
  props
) {
  return (dispatch) => {
    dispatch({
      type: types.CREATE_TEMPLATE_PARAMETER_LOG,
    });

    instance
      .post(
        `/patients/${patientId}/template_parameters/${templateParameterId}`,
        props
      )
      .then(
        (res) =>
          dispatch({
            type: types.CREATE_TEMPLATE_PARAMETER_LOG_SUCCESS,
            payload: res.data,
          }),
        (err) =>
          dipatch({
            type: types.CREATE_TEMPLATE_PARAMETER_LOG_FAILURE,
            payload: err.response.data,
          })
      );
  };
}

export function fetchTemplateParameterLogs(patientId, templateParameterId) {
  return (dispatch) => {
    dispatch({
      type: types.FETCH_TEMPLATE_PARAMETER_LOGS,
    });

    instance
      .get(`/patients/${patientId}/template_parameters/${templateParameterId}`)
      .then(
        (res) =>
          dispatch({
            type: types.FETCH_TEMPLATE_PARAMETER_LOGS_SUCCESS,
            payload: res.data,
          }),
        (err) =>
          dipatch({
            type: types.FETCH_TEMPLATE_PARAMETER_LOGS_FAILURE,
            payload: err.response.data,
          })
      );
  };
}
