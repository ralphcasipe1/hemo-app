import * as types from '../constants/PostObjectiveRemarkConstants';
import remarkApi from '../api/postObjectiveRemark';
import { instance } from './config';

export const createPostObjectiveRemark = (id, values) => (dispatch) => {
  dispatch({
    type: types.CREATE_POST_OBJECTIVE_REMARK,
  });

  remarkApi
    .create(id, values)
    .then((remark) =>
      dispatch({
        type: types.CREATE_POST_OBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.CREATE_POST_OBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const getPostObjectiveRemark = (id) => (dispatch) => {
  dispatch({
    type: types.GET_POST_OBJECTIVE_REMARK,
  });

  remarkApi
    .fetch(id)
    .then((res) =>
      dispatch({
        type: types.GET_POST_OBJECTIVE_REMARK_SUCCESS,
        payload: res,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.GET_POST_OBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const deletePostObjectiveRemark = (id, remarkId) => (dispatch) => {
  dispatch({
    type: types.DELETE_POST_OBJECTIVE_REMARK,
  });

  remarkApi
    .delete(id, remarkId)
    .then((remark) =>
      dispatch({
        type: types.DELETE_POST_OBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.DELETE_POST_OBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const resetNewPostObjectiveRemark = () => ({
  type: types.RESET_NEW_POST_OBJECTIVE_REMARK,
});

export const resetGetPostObjectiveRemark = () => ({
  type: types.RESET_GET_POST_OBJECTIVE_REMARK,
});
