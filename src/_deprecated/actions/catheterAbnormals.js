import { instance } from './config';

export const SET_CATHETER_ABNORMAL = 'SET_CATHETER_ABNORMAL';
export const SET_CATHETER_ABNORMAL_SUCCESS = 'SET_CATHETER_ABNORMAL_SUCCESS';
export const SET_CATHETER_ABNORMAL_FAILURE = 'SET_CATHETER_ABNORMAL_FAILURE';

export const setCatheterAbnormal = (id, props) => (dispatch) => {
  dispatch({
    type: SET_CATHETER_ABNORMAL,
  });

  instance.post(`catheters/${id}/abnormals`, props).then(
    (res) =>
      dispatch({
        type: SET_CATHETER_ABNORMAL_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: SET_CATHETER_ABNORMAL_FAILURE,
        payload: err.response.data,
      })
  );
};
