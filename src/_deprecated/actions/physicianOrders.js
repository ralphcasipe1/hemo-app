import * as types from '../constants/PhysicianOrderConstants';
import api from '../api/physicianOrder';
import { instance } from './config';

/**
 * @note: PhysicianOrders -> PhyOrders
 * @param {*} id
 */

export const fetchPatientRegistryPhyOrders = (id) => (dispatch) => {
  dispatch({
    type: types.FETCH_PATIENT_REGISTRY_PHY_ORDERS,
  });
  api
    .fetchAll(id)
    .then((orders) =>
      dispatch({
        type: types.FETCH_PATIENT_REGISTRY_PHY_ORDERS_SUCCESS,
        payload: orders,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.FETCH_PATIENT_REGISTRY_PHY_ORDERS_FAILURE,
        payload: err.response.data,
      })
    );
};

/**
 * @note: PhysicianOrder -> PhyOrder
 * @param {*} id
 * @param {*} props
 */
export const createPatientRegistryPhyOrder = (id, values) => (dispatch) => {
  dispatch({
    type: types.CREATE_PATIENT_REGISTRY_PHY_ORDER,
  });

  api
    .create(id, values)
    .then((order) =>
      dispatch({
        type: types.CREATE_PATIENT_REGISTRY_PHY_ORDER_SUCCESS,
        payload: order,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.CREATE_PATIENT_REGISTRY_PHY_ORDER_FAILURE,
        payload: err.response.data,
      })
    );
};

/**
 * @note: PhysicianOrders -> PhyOrders
 */
export const resetPatientRegistryPhyOrders = () => ({
  type: types.RESET_PATIENT_REGISTRY_PHY_ORDERS,
});

export const editPatientRegistryPhyOrder = (id, physicianOrderId, values) => (
  dispatch
) => {
  dispatch({
    type: types.UPDATE_PATIENT_REGISTRY_PHY_ORDER,
  });

  api
    .update(id, physicianOrderId, values)
    .then((order) =>
      dispatch({
        type: types.UPDATE_PATIENT_REGISTRY_PHY_ORDER_SUCCESS,
        payload: order,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.UPDATE_PATIENT_REGISTRY_PHY_ORDER_FAILURE,
        payload: err.response.data,
      })
    );
};

export const resetEditPatientRegistryPhyOrder = () => ({
  type: types.RESET_UPDATED_PATIENT_REGISTRY_PHY_ORDER,
});

export const deletePatientRegistryPhyOrder = (id, physicianOrderId) => (
  dispatch
) => {
  dispatch({
    type: types.DELETE_PATIENT_REGISTRY_PHY_ORDER,
  });

  api
    .delete(id, physicianOrderId)
    .then((order) =>
      dispatch({
        type: types.DELETE_PATIENT_REGISTRY_PHY_ORDER_SUCCESS,
        payload: order,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.DELETE_PATIENT_REGISTRY_PHY_ORDER_FAILURE,
        payload: err.response.data,
      })
    );
};

export const resetDeletedPatientRegistryPhyOrder = () => ({
  type: types.RESET_DELETED_PATIENT_REGISTRY_PHY_ORDER,
});

export const resetNewPatientRegistryPhyOrder = () => ({
  type: types.RESET_NEW_PATIENT_REGISTRY_PHY_ORDER,
});
