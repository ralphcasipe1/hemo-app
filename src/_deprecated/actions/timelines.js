import * as types from '../constants/TimelineConstants';
import api from '../api/timeline';

export const fetchTimelines = (id) => (dispatch) => {
  dispatch({
    type: types.FETCH_TIMELINE,
  });

  api
    .fetchAll()
    .then((timelines) =>
      dispatch({
        type: types.FETCH_TIMELINE_SUCCESS,
        payload: timelines,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.FLASH_ERROR,
        payload: err.response.data,
      })
    );
};

export const createTimeline = (values) => (dispatch) =>
  api
    .create(values)
    .then((timeline) =>
      dispatch({
        type: types.CREATE_TIMELINE,
        payload: timeline,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.FLASH_ERROR,
        payload: err.response.data,
      })
    );
