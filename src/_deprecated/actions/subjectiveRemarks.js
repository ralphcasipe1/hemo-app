import * as types from '../constants/SubjectiveRemarkConstants';
import remarkApi from '../api/subjectiveRemark';

export const createSubjectiveRemark = (id, values) => (dispatch) => {
  dispatch({
    type: types.CREATE_SUBJECTIVE_REMARK,
  });

  remarkApi
    .create(id, values)
    .then((remark) =>
      dispatch({
        type: types.CREATE_SUBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.CREATE_SUBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const getSubjectiveRemark = (id) => (dispatch) => {
  dispatch({
    type: types.GET_SUBJECTIVE_REMARK,
  });

  remarkApi
    .fetch(id)
    .then((remark) =>
      dispatch({
        type: types.GET_SUBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.GET_SUBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const deleteSubjectiveRemark = (id, remarkId) => (dispatch) => {
  dispatch({
    type: types.DELETE_SUBJECTIVE_REMARK,
  });

  remarkApi
    .delete(id, remarkId)
    .then((remark) =>
      dispatch({
        type: types.DELETE_SUBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.DELETE_SUBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export function resetNewSubjectiveRemark() {
  return {
    type: types.RESET_NEW_SUBJECTIVE_REMARK,
  };
}

export function resetGetSubjectiveRemark() {
  return {
    type: types.RESET_GET_SUBJECTIVE_REMARK,
  };
}
