import { instance } from './config';

export const UPDATE_NORMAL_VASCULAR = 'UPDATE_NORMAL_VASCULAR';
export const UPDATE_NORMAL_VASCULAR_SUCCESS = 'UPDATE_NORMAL_VASCULAR_SUCCESS';
export const UPDATE_NORMAL_VASCULAR_FAILURE = 'UPDATE_NORMAL_VASCULAR_FAILURE';

export const editNormalVascular = (id, normalId, values) => (dispatch) => {
  dispatch({
    type: UPDATE_NORMAL_VASCULAR,
  });

  instance.patch(`vasculars/${id}/normal_assessments/${normalId}`, props).then(
    (res) =>
      dispatch({
        type: UPDATE_NORMAL_VASCULAR_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: UPDATE_NORMAL_VASCULAR_FAILURE,
        payload: err.response.data,
      })
  );
};
