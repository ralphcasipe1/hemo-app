import * as types from '../constants/PostSubjectiveRemarkConstants';
import remarkApi from '../api/postSubjectiveRemark';
import { instance } from './config';

export const createPostSubjectiveRemark = (id, values) => (dispatch) => {
  dispatch({
    type: types.CREATE_POST_SUBJECTIVE_REMARK,
  });

  remarkApi
    .create(id, values)
    .then((remark) =>
      dispatch({
        type: types.CREATE_POST_SUBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.CREATE_POST_SUBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const getPostSubjectiveRemark = (id) => (dispatch) => {
  dispatch({
    type: types.GET_POST_SUBJECTIVE_REMARK,
  });

  remarkApi
    .fetch(id)
    .then((remark) =>
      dispatch({
        type: types.GET_POST_SUBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.GET_POST_SUBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const deletePostSubjectiveRemark = (id, remarkId) => (dispatch) => {
  dispatch({
    type: types.DELETE_POST_SUBJECTIVE_REMARK,
  });

  remarkApi
    .delete(id, remarkId)
    .then((remark) =>
      dispatch({
        type: types.DELETE_POST_SUBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.DELETE_POST_SUBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const resetNewPostSubjectiveRemark = () => ({
  type: types.RESET_NEW_POST_SUBJECTIVE_REMARK,
});

export const resetGetPostSubjectiveRemark = () => ({
  type: types.RESET_GET_POST_SUBJECTIVE_REMARK,
});
