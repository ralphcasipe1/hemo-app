import { instance } from './config';

export const SET_VASCULAR_ABNORMAL = 'SET_VASCULAR_ABNORMAL';
export const SET_VASCULAR_ABNORMAL_SUCCESS = 'SET_VASCULAR_ABNORMAL_SUCCESS';
export const SET_VASCULAR_ABNORMAL_FAILURE = 'SET_VASCULAR_ABNORMAL_FAILURE';

export function editAbnormalVascular(id, props) {
  return function(dispatch) {
    dispatch({
      type: SET_VASCULAR_ABNORMAL,
    });

    instance.post(`vasculars/${id}/abnormals`, props).then(
      (res) =>
        dispatch({
          type: SET_VASCULAR_ABNORMAL_SUCCESS,
          payload: res.data,
        }),
      (err) =>
        dispatch({
          type: SET_VASCULAR_ABNORMAL_FAILURE,
          payload: err.response.data,
        })
    );
  };
}
