import * as types from '../constants/PhysicianConstants';
import { instance } from './config';

export const createPhysician = (props) => (dispatch) => {
  dispatch({
    type: types.CREATE_PHYSICIAN,
  });

  instance.post(`/physicians`, props).then(
    (res) =>
      dispatch({
        type: types.CREATE_PHYSICIAN_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.CREATE_PHYSICIAN_FAILURE,
        payload: err.response.data,
      })
  );
};

export const fetchPhysicians = () => (dispatch) => {
  dispatch({
    type: types.FETCH_PHYSICIANS,
  });

  instance.get(`/physicians`).then(
    (res) =>
      dispatch({
        type: types.FETCH_PHYSICIANS_SUCCESS,
        payload: res.data,
      }),
    (err) =>
      dispatch({
        type: types.FETCH_PHYSICIANS_FAILURE,
        payload: err,
      })
  );
};

export const resetPhysicians = () => ({ type: types.RESET_PHYSICIANS });

export const resetNewPhysician = () => ({ type: types.RESET_NEW_PHYSICIAN });
