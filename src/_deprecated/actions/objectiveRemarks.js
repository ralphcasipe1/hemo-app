import * as types from '../constants/ObjectiveRemarkConstants';
import remarkApi from '../api/objectiveRemark';

export const createObjectiveRemark = (id, values) => (dispatch) => {
  dispatch({
    type: types.CREATE_OBJECTIVE_REMARK,
  });

  remarkApi
    .create(id, values)
    .then((remark) =>
      dispatch({
        type: types.CREATE_OBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.CREATE_OBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const getObjectiveRemark = (id) => (dispatch) => {
  dispatch({
    type: types.GET_OBJECTIVE_REMARK,
  });

  remarkApi
    .fetch(id)
    .then((remark) =>
      dispatch({
        type: types.GET_OBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.GET_OBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export const deleteObjectiveRemark = (id, remarkId) => (dispatch) => {
  dispatch({
    type: types.DELETE_OBJECTIVE_REMARK,
  });

  remarkApi
    .delete(id, remarkId)
    .then((remark) =>
      dispatch({
        type: types.DELETE_OBJECTIVE_REMARK_SUCCESS,
        payload: remark,
      })
    )
    .catch((err) =>
      dispatch({
        type: types.DELETE_OBJECTIVE_REMARK_FAILURE,
        payload: err.response.data,
      })
    );
};

export function resetNewObjectiveRemark() {
  return {
    type: types.RESET_GET_OBJECTIVE_REMARK,
  };
}

export function resetGetObjectiveRemark() {
  return {
    type: types.RESET_GET_OBJECTIVE_REMARK,
  };
}
