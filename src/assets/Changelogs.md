Changelogs v.1.0.0.beta
====================

### Adjustments
  - Adjust sidebar transition
  - Adjust **Patient Registry** tab menu
  - Adjust interface in **Admin/Physicians**
  - Change icon for **Admin/Abnormals**
  - Change default nurse notes interface
  - 
### Fixes
 - Fix **Standing Orders**
 - Fix **Basic Parameters**
 - Fix **Nurse Notes**
 - Fix automated local patients and bizbox's patients comparison at the start of the app
 - Fix patient registry fetcher at the patient registry module
 - 

### Improvements
  - Performance benchark and initial loading for patients
  - Improve tab navigation in Admin modules and it's sub-modules
  - 
### Disabled Modules and Functions(Temporary)
  - Standing order for Medications
  - Update and delete for standing orders
  - Application of standing orders to actual assessments of basic parameters
  - Update and delete in Vital Signs
  - Update and delete in Medications
  - Update and delete in Nurse notes
